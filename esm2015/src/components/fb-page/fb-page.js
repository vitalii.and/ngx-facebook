/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Page Plugin
 * \@shortdesc Page plugin component
 * \@fbdoc https://developers.facebook.com/docs/plugins/page-plugin
 * \@description
 * The Page plugin lets you easily embed and promote any Facebook Page on your website. Just like on Facebook, your visitors can like and share the Page without leaving your site.
 * \@usage
 * ```html
 * <fb-page href="https://facebook.com/facebook"></fb-page>
 * ```
 */
export class FBPageComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-page');
    }
}
FBPageComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-page',
                template: ''
            }] }
];
/** @nocollapse */
FBPageComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBPageComponent.propDecorators = {
    "href": [{ type: Input },],
    "width": [{ type: Input },],
    "height": [{ type: Input },],
    "tabs": [{ type: Input },],
    "hideCover": [{ type: Input },],
    "showFacepile": [{ type: Input },],
    "hideCTA": [{ type: Input },],
    "smallHeader": [{ type: Input },],
    "adaptContainerWidth": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBPageComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Number)
], FBPageComponent.prototype, "width", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Number)
], FBPageComponent.prototype, "height", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBPageComponent.prototype, "tabs", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBPageComponent.prototype, "hideCover", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBPageComponent.prototype, "showFacepile", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBPageComponent.prototype, "hideCTA", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBPageComponent.prototype, "smallHeader", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBPageComponent.prototype, "adaptContainerWidth", void 0);
function FBPageComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBPageComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBPageComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBPageComponent.propDecorators;
    /**
     * The URL of the Facebook Page
     * @type {?}
     */
    FBPageComponent.prototype.href;
    /**
     * The pixel width of the plugin. Min. is `180` & Max. is `500`.
     * Defaults to `340`.
     * @type {?}
     */
    FBPageComponent.prototype.width;
    /**
     * The pixel height of the plugin. Min. is `70`.
     * Defaults to `500`.
     * @type {?}
     */
    FBPageComponent.prototype.height;
    /**
     * Tabs to render i.e. `timeline`, `events`, `messages`. Use a comma-separated list to add multiple tabs, i.e. `timeline`, `events`.
     * Defaults to `timeline`.
     * @type {?}
     */
    FBPageComponent.prototype.tabs;
    /**
     * Hide cover photo in the header.
     * Defaults to `false`.
     * @type {?}
     */
    FBPageComponent.prototype.hideCover;
    /**
     * Show profile photos when friends like this.
     * Defaults to `true`.
     * @type {?}
     */
    FBPageComponent.prototype.showFacepile;
    /**
     * Hide the custom call to action button (if available).
     * Default to `false`.
     * @type {?}
     */
    FBPageComponent.prototype.hideCTA;
    /**
     * Use the small header instead.
     * Defaults to `false`.
     * @type {?}
     */
    FBPageComponent.prototype.smallHeader;
    /**
     * Try to fit inside the container width.
     * Defaults to `true`.
     * @type {?}
     */
    FBPageComponent.prototype.adaptContainerWidth;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItcGFnZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2ZiLXBhZ2UvZmItcGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7O0FBaUJqRSxNQUFNLHNCQUF1QixTQUFRLGFBQWE7Ozs7O0lBeUVoRCxZQUNFLEVBQWMsRUFDZCxHQUFhO1FBRWIsS0FBSyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7S0FDM0I7OztZQWxGRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFNBQVM7Z0JBQ25CLFFBQVEsRUFBRSxFQUFFO2FBQ2I7Ozs7WUFqQjBCLFVBQVU7WUFBRSxRQUFROzs7cUJBdUI1QyxLQUFLO3NCQVFMLEtBQUs7dUJBUUwsS0FBSztxQkFRTCxLQUFLOzBCQVFMLEtBQUs7NkJBUUwsS0FBSzt3QkFRTCxLQUFLOzRCQVFMLEtBQUs7b0NBUUwsS0FBSzs7O0lBL0RMLGFBQWE7Ozs7SUFRYixhQUFhOzs7O0lBUWIsYUFBYTs7OztJQVFiLGFBQWE7Ozs7SUFRYixhQUFhOzs7O0lBUWIsYUFBYTs7OztJQVFiLGFBQWE7Ozs7SUFRYixhQUFhOzs7O0lBUWIsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIFBhZ2UgUGx1Z2luXG4gKiBAc2hvcnRkZXNjIFBhZ2UgcGx1Z2luIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3BhZ2UtcGx1Z2luXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBQYWdlIHBsdWdpbiBsZXRzIHlvdSBlYXNpbHkgZW1iZWQgYW5kIHByb21vdGUgYW55IEZhY2Vib29rIFBhZ2Ugb24geW91ciB3ZWJzaXRlLiBKdXN0IGxpa2Ugb24gRmFjZWJvb2ssIHlvdXIgdmlzaXRvcnMgY2FuIGxpa2UgYW5kIHNoYXJlIHRoZSBQYWdlIHdpdGhvdXQgbGVhdmluZyB5b3VyIHNpdGUuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1wYWdlIGhyZWY9XCJodHRwczovL2ZhY2Vib29rLmNvbS9mYWNlYm9va1wiPjwvZmItcGFnZT5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1wYWdlJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCUGFnZUNvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgVVJMIG9mIHRoZSBGYWNlYm9vayBQYWdlXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBwaXhlbCB3aWR0aCBvZiB0aGUgcGx1Z2luLiBNaW4uIGlzIGAxODBgICYgTWF4LiBpcyBgNTAwYC5cbiAgICogRGVmYXVsdHMgdG8gYDM0MGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBUaGUgcGl4ZWwgaGVpZ2h0IG9mIHRoZSBwbHVnaW4uIE1pbi4gaXMgYDcwYC5cbiAgICogRGVmYXVsdHMgdG8gYDUwMGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBoZWlnaHQ6IG51bWJlcjtcblxuICAvKipcbiAgICogVGFicyB0byByZW5kZXIgaS5lLiBgdGltZWxpbmVgLCBgZXZlbnRzYCwgYG1lc3NhZ2VzYC4gVXNlIGEgY29tbWEtc2VwYXJhdGVkIGxpc3QgdG8gYWRkIG11bHRpcGxlIHRhYnMsIGkuZS4gYHRpbWVsaW5lYCwgYGV2ZW50c2AuXG4gICAqIERlZmF1bHRzIHRvIGB0aW1lbGluZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB0YWJzOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEhpZGUgY292ZXIgcGhvdG8gaW4gdGhlIGhlYWRlci5cbiAgICogRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhpZGVDb3ZlcjogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2hvdyBwcm9maWxlIHBob3RvcyB3aGVuIGZyaWVuZHMgbGlrZSB0aGlzLlxuICAgKiBEZWZhdWx0cyB0byBgdHJ1ZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93RmFjZXBpbGU6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEhpZGUgdGhlIGN1c3RvbSBjYWxsIHRvIGFjdGlvbiBidXR0b24gKGlmIGF2YWlsYWJsZSkuXG4gICAqIERlZmF1bHQgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhpZGVDVEE6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFVzZSB0aGUgc21hbGwgaGVhZGVyIGluc3RlYWQuXG4gICAqIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzbWFsbEhlYWRlcjogYm9vbGVhbjtcblxuICAvKipcbiAgICogVHJ5IHRvIGZpdCBpbnNpZGUgdGhlIGNvbnRhaW5lciB3aWR0aC5cbiAgICogRGVmYXVsdHMgdG8gYHRydWVgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgYWRhcHRDb250YWluZXJXaWR0aDogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1wYWdlJyk7XG4gIH1cblxufVxuIl19