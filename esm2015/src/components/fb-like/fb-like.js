/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Like Button
 * \@shortdesc Like button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/like-button
 * \@description
 * A single click on the Like button will 'like' pieces of content on the web and share them on Facebook.
 * You can also display a Share button next to the Like button to let people add a personal message and customize who they share with.
 * \@usage
 * ```html
 * <fb-like href="https://www.facebook.com/zuck"></fb-like>
 * ```
 */
export class FBLikeComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-like');
        /**
         * The absolute URL of the page that will be liked.
         * Defaults to the current URL.
         */
        this.href = window.location.href;
    }
}
FBLikeComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-like',
                template: ''
            }] }
];
/** @nocollapse */
FBLikeComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBLikeComponent.propDecorators = {
    "action": [{ type: Input },],
    "colorScheme": [{ type: Input },],
    "href": [{ type: Input },],
    "kidDirectedSite": [{ type: Input },],
    "layout": [{ type: Input },],
    "ref": [{ type: Input },],
    "share": [{ type: Input },],
    "showFaces": [{ type: Input },],
    "size": [{ type: Input },],
    "width": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBLikeComponent.prototype, "action", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBLikeComponent.prototype, "colorScheme", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBLikeComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBLikeComponent.prototype, "kidDirectedSite", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBLikeComponent.prototype, "layout", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBLikeComponent.prototype, "ref", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBLikeComponent.prototype, "share", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBLikeComponent.prototype, "showFaces", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBLikeComponent.prototype, "size", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBLikeComponent.prototype, "width", void 0);
function FBLikeComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBLikeComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBLikeComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBLikeComponent.propDecorators;
    /**
     * The verb to display on the button. Can be either `like` or `recommend`.
     * Defaults to `like`.
     * @type {?}
     */
    FBLikeComponent.prototype.action;
    /**
     * The color scheme used by the plugin for any text outside of the button itself. Can be `light` or `dark`.
     * Defaults to `light`.
     * @type {?}
     */
    FBLikeComponent.prototype.colorScheme;
    /**
     * The absolute URL of the page that will be liked.
     * Defaults to the current URL.
     * @type {?}
     */
    FBLikeComponent.prototype.href;
    /**
     * If your web site or online service, or a portion of your service, is directed to children under 13 [you must enable this](https://developers.facebook.com/docs/plugins/restrictions/).
     * Defaults to `false`.
     * @type {?}
     */
    FBLikeComponent.prototype.kidDirectedSite;
    /**
     * Selects one of the different layouts that are available for the plugin.
     * Can be one of `standard`, `button_count`, `button` or `box_count`.
     * See the [FAQ](https://developers.facebook.com/docs/plugins/like-button#faqlayout) for more details.
     * Defaults to `standard`.
     * @type {?}
     */
    FBLikeComponent.prototype.layout;
    /**
     * A label for tracking referrals which must be less than 50 characters and can contain alphanumeric characters and some punctuation (currently +/=-.:_).
     * See the [FAQ](https://developers.facebook.com/docs/plugins/faqs#ref) for more details.
     * @type {?}
     */
    FBLikeComponent.prototype.ref;
    /**
     * Specifies whether to include a share button beside the Like button.
     * This only works with the XFBML version.
     * Defaults to `false`.
     * @type {?}
     */
    FBLikeComponent.prototype.share;
    /**
     * Specifies whether to display profile photos below the button (standard layout only).
     * You must not enable this on child-directed sites.
     * Defaults to `false`.
     * @type {?}
     */
    FBLikeComponent.prototype.showFaces;
    /**
     * The button is offered in 2 sizes i.e. `large` and `small`.
     * Defaults to `small`.
     * @type {?}
     */
    FBLikeComponent.prototype.size;
    /**
     * The width of the plugin (standard layout only), which is subject to the minimum and default width.
     * See [Layout Settings](https://developers.facebook.com/docs/plugins/like-button#faqlayout) in the official docs for more details.
     * @type {?}
     */
    FBLikeComponent.prototype.width;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItbGlrZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2ZiLWxpa2UvZmItbGlrZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7OztBQWtCakUsTUFBTSxzQkFBdUIsU0FBUSxhQUFhOzs7OztJQXNGaEQsWUFBWSxFQUFjLEVBQUUsR0FBYTtRQUN2QyxLQUFLLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQzs7Ozs7b0JBL0RiLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTtLQWdFbEM7OztZQTVGRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFNBQVM7Z0JBQ25CLFFBQVEsRUFBRSxFQUFFO2FBQ2I7Ozs7WUFsQjBCLFVBQVU7WUFBRSxRQUFROzs7dUJBeUI1QyxLQUFLOzRCQVFMLEtBQUs7cUJBUUwsS0FBSztnQ0FRTCxLQUFLO3VCQVVMLEtBQUs7b0JBUUwsS0FBSztzQkFTTCxLQUFLOzBCQVNMLEtBQUs7cUJBUUwsS0FBSztzQkFRTCxLQUFLOzs7SUEzRUwsYUFBYTs7OztJQVFiLGFBQWE7Ozs7SUFRYixhQUFhOzs7O0lBUWIsYUFBYTs7OztJQVViLGFBQWE7Ozs7SUFRYixhQUFhOzs7O0lBU2IsYUFBYTs7OztJQVNiLGFBQWE7Ozs7SUFRYixhQUFhOzs7O0lBUWIsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIExpa2UgQnV0dG9uXG4gKiBAc2hvcnRkZXNjIExpa2UgYnV0dG9uIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2xpa2UtYnV0dG9uXG4gKiBAZGVzY3JpcHRpb25cbiAqIEEgc2luZ2xlIGNsaWNrIG9uIHRoZSBMaWtlIGJ1dHRvbiB3aWxsICdsaWtlJyBwaWVjZXMgb2YgY29udGVudCBvbiB0aGUgd2ViIGFuZCBzaGFyZSB0aGVtIG9uIEZhY2Vib29rLlxuICogWW91IGNhbiBhbHNvIGRpc3BsYXkgYSBTaGFyZSBidXR0b24gbmV4dCB0byB0aGUgTGlrZSBidXR0b24gdG8gbGV0IHBlb3BsZSBhZGQgYSBwZXJzb25hbCBtZXNzYWdlIGFuZCBjdXN0b21pemUgd2hvIHRoZXkgc2hhcmUgd2l0aC5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLWxpa2UgaHJlZj1cImh0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS96dWNrXCI+PC9mYi1saWtlPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLWxpa2UnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJMaWtlQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSB2ZXJiIHRvIGRpc3BsYXkgb24gdGhlIGJ1dHRvbi4gQ2FuIGJlIGVpdGhlciBgbGlrZWAgb3IgYHJlY29tbWVuZGAuXG4gICAqIERlZmF1bHRzIHRvIGBsaWtlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGFjdGlvbjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igc2NoZW1lIHVzZWQgYnkgdGhlIHBsdWdpbiBmb3IgYW55IHRleHQgb3V0c2lkZSBvZiB0aGUgYnV0dG9uIGl0c2VsZi4gQ2FuIGJlIGBsaWdodGAgb3IgYGRhcmtgLlxuICAgKiBEZWZhdWx0cyB0byBgbGlnaHRgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgY29sb3JTY2hlbWU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgcGFnZSB0aGF0IHdpbGwgYmUgbGlrZWQuXG4gICAqIERlZmF1bHRzIHRvIHRoZSBjdXJyZW50IFVSTC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZyA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuXG4gIC8qKlxuICAgKiBJZiB5b3VyIHdlYiBzaXRlIG9yIG9ubGluZSBzZXJ2aWNlLCBvciBhIHBvcnRpb24gb2YgeW91ciBzZXJ2aWNlLCBpcyBkaXJlY3RlZCB0byBjaGlsZHJlbiB1bmRlciAxMyBbeW91IG11c3QgZW5hYmxlIHRoaXNdKGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3Jlc3RyaWN0aW9ucy8pLlxuICAgKiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAga2lkRGlyZWN0ZWRTaXRlOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBTZWxlY3RzIG9uZSBvZiB0aGUgZGlmZmVyZW50IGxheW91dHMgdGhhdCBhcmUgYXZhaWxhYmxlIGZvciB0aGUgcGx1Z2luLlxuICAgKiBDYW4gYmUgb25lIG9mIGBzdGFuZGFyZGAsIGBidXR0b25fY291bnRgLCBgYnV0dG9uYCBvciBgYm94X2NvdW50YC5cbiAgICogU2VlIHRoZSBbRkFRXShodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9saWtlLWJ1dHRvbiNmYXFsYXlvdXQpIGZvciBtb3JlIGRldGFpbHMuXG4gICAqIERlZmF1bHRzIHRvIGBzdGFuZGFyZGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBsYXlvdXQ6IHN0cmluZztcblxuICAvKipcbiAgICogQSBsYWJlbCBmb3IgdHJhY2tpbmcgcmVmZXJyYWxzIHdoaWNoIG11c3QgYmUgbGVzcyB0aGFuIDUwIGNoYXJhY3RlcnMgYW5kIGNhbiBjb250YWluIGFscGhhbnVtZXJpYyBjaGFyYWN0ZXJzIGFuZCBzb21lIHB1bmN0dWF0aW9uIChjdXJyZW50bHkgKy89LS46XykuXG4gICAqIFNlZSB0aGUgW0ZBUV0oaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZmFxcyNyZWYpIGZvciBtb3JlIGRldGFpbHMuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICByZWY6IHN0cmluZztcblxuICAvKipcbiAgICogU3BlY2lmaWVzIHdoZXRoZXIgdG8gaW5jbHVkZSBhIHNoYXJlIGJ1dHRvbiBiZXNpZGUgdGhlIExpa2UgYnV0dG9uLlxuICAgKiBUaGlzIG9ubHkgd29ya3Mgd2l0aCB0aGUgWEZCTUwgdmVyc2lvbi5cbiAgICogRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNoYXJlOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBTcGVjaWZpZXMgd2hldGhlciB0byBkaXNwbGF5IHByb2ZpbGUgcGhvdG9zIGJlbG93IHRoZSBidXR0b24gKHN0YW5kYXJkIGxheW91dCBvbmx5KS5cbiAgICogWW91IG11c3Qgbm90IGVuYWJsZSB0aGlzIG9uIGNoaWxkLWRpcmVjdGVkIHNpdGVzLlxuICAgKiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hvd0ZhY2VzOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBUaGUgYnV0dG9uIGlzIG9mZmVyZWQgaW4gMiBzaXplcyBpLmUuIGBsYXJnZWAgYW5kIGBzbWFsbGAuXG4gICAqIERlZmF1bHRzIHRvIGBzbWFsbGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaXplOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSB3aWR0aCBvZiB0aGUgcGx1Z2luIChzdGFuZGFyZCBsYXlvdXQgb25seSksIHdoaWNoIGlzIHN1YmplY3QgdG8gdGhlIG1pbmltdW0gYW5kIGRlZmF1bHQgd2lkdGguXG4gICAqIFNlZSBbTGF5b3V0IFNldHRpbmdzXShodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9saWtlLWJ1dHRvbiNmYXFsYXlvdXQpIGluIHRoZSBvZmZpY2lhbCBkb2NzIGZvciBtb3JlIGRldGFpbHMuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKGVsOiBFbGVtZW50UmVmLCBybmQ6IFJlbmRlcmVyKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLWxpa2UnKTtcbiAgfVxuXG59XG4iXX0=