/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, ElementRef, Renderer, EventEmitter } from '@angular/core';
import { FBMLAttribute, FBMLComponent, FBMLInstanceMethod } from '../fbml-component';
/**
 * \@name Embedded Video
 * \@shortdesc Component to embed Facebook videos
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-video-player
 * \@description Component to embed Facebook videos and control them.
 * \@usage
 * ```html
 * <!-- basic usage -->
 * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/"></fb-video>
 *
 * <!-- event emitters -->
 * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/" (paused)="onVideoPaused($event)"></fb-video>
 * ```
 *
 * To manually interact with the video player, fetch it using `ViewChild`.
 *
 * ```ts
 * import { Component, ViewChild } from '\@angular/core';
 * import { FBVideoComponent } from 'ng2-facebook-sdk';
 *
 * \@Component(...)
 * export class MyComponent {
 *
 *   \@ViewChild(FBVideoComponent) video: FBVideoComponent;
 *
 *   ngAfterViewInit() {
 *     this.video.play();
 *     this.video.pause();
 *     this.video.getVolume();
 *   }
 *
 * }
 *
 * ```
 */
export class FBVideoComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-video');
        /**
         * Fired when video starts to play.
         */
        this.startedPlaying = new EventEmitter();
        /**
         * Fired when video pauses.
         */
        this.paused = new EventEmitter();
        /**
         *
         * Fired when video finishes playing.
         */
        this.finishedPlaying = new EventEmitter();
        /**
         * Fired when video starts to buffer.
         */
        this.startedBuffering = new EventEmitter();
        /**
         * Fired when video recovers from buffering.
         */
        this.finishedBuffering = new EventEmitter();
        /**
         * Fired when an error occurs on the video.
         */
        this.error = new EventEmitter();
        this._listeners = [];
        this.nativeElement.id = this._id = 'video-' + String(Math.floor((Math.random() * 10000) + 1));
    }
    /**
     * @hidden
     * @return {?}
     */
    ngOnInit() {
        FB.Event.subscribe('xfbml.ready', (msg) => {
            if (msg.type === 'video' && msg.id === this._id) {
                this._instance = msg.instance;
                this._listeners.push(this._instance.subscribe('startedPlaying', (e) => this.startedPlaying.emit(e)), this._instance.subscribe('paused', (e) => this.paused.emit(e)), this._instance.subscribe('finishedPlaying', (e) => this.finishedPlaying.emit(e)), this._instance.subscribe('startedBuffering', (e) => this.startedBuffering.emit(e)), this._instance.subscribe('finishedBuffering', (e) => this.finishedBuffering.emit(e)), this._instance.subscribe('error', (e) => this.error.emit(e)));
            }
        });
    }
    /**
     * @hidden
     * @return {?}
     */
    ngOnDestroy() {
        this._listeners.forEach(l => {
            if (typeof l.release === 'function') {
                l.release();
            }
        });
    }
    /**
     * Plays the video.
     * @return {?}
     */
    play() { }
    /**
     * Pauses the video.
     * @return {?}
     */
    pause() { }
    /**
     * Seeks to specified position.
     * @param {?} seconds {number}
     * @return {?}
     */
    seek(seconds) { }
    /**
     * Mute the video.
     * @return {?}
     */
    mute() { }
    /**
     * Unmute the video.
     * @return {?}
     */
    unmute() { }
    /**
     * Returns true if video is muted, false if not.
     * @return {?}
     */
    isMuted() { return; }
    /**
     * Set the volume
     * @param {?} volume
     * @return {?}
     */
    setVolume(volume) { }
    /**
     * Get the volume
     * @return {?}
     */
    getVolume() { return; }
    /**
     * Returns the current video time position in seconds
     * @return {?}
     */
    getCurrentPosition() { }
    /**
     * Returns the video duration in seconds
     * @return {?}
     */
    getDuration() { }
}
FBVideoComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-video',
                template: ''
            }] }
];
/** @nocollapse */
FBVideoComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBVideoComponent.propDecorators = {
    "href": [{ type: Input },],
    "allowfullscreen": [{ type: Input },],
    "autoplay": [{ type: Input },],
    "width": [{ type: Input },],
    "showText": [{ type: Input },],
    "showCaptions": [{ type: Input },],
    "startedPlaying": [{ type: Output },],
    "paused": [{ type: Output },],
    "finishedPlaying": [{ type: Output },],
    "startedBuffering": [{ type: Output },],
    "finishedBuffering": [{ type: Output },],
    "error": [{ type: Output },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBVideoComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBVideoComponent.prototype, "allowfullscreen", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBVideoComponent.prototype, "autoplay", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBVideoComponent.prototype, "width", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBVideoComponent.prototype, "showText", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBVideoComponent.prototype, "showCaptions", void 0);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "play", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "pause", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number]),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "seek", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "mute", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "unmute", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Boolean)
], FBVideoComponent.prototype, "isMuted", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number]),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "setVolume", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Number)
], FBVideoComponent.prototype, "getVolume", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "getCurrentPosition", null);
tslib_1.__decorate([
    FBMLInstanceMethod,
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], FBVideoComponent.prototype, "getDuration", null);
function FBVideoComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBVideoComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBVideoComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBVideoComponent.propDecorators;
    /** @type {?} */
    FBVideoComponent.prototype._instance;
    /**
     * The absolute URL of the video.
     * @type {?}
     */
    FBVideoComponent.prototype.href;
    /**
     * Allow the video to be played in fullscreen mode. Can be false or true. Defaults to false;
     * @type {?}
     */
    FBVideoComponent.prototype.allowfullscreen;
    /**
     * Automatically start playing the video when the page loads. The video will be played without sound (muted). People can turn on sound via the video player controls. This setting does not apply to mobile devices. Can be false or true. Defaults to false.
     * @type {?}
     */
    FBVideoComponent.prototype.autoplay;
    /**
     * The width of the video container. Min. 220px.
     * @type {?}
     */
    FBVideoComponent.prototype.width;
    /**
     * Set to true to include the text from the Facebook post associated with the video, if any.
     * @type {?}
     */
    FBVideoComponent.prototype.showText;
    /**
     * Set to true to show captions (if available) by default. Captions are only available on desktop.
     * @type {?}
     */
    FBVideoComponent.prototype.showCaptions;
    /**
     * Fired when video starts to play.
     * @type {?}
     */
    FBVideoComponent.prototype.startedPlaying;
    /**
     * Fired when video pauses.
     * @type {?}
     */
    FBVideoComponent.prototype.paused;
    /**
     *
     * Fired when video finishes playing.
     * @type {?}
     */
    FBVideoComponent.prototype.finishedPlaying;
    /**
     * Fired when video starts to buffer.
     * @type {?}
     */
    FBVideoComponent.prototype.startedBuffering;
    /**
     * Fired when video recovers from buffering.
     * @type {?}
     */
    FBVideoComponent.prototype.finishedBuffering;
    /**
     * Fired when an error occurs on the video.
     * @type {?}
     */
    FBVideoComponent.prototype.error;
    /** @type {?} */
    FBVideoComponent.prototype._id;
    /** @type {?} */
    FBVideoComponent.prototype._listeners;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItdmlkZW8uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi12aWRlby9mYi12aWRlby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFxQixZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEgsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMENyRixNQUFNLHVCQUF3QixTQUFRLGFBQWE7Ozs7O0lBdUZqRCxZQUNFLEVBQWMsRUFDZCxHQUFhO1FBRWIsS0FBSyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUM7Ozs7OEJBekNPLElBQUksWUFBWSxFQUFPOzs7O3NCQU0vQixJQUFJLFlBQVksRUFBTzs7Ozs7K0JBT2QsSUFBSSxZQUFZLEVBQU87Ozs7Z0NBTXRCLElBQUksWUFBWSxFQUFPOzs7O2lDQU10QixJQUFJLFlBQVksRUFBTzs7OztxQkFNbkMsSUFBSSxZQUFZLEVBQU87MEJBSXRCLEVBQUU7UUFPNUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsR0FBRyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUMvRjs7Ozs7SUFLRCxRQUFRO1FBQ04sRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsR0FBUSxFQUFFLEVBQUU7WUFDN0MsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxPQUFPLElBQUksR0FBRyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO2dCQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFNLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ25GLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQU0sRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFDbkUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFNLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ3JGLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBTSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ3ZGLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBTSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ3pGLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQU0sRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FDbEUsQ0FBQzthQUNIO1NBQ0YsQ0FBQyxDQUFDO0tBQ0o7Ozs7O0lBS0QsV0FBVztRQUNULElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzFCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNwQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDYjtTQUNGLENBQUMsQ0FBQztLQUNKOzs7OztJQU1ELElBQUksTUFBSzs7Ozs7SUFNVCxLQUFLLE1BQUs7Ozs7OztJQU9WLElBQUksQ0FBQyxPQUFlLEtBQUk7Ozs7O0lBTXhCLElBQUksTUFBSzs7Ozs7SUFNVCxNQUFNLE1BQUs7Ozs7O0lBTVgsT0FBTyxLQUFjLE1BQU0sQ0FBQyxFQUFFOzs7Ozs7SUFPOUIsU0FBUyxDQUFDLE1BQWMsS0FBSTs7Ozs7SUFNNUIsU0FBUyxLQUFhLE1BQU0sQ0FBQyxFQUFFOzs7OztJQU0vQixrQkFBa0IsTUFBSzs7Ozs7SUFNdkIsV0FBVyxNQUFLOzs7WUE3TGpCLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsUUFBUSxFQUFFLEVBQUU7YUFDYjs7OztZQTFDa0MsVUFBVTtZQUFFLFFBQVE7OztxQkFrRHBELEtBQUs7Z0NBT0wsS0FBSzt5QkFPTCxLQUFLO3NCQU9MLEtBQUs7eUJBT0wsS0FBSzs2QkFPTCxLQUFLOytCQU9MLE1BQU07dUJBTU4sTUFBTTtnQ0FPTixNQUFNO2lDQU1OLE1BQU07a0NBTU4sTUFBTTtzQkFNTixNQUFNOzs7SUF4RU4sYUFBYTs7OztJQU9iLGFBQWE7Ozs7SUFPYixhQUFhOzs7O0lBT2IsYUFBYTs7OztJQU9iLGFBQWE7Ozs7SUFPYixhQUFhOzs7O0lBcUZiLGtCQUFrQjs7Ozs0Q0FDVjs7SUFLUixrQkFBa0I7Ozs7NkNBQ1Q7O0lBTVQsa0JBQWtCOzs7OzRDQUNLOztJQUt2QixrQkFBa0I7Ozs7NENBQ1Y7O0lBS1Isa0JBQWtCOzs7OzhDQUNSOztJQUtWLGtCQUFrQjs7OzsrQ0FDVzs7SUFNN0Isa0JBQWtCOzs7O2lEQUNTOztJQUszQixrQkFBa0I7Ozs7aURBQ1k7O0lBSzlCLGtCQUFrQjs7OzswREFDSTs7SUFLdEIsa0JBQWtCOzs7O21EQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciwgT25Jbml0LCBPbkRlc3Ryb3ksIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCwgRkJNTEluc3RhbmNlTWV0aG9kIH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuZGVjbGFyZSB2YXIgRkI6IGFueTtcblxuLyoqXG4gKiBAbmFtZSBFbWJlZGRlZCBWaWRlb1xuICogQHNob3J0ZGVzYyBDb21wb25lbnQgdG8gZW1iZWQgRmFjZWJvb2sgdmlkZW9zXG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZW1iZWRkZWQtdmlkZW8tcGxheWVyXG4gKiBAZGVzY3JpcHRpb24gQ29tcG9uZW50IHRvIGVtYmVkIEZhY2Vib29rIHZpZGVvcyBhbmQgY29udHJvbCB0aGVtLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8IS0tIGJhc2ljIHVzYWdlIC0tPlxuICogPGZiLXZpZGVvIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vZmFjZWJvb2svdmlkZW9zLzEwMTUzMjMxMzc5OTQ2NzI5L1wiPjwvZmItdmlkZW8+XG4gKlxuICogPCEtLSBldmVudCBlbWl0dGVycyAtLT5cbiAqIDxmYi12aWRlbyBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL2ZhY2Vib29rL3ZpZGVvcy8xMDE1MzIzMTM3OTk0NjcyOS9cIiAocGF1c2VkKT1cIm9uVmlkZW9QYXVzZWQoJGV2ZW50KVwiPjwvZmItdmlkZW8+XG4gKiBgYGBcbiAqXG4gKiBUbyBtYW51YWxseSBpbnRlcmFjdCB3aXRoIHRoZSB2aWRlbyBwbGF5ZXIsIGZldGNoIGl0IHVzaW5nIGBWaWV3Q2hpbGRgLlxuICpcbiAqIGBgYHRzXG4gKiBpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuICogaW1wb3J0IHsgRkJWaWRlb0NvbXBvbmVudCB9IGZyb20gJ25nMi1mYWNlYm9vay1zZGsnO1xuICpcbiAqIEBDb21wb25lbnQoLi4uKVxuICogZXhwb3J0IGNsYXNzIE15Q29tcG9uZW50IHtcbiAqXG4gKiAgIEBWaWV3Q2hpbGQoRkJWaWRlb0NvbXBvbmVudCkgdmlkZW86IEZCVmlkZW9Db21wb25lbnQ7XG4gKlxuICogICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gKiAgICAgdGhpcy52aWRlby5wbGF5KCk7XG4gKiAgICAgdGhpcy52aWRlby5wYXVzZSgpO1xuICogICAgIHRoaXMudmlkZW8uZ2V0Vm9sdW1lKCk7XG4gKiAgIH1cbiAqXG4gKiB9XG4gKlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXZpZGVvJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCVmlkZW9Db21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG4gIHByaXZhdGUgX2luc3RhbmNlOiBhbnk7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHZpZGVvLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBBbGxvdyB0aGUgdmlkZW8gdG8gYmUgcGxheWVkIGluIGZ1bGxzY3JlZW4gbW9kZS4gQ2FuIGJlIGZhbHNlIG9yIHRydWUuIERlZmF1bHRzIHRvIGZhbHNlO1xuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgYWxsb3dmdWxsc2NyZWVuOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBBdXRvbWF0aWNhbGx5IHN0YXJ0IHBsYXlpbmcgdGhlIHZpZGVvIHdoZW4gdGhlIHBhZ2UgbG9hZHMuIFRoZSB2aWRlbyB3aWxsIGJlIHBsYXllZCB3aXRob3V0IHNvdW5kIChtdXRlZCkuIFBlb3BsZSBjYW4gdHVybiBvbiBzb3VuZCB2aWEgdGhlIHZpZGVvIHBsYXllciBjb250cm9scy4gVGhpcyBzZXR0aW5nIGRvZXMgbm90IGFwcGx5IHRvIG1vYmlsZSBkZXZpY2VzLiBDYW4gYmUgZmFsc2Ugb3IgdHJ1ZS4gRGVmYXVsdHMgdG8gZmFsc2UuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBhdXRvcGxheTogYm9vbGVhbjtcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSB2aWRlbyBjb250YWluZXIuIE1pbi4gMjIwcHguXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTZXQgdG8gdHJ1ZSB0byBpbmNsdWRlIHRoZSB0ZXh0IGZyb20gdGhlIEZhY2Vib29rIHBvc3QgYXNzb2NpYXRlZCB3aXRoIHRoZSB2aWRlbywgaWYgYW55LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hvd1RleHQ6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFNldCB0byB0cnVlIHRvIHNob3cgY2FwdGlvbnMgKGlmIGF2YWlsYWJsZSkgYnkgZGVmYXVsdC4gQ2FwdGlvbnMgYXJlIG9ubHkgYXZhaWxhYmxlIG9uIGRlc2t0b3AuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93Q2FwdGlvbnM6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gc3RhcnRzIHRvIHBsYXkuXG4gICAqL1xuICBAT3V0cHV0KClcbiAgc3RhcnRlZFBsYXlpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gcGF1c2VzLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIHBhdXNlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAvKipcbiAgICpcbiAgIEZpcmVkIHdoZW4gdmlkZW8gZmluaXNoZXMgcGxheWluZy5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBmaW5pc2hlZFBsYXlpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gc3RhcnRzIHRvIGJ1ZmZlci5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBzdGFydGVkQnVmZmVyaW5nOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIHZpZGVvIHJlY292ZXJzIGZyb20gYnVmZmVyaW5nLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIGZpbmlzaGVkQnVmZmVyaW5nOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIGFuIGVycm9yIG9jY3VycyBvbiB0aGUgdmlkZW8uXG4gICAqL1xuICBAT3V0cHV0KClcbiAgZXJyb3I6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgcHJpdmF0ZSBfaWQ6IHN0cmluZztcblxuICBwcml2YXRlIF9saXN0ZW5lcnM6IGFueVtdID0gW107XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItdmlkZW8nKTtcbiAgICB0aGlzLm5hdGl2ZUVsZW1lbnQuaWQgPSB0aGlzLl9pZCA9ICd2aWRlby0nICsgU3RyaW5nKE1hdGguZmxvb3IoKE1hdGgucmFuZG9tKCkgKiAxMDAwMCkgKyAxKSk7XG4gIH1cblxuICAvKipcbiAgICogQGhpZGRlblxuICAgKi9cbiAgbmdPbkluaXQoKSB7XG4gICAgRkIuRXZlbnQuc3Vic2NyaWJlKCd4ZmJtbC5yZWFkeScsIChtc2c6IGFueSkgPT4ge1xuICAgICAgaWYgKG1zZy50eXBlID09PSAndmlkZW8nICYmIG1zZy5pZCA9PT0gdGhpcy5faWQpIHtcbiAgICAgICAgdGhpcy5faW5zdGFuY2UgPSBtc2cuaW5zdGFuY2U7XG4gICAgICAgIHRoaXMuX2xpc3RlbmVycy5wdXNoKFxuICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnN1YnNjcmliZSgnc3RhcnRlZFBsYXlpbmcnLCAoZTogYW55KSA9PiB0aGlzLnN0YXJ0ZWRQbGF5aW5nLmVtaXQoZSkpLFxuICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnN1YnNjcmliZSgncGF1c2VkJywgKGU6IGFueSkgPT4gdGhpcy5wYXVzZWQuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdmaW5pc2hlZFBsYXlpbmcnLCAoZTogYW55KSA9PiB0aGlzLmZpbmlzaGVkUGxheWluZy5lbWl0KGUpKSxcbiAgICAgICAgICB0aGlzLl9pbnN0YW5jZS5zdWJzY3JpYmUoJ3N0YXJ0ZWRCdWZmZXJpbmcnLCAoZTogYW55KSA9PiB0aGlzLnN0YXJ0ZWRCdWZmZXJpbmcuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdmaW5pc2hlZEJ1ZmZlcmluZycsIChlOiBhbnkpID0+IHRoaXMuZmluaXNoZWRCdWZmZXJpbmcuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdlcnJvcicsIChlOiBhbnkpID0+IHRoaXMuZXJyb3IuZW1pdChlKSlcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAaGlkZGVuXG4gICAqL1xuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLl9saXN0ZW5lcnMuZm9yRWFjaChsID0+IHtcbiAgICAgIGlmICh0eXBlb2YgbC5yZWxlYXNlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGwucmVsZWFzZSgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFBsYXlzIHRoZSB2aWRlby5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgcGxheSgpIHt9XG5cbiAgLyoqXG4gICAqIFBhdXNlcyB0aGUgdmlkZW8uXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIHBhdXNlKCkge31cblxuICAvKipcbiAgICogU2Vla3MgdG8gc3BlY2lmaWVkIHBvc2l0aW9uLlxuICAgKiBAcGFyYW0gc2Vjb25kcyB7bnVtYmVyfVxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBzZWVrKHNlY29uZHM6IG51bWJlcikge31cblxuICAvKipcbiAgICogTXV0ZSB0aGUgdmlkZW8uXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIG11dGUoKSB7fVxuXG4gIC8qKlxuICAgKiBVbm11dGUgdGhlIHZpZGVvLlxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICB1bm11dGUoKSB7fVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRydWUgaWYgdmlkZW8gaXMgbXV0ZWQsIGZhbHNlIGlmIG5vdC5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgaXNNdXRlZCgpOiBib29sZWFuIHsgcmV0dXJuOyB9XG5cbiAgLyoqXG4gICAqIFNldCB0aGUgdm9sdW1lXG4gICAqIEBwYXJhbSB2b2x1bWVcbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgc2V0Vm9sdW1lKHZvbHVtZTogbnVtYmVyKSB7fVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIHZvbHVtZVxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBnZXRWb2x1bWUoKTogbnVtYmVyIHsgcmV0dXJuOyB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIGN1cnJlbnQgdmlkZW8gdGltZSBwb3NpdGlvbiBpbiBzZWNvbmRzXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIGdldEN1cnJlbnRQb3NpdGlvbigpIHt9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIHZpZGVvIGR1cmF0aW9uIGluIHNlY29uZHNcbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgZ2V0RHVyYXRpb24oKSB7fVxuXG59XG4iXX0=