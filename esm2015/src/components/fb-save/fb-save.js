/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Save Button
 * \@shortdesc Save button component.
 * \@fbdoc https://developers.facebook.com/docs/plugins/save
 * \@description
 * The Save button lets people save items or services to a private list on Facebook, share it with friends, and receive relevant notifications.
 * \@usage
 * ```html
 * <fb-save uri="https://github.com/zyra/ng2-facebook-sdk/"></fb-save>
 * ```
 */
export class FBSaveComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-save');
        /**
         * The absolute link of the page that will be saved.
         * Current Link/Address
         */
        this.uri = window.location.href;
    }
}
FBSaveComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-save',
                template: ''
            }] }
];
/** @nocollapse */
FBSaveComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBSaveComponent.propDecorators = {
    "uri": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBSaveComponent.prototype, "uri", void 0);
function FBSaveComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBSaveComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBSaveComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBSaveComponent.propDecorators;
    /**
     * The absolute link of the page that will be saved.
     * Current Link/Address
     * @type {?}
     */
    FBSaveComponent.prototype.uri;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItc2F2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2ZiLXNhdmUvZmItc2F2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7O0FBaUJqRSxNQUFNLHNCQUF1QixTQUFRLGFBQWE7Ozs7O0lBVWhELFlBQ0UsRUFBYyxFQUNkLEdBQWE7UUFFYixLQUFLLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQzs7Ozs7bUJBTmQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJO0tBT2pDOzs7WUFuQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxTQUFTO2dCQUNuQixRQUFRLEVBQUUsRUFBRTthQUNiOzs7O1lBakIwQixVQUFVO1lBQUUsUUFBUTs7O29CQXdCNUMsS0FBSzs7O0lBQ0wsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIFNhdmUgQnV0dG9uXG4gKiBAc2hvcnRkZXNjIFNhdmUgYnV0dG9uIGNvbXBvbmVudC5cbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9zYXZlXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBTYXZlIGJ1dHRvbiBsZXRzIHBlb3BsZSBzYXZlIGl0ZW1zIG9yIHNlcnZpY2VzIHRvIGEgcHJpdmF0ZSBsaXN0IG9uIEZhY2Vib29rLCBzaGFyZSBpdCB3aXRoIGZyaWVuZHMsIGFuZCByZWNlaXZlIHJlbGV2YW50IG5vdGlmaWNhdGlvbnMuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1zYXZlIHVyaT1cImh0dHBzOi8vZ2l0aHViLmNvbS96eXJhL25nMi1mYWNlYm9vay1zZGsvXCI+PC9mYi1zYXZlPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXNhdmUnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJTYXZlQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBsaW5rIG9mIHRoZSBwYWdlIHRoYXQgd2lsbCBiZSBzYXZlZC5cbiAgICogQ3VycmVudCBMaW5rL0FkZHJlc3NcbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHVyaTogc3RyaW5nID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItc2F2ZScpO1xuICB9XG5cbn1cbiJdfQ==