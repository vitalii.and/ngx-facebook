/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @hidden
 * @param {?} target
 * @param {?} key
 * @return {?}
 */
export function FBMLAttribute(target, key) {
    const /** @type {?} */ processKey = (_k) => 'data-' + _k.toString().replace(/([a-z\d])([A-Z])/g, '$1-$2').toLowerCase();
    Object.defineProperty(target, key, {
        set: function (value) {
            value = value.toString();
            this.setAttribute(processKey(key), value);
        },
        get: function () {
            return this.getAttribute(processKey(key));
        },
        enumerable: true
    });
}
const ɵ0 = function (...args) {
    if (this._instance) {
        return this._instance[key].apply(this._instance, args);
    }
    else {
        console.warn('ng2-facebook-sdk: tried calling instance method before component is ready.');
        return null;
    }
};
/**
 * @hidden
 * @param {?} target
 * @param {?} key
 * @return {?}
 */
export function FBMLInstanceMethod(target, key) {
    return {
        enumerable: true,
        value: ɵ0
    };
}
/**
 * @hidden
 */
export class FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     * @param {?} fbClass
     */
    constructor(el, rnd, fbClass) {
        this.el = el;
        this.rnd = rnd;
        this.fbClass = fbClass;
        this.nativeElement = this.el.nativeElement;
        this.rnd.setElementClass(this.nativeElement, this.fbClass, true);
    }
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    setAttribute(name, value) {
        if (!name || !value)
            return;
        this.rnd.setElementAttribute(this.nativeElement, name, value);
    }
    /**
     * @param {?} name
     * @return {?}
     */
    getAttribute(name) {
        if (!name)
            return;
        return this.nativeElement.getAttribute(name);
    }
}
function FBMLComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    FBMLComponent.prototype.nativeElement;
    /** @type {?} */
    FBMLComponent.prototype.el;
    /** @type {?} */
    FBMLComponent.prototype.rnd;
    /** @type {?} */
    FBMLComponent.prototype.fbClass;
}
export { ɵ0 };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmJtbC1jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYm1sLWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBS0EsTUFBTSx3QkFBd0IsTUFBVyxFQUFFLEdBQVc7SUFDcEQsdUJBQU0sVUFBVSxHQUFHLENBQUMsRUFBVSxFQUFFLEVBQUUsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxPQUFPLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUMvRyxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUU7UUFDakMsR0FBRyxFQUFFLFVBQVMsS0FBSztZQUNqQixLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsR0FBRyxFQUFFO1lBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDM0M7UUFDRCxVQUFVLEVBQUUsSUFBSTtLQUNqQixDQUFDLENBQUM7Q0FDSjtXQVFVLFVBQVMsR0FBRyxJQUFXO0lBQzVCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3hEO0lBQUMsSUFBSSxDQUFDLENBQUM7UUFDTixPQUFPLENBQUMsSUFBSSxDQUFDLDRFQUE0RSxDQUFDLENBQUM7UUFDM0YsTUFBTSxDQUFDLElBQUksQ0FBQztLQUNiO0NBQ0Y7Ozs7Ozs7QUFWTCxNQUFNLDZCQUE2QixNQUFXLEVBQUUsR0FBVztJQUN6RCxNQUFNLENBQUM7UUFDTCxVQUFVLEVBQUUsSUFBSTtRQUNoQixLQUFLLElBT0o7S0FDRixDQUFDO0NBQ0g7Ozs7QUFLRCxNQUFNOzs7Ozs7SUFJSixZQUNVLElBQ0EsS0FDQTtRQUZBLE9BQUUsR0FBRixFQUFFO1FBQ0YsUUFBRyxHQUFILEdBQUc7UUFDSCxZQUFPLEdBQVAsT0FBTztRQUVmLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUM7UUFDM0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ2xFOzs7Ozs7SUFFUyxZQUFZLENBQUMsSUFBWSxFQUFFLEtBQWE7UUFDaEQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUM7WUFBQyxNQUFNLENBQUM7UUFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztLQUMvRDs7Ozs7SUFFUyxZQUFZLENBQUMsSUFBWTtRQUNqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUNsQixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDOUM7Q0FFRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbi8qKlxuICogQGhpZGRlblxuICovXG5leHBvcnQgZnVuY3Rpb24gRkJNTEF0dHJpYnV0ZSh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcpIHtcbiAgY29uc3QgcHJvY2Vzc0tleSA9IChfazogc3RyaW5nKSA9PiAnZGF0YS0nICsgX2sudG9TdHJpbmcoKS5yZXBsYWNlKC8oW2EtelxcZF0pKFtBLVpdKS9nLCAnJDEtJDInKS50b0xvd2VyQ2FzZSgpO1xuICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHtcbiAgICBzZXQ6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICB2YWx1ZSA9IHZhbHVlLnRvU3RyaW5nKCk7XG4gICAgICB0aGlzLnNldEF0dHJpYnV0ZShwcm9jZXNzS2V5KGtleSksIHZhbHVlKTtcbiAgICB9LFxuICAgIGdldDogZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5nZXRBdHRyaWJ1dGUocHJvY2Vzc0tleShrZXkpKTtcbiAgICB9LFxuICAgIGVudW1lcmFibGU6IHRydWVcbiAgfSk7XG59XG5cbi8qKlxuICogQGhpZGRlblxuICovXG5leHBvcnQgZnVuY3Rpb24gRkJNTEluc3RhbmNlTWV0aG9kKHRhcmdldDogYW55LCBrZXk6IHN0cmluZykge1xuICByZXR1cm4ge1xuICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAgdmFsdWU6IGZ1bmN0aW9uKC4uLmFyZ3M6IGFueVtdKSB7XG4gICAgICBpZiAodGhpcy5faW5zdGFuY2UpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luc3RhbmNlW2tleV0uYXBwbHkodGhpcy5faW5zdGFuY2UsIGFyZ3MpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS53YXJuKCduZzItZmFjZWJvb2stc2RrOiB0cmllZCBjYWxsaW5nIGluc3RhbmNlIG1ldGhvZCBiZWZvcmUgY29tcG9uZW50IGlzIHJlYWR5LicpO1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cbiAgICB9XG4gIH07XG59XG5cbi8qKlxuICogQGhpZGRlblxuICovXG5leHBvcnQgY2xhc3MgRkJNTENvbXBvbmVudCB7XG5cbiAgcHJvdGVjdGVkIG5hdGl2ZUVsZW1lbnQ6IEhUTUxFbGVtZW50O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsXG4gICAgcHJpdmF0ZSBybmQ6IFJlbmRlcmVyLFxuICAgIHByaXZhdGUgZmJDbGFzczogc3RyaW5nXG4gICkge1xuICAgIHRoaXMubmF0aXZlRWxlbWVudCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudDtcbiAgICB0aGlzLnJuZC5zZXRFbGVtZW50Q2xhc3ModGhpcy5uYXRpdmVFbGVtZW50LCB0aGlzLmZiQ2xhc3MsIHRydWUpO1xuICB9XG5cbiAgcHJvdGVjdGVkIHNldEF0dHJpYnV0ZShuYW1lOiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcpIHtcbiAgICBpZiAoIW5hbWUgfHwgIXZhbHVlKSByZXR1cm47XG4gICAgdGhpcy5ybmQuc2V0RWxlbWVudEF0dHJpYnV0ZSh0aGlzLm5hdGl2ZUVsZW1lbnQsIG5hbWUsIHZhbHVlKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRBdHRyaWJ1dGUobmFtZTogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBpZiAoIW5hbWUpIHJldHVybjtcbiAgICByZXR1cm4gdGhpcy5uYXRpdmVFbGVtZW50LmdldEF0dHJpYnV0ZShuYW1lKTtcbiAgfVxuXG59XG4iXX0=