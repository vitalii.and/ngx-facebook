/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Follow Button
 * \@shortdesc Follow button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/follow-button
 * \@description The Follow button lets people subscribe to the public updates of others on Facebook.
 * \@usage
 * ```html
 * <fb-follow href="https://www.facebook.com/zuck"></fb-follow>
 * ```
 */
export class FBFollowComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-follow');
    }
}
FBFollowComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-follow',
                template: ''
            }] }
];
/** @nocollapse */
FBFollowComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBFollowComponent.propDecorators = {
    "colorScheme": [{ type: Input },],
    "href": [{ type: Input },],
    "kidDirectedSite": [{ type: Input },],
    "layout": [{ type: Input },],
    "showFaces": [{ type: Input },],
    "size": [{ type: Input },],
    "width": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBFollowComponent.prototype, "colorScheme", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBFollowComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBFollowComponent.prototype, "kidDirectedSite", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBFollowComponent.prototype, "layout", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBFollowComponent.prototype, "showFaces", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBFollowComponent.prototype, "size", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBFollowComponent.prototype, "width", void 0);
function FBFollowComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBFollowComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBFollowComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBFollowComponent.propDecorators;
    /**
     * The color scheme used by the plugin. Can be `light` or `dark`. Defaults to `light`.
     * @type {?}
     */
    FBFollowComponent.prototype.colorScheme;
    /**
     * The Facebook.com profile URL of the user to follow.
     * @type {?}
     */
    FBFollowComponent.prototype.href;
    /**
     * If your web site or online service, or a portion of your service, is directed to children under 13 you must enable this. Defaults to `false`.
     * @type {?}
     */
    FBFollowComponent.prototype.kidDirectedSite;
    /**
     * Selects one of the different layouts that are available for the plugin. Can be one of `standard`, `button_count`, or `box_count`.
     * Defaults to `standard`.
     * @type {?}
     */
    FBFollowComponent.prototype.layout;
    /**
     * Specifies whether to display profile photos below the button. Defaults to `false`.
     * @type {?}
     */
    FBFollowComponent.prototype.showFaces;
    /**
     * The button is offered in 2 sizes i.e. `large` and `small`. Defaults to `small`.
     * @type {?}
     */
    FBFollowComponent.prototype.size;
    /**
     * The width of the plugin. The layout you choose affects the minimum and default widths you can use.
     * @type {?}
     */
    FBFollowComponent.prototype.width;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItZm9sbG93LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWZhY2Vib29rLyIsInNvdXJjZXMiOlsic3JjL2NvbXBvbmVudHMvZmItZm9sbG93L2ZiLWZvbGxvdy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7QUFnQmpFLE1BQU0sd0JBQXlCLFNBQVEsYUFBYTs7Ozs7SUFvRGxELFlBQ0UsRUFBYyxFQUNkLEdBQWE7UUFFYixLQUFLLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztLQUM3Qjs7O1lBN0RGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsV0FBVztnQkFDckIsUUFBUSxFQUFFLEVBQUU7YUFDYjs7OztZQWhCMEIsVUFBVTtZQUFFLFFBQVE7Ozs0QkFzQjVDLEtBQUs7cUJBT0wsS0FBSztnQ0FPTCxLQUFLO3VCQVFMLEtBQUs7MEJBT0wsS0FBSztxQkFPTCxLQUFLO3NCQU9MLEtBQUs7OztJQTFDTCxhQUFhOzs7O0lBT2IsYUFBYTs7OztJQU9iLGFBQWE7Ozs7SUFRYixhQUFhOzs7O0lBT2IsYUFBYTs7OztJQU9iLGFBQWE7Ozs7SUFPYixhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgRm9sbG93IEJ1dHRvblxuICogQHNob3J0ZGVzYyBGb2xsb3cgYnV0dG9uIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2ZvbGxvdy1idXR0b25cbiAqIEBkZXNjcmlwdGlvbiBUaGUgRm9sbG93IGJ1dHRvbiBsZXRzIHBlb3BsZSBzdWJzY3JpYmUgdG8gdGhlIHB1YmxpYyB1cGRhdGVzIG9mIG90aGVycyBvbiBGYWNlYm9vay5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLWZvbGxvdyBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL3p1Y2tcIj48L2ZiLWZvbGxvdz5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1mb2xsb3cnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJGb2xsb3dDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGNvbG9yIHNjaGVtZSB1c2VkIGJ5IHRoZSBwbHVnaW4uIENhbiBiZSBgbGlnaHRgIG9yIGBkYXJrYC4gRGVmYXVsdHMgdG8gYGxpZ2h0YC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGNvbG9yU2NoZW1lOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBGYWNlYm9vay5jb20gcHJvZmlsZSBVUkwgb2YgdGhlIHVzZXIgdG8gZm9sbG93LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBJZiB5b3VyIHdlYiBzaXRlIG9yIG9ubGluZSBzZXJ2aWNlLCBvciBhIHBvcnRpb24gb2YgeW91ciBzZXJ2aWNlLCBpcyBkaXJlY3RlZCB0byBjaGlsZHJlbiB1bmRlciAxMyB5b3UgbXVzdCBlbmFibGUgdGhpcy4gRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGtpZERpcmVjdGVkU2l0ZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2VsZWN0cyBvbmUgb2YgdGhlIGRpZmZlcmVudCBsYXlvdXRzIHRoYXQgYXJlIGF2YWlsYWJsZSBmb3IgdGhlIHBsdWdpbi4gQ2FuIGJlIG9uZSBvZiBgc3RhbmRhcmRgLCBgYnV0dG9uX2NvdW50YCwgb3IgYGJveF9jb3VudGAuXG4gICAqIERlZmF1bHRzIHRvIGBzdGFuZGFyZGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBsYXlvdXQ6IHN0cmluZztcblxuICAvKipcbiAgICogU3BlY2lmaWVzIHdoZXRoZXIgdG8gZGlzcGxheSBwcm9maWxlIHBob3RvcyBiZWxvdyB0aGUgYnV0dG9uLiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hvd0ZhY2VzOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBidXR0b24gaXMgb2ZmZXJlZCBpbiAyIHNpemVzIGkuZS4gYGxhcmdlYCBhbmQgYHNtYWxsYC4gRGVmYXVsdHMgdG8gYHNtYWxsYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNpemU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSBwbHVnaW4uIFRoZSBsYXlvdXQgeW91IGNob29zZSBhZmZlY3RzIHRoZSBtaW5pbXVtIGFuZCBkZWZhdWx0IHdpZHRocyB5b3UgY2FuIHVzZS5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItZm9sbG93Jyk7XG4gIH1cblxufVxuIl19