/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Comments
 * \@shortdesc Comments component
 * \@fbdoc https://developers.facebook.com/docs/plugins/comments
 * \@description
 * The comments plugin lets people comment on content on your site using their Facebook account.
 * People can choose to share their comment activity with their friends (and friends of their friends) on Facebook as well.
 * The comments plugin also includes built-in moderation tools and social relevance ranking.
 *
 * \@usage
 * ```html
 * <fb-comments></fb-comments>
 * ```
 */
export class FBCommentsComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-comments');
        /**
         * The absolute URL that comments posted in the plugin will be permanently associated with.
         * All stories shared on Facebook about comments posted using the comments plugin will link to this URL.
         * Defaults to current URL.
         */
        this.href = window.location.href;
    }
}
FBCommentsComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-comments',
                template: ''
            }] }
];
/** @nocollapse */
FBCommentsComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBCommentsComponent.propDecorators = {
    "colorscheme": [{ type: Input },],
    "href": [{ type: Input },],
    "mobile": [{ type: Input },],
    "numposts": [{ type: Input },],
    "orderBy": [{ type: Input },],
    "width": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBCommentsComponent.prototype, "colorscheme", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBCommentsComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBCommentsComponent.prototype, "mobile", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Number)
], FBCommentsComponent.prototype, "numposts", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBCommentsComponent.prototype, "orderBy", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBCommentsComponent.prototype, "width", void 0);
function FBCommentsComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBCommentsComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBCommentsComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBCommentsComponent.propDecorators;
    /**
     * The color scheme used by the comments plugin. Can be `light` or `dark`. Defaults to `light`.
     * @type {?}
     */
    FBCommentsComponent.prototype.colorscheme;
    /**
     * The absolute URL that comments posted in the plugin will be permanently associated with.
     * All stories shared on Facebook about comments posted using the comments plugin will link to this URL.
     * Defaults to current URL.
     * @type {?}
     */
    FBCommentsComponent.prototype.href;
    /**
     * A boolean value that specifies whether to show the mobile-optimized version or not. If no value is given, it will be automatically detected.
     * @type {?}
     */
    FBCommentsComponent.prototype.mobile;
    /**
     * The number of comments to show by default. The minimum value is `1`. Defaults to `10`.
     * @type {?}
     */
    FBCommentsComponent.prototype.numposts;
    /**
     * The order to use when displaying comments. Can be `social`, `reverse_time`, or `time`. The different order types are explained [in the FAQ](https://developers.facebook.com/docs/plugins/comments#faqorder). Defaults to `social`
     * @type {?}
     */
    FBCommentsComponent.prototype.orderBy;
    /**
     * The width of the comments plugin on the webpage.
     * This can be either a pixel value or a percentage (such as 100%) for fluid width.
     * The mobile version of the comments plugin ignores the width parameter and instead has a fluid width of 100%.
     * The minimum width supported by the comments plugin is 320px.
     * Defaults to `550px`.
     * @type {?}
     */
    FBCommentsComponent.prototype.width;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItY29tbWVudHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi1jb21tZW50cy9mYi1jb21tZW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FBb0JqRSxNQUFNLDBCQUEyQixTQUFRLGFBQWE7Ozs7O0lBa0RwRCxZQUNFLEVBQWMsRUFDZCxHQUFhO1FBRWIsS0FBSyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsYUFBYSxDQUFDLENBQUM7Ozs7OztvQkF0Q2pCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTtLQXVDbEM7OztZQTNERixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLFFBQVEsRUFBRSxFQUFFO2FBQ2I7Ozs7WUFwQjBCLFVBQVU7WUFBRSxRQUFROzs7NEJBMEI1QyxLQUFLO3FCQVNMLEtBQUs7dUJBT0wsS0FBSzt5QkFPTCxLQUFLO3dCQU9MLEtBQUs7c0JBV0wsS0FBSzs7O0lBeENMLGFBQWE7Ozs7SUFTYixhQUFhOzs7O0lBT2IsYUFBYTs7OztJQU9iLGFBQWE7Ozs7SUFPYixhQUFhOzs7O0lBV2IsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIENvbW1lbnRzXG4gKiBAc2hvcnRkZXNjIENvbW1lbnRzIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2NvbW1lbnRzXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBjb21tZW50cyBwbHVnaW4gbGV0cyBwZW9wbGUgY29tbWVudCBvbiBjb250ZW50IG9uIHlvdXIgc2l0ZSB1c2luZyB0aGVpciBGYWNlYm9vayBhY2NvdW50LlxuICogUGVvcGxlIGNhbiBjaG9vc2UgdG8gc2hhcmUgdGhlaXIgY29tbWVudCBhY3Rpdml0eSB3aXRoIHRoZWlyIGZyaWVuZHMgKGFuZCBmcmllbmRzIG9mIHRoZWlyIGZyaWVuZHMpIG9uIEZhY2Vib29rIGFzIHdlbGwuXG4gKiBUaGUgY29tbWVudHMgcGx1Z2luIGFsc28gaW5jbHVkZXMgYnVpbHQtaW4gbW9kZXJhdGlvbiB0b29scyBhbmQgc29jaWFsIHJlbGV2YW5jZSByYW5raW5nLlxuICpcbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLWNvbW1lbnRzPjwvZmItY29tbWVudHM+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItY29tbWVudHMnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJDb21tZW50c0NvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igc2NoZW1lIHVzZWQgYnkgdGhlIGNvbW1lbnRzIHBsdWdpbi4gQ2FuIGJlIGBsaWdodGAgb3IgYGRhcmtgLiBEZWZhdWx0cyB0byBgbGlnaHRgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgY29sb3JzY2hlbWU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCB0aGF0IGNvbW1lbnRzIHBvc3RlZCBpbiB0aGUgcGx1Z2luIHdpbGwgYmUgcGVybWFuZW50bHkgYXNzb2NpYXRlZCB3aXRoLlxuICAgKiBBbGwgc3RvcmllcyBzaGFyZWQgb24gRmFjZWJvb2sgYWJvdXQgY29tbWVudHMgcG9zdGVkIHVzaW5nIHRoZSBjb21tZW50cyBwbHVnaW4gd2lsbCBsaW5rIHRvIHRoaXMgVVJMLlxuICAgKiBEZWZhdWx0cyB0byBjdXJyZW50IFVSTC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZyA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuXG4gIC8qKlxuICAgKiBBIGJvb2xlYW4gdmFsdWUgdGhhdCBzcGVjaWZpZXMgd2hldGhlciB0byBzaG93IHRoZSBtb2JpbGUtb3B0aW1pemVkIHZlcnNpb24gb3Igbm90LiBJZiBubyB2YWx1ZSBpcyBnaXZlbiwgaXQgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IGRldGVjdGVkLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbW9iaWxlOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBUaGUgbnVtYmVyIG9mIGNvbW1lbnRzIHRvIHNob3cgYnkgZGVmYXVsdC4gVGhlIG1pbmltdW0gdmFsdWUgaXMgYDFgLiBEZWZhdWx0cyB0byBgMTBgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbnVtcG9zdHM6IG51bWJlcjtcblxuICAvKipcbiAgICogVGhlIG9yZGVyIHRvIHVzZSB3aGVuIGRpc3BsYXlpbmcgY29tbWVudHMuIENhbiBiZSBgc29jaWFsYCwgYHJldmVyc2VfdGltZWAsIG9yIGB0aW1lYC4gVGhlIGRpZmZlcmVudCBvcmRlciB0eXBlcyBhcmUgZXhwbGFpbmVkIFtpbiB0aGUgRkFRXShodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9jb21tZW50cyNmYXFvcmRlcikuIERlZmF1bHRzIHRvIGBzb2NpYWxgXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBvcmRlckJ5OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSB3aWR0aCBvZiB0aGUgY29tbWVudHMgcGx1Z2luIG9uIHRoZSB3ZWJwYWdlLlxuICAgKiBUaGlzIGNhbiBiZSBlaXRoZXIgYSBwaXhlbCB2YWx1ZSBvciBhIHBlcmNlbnRhZ2UgKHN1Y2ggYXMgMTAwJSkgZm9yIGZsdWlkIHdpZHRoLlxuICAgKiBUaGUgbW9iaWxlIHZlcnNpb24gb2YgdGhlIGNvbW1lbnRzIHBsdWdpbiBpZ25vcmVzIHRoZSB3aWR0aCBwYXJhbWV0ZXIgYW5kIGluc3RlYWQgaGFzIGEgZmx1aWQgd2lkdGggb2YgMTAwJS5cbiAgICogVGhlIG1pbmltdW0gd2lkdGggc3VwcG9ydGVkIGJ5IHRoZSBjb21tZW50cyBwbHVnaW4gaXMgMzIwcHguXG4gICAqIERlZmF1bHRzIHRvIGA1NTBweGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLWNvbW1lbnRzJyk7XG4gIH1cblxufVxuIl19