/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Share Button
 * \@shortdesc Share button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/share-button
 * \@description
 * The Share button lets people add a personalized message to links before sharing on their timeline, in groups, or to their friends via a Facebook Message.
 * \@usage
 * ```html
 * <fb-share href="https://github.com/zyra/ng2-facebook-sdk/"></fb-share>
 * ```
 */
export class FBShareComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-share-button');
    }
}
FBShareComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-share',
                template: ''
            }] }
];
/** @nocollapse */
FBShareComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBShareComponent.propDecorators = {
    "href": [{ type: Input },],
    "layout": [{ type: Input },],
    "mobileIframe": [{ type: Input },],
    "size": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBShareComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBShareComponent.prototype, "layout", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBShareComponent.prototype, "mobileIframe", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBShareComponent.prototype, "size", void 0);
function FBShareComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBShareComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBShareComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBShareComponent.propDecorators;
    /**
     * The absolute URL of the page that will be shared. Defaults to the current URL.
     * @type {?}
     */
    FBShareComponent.prototype.href;
    /**
     * Selects one of the different layouts that are available for the plugin. Can be one of `box_count`, `button_count`, `button`. Defaults to `icon_link`.
     * @type {?}
     */
    FBShareComponent.prototype.layout;
    /**
     * If set to true, the share button will open the share dialog in an iframe (instead of a popup) on top of your website on mobile. This option is only available for mobile, not desktop. Defaults to `false`.
     * @type {?}
     */
    FBShareComponent.prototype.mobileIframe;
    /**
     * The button is offered in 2 sizes i.e. large and small. Defaults to `small`.
     * @type {?}
     */
    FBShareComponent.prototype.size;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItc2hhcmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi1zaGFyZS9mYi1zaGFyZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7O0FBaUJqRSxNQUFNLHVCQUF3QixTQUFRLGFBQWE7Ozs7O0lBOEJqRCxZQUNFLEVBQWMsRUFDZCxHQUFhO1FBRWIsS0FBSyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztLQUNuQzs7O1lBdkNGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsUUFBUSxFQUFFLEVBQUU7YUFDYjs7OztZQWpCMEIsVUFBVTtZQUFFLFFBQVE7OztxQkF1QjVDLEtBQUs7dUJBT0wsS0FBSzs2QkFPTCxLQUFLO3FCQU9MLEtBQUs7OztJQXBCTCxhQUFhOzs7O0lBT2IsYUFBYTs7OztJQU9iLGFBQWE7Ozs7SUFPYixhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgU2hhcmUgQnV0dG9uXG4gKiBAc2hvcnRkZXNjIFNoYXJlIGJ1dHRvbiBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9zaGFyZS1idXR0b25cbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIFNoYXJlIGJ1dHRvbiBsZXRzIHBlb3BsZSBhZGQgYSBwZXJzb25hbGl6ZWQgbWVzc2FnZSB0byBsaW5rcyBiZWZvcmUgc2hhcmluZyBvbiB0aGVpciB0aW1lbGluZSwgaW4gZ3JvdXBzLCBvciB0byB0aGVpciBmcmllbmRzIHZpYSBhIEZhY2Vib29rIE1lc3NhZ2UuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1zaGFyZSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3p5cmEvbmcyLWZhY2Vib29rLXNkay9cIj48L2ZiLXNoYXJlPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXNoYXJlJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCU2hhcmVDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgcGFnZSB0aGF0IHdpbGwgYmUgc2hhcmVkLiBEZWZhdWx0cyB0byB0aGUgY3VycmVudCBVUkwuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNlbGVjdHMgb25lIG9mIHRoZSBkaWZmZXJlbnQgbGF5b3V0cyB0aGF0IGFyZSBhdmFpbGFibGUgZm9yIHRoZSBwbHVnaW4uIENhbiBiZSBvbmUgb2YgYGJveF9jb3VudGAsIGBidXR0b25fY291bnRgLCBgYnV0dG9uYC4gRGVmYXVsdHMgdG8gYGljb25fbGlua2AuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBsYXlvdXQ6IHN0cmluZztcblxuICAvKipcbiAgICogSWYgc2V0IHRvIHRydWUsIHRoZSBzaGFyZSBidXR0b24gd2lsbCBvcGVuIHRoZSBzaGFyZSBkaWFsb2cgaW4gYW4gaWZyYW1lIChpbnN0ZWFkIG9mIGEgcG9wdXApIG9uIHRvcCBvZiB5b3VyIHdlYnNpdGUgb24gbW9iaWxlLiBUaGlzIG9wdGlvbiBpcyBvbmx5IGF2YWlsYWJsZSBmb3IgbW9iaWxlLCBub3QgZGVza3RvcC4gRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIG1vYmlsZUlmcmFtZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogVGhlIGJ1dHRvbiBpcyBvZmZlcmVkIGluIDIgc2l6ZXMgaS5lLiBsYXJnZSBhbmQgc21hbGwuIERlZmF1bHRzIHRvIGBzbWFsbGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaXplOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItc2hhcmUtYnV0dG9uJyk7XG4gIH1cblxufVxuIl19