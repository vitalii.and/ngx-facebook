/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Embedded Comments
 * \@shortdesc Embedded comments component
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-comments
 * \@description
 * Embedded comments are a simple way to put public post comments - by a Page or a person on Facebook - into the content of your web site or web page.
 * Only public comments from Facebook Pages and profiles can be embedded.
 * \@usage
 * ```html
 * <fb-comment-embed href="https://www.facebook.com/zuck/posts/10102735452532991?comment_id=1070233703036185" width="500"></fb-comment-embed>
 * ```
 */
export class FBCommentEmbedComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-comment-embed');
    }
}
FBCommentEmbedComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-comment-embed',
                template: ''
            }] }
];
/** @nocollapse */
FBCommentEmbedComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBCommentEmbedComponent.propDecorators = {
    "href": [{ type: Input },],
    "width": [{ type: Input },],
    "includeParent": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBCommentEmbedComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBCommentEmbedComponent.prototype, "width", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBCommentEmbedComponent.prototype, "includeParent", void 0);
function FBCommentEmbedComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBCommentEmbedComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBCommentEmbedComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBCommentEmbedComponent.propDecorators;
    /**
     * The absolute URL of the comment.
     * @type {?}
     */
    FBCommentEmbedComponent.prototype.href;
    /**
     * The width of the embedded comment container. Min. `220px`. Defaults to `560px`.
     * @type {?}
     */
    FBCommentEmbedComponent.prototype.width;
    /**
     * Set to `true` to include parent comment (if URL is a reply). Defaults to `false`.
     * @type {?}
     */
    FBCommentEmbedComponent.prototype.includeParent;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItY29tbWVudC1lbWJlZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2ZiLWNvbW1lbnQtZW1iZWQvZmItY29tbWVudC1lbWJlZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7OztBQWtCakUsTUFBTSw4QkFBK0IsU0FBUSxhQUFhOzs7OztJQXVCeEQsWUFDRSxFQUFjLEVBQ2QsR0FBYTtRQUViLEtBQUssQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLGtCQUFrQixDQUFDLENBQUM7S0FDcEM7OztZQWhDRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsUUFBUSxFQUFFLEVBQUU7YUFDYjs7OztZQWxCMEIsVUFBVTtZQUFFLFFBQVE7OztxQkF3QjVDLEtBQUs7c0JBT0wsS0FBSzs4QkFPTCxLQUFLOzs7SUFiTCxhQUFhOzs7O0lBT2IsYUFBYTs7OztJQU9iLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBFbWJlZGRlZCBDb21tZW50c1xuICogQHNob3J0ZGVzYyBFbWJlZGRlZCBjb21tZW50cyBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9lbWJlZGRlZC1jb21tZW50c1xuICogQGRlc2NyaXB0aW9uXG4gKiBFbWJlZGRlZCBjb21tZW50cyBhcmUgYSBzaW1wbGUgd2F5IHRvIHB1dCBwdWJsaWMgcG9zdCBjb21tZW50cyAtIGJ5IGEgUGFnZSBvciBhIHBlcnNvbiBvbiBGYWNlYm9vayAtIGludG8gdGhlIGNvbnRlbnQgb2YgeW91ciB3ZWIgc2l0ZSBvciB3ZWIgcGFnZS5cbiAqIE9ubHkgcHVibGljIGNvbW1lbnRzIGZyb20gRmFjZWJvb2sgUGFnZXMgYW5kIHByb2ZpbGVzIGNhbiBiZSBlbWJlZGRlZC5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLWNvbW1lbnQtZW1iZWQgaHJlZj1cImh0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS96dWNrL3Bvc3RzLzEwMTAyNzM1NDUyNTMyOTkxP2NvbW1lbnRfaWQ9MTA3MDIzMzcwMzAzNjE4NVwiIHdpZHRoPVwiNTAwXCI+PC9mYi1jb21tZW50LWVtYmVkPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLWNvbW1lbnQtZW1iZWQnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJDb21tZW50RW1iZWRDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgY29tbWVudC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSBlbWJlZGRlZCBjb21tZW50IGNvbnRhaW5lci4gTWluLiBgMjIwcHhgLiBEZWZhdWx0cyB0byBgNTYwcHhgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgd2lkdGg6IHN0cmluZztcblxuICAvKipcbiAgICogU2V0IHRvIGB0cnVlYCB0byBpbmNsdWRlIHBhcmVudCBjb21tZW50IChpZiBVUkwgaXMgYSByZXBseSkuIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBpbmNsdWRlUGFyZW50OiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLWNvbW1lbnQtZW1iZWQnKTtcbiAgfVxuXG59XG4iXX0=