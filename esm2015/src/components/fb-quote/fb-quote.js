/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Quote Plugin
 * \@shortdesc Quote plugin component
 * \@fbdoc https://developers.facebook.com/docs/plugins/quote
 * \@description
 * The quote plugin lets people select text on your page and add it to their share, so they can tell a more expressive story.
 * Note that you do not need to implement Facebook login or request any additional permissions through app review in order to use this plugin.
 * \@usage
 * ```html
 * <fb-quote></fb-quote>
 * ```
 */
export class FBQuoteComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-quote');
    }
}
FBQuoteComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-quote',
                template: ''
            }] }
];
/** @nocollapse */
FBQuoteComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBQuoteComponent.propDecorators = {
    "href": [{ type: Input },],
    "layout": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBQuoteComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBQuoteComponent.prototype, "layout", void 0);
function FBQuoteComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBQuoteComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBQuoteComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBQuoteComponent.propDecorators;
    /**
     * The absolute URL of the page that will be quoted.
     * Defaults to the current URL
     * @type {?}
     */
    FBQuoteComponent.prototype.href;
    /**
     * Can be set to quote or button. Defaults to quote.
     * @type {?}
     */
    FBQuoteComponent.prototype.layout;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItcXVvdGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi1xdW90ZS9mYi1xdW90ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7OztBQWtCakUsTUFBTSx1QkFBd0IsU0FBUSxhQUFhOzs7OztJQWlCakQsWUFDRSxFQUFjLEVBQ2QsR0FBYTtRQUViLEtBQUssQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0tBQzVCOzs7WUExQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxVQUFVO2dCQUNwQixRQUFRLEVBQUUsRUFBRTthQUNiOzs7O1lBbEIwQixVQUFVO1lBQUUsUUFBUTs7O3FCQXlCNUMsS0FBSzt1QkFPTCxLQUFLOzs7SUFOTCxhQUFhOzs7O0lBT2IsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIFF1b3RlIFBsdWdpblxuICogQHNob3J0ZGVzYyBRdW90ZSBwbHVnaW4gY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvcXVvdGVcbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIHF1b3RlIHBsdWdpbiBsZXRzIHBlb3BsZSBzZWxlY3QgdGV4dCBvbiB5b3VyIHBhZ2UgYW5kIGFkZCBpdCB0byB0aGVpciBzaGFyZSwgc28gdGhleSBjYW4gdGVsbCBhIG1vcmUgZXhwcmVzc2l2ZSBzdG9yeS5cbiAqIE5vdGUgdGhhdCB5b3UgZG8gbm90IG5lZWQgdG8gaW1wbGVtZW50IEZhY2Vib29rIGxvZ2luIG9yIHJlcXVlc3QgYW55IGFkZGl0aW9uYWwgcGVybWlzc2lvbnMgdGhyb3VnaCBhcHAgcmV2aWV3IGluIG9yZGVyIHRvIHVzZSB0aGlzIHBsdWdpbi5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLXF1b3RlPjwvZmItcXVvdGU+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItcXVvdGUnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJRdW90ZUNvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBwYWdlIHRoYXQgd2lsbCBiZSBxdW90ZWQuXG4gICAqIERlZmF1bHRzIHRvIHRoZSBjdXJyZW50IFVSTFxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBDYW4gYmUgc2V0IHRvIHF1b3RlIG9yIGJ1dHRvbi4gRGVmYXVsdHMgdG8gcXVvdGUuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBsYXlvdXQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1xdW90ZScpO1xuICB9XG5cbn1cbiJdfQ==