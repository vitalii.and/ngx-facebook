/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Embedded Posts
 * \@shortdesc Embedded post component
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-posts
 * \@description
 * Embedded Posts are a simple way to put public posts - by a Page or a person on Facebook - into the content of your web site or web page.
 * Only public posts from Facebook Pages and profiles can be embedded.
 * \@usage
 * ```html
 * <fb-post href="https://www.facebook.com/20531316728/posts/10154009990506729/"></fb-post>
 * ```
 */
export class FBPostComponent extends FBMLComponent {
    /**
     * @param {?} el
     * @param {?} rnd
     */
    constructor(el, rnd) {
        super(el, rnd, 'fb-post');
    }
}
FBPostComponent.decorators = [
    { type: Component, args: [{
                selector: 'fb-post',
                template: ''
            }] }
];
/** @nocollapse */
FBPostComponent.ctorParameters = () => [
    { type: ElementRef, },
    { type: Renderer, },
];
FBPostComponent.propDecorators = {
    "href": [{ type: Input },],
    "width": [{ type: Input },],
    "showText": [{ type: Input },],
};
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBPostComponent.prototype, "href", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", String)
], FBPostComponent.prototype, "width", void 0);
tslib_1.__decorate([
    FBMLAttribute,
    tslib_1.__metadata("design:type", Boolean)
], FBPostComponent.prototype, "showText", void 0);
function FBPostComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBPostComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBPostComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBPostComponent.propDecorators;
    /**
     * The absolute URL of the post.
     * @type {?}
     */
    FBPostComponent.prototype.href;
    /**
     * The width of the post. Min. `350` pixel; Max. `750` pixel. Set to auto to use fluid width. Defaults to `auto`.
     * @type {?}
     */
    FBPostComponent.prototype.width;
    /**
     * Applied to photo post. Set to `true` to include the text from the Facebook post, if any. Defaults to `false`.
     * @type {?}
     */
    FBPostComponent.prototype.showText;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItcG9zdC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2ZiLXBvc3QvZmItcG9zdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7OztBQWtCakUsTUFBTSxzQkFBdUIsU0FBUSxhQUFhOzs7OztJQXVCaEQsWUFDRSxFQUFjLEVBQ2QsR0FBYTtRQUViLEtBQUssQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0tBQzNCOzs7WUFoQ0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxTQUFTO2dCQUNuQixRQUFRLEVBQUUsRUFBRTthQUNiOzs7O1lBbEIwQixVQUFVO1lBQUUsUUFBUTs7O3FCQXdCNUMsS0FBSztzQkFPTCxLQUFLO3lCQU9MLEtBQUs7OztJQWJMLGFBQWE7Ozs7SUFPYixhQUFhOzs7O0lBT2IsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIEVtYmVkZGVkIFBvc3RzXG4gKiBAc2hvcnRkZXNjIEVtYmVkZGVkIHBvc3QgY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZW1iZWRkZWQtcG9zdHNcbiAqIEBkZXNjcmlwdGlvblxuICogRW1iZWRkZWQgUG9zdHMgYXJlIGEgc2ltcGxlIHdheSB0byBwdXQgcHVibGljIHBvc3RzIC0gYnkgYSBQYWdlIG9yIGEgcGVyc29uIG9uIEZhY2Vib29rIC0gaW50byB0aGUgY29udGVudCBvZiB5b3VyIHdlYiBzaXRlIG9yIHdlYiBwYWdlLlxuICogT25seSBwdWJsaWMgcG9zdHMgZnJvbSBGYWNlYm9vayBQYWdlcyBhbmQgcHJvZmlsZXMgY2FuIGJlIGVtYmVkZGVkLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItcG9zdCBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tLzIwNTMxMzE2NzI4L3Bvc3RzLzEwMTU0MDA5OTkwNTA2NzI5L1wiPjwvZmItcG9zdD5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1wb3N0JyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCUG9zdENvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBwb3N0LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIHBvc3QuIE1pbi4gYDM1MGAgcGl4ZWw7IE1heC4gYDc1MGAgcGl4ZWwuIFNldCB0byBhdXRvIHRvIHVzZSBmbHVpZCB3aWR0aC4gRGVmYXVsdHMgdG8gYGF1dG9gLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgd2lkdGg6IHN0cmluZztcblxuICAvKipcbiAgICogQXBwbGllZCB0byBwaG90byBwb3N0LiBTZXQgdG8gYHRydWVgIHRvIGluY2x1ZGUgdGhlIHRleHQgZnJvbSB0aGUgRmFjZWJvb2sgcG9zdCwgaWYgYW55LiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hvd1RleHQ6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItcG9zdCcpO1xuICB9XG5cbn1cbiJdfQ==