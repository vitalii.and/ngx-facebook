/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
/**
 * \@shortdesc
 * Angular 2 service to inject to use Facebook's SDK
 * \@description
 * You only need to inject this service in your application if you aren't using [`FacebookModule`](../facebook-module).
 * \@usage
 * ```typescript
 * import { FacebookService, InitParams } from 'ng2-facebook-sdk';
 *
 * constructor(private fb: FacebookService) {
 *
 *   const params: InitParams = {
 *
 *   };
 *
 *   fb.init(params);
 *
 * }
 * ```
 */
export class FacebookService {
    /**
     * This method is used to initialize and setup the SDK.
     * @param {?} params {InitParams} Initialization parameters
     * @return {?} return {Promise<any>}
     */
    init(params) {
        try {
            return Promise.resolve(FB.init(params));
        }
        catch (/** @type {?} */ e) {
            return Promise.reject(e);
        }
    }
    /**
     * This method lets you make calls to the Graph API
     * \@usage
     * ```typescript
     * this.fb.api('somepath')
     *   .then(res => console.log(res))
     *   .catch(e => console.log(e));
     * ```
     * @param {?} path {string} The Graph API endpoint path that you want to call.
     * @param {?=} method
     * @param {?=} params
     * @return {?} return {Promise<any>}
     */
    api(path, method = 'get', params = {}) {
        return new Promise((resolve, reject) => {
            try {
                FB.api(path, method, params, (response) => {
                    if (!response) {
                        reject();
                    }
                    else if (response.error) {
                        reject(response.error);
                    }
                    else {
                        resolve(response);
                    }
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    }
    /**
     * This method is used to trigger different forms of Facebook created UI dialogs.
     * These dialogs include:
     * - Share dialog
     * - Login dialog
     * - Add page tab dialog
     * - Requests dialog
     * - Send dialog
     * - Payments dialog
     * - Go Live dialog
     * @param {?} params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
     * @return {?} return {Promise<UIResponse>}
     */
    ui(params) {
        return new Promise((resolve, reject) => {
            try {
                FB.ui(params, (response) => {
                    if (!response)
                        reject();
                    else if (response.error)
                        reject(response.error);
                    else
                        resolve(response);
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    }
    /**
     * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * @param {?=} forceFreshResponse
     * @return {?} return {Promise<LoginStatus>}
     */
    getLoginStatus(forceFreshResponse) {
        return new Promise((resolve, reject) => {
            try {
                FB.getLoginStatus((response) => {
                    if (!response) {
                        reject();
                    }
                    else {
                        resolve(response);
                    }
                }, forceFreshResponse);
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    }
    /**
     * Login the user
     * \@usage
     * ```typescript
     * // login without options
     * this.fb.login()
     *   .then((response: LoginResponse) => console.log('Logged in', response))
     *   .catch(e => console.error('Error logging in'));
     *
     * // login with options
     * const options: LoginOptions = {
     *   scope: 'public_profile,user_friends,email,pages_show_list',
     *   return_scopes: true,
     *   enable_profile_selector: true
     * };
     * this.fb.login(options)
     *   .then(...)
     *   .catch(...);
     * ```
     * @param {?=} options
     * @return {?} return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
     */
    login(options) {
        return new Promise((resolve, reject) => {
            try {
                FB.login((response) => {
                    if (response.authResponse) {
                        resolve(response);
                    }
                    else {
                        reject();
                    }
                }, options);
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    }
    /**
     * Logout the user
     * \@usage
     * ```typescript
     * this.fb.logout().then(() => console.log('Logged out!'));
     * ```
     * @return {?} return {Promise<any>} returns a promise that resolves when the user is logged out
     */
    logout() {
        return new Promise((resolve, reject) => {
            try {
                FB.logout((response) => {
                    resolve(response);
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    }
    /**
     * This synchronous function returns back the current authResponse.
     * \@usage
     * ```typescript
     * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
     *
     * ...
     *
     * const authResponse: AuthResponse = this.fb.getAuthResponse();
     * ```
     * @return {?} return {AuthResponse} returns an [AuthResponse](../auth-response) object
     */
    getAuthResponse() {
        try {
            return /** @type {?} */ (FB.getAuthResponse());
        }
        catch (/** @type {?} */ e) {
            console.error('ng2-facebook-sdk: ', e);
        }
    }
}
FacebookService.decorators = [
    { type: Injectable }
];
function FacebookService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FacebookService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FacebookService.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZWJvb2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvcHJvdmlkZXJzL2ZhY2Vib29rLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQzNDLE1BQU07Ozs7OztJQU9KLElBQUksQ0FBQyxNQUFrQjtRQUNyQixJQUFJLENBQUM7WUFDSCxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDekM7UUFBQyxLQUFLLENBQUMsQ0FBQyxpQkFBQSxDQUFDLEVBQUUsQ0FBQztZQUNYLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzFCO0tBQ0Y7Ozs7Ozs7Ozs7Ozs7O0lBZUQsR0FBRyxDQUFDLElBQVksRUFBRSxTQUFvQixLQUFLLEVBQUUsU0FBYyxFQUFFO1FBQzNELE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUUxQyxJQUFJLENBQUM7Z0JBQ0gsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLFFBQWEsRUFBRSxFQUFFO29CQUM3QyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7d0JBQ2QsTUFBTSxFQUFFLENBQUM7cUJBQ1Y7b0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUMxQixNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN4QjtvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDTixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ25CO2lCQUNGLENBQUMsQ0FBQzthQUNKO1lBQUMsS0FBSyxDQUFDLENBQUMsaUJBQUEsQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1g7U0FFRixDQUFDLENBQUM7S0FDSjs7Ozs7Ozs7Ozs7Ozs7SUFlRCxFQUFFLENBQUMsTUFBZ0I7UUFDakIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFNLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBRTFDLElBQUksQ0FBQztnQkFDSCxFQUFFLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQWEsRUFBRSxFQUFFO29CQUM5QixFQUFFLENBQUEsQ0FBQyxDQUFDLFFBQVEsQ0FBQzt3QkFBQyxNQUFNLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7d0JBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDL0MsSUFBSTt3QkFBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3hCLENBQUMsQ0FBQzthQUNKO1lBQUMsS0FBSyxDQUFDLENBQUMsaUJBQUEsQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1g7U0FFRixDQUFDLENBQUM7S0FDSjs7Ozs7O0lBT0QsY0FBYyxDQUFDLGtCQUE0QjtRQUN6QyxNQUFNLENBQUMsSUFBSSxPQUFPLENBQWMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFbEQsSUFBSSxDQUFDO2dCQUNILEVBQUUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxRQUFxQixFQUFFLEVBQUU7b0JBQzFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt3QkFDZCxNQUFNLEVBQUUsQ0FBQztxQkFDVjtvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDTixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ25CO2lCQUNGLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzthQUN4QjtZQUFDLEtBQUssQ0FBQyxDQUFDLGlCQUFBLENBQUMsRUFBRSxDQUFDO2dCQUNYLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNYO1NBRUYsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBd0JELEtBQUssQ0FBQyxPQUFzQjtRQUMxQixNQUFNLENBQUMsSUFBSSxPQUFPLENBQWdCLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBRXBELElBQUksQ0FBQztnQkFDSCxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBdUIsRUFBRSxFQUFFO29CQUNuQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzt3QkFDMUIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUNuQjtvQkFBQSxJQUFJLENBQUEsQ0FBQzt3QkFDSixNQUFNLEVBQUUsQ0FBQztxQkFDVjtpQkFDRixFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ2I7WUFBQyxLQUFLLENBQUMsQ0FBQyxpQkFBQSxDQUFDLEVBQUUsQ0FBQztnQkFDWCxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDWDtTQUVGLENBQUMsQ0FBQztLQUNKOzs7Ozs7Ozs7SUFVRCxNQUFNO1FBQ0osTUFBTSxDQUFDLElBQUksT0FBTyxDQUFNLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBRTFDLElBQUksQ0FBQztnQkFDSCxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7b0JBQzFCLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDbkIsQ0FBQyxDQUFDO2FBQ0o7WUFBQyxLQUFLLENBQUMsQ0FBQyxpQkFBQSxDQUFDLEVBQUUsQ0FBQztnQkFDWCxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDWDtTQUVGLENBQUMsQ0FBQztLQUNKOzs7Ozs7Ozs7Ozs7O0lBY0QsZUFBZTtRQUNiLElBQUksQ0FBQztZQUNILE1BQU0sbUJBQWUsRUFBRSxDQUFDLGVBQWUsRUFBRSxFQUFDO1NBQzNDO1FBQUMsS0FBSyxDQUFDLENBQUMsaUJBQUEsQ0FBQyxFQUFFLENBQUM7WUFDWCxPQUFPLENBQUMsS0FBSyxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3hDO0tBQ0Y7OztZQXJMRixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXV0aFJlc3BvbnNlIH0gZnJvbSAnLi4vbW9kZWxzL2F1dGgtcmVzcG9uc2UnO1xuaW1wb3J0IHsgSW5pdFBhcmFtcyB9IGZyb20gJy4uL21vZGVscy9pbml0LXBhcmFtcyc7XG5pbXBvcnQgeyBMb2dpbk9wdGlvbnMgfSBmcm9tICcuLi9tb2RlbHMvbG9naW4tb3B0aW9ucyc7XG5pbXBvcnQgeyBMb2dpblJlc3BvbnNlIH0gZnJvbSAnLi4vbW9kZWxzL2xvZ2luLXJlc3BvbnNlJztcbmltcG9ydCB7IExvZ2luU3RhdHVzIH0gZnJvbSAnLi4vbW9kZWxzL2xvZ2luLXN0YXR1cyc7XG5pbXBvcnQgeyBVSVBhcmFtcyB9IGZyb20gJy4uL21vZGVscy91aS1wYXJhbXMnO1xuaW1wb3J0IHsgVUlSZXNwb25zZSB9IGZyb20gJy4uL21vZGVscy91aS1yZXNwb25zZSc7XG5cbmRlY2xhcmUgdmFyIEZCOiBhbnk7XG5cbi8qKlxuICogQGhpZGRlblxuICovXG5leHBvcnQgdHlwZSBBcGlNZXRob2QgPSAnZ2V0JyB8ICdwb3N0JyB8ICdkZWxldGUnO1xuXG4vKipcbiAqIEBzaG9ydGRlc2NcbiAqIEFuZ3VsYXIgMiBzZXJ2aWNlIHRvIGluamVjdCB0byB1c2UgRmFjZWJvb2sncyBTREtcbiAqIEBkZXNjcmlwdGlvblxuICogWW91IG9ubHkgbmVlZCB0byBpbmplY3QgdGhpcyBzZXJ2aWNlIGluIHlvdXIgYXBwbGljYXRpb24gaWYgeW91IGFyZW4ndCB1c2luZyBbYEZhY2Vib29rTW9kdWxlYF0oLi4vZmFjZWJvb2stbW9kdWxlKS5cbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgRmFjZWJvb2tTZXJ2aWNlLCBJbml0UGFyYW1zIH0gZnJvbSAnbmcyLWZhY2Vib29rLXNkayc7XG4gKlxuICogY29uc3RydWN0b3IocHJpdmF0ZSBmYjogRmFjZWJvb2tTZXJ2aWNlKSB7XG4gKlxuICogICBjb25zdCBwYXJhbXM6IEluaXRQYXJhbXMgPSB7XG4gKlxuICogICB9O1xuICpcbiAqICAgZmIuaW5pdChwYXJhbXMpO1xuICpcbiAqIH1cbiAqIGBgYFxuICovXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRmFjZWJvb2tTZXJ2aWNlIHtcblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgaXMgdXNlZCB0byBpbml0aWFsaXplIGFuZCBzZXR1cCB0aGUgU0RLLlxuICAgKiBAcGFyYW0gcGFyYW1zIHtJbml0UGFyYW1zfSBJbml0aWFsaXphdGlvbiBwYXJhbWV0ZXJzXG4gICAqIEByZXR1cm5zIHJldHVybiB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgaW5pdChwYXJhbXM6IEluaXRQYXJhbXMpOiBQcm9taXNlPGFueT4ge1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKEZCLmluaXQocGFyYW1zKSk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGUpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIG1ldGhvZCBsZXRzIHlvdSBtYWtlIGNhbGxzIHRvIHRoZSBHcmFwaCBBUElcbiAgICogQHVzYWdlXG4gICAqIGBgYHR5cGVzY3JpcHRcbiAgICogdGhpcy5mYi5hcGkoJ3NvbWVwYXRoJylcbiAgICogICAudGhlbihyZXMgPT4gY29uc29sZS5sb2cocmVzKSlcbiAgICogICAuY2F0Y2goZSA9PiBjb25zb2xlLmxvZyhlKSk7XG4gICAqIGBgYFxuICAgKiBAcGFyYW0gcGF0aCB7c3RyaW5nfSBUaGUgR3JhcGggQVBJIGVuZHBvaW50IHBhdGggdGhhdCB5b3Ugd2FudCB0byBjYWxsLlxuICAgKiBAcGFyYW0gW21ldGhvZD1nZXRdIHtzdHJpbmd9IFRoZSBIVFRQIG1ldGhvZCB0aGF0IHlvdSB3YW50IHRvIHVzZSBmb3IgdGhlIEFQSSByZXF1ZXN0LlxuICAgKiBAcGFyYW0gW3BhcmFtc10ge09iamVjdH0gQW4gb2JqZWN0IGNvbnNpc3Rpbmcgb2YgYW55IHBhcmFtZXRlcnMgdGhhdCB5b3Ugd2FudCB0byBwYXNzIGludG8geW91ciBHcmFwaCBBUEkgY2FsbC5cbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBhcGkocGF0aDogc3RyaW5nLCBtZXRob2Q6IEFwaU1ldGhvZCA9ICdnZXQnLCBwYXJhbXM6IGFueSA9IHt9KTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIEZCLmFwaShwYXRoLCBtZXRob2QsIHBhcmFtcywgKHJlc3BvbnNlOiBhbnkpID0+IHtcbiAgICAgICAgICBpZiAoIXJlc3BvbnNlKSB7XG4gICAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHJlc3BvbnNlLmVycm9yKSB7XG4gICAgICAgICAgICByZWplY3QocmVzcG9uc2UuZXJyb3IpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIG1ldGhvZCBpcyB1c2VkIHRvIHRyaWdnZXIgZGlmZmVyZW50IGZvcm1zIG9mIEZhY2Vib29rIGNyZWF0ZWQgVUkgZGlhbG9ncy5cbiAgICogVGhlc2UgZGlhbG9ncyBpbmNsdWRlOlxuICAgKiAtIFNoYXJlIGRpYWxvZ1xuICAgKiAtIExvZ2luIGRpYWxvZ1xuICAgKiAtIEFkZCBwYWdlIHRhYiBkaWFsb2dcbiAgICogLSBSZXF1ZXN0cyBkaWFsb2dcbiAgICogLSBTZW5kIGRpYWxvZ1xuICAgKiAtIFBheW1lbnRzIGRpYWxvZ1xuICAgKiAtIEdvIExpdmUgZGlhbG9nXG4gICAqIEBwYXJhbSBwYXJhbXMge1VJUGFyYW1zfSBBIGNvbGxlY3Rpb24gb2YgcGFyYW1ldGVycyB0aGF0IGNvbnRyb2wgd2hpY2ggZGlhbG9nIGlzIGxvYWRlZCwgYW5kIHJlbGV2YW50IHNldHRpbmdzLlxuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8VUlSZXNwb25zZT59XG4gICAqL1xuICB1aShwYXJhbXM6IFVJUGFyYW1zKTogUHJvbWlzZTxVSVJlc3BvbnNlPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGFueT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICB0cnkge1xuICAgICAgICBGQi51aShwYXJhbXMsIChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgaWYoIXJlc3BvbnNlKSByZWplY3QoKTtcbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlLmVycm9yKSByZWplY3QocmVzcG9uc2UuZXJyb3IpO1xuICAgICAgICAgIGVsc2UgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIG1ldGhvZCBhbGxvd3MgeW91IHRvIGRldGVybWluZSBpZiBhIHVzZXIgaXMgbG9nZ2VkIGluIHRvIEZhY2Vib29rIGFuZCBoYXMgYXV0aGVudGljYXRlZCB5b3VyIGFwcC5cbiAgICogQHBhcmFtIFtmb3JjZUZyZXNoUmVzcG9uc2U9ZmFsc2VdIHtib29sZWFufSBGb3JjZSBhIGZyZXNoIHJlc3BvbnNlLlxuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8TG9naW5TdGF0dXM+fVxuICAgKi9cbiAgZ2V0TG9naW5TdGF0dXMoZm9yY2VGcmVzaFJlc3BvbnNlPzogYm9vbGVhbik6IFByb21pc2U8TG9naW5TdGF0dXM+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8TG9naW5TdGF0dXM+KChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgdHJ5IHtcbiAgICAgICAgRkIuZ2V0TG9naW5TdGF0dXMoKHJlc3BvbnNlOiBMb2dpblN0YXR1cykgPT4ge1xuICAgICAgICAgIGlmICghcmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHJlamVjdCgpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sIGZvcmNlRnJlc2hSZXNwb25zZSk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJlamVjdChlKTtcbiAgICAgIH1cblxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIExvZ2luIHRoZSB1c2VyXG4gICAqIEB1c2FnZVxuICAgKiBgYGB0eXBlc2NyaXB0XG4gICAqIC8vIGxvZ2luIHdpdGhvdXQgb3B0aW9uc1xuICAgKiB0aGlzLmZiLmxvZ2luKClcbiAgICogICAudGhlbigocmVzcG9uc2U6IExvZ2luUmVzcG9uc2UpID0+IGNvbnNvbGUubG9nKCdMb2dnZWQgaW4nLCByZXNwb25zZSkpXG4gICAqICAgLmNhdGNoKGUgPT4gY29uc29sZS5lcnJvcignRXJyb3IgbG9nZ2luZyBpbicpKTtcbiAgICpcbiAgICogLy8gbG9naW4gd2l0aCBvcHRpb25zXG4gICAqIGNvbnN0IG9wdGlvbnM6IExvZ2luT3B0aW9ucyA9IHtcbiAgICogICBzY29wZTogJ3B1YmxpY19wcm9maWxlLHVzZXJfZnJpZW5kcyxlbWFpbCxwYWdlc19zaG93X2xpc3QnLFxuICAgKiAgIHJldHVybl9zY29wZXM6IHRydWUsXG4gICAqICAgZW5hYmxlX3Byb2ZpbGVfc2VsZWN0b3I6IHRydWVcbiAgICogfTtcbiAgICogdGhpcy5mYi5sb2dpbihvcHRpb25zKVxuICAgKiAgIC50aGVuKC4uLilcbiAgICogICAuY2F0Y2goLi4uKTtcbiAgICogYGBgXG4gICAqIEBwYXJhbSBbb3B0aW9uc10ge0xvZ2luT3B0aW9uc30gTG9naW4gb3B0aW9uc1xuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8TG9naW5SZXNwb25zZT59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2l0aCBbTG9naW5SZXNwb25zZV0oLi4vbG9naW4tcmVzcG9uc2UpIG9iamVjdCwgb3IgcmVqZWN0cyB3aXRoIGFuIGVycm9yXG4gICAqL1xuICBsb2dpbihvcHRpb25zPzogTG9naW5PcHRpb25zKTogUHJvbWlzZTxMb2dpblJlc3BvbnNlPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPExvZ2luUmVzcG9uc2U+KChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgdHJ5IHtcbiAgICAgICAgRkIubG9naW4oKHJlc3BvbnNlOiBMb2dpblJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgaWYgKHJlc3BvbnNlLmF1dGhSZXNwb25zZSkge1xuICAgICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sIG9wdGlvbnMpO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb2dvdXQgdGhlIHVzZXJcbiAgICogQHVzYWdlXG4gICAqIGBgYHR5cGVzY3JpcHRcbiAgICogdGhpcy5mYi5sb2dvdXQoKS50aGVuKCgpID0+IGNvbnNvbGUubG9nKCdMb2dnZWQgb3V0IScpKTtcbiAgICogYGBgXG4gICAqIEByZXR1cm5zIHJldHVybiB7UHJvbWlzZTxhbnk+fSByZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gdGhlIHVzZXIgaXMgbG9nZ2VkIG91dFxuICAgKi9cbiAgbG9nb3V0KCk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGFueT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICB0cnkge1xuICAgICAgICBGQi5sb2dvdXQoKHJlc3BvbnNlOiBhbnkpID0+IHtcbiAgICAgICAgICByZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJlamVjdChlKTtcbiAgICAgIH1cblxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgc3luY2hyb25vdXMgZnVuY3Rpb24gcmV0dXJucyBiYWNrIHRoZSBjdXJyZW50IGF1dGhSZXNwb25zZS5cbiAgICogQHVzYWdlXG4gICAqIGBgYHR5cGVzY3JpcHRcbiAgICogaW1wb3J0IHsgQXV0aFJlc3BvbnNlLCBGYWNlYm9va1NlcnZpY2UgfSBmcm9tICduZzItZmFjZWJvb2stc2RrJztcbiAgICpcbiAgICogLi4uXG4gICAqXG4gICAqIGNvbnN0IGF1dGhSZXNwb25zZTogQXV0aFJlc3BvbnNlID0gdGhpcy5mYi5nZXRBdXRoUmVzcG9uc2UoKTtcbiAgICogYGBgXG4gICAqIEByZXR1cm5zIHJldHVybiB7QXV0aFJlc3BvbnNlfSByZXR1cm5zIGFuIFtBdXRoUmVzcG9uc2VdKC4uL2F1dGgtcmVzcG9uc2UpIG9iamVjdFxuICAgKi9cbiAgZ2V0QXV0aFJlc3BvbnNlKCk6IEF1dGhSZXNwb25zZSB7XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiA8QXV0aFJlc3BvbnNlPkZCLmdldEF1dGhSZXNwb25zZSgpO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ25nMi1mYWNlYm9vay1zZGs6ICcsIGUpO1xuICAgIH1cbiAgfVxuXG59XG5cbiJdfQ==