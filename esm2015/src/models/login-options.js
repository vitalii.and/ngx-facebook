/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Options that can be passed to the [login](../facebook-service/#login) method.
 * @record
 */
export function LoginOptions() { }
function LoginOptions_tsickle_Closure_declarations() {
    /**
     * Optional key, only supports one value: rerequest. Use this when re-requesting a declined permission.
     * @type {?|undefined}
     */
    LoginOptions.prototype.auth_type;
    /**
     * Comma separated list of extended permissions
     * @type {?|undefined}
     */
    LoginOptions.prototype.scope;
    /**
     * When true, the granted scopes will be returned in a comma-separated list.
     * @type {?|undefined}
     */
    LoginOptions.prototype.return_scopes;
    /**
     * When true, prompt the user to grant permission for one or more Pages.
     * @type {?|undefined}
     */
    LoginOptions.prototype.enable_profile_selector;
    /**
     * Comma separated list of IDs to display in the profile selector
     * @type {?|undefined}
     */
    LoginOptions.prototype.profile_selector_ids;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9tb2RlbHMvbG9naW4tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBPcHRpb25zIHRoYXQgY2FuIGJlIHBhc3NlZCB0byB0aGUgW2xvZ2luXSguLi9mYWNlYm9vay1zZXJ2aWNlLyNsb2dpbikgbWV0aG9kLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIExvZ2luT3B0aW9ucyB7XG4gICAgLyoqXG4gICAgICogT3B0aW9uYWwga2V5LCBvbmx5IHN1cHBvcnRzIG9uZSB2YWx1ZTogcmVyZXF1ZXN0LiBVc2UgdGhpcyB3aGVuIHJlLXJlcXVlc3RpbmcgYSBkZWNsaW5lZCBwZXJtaXNzaW9uLlxuICAgICAqL1xuICAgIGF1dGhfdHlwZT86IHN0cmluZztcbiAgICAvKipcbiAgICAgKiBDb21tYSBzZXBhcmF0ZWQgbGlzdCBvZiBleHRlbmRlZCBwZXJtaXNzaW9uc1xuICAgICAqL1xuICAgIHNjb3BlPzogc3RyaW5nO1xuICAgIC8qKlxuICAgICAqIFdoZW4gdHJ1ZSwgdGhlIGdyYW50ZWQgc2NvcGVzIHdpbGwgYmUgcmV0dXJuZWQgaW4gYSBjb21tYS1zZXBhcmF0ZWQgbGlzdC5cbiAgICAgKi9cbiAgICByZXR1cm5fc2NvcGVzPzogYm9vbGVhbjtcbiAgICAvKipcbiAgICAgKiBXaGVuIHRydWUsIHByb21wdCB0aGUgdXNlciB0byBncmFudCBwZXJtaXNzaW9uIGZvciBvbmUgb3IgbW9yZSBQYWdlcy5cbiAgICAgKi9cbiAgICBlbmFibGVfcHJvZmlsZV9zZWxlY3Rvcj86IGJvb2xlYW47XG4gICAgLyoqXG4gICAgICogQ29tbWEgc2VwYXJhdGVkIGxpc3Qgb2YgSURzIHRvIGRpc3BsYXkgaW4gdGhlIHByb2ZpbGUgc2VsZWN0b3JcbiAgICAgKi9cbiAgICBwcm9maWxlX3NlbGVjdG9yX2lkcz86IHN0cmluZztcbn1cbiJdfQ==