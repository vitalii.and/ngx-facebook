/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * The response object returned by the [getAuthResponse](../facebook-service/#getAuthResponse) method, and is also used in [LoginResponse](../login-response/) and [LoginStatus](../login-status/).
 * @record
 */
export function AuthResponse() { }
function AuthResponse_tsickle_Closure_declarations() {
    /**
     * User access token
     * @type {?}
     */
    AuthResponse.prototype.accessToken;
    /**
     * Access token lifetime in seconds
     * @type {?}
     */
    AuthResponse.prototype.expiresIn;
    /**
     *
     * @type {?}
     */
    AuthResponse.prototype.signedRequest;
    /**
     * The Facebook user ID
     * @type {?}
     */
    AuthResponse.prototype.userID;
    /**
     * The granted scopes. This field is only available if you set `return_scopes` to true when calling login method.
     * @type {?|undefined}
     */
    AuthResponse.prototype.grantedScopes;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1yZXNwb25zZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9tb2RlbHMvYXV0aC1yZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBUaGUgcmVzcG9uc2Ugb2JqZWN0IHJldHVybmVkIGJ5IHRoZSBbZ2V0QXV0aFJlc3BvbnNlXSguLi9mYWNlYm9vay1zZXJ2aWNlLyNnZXRBdXRoUmVzcG9uc2UpIG1ldGhvZCwgYW5kIGlzIGFsc28gdXNlZCBpbiBbTG9naW5SZXNwb25zZV0oLi4vbG9naW4tcmVzcG9uc2UvKSBhbmQgW0xvZ2luU3RhdHVzXSguLi9sb2dpbi1zdGF0dXMvKS5cbiAqL1xuZXhwb3J0IGludGVyZmFjZSBBdXRoUmVzcG9uc2Uge1xuICAgIC8qKlxuICAgICAqIFVzZXIgYWNjZXNzIHRva2VuXG4gICAgICovXG4gICAgYWNjZXNzVG9rZW46IHN0cmluZztcbiAgICAvKipcbiAgICAgKiBBY2Nlc3MgdG9rZW4gbGlmZXRpbWUgaW4gc2Vjb25kc1xuICAgICAqL1xuICAgIGV4cGlyZXNJbjogbnVtYmVyO1xuICAgIC8qKlxuICAgICAqXG4gICAgICovXG4gICAgc2lnbmVkUmVxdWVzdDogc3RyaW5nO1xuICAgIC8qKlxuICAgICAqIFRoZSBGYWNlYm9vayB1c2VyIElEXG4gICAgICovXG4gICAgdXNlcklEOiBzdHJpbmc7XG4gICAgLyoqXG4gICAgICogVGhlIGdyYW50ZWQgc2NvcGVzLiBUaGlzIGZpZWxkIGlzIG9ubHkgYXZhaWxhYmxlIGlmIHlvdSBzZXQgYHJldHVybl9zY29wZXNgIHRvIHRydWUgd2hlbiBjYWxsaW5nIGxvZ2luIG1ldGhvZC5cbiAgICAgKi9cbiAgICBncmFudGVkU2NvcGVzPzogc3RyaW5nO1xufVxuIl19