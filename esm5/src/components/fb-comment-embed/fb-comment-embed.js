/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Embedded Comments
 * \@shortdesc Embedded comments component
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-comments
 * \@description
 * Embedded comments are a simple way to put public post comments - by a Page or a person on Facebook - into the content of your web site or web page.
 * Only public comments from Facebook Pages and profiles can be embedded.
 * \@usage
 * ```html
 * <fb-comment-embed href="https://www.facebook.com/zuck/posts/10102735452532991?comment_id=1070233703036185" width="500"></fb-comment-embed>
 * ```
 */
var FBCommentEmbedComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FBCommentEmbedComponent, _super);
    function FBCommentEmbedComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-comment-embed') || this;
    }
    FBCommentEmbedComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-comment-embed',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBCommentEmbedComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBCommentEmbedComponent.propDecorators = {
        "href": [{ type: Input },],
        "width": [{ type: Input },],
        "includeParent": [{ type: Input },],
    };
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBCommentEmbedComponent.prototype, "href", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBCommentEmbedComponent.prototype, "width", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBCommentEmbedComponent.prototype, "includeParent", void 0);
    return FBCommentEmbedComponent;
}(FBMLComponent));
export { FBCommentEmbedComponent };
function FBCommentEmbedComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBCommentEmbedComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBCommentEmbedComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBCommentEmbedComponent.propDecorators;
    /**
     * The absolute URL of the comment.
     * @type {?}
     */
    FBCommentEmbedComponent.prototype.href;
    /**
     * The width of the embedded comment container. Min. `220px`. Defaults to `560px`.
     * @type {?}
     */
    FBCommentEmbedComponent.prototype.width;
    /**
     * Set to `true` to include parent comment (if URL is a reply). Defaults to `false`.
     * @type {?}
     */
    FBCommentEmbedComponent.prototype.includeParent;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItY29tbWVudC1lbWJlZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2ZiLWNvbW1lbnQtZW1iZWQvZmItY29tbWVudC1lbWJlZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7SUFrQnBCLG1EQUFhO0lBdUJ4RCxpQ0FDRSxFQUFjLEVBQ2QsR0FBYTtlQUViLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsa0JBQWtCLENBQUM7S0FDbkM7O2dCQWhDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBbEIwQixVQUFVO2dCQUFFLFFBQVE7Ozt5QkF3QjVDLEtBQUs7MEJBT0wsS0FBSztrQ0FPTCxLQUFLOzs7UUFiTCxhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7OztrQ0F2Q2hCO0VBbUI2QyxhQUFhO1NBQTdDLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIEVtYmVkZGVkIENvbW1lbnRzXG4gKiBAc2hvcnRkZXNjIEVtYmVkZGVkIGNvbW1lbnRzIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2VtYmVkZGVkLWNvbW1lbnRzXG4gKiBAZGVzY3JpcHRpb25cbiAqIEVtYmVkZGVkIGNvbW1lbnRzIGFyZSBhIHNpbXBsZSB3YXkgdG8gcHV0IHB1YmxpYyBwb3N0IGNvbW1lbnRzIC0gYnkgYSBQYWdlIG9yIGEgcGVyc29uIG9uIEZhY2Vib29rIC0gaW50byB0aGUgY29udGVudCBvZiB5b3VyIHdlYiBzaXRlIG9yIHdlYiBwYWdlLlxuICogT25seSBwdWJsaWMgY29tbWVudHMgZnJvbSBGYWNlYm9vayBQYWdlcyBhbmQgcHJvZmlsZXMgY2FuIGJlIGVtYmVkZGVkLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItY29tbWVudC1lbWJlZCBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL3p1Y2svcG9zdHMvMTAxMDI3MzU0NTI1MzI5OTE/Y29tbWVudF9pZD0xMDcwMjMzNzAzMDM2MTg1XCIgd2lkdGg9XCI1MDBcIj48L2ZiLWNvbW1lbnQtZW1iZWQ+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItY29tbWVudC1lbWJlZCcsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQkNvbW1lbnRFbWJlZENvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBjb21tZW50LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIGVtYmVkZGVkIGNvbW1lbnQgY29udGFpbmVyLiBNaW4uIGAyMjBweGAuIERlZmF1bHRzIHRvIGA1NjBweGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTZXQgdG8gYHRydWVgIHRvIGluY2x1ZGUgcGFyZW50IGNvbW1lbnQgKGlmIFVSTCBpcyBhIHJlcGx5KS4gRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGluY2x1ZGVQYXJlbnQ6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItY29tbWVudC1lbWJlZCcpO1xuICB9XG5cbn1cbiJdfQ==