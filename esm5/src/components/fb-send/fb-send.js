/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Send Button
 * \@shortdesc Send button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/send-button
 * \@description
 * The Send button lets people privately send content on your site to one or more friends in a Facebook message.
 * \@usage
 * ```html
 * <fb-send href="https://github.com/zyra/ng2-facebook-sdk/"></fb-send>
 * ```
 */
var FBSendComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FBSendComponent, _super);
    function FBSendComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-send') || this;
        /**
         * The absolute URL of the page that will be sent. Defaults to the current URL.
         */
        _this.href = window.location.href;
        return _this;
    }
    FBSendComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-send',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBSendComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBSendComponent.propDecorators = {
        "colorScheme": [{ type: Input },],
        "href": [{ type: Input },],
        "kidDirectedSite": [{ type: Input },],
        "ref": [{ type: Input },],
        "size": [{ type: Input },],
    };
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBSendComponent.prototype, "colorScheme", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBSendComponent.prototype, "href", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBSendComponent.prototype, "kidDirectedSite", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBSendComponent.prototype, "ref", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBSendComponent.prototype, "size", void 0);
    return FBSendComponent;
}(FBMLComponent));
export { FBSendComponent };
function FBSendComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBSendComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBSendComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBSendComponent.propDecorators;
    /**
     * The color scheme used by the plugin. Can be "light" or "dark". Defaults to light.
     * @type {?}
     */
    FBSendComponent.prototype.colorScheme;
    /**
     * The absolute URL of the page that will be sent. Defaults to the current URL.
     * @type {?}
     */
    FBSendComponent.prototype.href;
    /**
     * If your web site or online service, or a portion of your service, is directed to children under 13 you must enable this.
     * @type {?}
     */
    FBSendComponent.prototype.kidDirectedSite;
    /**
     * A label for tracking referrals which must be less than 50 characters, and can contain alphanumeric characters and some punctuation (currently +/=-.:_).
     * @type {?}
     */
    FBSendComponent.prototype.ref;
    /**
     * Size of the button. Defaults to small.
     * @type {?}
     */
    FBSendComponent.prototype.size;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItc2VuZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1mYWNlYm9vay8iLCJzb3VyY2VzIjpbInNyYy9jb21wb25lbnRzL2ZiLXNlbmQvZmItc2VuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7OztJQWlCNUIsMkNBQWE7SUFxQ2hELHlCQUNFLEVBQWMsRUFDZCxHQUFhO1FBRmYsWUFJRSxrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQyxTQUMxQjs7OztxQkE1QmMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJOztLQTRCbEM7O2dCQTlDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFFBQVEsRUFBRSxFQUFFO2lCQUNiOzs7O2dCQWpCMEIsVUFBVTtnQkFBRSxRQUFROzs7Z0NBdUI1QyxLQUFLO3lCQU9MLEtBQUs7b0NBT0wsS0FBSzt3QkFPTCxLQUFLO3lCQU9MLEtBQUs7OztRQTNCTCxhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OzBCQXBEaEI7RUFrQnFDLGFBQWE7U0FBckMsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIFNlbmQgQnV0dG9uXG4gKiBAc2hvcnRkZXNjIFNlbmQgYnV0dG9uIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3NlbmQtYnV0dG9uXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBTZW5kIGJ1dHRvbiBsZXRzIHBlb3BsZSBwcml2YXRlbHkgc2VuZCBjb250ZW50IG9uIHlvdXIgc2l0ZSB0byBvbmUgb3IgbW9yZSBmcmllbmRzIGluIGEgRmFjZWJvb2sgbWVzc2FnZS5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLXNlbmQgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS96eXJhL25nMi1mYWNlYm9vay1zZGsvXCI+PC9mYi1zZW5kPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXNlbmQnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJTZW5kQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBzY2hlbWUgdXNlZCBieSB0aGUgcGx1Z2luLiBDYW4gYmUgXCJsaWdodFwiIG9yIFwiZGFya1wiLiBEZWZhdWx0cyB0byBsaWdodC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGNvbG9yU2NoZW1lOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHBhZ2UgdGhhdCB3aWxsIGJlIHNlbnQuIERlZmF1bHRzIHRvIHRoZSBjdXJyZW50IFVSTC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZyA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuXG4gIC8qKlxuICAgKiBJZiB5b3VyIHdlYiBzaXRlIG9yIG9ubGluZSBzZXJ2aWNlLCBvciBhIHBvcnRpb24gb2YgeW91ciBzZXJ2aWNlLCBpcyBkaXJlY3RlZCB0byBjaGlsZHJlbiB1bmRlciAxMyB5b3UgbXVzdCBlbmFibGUgdGhpcy5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGtpZERpcmVjdGVkU2l0ZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogQSBsYWJlbCBmb3IgdHJhY2tpbmcgcmVmZXJyYWxzIHdoaWNoIG11c3QgYmUgbGVzcyB0aGFuIDUwIGNoYXJhY3RlcnMsIGFuZCBjYW4gY29udGFpbiBhbHBoYW51bWVyaWMgY2hhcmFjdGVycyBhbmQgc29tZSBwdW5jdHVhdGlvbiAoY3VycmVudGx5ICsvPS0uOl8pLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgcmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNpemUgb2YgdGhlIGJ1dHRvbi4gRGVmYXVsdHMgdG8gc21hbGwuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaXplOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItc2VuZCcpO1xuICB9XG5cbn1cbiJdfQ==