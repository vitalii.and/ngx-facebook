/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Share Button
 * \@shortdesc Share button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/share-button
 * \@description
 * The Share button lets people add a personalized message to links before sharing on their timeline, in groups, or to their friends via a Facebook Message.
 * \@usage
 * ```html
 * <fb-share href="https://github.com/zyra/ng2-facebook-sdk/"></fb-share>
 * ```
 */
var FBShareComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FBShareComponent, _super);
    function FBShareComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-share-button') || this;
    }
    FBShareComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-share',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBShareComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBShareComponent.propDecorators = {
        "href": [{ type: Input },],
        "layout": [{ type: Input },],
        "mobileIframe": [{ type: Input },],
        "size": [{ type: Input },],
    };
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBShareComponent.prototype, "href", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBShareComponent.prototype, "layout", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBShareComponent.prototype, "mobileIframe", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBShareComponent.prototype, "size", void 0);
    return FBShareComponent;
}(FBMLComponent));
export { FBShareComponent };
function FBShareComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBShareComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBShareComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBShareComponent.propDecorators;
    /**
     * The absolute URL of the page that will be shared. Defaults to the current URL.
     * @type {?}
     */
    FBShareComponent.prototype.href;
    /**
     * Selects one of the different layouts that are available for the plugin. Can be one of `box_count`, `button_count`, `button`. Defaults to `icon_link`.
     * @type {?}
     */
    FBShareComponent.prototype.layout;
    /**
     * If set to true, the share button will open the share dialog in an iframe (instead of a popup) on top of your website on mobile. This option is only available for mobile, not desktop. Defaults to `false`.
     * @type {?}
     */
    FBShareComponent.prototype.mobileIframe;
    /**
     * The button is offered in 2 sizes i.e. large and small. Defaults to `small`.
     * @type {?}
     */
    FBShareComponent.prototype.size;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItc2hhcmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi1zaGFyZS9mYi1zaGFyZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7OztJQWlCM0IsNENBQWE7SUE4QmpELDBCQUNFLEVBQWMsRUFDZCxHQUFhO2VBRWIsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQztLQUNsQzs7Z0JBdkNGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsVUFBVTtvQkFDcEIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBakIwQixVQUFVO2dCQUFFLFFBQVE7Ozt5QkF1QjVDLEtBQUs7MkJBT0wsS0FBSztpQ0FPTCxLQUFLO3lCQU9MLEtBQUs7OztRQXBCTCxhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7MkJBN0NoQjtFQWtCc0MsYUFBYTtTQUF0QyxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBTaGFyZSBCdXR0b25cbiAqIEBzaG9ydGRlc2MgU2hhcmUgYnV0dG9uIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3NoYXJlLWJ1dHRvblxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgU2hhcmUgYnV0dG9uIGxldHMgcGVvcGxlIGFkZCBhIHBlcnNvbmFsaXplZCBtZXNzYWdlIHRvIGxpbmtzIGJlZm9yZSBzaGFyaW5nIG9uIHRoZWlyIHRpbWVsaW5lLCBpbiBncm91cHMsIG9yIHRvIHRoZWlyIGZyaWVuZHMgdmlhIGEgRmFjZWJvb2sgTWVzc2FnZS5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLXNoYXJlIGhyZWY9XCJodHRwczovL2dpdGh1Yi5jb20venlyYS9uZzItZmFjZWJvb2stc2RrL1wiPjwvZmItc2hhcmU+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItc2hhcmUnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJTaGFyZUNvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBwYWdlIHRoYXQgd2lsbCBiZSBzaGFyZWQuIERlZmF1bHRzIHRvIHRoZSBjdXJyZW50IFVSTC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZztcblxuICAvKipcbiAgICogU2VsZWN0cyBvbmUgb2YgdGhlIGRpZmZlcmVudCBsYXlvdXRzIHRoYXQgYXJlIGF2YWlsYWJsZSBmb3IgdGhlIHBsdWdpbi4gQ2FuIGJlIG9uZSBvZiBgYm94X2NvdW50YCwgYGJ1dHRvbl9jb3VudGAsIGBidXR0b25gLiBEZWZhdWx0cyB0byBgaWNvbl9saW5rYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGxheW91dDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBJZiBzZXQgdG8gdHJ1ZSwgdGhlIHNoYXJlIGJ1dHRvbiB3aWxsIG9wZW4gdGhlIHNoYXJlIGRpYWxvZyBpbiBhbiBpZnJhbWUgKGluc3RlYWQgb2YgYSBwb3B1cCkgb24gdG9wIG9mIHlvdXIgd2Vic2l0ZSBvbiBtb2JpbGUuIFRoaXMgb3B0aW9uIGlzIG9ubHkgYXZhaWxhYmxlIGZvciBtb2JpbGUsIG5vdCBkZXNrdG9wLiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbW9iaWxlSWZyYW1lOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBUaGUgYnV0dG9uIGlzIG9mZmVyZWQgaW4gMiBzaXplcyBpLmUuIGxhcmdlIGFuZCBzbWFsbC4gRGVmYXVsdHMgdG8gYHNtYWxsYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNpemU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1zaGFyZS1idXR0b24nKTtcbiAgfVxuXG59XG4iXX0=