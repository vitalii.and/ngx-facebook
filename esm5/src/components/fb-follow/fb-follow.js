/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Follow Button
 * \@shortdesc Follow button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/follow-button
 * \@description The Follow button lets people subscribe to the public updates of others on Facebook.
 * \@usage
 * ```html
 * <fb-follow href="https://www.facebook.com/zuck"></fb-follow>
 * ```
 */
var FBFollowComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FBFollowComponent, _super);
    function FBFollowComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-follow') || this;
    }
    FBFollowComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-follow',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBFollowComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBFollowComponent.propDecorators = {
        "colorScheme": [{ type: Input },],
        "href": [{ type: Input },],
        "kidDirectedSite": [{ type: Input },],
        "layout": [{ type: Input },],
        "showFaces": [{ type: Input },],
        "size": [{ type: Input },],
        "width": [{ type: Input },],
    };
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBFollowComponent.prototype, "colorScheme", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBFollowComponent.prototype, "href", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBFollowComponent.prototype, "kidDirectedSite", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBFollowComponent.prototype, "layout", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBFollowComponent.prototype, "showFaces", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBFollowComponent.prototype, "size", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBFollowComponent.prototype, "width", void 0);
    return FBFollowComponent;
}(FBMLComponent));
export { FBFollowComponent };
function FBFollowComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBFollowComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBFollowComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBFollowComponent.propDecorators;
    /**
     * The color scheme used by the plugin. Can be `light` or `dark`. Defaults to `light`.
     * @type {?}
     */
    FBFollowComponent.prototype.colorScheme;
    /**
     * The Facebook.com profile URL of the user to follow.
     * @type {?}
     */
    FBFollowComponent.prototype.href;
    /**
     * If your web site or online service, or a portion of your service, is directed to children under 13 you must enable this. Defaults to `false`.
     * @type {?}
     */
    FBFollowComponent.prototype.kidDirectedSite;
    /**
     * Selects one of the different layouts that are available for the plugin. Can be one of `standard`, `button_count`, or `box_count`.
     * Defaults to `standard`.
     * @type {?}
     */
    FBFollowComponent.prototype.layout;
    /**
     * Specifies whether to display profile photos below the button. Defaults to `false`.
     * @type {?}
     */
    FBFollowComponent.prototype.showFaces;
    /**
     * The button is offered in 2 sizes i.e. `large` and `small`. Defaults to `small`.
     * @type {?}
     */
    FBFollowComponent.prototype.size;
    /**
     * The width of the plugin. The layout you choose affects the minimum and default widths you can use.
     * @type {?}
     */
    FBFollowComponent.prototype.width;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItZm9sbG93LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWZhY2Vib29rLyIsInNvdXJjZXMiOlsic3JjL2NvbXBvbmVudHMvZmItZm9sbG93L2ZiLWZvbGxvdy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7O0lBZ0IxQiw2Q0FBYTtJQW9EbEQsMkJBQ0UsRUFBYyxFQUNkLEdBQWE7ZUFFYixrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQztLQUM1Qjs7Z0JBN0RGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsV0FBVztvQkFDckIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBaEIwQixVQUFVO2dCQUFFLFFBQVE7OztnQ0FzQjVDLEtBQUs7eUJBT0wsS0FBSztvQ0FPTCxLQUFLOzJCQVFMLEtBQUs7OEJBT0wsS0FBSzt5QkFPTCxLQUFLOzBCQU9MLEtBQUs7OztRQTFDTCxhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFRYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7NEJBbEVoQjtFQWlCdUMsYUFBYTtTQUF2QyxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBGb2xsb3cgQnV0dG9uXG4gKiBAc2hvcnRkZXNjIEZvbGxvdyBidXR0b24gY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZm9sbG93LWJ1dHRvblxuICogQGRlc2NyaXB0aW9uIFRoZSBGb2xsb3cgYnV0dG9uIGxldHMgcGVvcGxlIHN1YnNjcmliZSB0byB0aGUgcHVibGljIHVwZGF0ZXMgb2Ygb3RoZXJzIG9uIEZhY2Vib29rLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItZm9sbG93IGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20venVja1wiPjwvZmItZm9sbG93PlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLWZvbGxvdycsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQkZvbGxvd0NvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igc2NoZW1lIHVzZWQgYnkgdGhlIHBsdWdpbi4gQ2FuIGJlIGBsaWdodGAgb3IgYGRhcmtgLiBEZWZhdWx0cyB0byBgbGlnaHRgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgY29sb3JTY2hlbWU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIEZhY2Vib29rLmNvbSBwcm9maWxlIFVSTCBvZiB0aGUgdXNlciB0byBmb2xsb3cuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIElmIHlvdXIgd2ViIHNpdGUgb3Igb25saW5lIHNlcnZpY2UsIG9yIGEgcG9ydGlvbiBvZiB5b3VyIHNlcnZpY2UsIGlzIGRpcmVjdGVkIHRvIGNoaWxkcmVuIHVuZGVyIDEzIHlvdSBtdXN0IGVuYWJsZSB0aGlzLiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAga2lkRGlyZWN0ZWRTaXRlOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBTZWxlY3RzIG9uZSBvZiB0aGUgZGlmZmVyZW50IGxheW91dHMgdGhhdCBhcmUgYXZhaWxhYmxlIGZvciB0aGUgcGx1Z2luLiBDYW4gYmUgb25lIG9mIGBzdGFuZGFyZGAsIGBidXR0b25fY291bnRgLCBvciBgYm94X2NvdW50YC5cbiAgICogRGVmYXVsdHMgdG8gYHN0YW5kYXJkYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGxheW91dDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTcGVjaWZpZXMgd2hldGhlciB0byBkaXNwbGF5IHByb2ZpbGUgcGhvdG9zIGJlbG93IHRoZSBidXR0b24uIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93RmFjZXM6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGJ1dHRvbiBpcyBvZmZlcmVkIGluIDIgc2l6ZXMgaS5lLiBgbGFyZ2VgIGFuZCBgc21hbGxgLiBEZWZhdWx0cyB0byBgc21hbGxgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2l6ZTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIHBsdWdpbi4gVGhlIGxheW91dCB5b3UgY2hvb3NlIGFmZmVjdHMgdGhlIG1pbmltdW0gYW5kIGRlZmF1bHQgd2lkdGhzIHlvdSBjYW4gdXNlLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgd2lkdGg6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1mb2xsb3cnKTtcbiAgfVxuXG59XG4iXX0=