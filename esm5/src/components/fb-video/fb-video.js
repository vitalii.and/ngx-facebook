/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, ElementRef, Renderer, EventEmitter } from '@angular/core';
import { FBMLAttribute, FBMLComponent, FBMLInstanceMethod } from '../fbml-component';
/**
 * \@name Embedded Video
 * \@shortdesc Component to embed Facebook videos
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-video-player
 * \@description Component to embed Facebook videos and control them.
 * \@usage
 * ```html
 * <!-- basic usage -->
 * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/"></fb-video>
 *
 * <!-- event emitters -->
 * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/" (paused)="onVideoPaused($event)"></fb-video>
 * ```
 *
 * To manually interact with the video player, fetch it using `ViewChild`.
 *
 * ```ts
 * import { Component, ViewChild } from '\@angular/core';
 * import { FBVideoComponent } from 'ng2-facebook-sdk';
 *
 * \@Component(...)
 * export class MyComponent {
 *
 *   \@ViewChild(FBVideoComponent) video: FBVideoComponent;
 *
 *   ngAfterViewInit() {
 *     this.video.play();
 *     this.video.pause();
 *     this.video.getVolume();
 *   }
 *
 * }
 *
 * ```
 */
var FBVideoComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FBVideoComponent, _super);
    function FBVideoComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-video') || this;
        /**
         * Fired when video starts to play.
         */
        _this.startedPlaying = new EventEmitter();
        /**
         * Fired when video pauses.
         */
        _this.paused = new EventEmitter();
        /**
         *
         * Fired when video finishes playing.
         */
        _this.finishedPlaying = new EventEmitter();
        /**
         * Fired when video starts to buffer.
         */
        _this.startedBuffering = new EventEmitter();
        /**
         * Fired when video recovers from buffering.
         */
        _this.finishedBuffering = new EventEmitter();
        /**
         * Fired when an error occurs on the video.
         */
        _this.error = new EventEmitter();
        _this._listeners = [];
        _this.nativeElement.id = _this._id = 'video-' + String(Math.floor((Math.random() * 10000) + 1));
        return _this;
    }
    /**
     * @hidden
     */
    /**
     * @hidden
     * @return {?}
     */
    FBVideoComponent.prototype.ngOnInit = /**
     * @hidden
     * @return {?}
     */
    function () {
        var _this = this;
        FB.Event.subscribe('xfbml.ready', function (msg) {
            if (msg.type === 'video' && msg.id === _this._id) {
                _this._instance = msg.instance;
                _this._listeners.push(_this._instance.subscribe('startedPlaying', function (e) { return _this.startedPlaying.emit(e); }), _this._instance.subscribe('paused', function (e) { return _this.paused.emit(e); }), _this._instance.subscribe('finishedPlaying', function (e) { return _this.finishedPlaying.emit(e); }), _this._instance.subscribe('startedBuffering', function (e) { return _this.startedBuffering.emit(e); }), _this._instance.subscribe('finishedBuffering', function (e) { return _this.finishedBuffering.emit(e); }), _this._instance.subscribe('error', function (e) { return _this.error.emit(e); }));
            }
        });
    };
    /**
     * @hidden
     */
    /**
     * @hidden
     * @return {?}
     */
    FBVideoComponent.prototype.ngOnDestroy = /**
     * @hidden
     * @return {?}
     */
    function () {
        this._listeners.forEach(function (l) {
            if (typeof l.release === 'function') {
                l.release();
            }
        });
    };
    /**
     * Plays the video.
     */
    /**
     * Plays the video.
     * @return {?}
     */
    FBVideoComponent.prototype.play = /**
     * Plays the video.
     * @return {?}
     */
    function () { };
    /**
     * Pauses the video.
     */
    /**
     * Pauses the video.
     * @return {?}
     */
    FBVideoComponent.prototype.pause = /**
     * Pauses the video.
     * @return {?}
     */
    function () { };
    /**
     * Seeks to specified position.
     * @param seconds {number}
     */
    /**
     * Seeks to specified position.
     * @param {?} seconds {number}
     * @return {?}
     */
    FBVideoComponent.prototype.seek = /**
     * Seeks to specified position.
     * @param {?} seconds {number}
     * @return {?}
     */
    function (seconds) { };
    /**
     * Mute the video.
     */
    /**
     * Mute the video.
     * @return {?}
     */
    FBVideoComponent.prototype.mute = /**
     * Mute the video.
     * @return {?}
     */
    function () { };
    /**
     * Unmute the video.
     */
    /**
     * Unmute the video.
     * @return {?}
     */
    FBVideoComponent.prototype.unmute = /**
     * Unmute the video.
     * @return {?}
     */
    function () { };
    /**
     * Returns true if video is muted, false if not.
     */
    /**
     * Returns true if video is muted, false if not.
     * @return {?}
     */
    FBVideoComponent.prototype.isMuted = /**
     * Returns true if video is muted, false if not.
     * @return {?}
     */
    function () { return; };
    /**
     * Set the volume
     * @param volume
     */
    /**
     * Set the volume
     * @param {?} volume
     * @return {?}
     */
    FBVideoComponent.prototype.setVolume = /**
     * Set the volume
     * @param {?} volume
     * @return {?}
     */
    function (volume) { };
    /**
     * Get the volume
     */
    /**
     * Get the volume
     * @return {?}
     */
    FBVideoComponent.prototype.getVolume = /**
     * Get the volume
     * @return {?}
     */
    function () { return; };
    /**
     * Returns the current video time position in seconds
     */
    /**
     * Returns the current video time position in seconds
     * @return {?}
     */
    FBVideoComponent.prototype.getCurrentPosition = /**
     * Returns the current video time position in seconds
     * @return {?}
     */
    function () { };
    /**
     * Returns the video duration in seconds
     */
    /**
     * Returns the video duration in seconds
     * @return {?}
     */
    FBVideoComponent.prototype.getDuration = /**
     * Returns the video duration in seconds
     * @return {?}
     */
    function () { };
    FBVideoComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-video',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBVideoComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBVideoComponent.propDecorators = {
        "href": [{ type: Input },],
        "allowfullscreen": [{ type: Input },],
        "autoplay": [{ type: Input },],
        "width": [{ type: Input },],
        "showText": [{ type: Input },],
        "showCaptions": [{ type: Input },],
        "startedPlaying": [{ type: Output },],
        "paused": [{ type: Output },],
        "finishedPlaying": [{ type: Output },],
        "startedBuffering": [{ type: Output },],
        "finishedBuffering": [{ type: Output },],
        "error": [{ type: Output },],
    };
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBVideoComponent.prototype, "href", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "allowfullscreen", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "autoplay", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBVideoComponent.prototype, "width", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "showText", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "showCaptions", void 0);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "play", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "pause", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Number]),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "seek", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "mute", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "unmute", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", Boolean)
    ], FBVideoComponent.prototype, "isMuted", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Number]),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "setVolume", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", Number)
    ], FBVideoComponent.prototype, "getVolume", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "getCurrentPosition", null);
    tslib_1.__decorate([
        FBMLInstanceMethod,
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "getDuration", null);
    return FBVideoComponent;
}(FBMLComponent));
export { FBVideoComponent };
function FBVideoComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBVideoComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBVideoComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBVideoComponent.propDecorators;
    /** @type {?} */
    FBVideoComponent.prototype._instance;
    /**
     * The absolute URL of the video.
     * @type {?}
     */
    FBVideoComponent.prototype.href;
    /**
     * Allow the video to be played in fullscreen mode. Can be false or true. Defaults to false;
     * @type {?}
     */
    FBVideoComponent.prototype.allowfullscreen;
    /**
     * Automatically start playing the video when the page loads. The video will be played without sound (muted). People can turn on sound via the video player controls. This setting does not apply to mobile devices. Can be false or true. Defaults to false.
     * @type {?}
     */
    FBVideoComponent.prototype.autoplay;
    /**
     * The width of the video container. Min. 220px.
     * @type {?}
     */
    FBVideoComponent.prototype.width;
    /**
     * Set to true to include the text from the Facebook post associated with the video, if any.
     * @type {?}
     */
    FBVideoComponent.prototype.showText;
    /**
     * Set to true to show captions (if available) by default. Captions are only available on desktop.
     * @type {?}
     */
    FBVideoComponent.prototype.showCaptions;
    /**
     * Fired when video starts to play.
     * @type {?}
     */
    FBVideoComponent.prototype.startedPlaying;
    /**
     * Fired when video pauses.
     * @type {?}
     */
    FBVideoComponent.prototype.paused;
    /**
     *
     * Fired when video finishes playing.
     * @type {?}
     */
    FBVideoComponent.prototype.finishedPlaying;
    /**
     * Fired when video starts to buffer.
     * @type {?}
     */
    FBVideoComponent.prototype.startedBuffering;
    /**
     * Fired when video recovers from buffering.
     * @type {?}
     */
    FBVideoComponent.prototype.finishedBuffering;
    /**
     * Fired when an error occurs on the video.
     * @type {?}
     */
    FBVideoComponent.prototype.error;
    /** @type {?} */
    FBVideoComponent.prototype._id;
    /** @type {?} */
    FBVideoComponent.prototype._listeners;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItdmlkZW8uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi12aWRlby9mYi12aWRlby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFxQixZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEgsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQTBDL0MsNENBQWE7SUF1RmpELDBCQUNFLEVBQWMsRUFDZCxHQUFhO1FBRmYsWUFJRSxrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxTQUUzQjs7OzsrQkEzQ21DLElBQUksWUFBWSxFQUFPOzs7O3VCQU0vQixJQUFJLFlBQVksRUFBTzs7Ozs7Z0NBT2QsSUFBSSxZQUFZLEVBQU87Ozs7aUNBTXRCLElBQUksWUFBWSxFQUFPOzs7O2tDQU10QixJQUFJLFlBQVksRUFBTzs7OztzQkFNbkMsSUFBSSxZQUFZLEVBQU87MkJBSXRCLEVBQUU7UUFPNUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsS0FBSSxDQUFDLEdBQUcsR0FBRyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7S0FDL0Y7SUFFRDs7T0FFRzs7Ozs7SUFDSCxtQ0FBUTs7OztJQUFSO1FBQUEsaUJBY0M7UUFiQyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsVUFBQyxHQUFRO1lBQ3pDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssT0FBTyxJQUFJLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELEtBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQztnQkFDOUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ2xCLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLFVBQUMsQ0FBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQTNCLENBQTJCLENBQUMsRUFDbkYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLFVBQUMsQ0FBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQW5CLENBQW1CLENBQUMsRUFDbkUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsVUFBQyxDQUFNLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBNUIsQ0FBNEIsQ0FBQyxFQUNyRixLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsRUFBRSxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQTdCLENBQTZCLENBQUMsRUFDdkYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLEVBQUUsVUFBQyxDQUFNLElBQUssT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUE5QixDQUE4QixDQUFDLEVBQ3pGLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFsQixDQUFrQixDQUFDLENBQ2xFLENBQUM7YUFDSDtTQUNGLENBQUMsQ0FBQztLQUNKO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsc0NBQVc7Ozs7SUFBWDtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQztZQUN2QixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDcEMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ2I7U0FDRixDQUFDLENBQUM7S0FDSjtJQUVEOztPQUVHOzs7OztJQUVILCtCQUFJOzs7O21CQUFLO0lBRVQ7O09BRUc7Ozs7O0lBRUgsZ0NBQUs7Ozs7bUJBQUs7SUFFVjs7O09BR0c7Ozs7OztJQUVILCtCQUFJOzs7OztjQUFDLE9BQWUsS0FBSTtJQUV4Qjs7T0FFRzs7Ozs7SUFFSCwrQkFBSTs7OzttQkFBSztJQUVUOztPQUVHOzs7OztJQUVILGlDQUFNOzs7O21CQUFLO0lBRVg7O09BRUc7Ozs7O0lBRUgsa0NBQU87Ozs7a0JBQWMsTUFBTSxDQUFDLEVBQUU7SUFFOUI7OztPQUdHOzs7Ozs7SUFFSCxvQ0FBUzs7Ozs7Y0FBQyxNQUFjLEtBQUk7SUFFNUI7O09BRUc7Ozs7O0lBRUgsb0NBQVM7Ozs7a0JBQWEsTUFBTSxDQUFDLEVBQUU7SUFFL0I7O09BRUc7Ozs7O0lBRUgsNkNBQWtCOzs7O21CQUFLO0lBRXZCOztPQUVHOzs7OztJQUVILHNDQUFXOzs7O21CQUFLOztnQkE3TGpCLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsVUFBVTtvQkFDcEIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBMUNrQyxVQUFVO2dCQUFFLFFBQVE7Ozt5QkFrRHBELEtBQUs7b0NBT0wsS0FBSzs2QkFPTCxLQUFLOzBCQU9MLEtBQUs7NkJBT0wsS0FBSztpQ0FPTCxLQUFLO21DQU9MLE1BQU07MkJBTU4sTUFBTTtvQ0FPTixNQUFNO3FDQU1OLE1BQU07c0NBTU4sTUFBTTswQkFNTixNQUFNOzs7UUF4RU4sYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBcUZiLGtCQUFrQjs7OztnREFDVjs7UUFLUixrQkFBa0I7Ozs7aURBQ1Q7O1FBTVQsa0JBQWtCOzs7O2dEQUNLOztRQUt2QixrQkFBa0I7Ozs7Z0RBQ1Y7O1FBS1Isa0JBQWtCOzs7O2tEQUNSOztRQUtWLGtCQUFrQjs7OzttREFDVzs7UUFNN0Isa0JBQWtCOzs7O3FEQUNTOztRQUszQixrQkFBa0I7Ozs7cURBQ1k7O1FBSzlCLGtCQUFrQjs7Ozs4REFDSTs7UUFLdEIsa0JBQWtCOzs7O3VEQUNIOzJCQXBPbEI7RUEyQ3NDLGFBQWE7U0FBdEMsZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciwgT25Jbml0LCBPbkRlc3Ryb3ksIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCwgRkJNTEluc3RhbmNlTWV0aG9kIH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuZGVjbGFyZSB2YXIgRkI6IGFueTtcblxuLyoqXG4gKiBAbmFtZSBFbWJlZGRlZCBWaWRlb1xuICogQHNob3J0ZGVzYyBDb21wb25lbnQgdG8gZW1iZWQgRmFjZWJvb2sgdmlkZW9zXG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZW1iZWRkZWQtdmlkZW8tcGxheWVyXG4gKiBAZGVzY3JpcHRpb24gQ29tcG9uZW50IHRvIGVtYmVkIEZhY2Vib29rIHZpZGVvcyBhbmQgY29udHJvbCB0aGVtLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8IS0tIGJhc2ljIHVzYWdlIC0tPlxuICogPGZiLXZpZGVvIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vZmFjZWJvb2svdmlkZW9zLzEwMTUzMjMxMzc5OTQ2NzI5L1wiPjwvZmItdmlkZW8+XG4gKlxuICogPCEtLSBldmVudCBlbWl0dGVycyAtLT5cbiAqIDxmYi12aWRlbyBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL2ZhY2Vib29rL3ZpZGVvcy8xMDE1MzIzMTM3OTk0NjcyOS9cIiAocGF1c2VkKT1cIm9uVmlkZW9QYXVzZWQoJGV2ZW50KVwiPjwvZmItdmlkZW8+XG4gKiBgYGBcbiAqXG4gKiBUbyBtYW51YWxseSBpbnRlcmFjdCB3aXRoIHRoZSB2aWRlbyBwbGF5ZXIsIGZldGNoIGl0IHVzaW5nIGBWaWV3Q2hpbGRgLlxuICpcbiAqIGBgYHRzXG4gKiBpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuICogaW1wb3J0IHsgRkJWaWRlb0NvbXBvbmVudCB9IGZyb20gJ25nMi1mYWNlYm9vay1zZGsnO1xuICpcbiAqIEBDb21wb25lbnQoLi4uKVxuICogZXhwb3J0IGNsYXNzIE15Q29tcG9uZW50IHtcbiAqXG4gKiAgIEBWaWV3Q2hpbGQoRkJWaWRlb0NvbXBvbmVudCkgdmlkZW86IEZCVmlkZW9Db21wb25lbnQ7XG4gKlxuICogICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gKiAgICAgdGhpcy52aWRlby5wbGF5KCk7XG4gKiAgICAgdGhpcy52aWRlby5wYXVzZSgpO1xuICogICAgIHRoaXMudmlkZW8uZ2V0Vm9sdW1lKCk7XG4gKiAgIH1cbiAqXG4gKiB9XG4gKlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXZpZGVvJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCVmlkZW9Db21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG4gIHByaXZhdGUgX2luc3RhbmNlOiBhbnk7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHZpZGVvLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBBbGxvdyB0aGUgdmlkZW8gdG8gYmUgcGxheWVkIGluIGZ1bGxzY3JlZW4gbW9kZS4gQ2FuIGJlIGZhbHNlIG9yIHRydWUuIERlZmF1bHRzIHRvIGZhbHNlO1xuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgYWxsb3dmdWxsc2NyZWVuOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBBdXRvbWF0aWNhbGx5IHN0YXJ0IHBsYXlpbmcgdGhlIHZpZGVvIHdoZW4gdGhlIHBhZ2UgbG9hZHMuIFRoZSB2aWRlbyB3aWxsIGJlIHBsYXllZCB3aXRob3V0IHNvdW5kIChtdXRlZCkuIFBlb3BsZSBjYW4gdHVybiBvbiBzb3VuZCB2aWEgdGhlIHZpZGVvIHBsYXllciBjb250cm9scy4gVGhpcyBzZXR0aW5nIGRvZXMgbm90IGFwcGx5IHRvIG1vYmlsZSBkZXZpY2VzLiBDYW4gYmUgZmFsc2Ugb3IgdHJ1ZS4gRGVmYXVsdHMgdG8gZmFsc2UuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBhdXRvcGxheTogYm9vbGVhbjtcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSB2aWRlbyBjb250YWluZXIuIE1pbi4gMjIwcHguXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTZXQgdG8gdHJ1ZSB0byBpbmNsdWRlIHRoZSB0ZXh0IGZyb20gdGhlIEZhY2Vib29rIHBvc3QgYXNzb2NpYXRlZCB3aXRoIHRoZSB2aWRlbywgaWYgYW55LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hvd1RleHQ6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFNldCB0byB0cnVlIHRvIHNob3cgY2FwdGlvbnMgKGlmIGF2YWlsYWJsZSkgYnkgZGVmYXVsdC4gQ2FwdGlvbnMgYXJlIG9ubHkgYXZhaWxhYmxlIG9uIGRlc2t0b3AuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93Q2FwdGlvbnM6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gc3RhcnRzIHRvIHBsYXkuXG4gICAqL1xuICBAT3V0cHV0KClcbiAgc3RhcnRlZFBsYXlpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gcGF1c2VzLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIHBhdXNlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAvKipcbiAgICpcbiAgIEZpcmVkIHdoZW4gdmlkZW8gZmluaXNoZXMgcGxheWluZy5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBmaW5pc2hlZFBsYXlpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gc3RhcnRzIHRvIGJ1ZmZlci5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBzdGFydGVkQnVmZmVyaW5nOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIHZpZGVvIHJlY292ZXJzIGZyb20gYnVmZmVyaW5nLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIGZpbmlzaGVkQnVmZmVyaW5nOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIGFuIGVycm9yIG9jY3VycyBvbiB0aGUgdmlkZW8uXG4gICAqL1xuICBAT3V0cHV0KClcbiAgZXJyb3I6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgcHJpdmF0ZSBfaWQ6IHN0cmluZztcblxuICBwcml2YXRlIF9saXN0ZW5lcnM6IGFueVtdID0gW107XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItdmlkZW8nKTtcbiAgICB0aGlzLm5hdGl2ZUVsZW1lbnQuaWQgPSB0aGlzLl9pZCA9ICd2aWRlby0nICsgU3RyaW5nKE1hdGguZmxvb3IoKE1hdGgucmFuZG9tKCkgKiAxMDAwMCkgKyAxKSk7XG4gIH1cblxuICAvKipcbiAgICogQGhpZGRlblxuICAgKi9cbiAgbmdPbkluaXQoKSB7XG4gICAgRkIuRXZlbnQuc3Vic2NyaWJlKCd4ZmJtbC5yZWFkeScsIChtc2c6IGFueSkgPT4ge1xuICAgICAgaWYgKG1zZy50eXBlID09PSAndmlkZW8nICYmIG1zZy5pZCA9PT0gdGhpcy5faWQpIHtcbiAgICAgICAgdGhpcy5faW5zdGFuY2UgPSBtc2cuaW5zdGFuY2U7XG4gICAgICAgIHRoaXMuX2xpc3RlbmVycy5wdXNoKFxuICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnN1YnNjcmliZSgnc3RhcnRlZFBsYXlpbmcnLCAoZTogYW55KSA9PiB0aGlzLnN0YXJ0ZWRQbGF5aW5nLmVtaXQoZSkpLFxuICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnN1YnNjcmliZSgncGF1c2VkJywgKGU6IGFueSkgPT4gdGhpcy5wYXVzZWQuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdmaW5pc2hlZFBsYXlpbmcnLCAoZTogYW55KSA9PiB0aGlzLmZpbmlzaGVkUGxheWluZy5lbWl0KGUpKSxcbiAgICAgICAgICB0aGlzLl9pbnN0YW5jZS5zdWJzY3JpYmUoJ3N0YXJ0ZWRCdWZmZXJpbmcnLCAoZTogYW55KSA9PiB0aGlzLnN0YXJ0ZWRCdWZmZXJpbmcuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdmaW5pc2hlZEJ1ZmZlcmluZycsIChlOiBhbnkpID0+IHRoaXMuZmluaXNoZWRCdWZmZXJpbmcuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdlcnJvcicsIChlOiBhbnkpID0+IHRoaXMuZXJyb3IuZW1pdChlKSlcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAaGlkZGVuXG4gICAqL1xuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLl9saXN0ZW5lcnMuZm9yRWFjaChsID0+IHtcbiAgICAgIGlmICh0eXBlb2YgbC5yZWxlYXNlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGwucmVsZWFzZSgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFBsYXlzIHRoZSB2aWRlby5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgcGxheSgpIHt9XG5cbiAgLyoqXG4gICAqIFBhdXNlcyB0aGUgdmlkZW8uXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIHBhdXNlKCkge31cblxuICAvKipcbiAgICogU2Vla3MgdG8gc3BlY2lmaWVkIHBvc2l0aW9uLlxuICAgKiBAcGFyYW0gc2Vjb25kcyB7bnVtYmVyfVxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBzZWVrKHNlY29uZHM6IG51bWJlcikge31cblxuICAvKipcbiAgICogTXV0ZSB0aGUgdmlkZW8uXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIG11dGUoKSB7fVxuXG4gIC8qKlxuICAgKiBVbm11dGUgdGhlIHZpZGVvLlxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICB1bm11dGUoKSB7fVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRydWUgaWYgdmlkZW8gaXMgbXV0ZWQsIGZhbHNlIGlmIG5vdC5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgaXNNdXRlZCgpOiBib29sZWFuIHsgcmV0dXJuOyB9XG5cbiAgLyoqXG4gICAqIFNldCB0aGUgdm9sdW1lXG4gICAqIEBwYXJhbSB2b2x1bWVcbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgc2V0Vm9sdW1lKHZvbHVtZTogbnVtYmVyKSB7fVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIHZvbHVtZVxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBnZXRWb2x1bWUoKTogbnVtYmVyIHsgcmV0dXJuOyB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIGN1cnJlbnQgdmlkZW8gdGltZSBwb3NpdGlvbiBpbiBzZWNvbmRzXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIGdldEN1cnJlbnRQb3NpdGlvbigpIHt9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIHZpZGVvIGR1cmF0aW9uIGluIHNlY29uZHNcbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgZ2V0RHVyYXRpb24oKSB7fVxuXG59XG4iXX0=