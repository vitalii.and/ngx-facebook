/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Comments
 * \@shortdesc Comments component
 * \@fbdoc https://developers.facebook.com/docs/plugins/comments
 * \@description
 * The comments plugin lets people comment on content on your site using their Facebook account.
 * People can choose to share their comment activity with their friends (and friends of their friends) on Facebook as well.
 * The comments plugin also includes built-in moderation tools and social relevance ranking.
 *
 * \@usage
 * ```html
 * <fb-comments></fb-comments>
 * ```
 */
var FBCommentsComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FBCommentsComponent, _super);
    function FBCommentsComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-comments') || this;
        /**
         * The absolute URL that comments posted in the plugin will be permanently associated with.
         * All stories shared on Facebook about comments posted using the comments plugin will link to this URL.
         * Defaults to current URL.
         */
        _this.href = window.location.href;
        return _this;
    }
    FBCommentsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-comments',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBCommentsComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBCommentsComponent.propDecorators = {
        "colorscheme": [{ type: Input },],
        "href": [{ type: Input },],
        "mobile": [{ type: Input },],
        "numposts": [{ type: Input },],
        "orderBy": [{ type: Input },],
        "width": [{ type: Input },],
    };
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBCommentsComponent.prototype, "colorscheme", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBCommentsComponent.prototype, "href", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Boolean)
    ], FBCommentsComponent.prototype, "mobile", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", Number)
    ], FBCommentsComponent.prototype, "numposts", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBCommentsComponent.prototype, "orderBy", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBCommentsComponent.prototype, "width", void 0);
    return FBCommentsComponent;
}(FBMLComponent));
export { FBCommentsComponent };
function FBCommentsComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBCommentsComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBCommentsComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBCommentsComponent.propDecorators;
    /**
     * The color scheme used by the comments plugin. Can be `light` or `dark`. Defaults to `light`.
     * @type {?}
     */
    FBCommentsComponent.prototype.colorscheme;
    /**
     * The absolute URL that comments posted in the plugin will be permanently associated with.
     * All stories shared on Facebook about comments posted using the comments plugin will link to this URL.
     * Defaults to current URL.
     * @type {?}
     */
    FBCommentsComponent.prototype.href;
    /**
     * A boolean value that specifies whether to show the mobile-optimized version or not. If no value is given, it will be automatically detected.
     * @type {?}
     */
    FBCommentsComponent.prototype.mobile;
    /**
     * The number of comments to show by default. The minimum value is `1`. Defaults to `10`.
     * @type {?}
     */
    FBCommentsComponent.prototype.numposts;
    /**
     * The order to use when displaying comments. Can be `social`, `reverse_time`, or `time`. The different order types are explained [in the FAQ](https://developers.facebook.com/docs/plugins/comments#faqorder). Defaults to `social`
     * @type {?}
     */
    FBCommentsComponent.prototype.orderBy;
    /**
     * The width of the comments plugin on the webpage.
     * This can be either a pixel value or a percentage (such as 100%) for fluid width.
     * The mobile version of the comments plugin ignores the width parameter and instead has a fluid width of 100%.
     * The minimum width supported by the comments plugin is 320px.
     * Defaults to `550px`.
     * @type {?}
     */
    FBCommentsComponent.prototype.width;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItY29tbWVudHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi1jb21tZW50cy9mYi1jb21tZW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztJQW9CeEIsK0NBQWE7SUFrRHBELDZCQUNFLEVBQWMsRUFDZCxHQUFhO1FBRmYsWUFJRSxrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLGFBQWEsQ0FBQyxTQUM5Qjs7Ozs7O3FCQXZDYyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUk7O0tBdUNsQzs7Z0JBM0RGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBcEIwQixVQUFVO2dCQUFFLFFBQVE7OztnQ0EwQjVDLEtBQUs7eUJBU0wsS0FBSzsyQkFPTCxLQUFLOzZCQU9MLEtBQUs7NEJBT0wsS0FBSzswQkFXTCxLQUFLOzs7UUF4Q0wsYUFBYTs7OztRQVNiLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFXYixhQUFhOzs7OEJBcEVoQjtFQXFCeUMsYUFBYTtTQUF6QyxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBDb21tZW50c1xuICogQHNob3J0ZGVzYyBDb21tZW50cyBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9jb21tZW50c1xuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgY29tbWVudHMgcGx1Z2luIGxldHMgcGVvcGxlIGNvbW1lbnQgb24gY29udGVudCBvbiB5b3VyIHNpdGUgdXNpbmcgdGhlaXIgRmFjZWJvb2sgYWNjb3VudC5cbiAqIFBlb3BsZSBjYW4gY2hvb3NlIHRvIHNoYXJlIHRoZWlyIGNvbW1lbnQgYWN0aXZpdHkgd2l0aCB0aGVpciBmcmllbmRzIChhbmQgZnJpZW5kcyBvZiB0aGVpciBmcmllbmRzKSBvbiBGYWNlYm9vayBhcyB3ZWxsLlxuICogVGhlIGNvbW1lbnRzIHBsdWdpbiBhbHNvIGluY2x1ZGVzIGJ1aWx0LWluIG1vZGVyYXRpb24gdG9vbHMgYW5kIHNvY2lhbCByZWxldmFuY2UgcmFua2luZy5cbiAqXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1jb21tZW50cz48L2ZiLWNvbW1lbnRzPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLWNvbW1lbnRzJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCQ29tbWVudHNDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGNvbG9yIHNjaGVtZSB1c2VkIGJ5IHRoZSBjb21tZW50cyBwbHVnaW4uIENhbiBiZSBgbGlnaHRgIG9yIGBkYXJrYC4gRGVmYXVsdHMgdG8gYGxpZ2h0YC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGNvbG9yc2NoZW1lOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgdGhhdCBjb21tZW50cyBwb3N0ZWQgaW4gdGhlIHBsdWdpbiB3aWxsIGJlIHBlcm1hbmVudGx5IGFzc29jaWF0ZWQgd2l0aC5cbiAgICogQWxsIHN0b3JpZXMgc2hhcmVkIG9uIEZhY2Vib29rIGFib3V0IGNvbW1lbnRzIHBvc3RlZCB1c2luZyB0aGUgY29tbWVudHMgcGx1Z2luIHdpbGwgbGluayB0byB0aGlzIFVSTC5cbiAgICogRGVmYXVsdHMgdG8gY3VycmVudCBVUkwuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmcgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcblxuICAvKipcbiAgICogQSBib29sZWFuIHZhbHVlIHRoYXQgc3BlY2lmaWVzIHdoZXRoZXIgdG8gc2hvdyB0aGUgbW9iaWxlLW9wdGltaXplZCB2ZXJzaW9uIG9yIG5vdC4gSWYgbm8gdmFsdWUgaXMgZ2l2ZW4sIGl0IHdpbGwgYmUgYXV0b21hdGljYWxseSBkZXRlY3RlZC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIG1vYmlsZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogVGhlIG51bWJlciBvZiBjb21tZW50cyB0byBzaG93IGJ5IGRlZmF1bHQuIFRoZSBtaW5pbXVtIHZhbHVlIGlzIGAxYC4gRGVmYXVsdHMgdG8gYDEwYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIG51bXBvc3RzOiBudW1iZXI7XG5cbiAgLyoqXG4gICAqIFRoZSBvcmRlciB0byB1c2Ugd2hlbiBkaXNwbGF5aW5nIGNvbW1lbnRzLiBDYW4gYmUgYHNvY2lhbGAsIGByZXZlcnNlX3RpbWVgLCBvciBgdGltZWAuIFRoZSBkaWZmZXJlbnQgb3JkZXIgdHlwZXMgYXJlIGV4cGxhaW5lZCBbaW4gdGhlIEZBUV0oaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvY29tbWVudHMjZmFxb3JkZXIpLiBEZWZhdWx0cyB0byBgc29jaWFsYFxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgb3JkZXJCeTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIGNvbW1lbnRzIHBsdWdpbiBvbiB0aGUgd2VicGFnZS5cbiAgICogVGhpcyBjYW4gYmUgZWl0aGVyIGEgcGl4ZWwgdmFsdWUgb3IgYSBwZXJjZW50YWdlIChzdWNoIGFzIDEwMCUpIGZvciBmbHVpZCB3aWR0aC5cbiAgICogVGhlIG1vYmlsZSB2ZXJzaW9uIG9mIHRoZSBjb21tZW50cyBwbHVnaW4gaWdub3JlcyB0aGUgd2lkdGggcGFyYW1ldGVyIGFuZCBpbnN0ZWFkIGhhcyBhIGZsdWlkIHdpZHRoIG9mIDEwMCUuXG4gICAqIFRoZSBtaW5pbXVtIHdpZHRoIHN1cHBvcnRlZCBieSB0aGUgY29tbWVudHMgcGx1Z2luIGlzIDMyMHB4LlxuICAgKiBEZWZhdWx0cyB0byBgNTUwcHhgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgd2lkdGg6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1jb21tZW50cycpO1xuICB9XG5cbn1cbiJdfQ==