/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { FBMLAttribute, FBMLComponent } from '../fbml-component';
/**
 * \@name Quote Plugin
 * \@shortdesc Quote plugin component
 * \@fbdoc https://developers.facebook.com/docs/plugins/quote
 * \@description
 * The quote plugin lets people select text on your page and add it to their share, so they can tell a more expressive story.
 * Note that you do not need to implement Facebook login or request any additional permissions through app review in order to use this plugin.
 * \@usage
 * ```html
 * <fb-quote></fb-quote>
 * ```
 */
var FBQuoteComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FBQuoteComponent, _super);
    function FBQuoteComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-quote') || this;
    }
    FBQuoteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-quote',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBQuoteComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBQuoteComponent.propDecorators = {
        "href": [{ type: Input },],
        "layout": [{ type: Input },],
    };
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBQuoteComponent.prototype, "href", void 0);
    tslib_1.__decorate([
        FBMLAttribute,
        tslib_1.__metadata("design:type", String)
    ], FBQuoteComponent.prototype, "layout", void 0);
    return FBQuoteComponent;
}(FBMLComponent));
export { FBQuoteComponent };
function FBQuoteComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FBQuoteComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FBQuoteComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    FBQuoteComponent.propDecorators;
    /**
     * The absolute URL of the page that will be quoted.
     * Defaults to the current URL
     * @type {?}
     */
    FBQuoteComponent.prototype.href;
    /**
     * Can be set to quote or button. Defaults to quote.
     * @type {?}
     */
    FBQuoteComponent.prototype.layout;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmItcXVvdGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYi1xdW90ZS9mYi1xdW90ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7SUFrQjNCLDRDQUFhO0lBaUJqRCwwQkFDRSxFQUFjLEVBQ2QsR0FBYTtlQUViLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDO0tBQzNCOztnQkExQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxVQUFVO29CQUNwQixRQUFRLEVBQUUsRUFBRTtpQkFDYjs7OztnQkFsQjBCLFVBQVU7Z0JBQUUsUUFBUTs7O3lCQXlCNUMsS0FBSzsyQkFPTCxLQUFLOzs7UUFOTCxhQUFhOzs7O1FBT2IsYUFBYTs7OzJCQWpDaEI7RUFtQnNDLGFBQWE7U0FBdEMsZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgUXVvdGUgUGx1Z2luXG4gKiBAc2hvcnRkZXNjIFF1b3RlIHBsdWdpbiBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9xdW90ZVxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgcXVvdGUgcGx1Z2luIGxldHMgcGVvcGxlIHNlbGVjdCB0ZXh0IG9uIHlvdXIgcGFnZSBhbmQgYWRkIGl0IHRvIHRoZWlyIHNoYXJlLCBzbyB0aGV5IGNhbiB0ZWxsIGEgbW9yZSBleHByZXNzaXZlIHN0b3J5LlxuICogTm90ZSB0aGF0IHlvdSBkbyBub3QgbmVlZCB0byBpbXBsZW1lbnQgRmFjZWJvb2sgbG9naW4gb3IgcmVxdWVzdCBhbnkgYWRkaXRpb25hbCBwZXJtaXNzaW9ucyB0aHJvdWdoIGFwcCByZXZpZXcgaW4gb3JkZXIgdG8gdXNlIHRoaXMgcGx1Z2luLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItcXVvdGU+PC9mYi1xdW90ZT5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1xdW90ZScsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQlF1b3RlQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHBhZ2UgdGhhdCB3aWxsIGJlIHF1b3RlZC5cbiAgICogRGVmYXVsdHMgdG8gdGhlIGN1cnJlbnQgVVJMXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIENhbiBiZSBzZXQgdG8gcXVvdGUgb3IgYnV0dG9uLiBEZWZhdWx0cyB0byBxdW90ZS5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGxheW91dDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLXF1b3RlJyk7XG4gIH1cblxufVxuIl19