/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @hidden
 * @param {?} target
 * @param {?} key
 * @return {?}
 */
export function FBMLAttribute(target, key) {
    var /** @type {?} */ processKey = function (_k) { return 'data-' + _k.toString().replace(/([a-z\d])([A-Z])/g, '$1-$2').toLowerCase(); };
    Object.defineProperty(target, key, {
        set: function (value) {
            value = value.toString();
            this.setAttribute(processKey(key), value);
        },
        get: function () {
            return this.getAttribute(processKey(key));
        },
        enumerable: true
    });
}
var ɵ0 = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    if (this._instance) {
        return this._instance[key].apply(this._instance, args);
    }
    else {
        console.warn('ng2-facebook-sdk: tried calling instance method before component is ready.');
        return null;
    }
};
/**
 * @hidden
 * @param {?} target
 * @param {?} key
 * @return {?}
 */
export function FBMLInstanceMethod(target, key) {
    return {
        enumerable: true,
        value: ɵ0
    };
}
/**
 * @hidden
 */
var /**
 * @hidden
 */
FBMLComponent = /** @class */ (function () {
    function FBMLComponent(el, rnd, fbClass) {
        this.el = el;
        this.rnd = rnd;
        this.fbClass = fbClass;
        this.nativeElement = this.el.nativeElement;
        this.rnd.setElementClass(this.nativeElement, this.fbClass, true);
    }
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    FBMLComponent.prototype.setAttribute = /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    function (name, value) {
        if (!name || !value)
            return;
        this.rnd.setElementAttribute(this.nativeElement, name, value);
    };
    /**
     * @param {?} name
     * @return {?}
     */
    FBMLComponent.prototype.getAttribute = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        if (!name)
            return;
        return this.nativeElement.getAttribute(name);
    };
    return FBMLComponent;
}());
/**
 * @hidden
 */
export { FBMLComponent };
function FBMLComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    FBMLComponent.prototype.nativeElement;
    /** @type {?} */
    FBMLComponent.prototype.el;
    /** @type {?} */
    FBMLComponent.prototype.rnd;
    /** @type {?} */
    FBMLComponent.prototype.fbClass;
}
export { ɵ0 };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmJtbC1jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvY29tcG9uZW50cy9mYm1sLWNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBS0EsTUFBTSx3QkFBd0IsTUFBVyxFQUFFLEdBQVc7SUFDcEQscUJBQU0sVUFBVSxHQUFHLFVBQUMsRUFBVSxJQUFLLE9BQUEsT0FBTyxHQUFHLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEVBQUUsT0FBTyxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQTNFLENBQTJFLENBQUM7SUFDL0csTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFO1FBQ2pDLEdBQUcsRUFBRSxVQUFTLEtBQUs7WUFDakIsS0FBSyxHQUFHLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMzQztRQUNELEdBQUcsRUFBRTtZQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsVUFBVSxFQUFFLElBQUk7S0FDakIsQ0FBQyxDQUFDO0NBQ0o7U0FRVTtJQUFTLGNBQWM7U0FBZCxVQUFjLEVBQWQscUJBQWMsRUFBZCxJQUFjO1FBQWQseUJBQWM7O0lBQzVCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3hEO0lBQUMsSUFBSSxDQUFDLENBQUM7UUFDTixPQUFPLENBQUMsSUFBSSxDQUFDLDRFQUE0RSxDQUFDLENBQUM7UUFDM0YsTUFBTSxDQUFDLElBQUksQ0FBQztLQUNiO0NBQ0Y7Ozs7Ozs7QUFWTCxNQUFNLDZCQUE2QixNQUFXLEVBQUUsR0FBVztJQUN6RCxNQUFNLENBQUM7UUFDTCxVQUFVLEVBQUUsSUFBSTtRQUNoQixLQUFLLElBT0o7S0FDRixDQUFDO0NBQ0g7Ozs7QUFLRDs7O0FBQUE7SUFJRSx1QkFDVSxJQUNBLEtBQ0E7UUFGQSxPQUFFLEdBQUYsRUFBRTtRQUNGLFFBQUcsR0FBSCxHQUFHO1FBQ0gsWUFBTyxHQUFQLE9BQU87UUFFZixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDO1FBQzNDLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztLQUNsRTs7Ozs7O0lBRVMsb0NBQVk7Ozs7O0lBQXRCLFVBQXVCLElBQVksRUFBRSxLQUFhO1FBQ2hELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQzVCLElBQUksQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDL0Q7Ozs7O0lBRVMsb0NBQVk7Ozs7SUFBdEIsVUFBdUIsSUFBWTtRQUNqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUNsQixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDOUM7d0JBNURIO0lBOERDLENBQUE7Ozs7QUF2QkQseUJBdUJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBGQk1MQXR0cmlidXRlKHRhcmdldDogYW55LCBrZXk6IHN0cmluZykge1xuICBjb25zdCBwcm9jZXNzS2V5ID0gKF9rOiBzdHJpbmcpID0+ICdkYXRhLScgKyBfay50b1N0cmluZygpLnJlcGxhY2UoLyhbYS16XFxkXSkoW0EtWl0pL2csICckMS0kMicpLnRvTG93ZXJDYXNlKCk7XG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwge1xuICAgIHNldDogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgIHZhbHVlID0gdmFsdWUudG9TdHJpbmcoKTtcbiAgICAgIHRoaXMuc2V0QXR0cmlidXRlKHByb2Nlc3NLZXkoa2V5KSwgdmFsdWUpO1xuICAgIH0sXG4gICAgZ2V0OiBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLmdldEF0dHJpYnV0ZShwcm9jZXNzS2V5KGtleSkpO1xuICAgIH0sXG4gICAgZW51bWVyYWJsZTogdHJ1ZVxuICB9KTtcbn1cblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBGQk1MSW5zdGFuY2VNZXRob2QodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nKSB7XG4gIHJldHVybiB7XG4gICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICB2YWx1ZTogZnVuY3Rpb24oLi4uYXJnczogYW55W10pIHtcbiAgICAgIGlmICh0aGlzLl9pbnN0YW5jZSkge1xuICAgICAgICByZXR1cm4gdGhpcy5faW5zdGFuY2Vba2V5XS5hcHBseSh0aGlzLl9pbnN0YW5jZSwgYXJncyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLndhcm4oJ25nMi1mYWNlYm9vay1zZGs6IHRyaWVkIGNhbGxpbmcgaW5zdGFuY2UgbWV0aG9kIGJlZm9yZSBjb21wb25lbnQgaXMgcmVhZHkuJyk7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuICAgIH1cbiAgfTtcbn1cblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBjbGFzcyBGQk1MQ29tcG9uZW50IHtcblxuICBwcm90ZWN0ZWQgbmF0aXZlRWxlbWVudDogSFRNTEVsZW1lbnQ7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZixcbiAgICBwcml2YXRlIHJuZDogUmVuZGVyZXIsXG4gICAgcHJpdmF0ZSBmYkNsYXNzOiBzdHJpbmdcbiAgKSB7XG4gICAgdGhpcy5uYXRpdmVFbGVtZW50ID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50O1xuICAgIHRoaXMucm5kLnNldEVsZW1lbnRDbGFzcyh0aGlzLm5hdGl2ZUVsZW1lbnQsIHRoaXMuZmJDbGFzcywgdHJ1ZSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgc2V0QXR0cmlidXRlKG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZykge1xuICAgIGlmICghbmFtZSB8fCAhdmFsdWUpIHJldHVybjtcbiAgICB0aGlzLnJuZC5zZXRFbGVtZW50QXR0cmlidXRlKHRoaXMubmF0aXZlRWxlbWVudCwgbmFtZSwgdmFsdWUpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEF0dHJpYnV0ZShuYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGlmICghbmFtZSkgcmV0dXJuO1xuICAgIHJldHVybiB0aGlzLm5hdGl2ZUVsZW1lbnQuZ2V0QXR0cmlidXRlKG5hbWUpO1xuICB9XG5cbn1cbiJdfQ==