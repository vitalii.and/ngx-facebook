/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
/**
 * \@shortdesc
 * Angular 2 service to inject to use Facebook's SDK
 * \@description
 * You only need to inject this service in your application if you aren't using [`FacebookModule`](../facebook-module).
 * \@usage
 * ```typescript
 * import { FacebookService, InitParams } from 'ng2-facebook-sdk';
 *
 * constructor(private fb: FacebookService) {
 *
 *   const params: InitParams = {
 *
 *   };
 *
 *   fb.init(params);
 *
 * }
 * ```
 */
var FacebookService = /** @class */ (function () {
    function FacebookService() {
    }
    /**
     * This method is used to initialize and setup the SDK.
     * @param params {InitParams} Initialization parameters
     * @returns return {Promise<any>}
     */
    /**
     * This method is used to initialize and setup the SDK.
     * @param {?} params {InitParams} Initialization parameters
     * @return {?} return {Promise<any>}
     */
    FacebookService.prototype.init = /**
     * This method is used to initialize and setup the SDK.
     * @param {?} params {InitParams} Initialization parameters
     * @return {?} return {Promise<any>}
     */
    function (params) {
        try {
            return Promise.resolve(FB.init(params));
        }
        catch (/** @type {?} */ e) {
            return Promise.reject(e);
        }
    };
    /**
     * This method lets you make calls to the Graph API
     * @usage
     * ```typescript
     * this.fb.api('somepath')
     *   .then(res => console.log(res))
     *   .catch(e => console.log(e));
     * ```
     * @param path {string} The Graph API endpoint path that you want to call.
     * @param [method=get] {string} The HTTP method that you want to use for the API request.
     * @param [params] {Object} An object consisting of any parameters that you want to pass into your Graph API call.
     * @returns return {Promise<any>}
     */
    /**
     * This method lets you make calls to the Graph API
     * \@usage
     * ```typescript
     * this.fb.api('somepath')
     *   .then(res => console.log(res))
     *   .catch(e => console.log(e));
     * ```
     * @param {?} path {string} The Graph API endpoint path that you want to call.
     * @param {?=} method
     * @param {?=} params
     * @return {?} return {Promise<any>}
     */
    FacebookService.prototype.api = /**
     * This method lets you make calls to the Graph API
     * \@usage
     * ```typescript
     * this.fb.api('somepath')
     *   .then(res => console.log(res))
     *   .catch(e => console.log(e));
     * ```
     * @param {?} path {string} The Graph API endpoint path that you want to call.
     * @param {?=} method
     * @param {?=} params
     * @return {?} return {Promise<any>}
     */
    function (path, method, params) {
        if (method === void 0) { method = 'get'; }
        if (params === void 0) { params = {}; }
        return new Promise(function (resolve, reject) {
            try {
                FB.api(path, method, params, function (response) {
                    if (!response) {
                        reject();
                    }
                    else if (response.error) {
                        reject(response.error);
                    }
                    else {
                        resolve(response);
                    }
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * This method is used to trigger different forms of Facebook created UI dialogs.
     * These dialogs include:
     * - Share dialog
     * - Login dialog
     * - Add page tab dialog
     * - Requests dialog
     * - Send dialog
     * - Payments dialog
     * - Go Live dialog
     * @param params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
     * @returns return {Promise<UIResponse>}
     */
    /**
     * This method is used to trigger different forms of Facebook created UI dialogs.
     * These dialogs include:
     * - Share dialog
     * - Login dialog
     * - Add page tab dialog
     * - Requests dialog
     * - Send dialog
     * - Payments dialog
     * - Go Live dialog
     * @param {?} params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
     * @return {?} return {Promise<UIResponse>}
     */
    FacebookService.prototype.ui = /**
     * This method is used to trigger different forms of Facebook created UI dialogs.
     * These dialogs include:
     * - Share dialog
     * - Login dialog
     * - Add page tab dialog
     * - Requests dialog
     * - Send dialog
     * - Payments dialog
     * - Go Live dialog
     * @param {?} params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
     * @return {?} return {Promise<UIResponse>}
     */
    function (params) {
        return new Promise(function (resolve, reject) {
            try {
                FB.ui(params, function (response) {
                    if (!response)
                        reject();
                    else if (response.error)
                        reject(response.error);
                    else
                        resolve(response);
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * @param [forceFreshResponse=false] {boolean} Force a fresh response.
     * @returns return {Promise<LoginStatus>}
     */
    /**
     * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * @param {?=} forceFreshResponse
     * @return {?} return {Promise<LoginStatus>}
     */
    FacebookService.prototype.getLoginStatus = /**
     * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * @param {?=} forceFreshResponse
     * @return {?} return {Promise<LoginStatus>}
     */
    function (forceFreshResponse) {
        return new Promise(function (resolve, reject) {
            try {
                FB.getLoginStatus(function (response) {
                    if (!response) {
                        reject();
                    }
                    else {
                        resolve(response);
                    }
                }, forceFreshResponse);
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * Login the user
     * @usage
     * ```typescript
     * // login without options
     * this.fb.login()
     *   .then((response: LoginResponse) => console.log('Logged in', response))
     *   .catch(e => console.error('Error logging in'));
     *
     * // login with options
     * const options: LoginOptions = {
     *   scope: 'public_profile,user_friends,email,pages_show_list',
     *   return_scopes: true,
     *   enable_profile_selector: true
     * };
     * this.fb.login(options)
     *   .then(...)
     *   .catch(...);
     * ```
     * @param [options] {LoginOptions} Login options
     * @returns return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
     */
    /**
     * Login the user
     * \@usage
     * ```typescript
     * // login without options
     * this.fb.login()
     *   .then((response: LoginResponse) => console.log('Logged in', response))
     *   .catch(e => console.error('Error logging in'));
     *
     * // login with options
     * const options: LoginOptions = {
     *   scope: 'public_profile,user_friends,email,pages_show_list',
     *   return_scopes: true,
     *   enable_profile_selector: true
     * };
     * this.fb.login(options)
     *   .then(...)
     *   .catch(...);
     * ```
     * @param {?=} options
     * @return {?} return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
     */
    FacebookService.prototype.login = /**
     * Login the user
     * \@usage
     * ```typescript
     * // login without options
     * this.fb.login()
     *   .then((response: LoginResponse) => console.log('Logged in', response))
     *   .catch(e => console.error('Error logging in'));
     *
     * // login with options
     * const options: LoginOptions = {
     *   scope: 'public_profile,user_friends,email,pages_show_list',
     *   return_scopes: true,
     *   enable_profile_selector: true
     * };
     * this.fb.login(options)
     *   .then(...)
     *   .catch(...);
     * ```
     * @param {?=} options
     * @return {?} return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
     */
    function (options) {
        return new Promise(function (resolve, reject) {
            try {
                FB.login(function (response) {
                    if (response.authResponse) {
                        resolve(response);
                    }
                    else {
                        reject();
                    }
                }, options);
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * Logout the user
     * @usage
     * ```typescript
     * this.fb.logout().then(() => console.log('Logged out!'));
     * ```
     * @returns return {Promise<any>} returns a promise that resolves when the user is logged out
     */
    /**
     * Logout the user
     * \@usage
     * ```typescript
     * this.fb.logout().then(() => console.log('Logged out!'));
     * ```
     * @return {?} return {Promise<any>} returns a promise that resolves when the user is logged out
     */
    FacebookService.prototype.logout = /**
     * Logout the user
     * \@usage
     * ```typescript
     * this.fb.logout().then(() => console.log('Logged out!'));
     * ```
     * @return {?} return {Promise<any>} returns a promise that resolves when the user is logged out
     */
    function () {
        return new Promise(function (resolve, reject) {
            try {
                FB.logout(function (response) {
                    resolve(response);
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * This synchronous function returns back the current authResponse.
     * @usage
     * ```typescript
     * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
     *
     * ...
     *
     * const authResponse: AuthResponse = this.fb.getAuthResponse();
     * ```
     * @returns return {AuthResponse} returns an [AuthResponse](../auth-response) object
     */
    /**
     * This synchronous function returns back the current authResponse.
     * \@usage
     * ```typescript
     * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
     *
     * ...
     *
     * const authResponse: AuthResponse = this.fb.getAuthResponse();
     * ```
     * @return {?} return {AuthResponse} returns an [AuthResponse](../auth-response) object
     */
    FacebookService.prototype.getAuthResponse = /**
     * This synchronous function returns back the current authResponse.
     * \@usage
     * ```typescript
     * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
     *
     * ...
     *
     * const authResponse: AuthResponse = this.fb.getAuthResponse();
     * ```
     * @return {?} return {AuthResponse} returns an [AuthResponse](../auth-response) object
     */
    function () {
        try {
            return /** @type {?} */ (FB.getAuthResponse());
        }
        catch (/** @type {?} */ e) {
            console.error('ng2-facebook-sdk: ', e);
        }
    };
    FacebookService.decorators = [
        { type: Injectable }
    ];
    return FacebookService;
}());
export { FacebookService };
function FacebookService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FacebookService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FacebookService.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZWJvb2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvcHJvdmlkZXJzL2ZhY2Vib29rLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUF1Q3pDOzs7O09BSUc7Ozs7OztJQUNILDhCQUFJOzs7OztJQUFKLFVBQUssTUFBa0I7UUFDckIsSUFBSSxDQUFDO1lBQ0gsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3pDO1FBQUMsS0FBSyxDQUFDLENBQUMsaUJBQUEsQ0FBQyxFQUFFLENBQUM7WUFDWCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxQjtLQUNGO0lBRUQ7Ozs7Ozs7Ozs7OztPQVlHOzs7Ozs7Ozs7Ozs7OztJQUNILDZCQUFHOzs7Ozs7Ozs7Ozs7O0lBQUgsVUFBSSxJQUFZLEVBQUUsTUFBeUIsRUFBRSxNQUFnQjtRQUEzQyx1QkFBQSxFQUFBLGNBQXlCO1FBQUUsdUJBQUEsRUFBQSxXQUFnQjtRQUMzRCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQU0sVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUV0QyxJQUFJLENBQUM7Z0JBQ0gsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxVQUFDLFFBQWE7b0JBQ3pDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt3QkFDZCxNQUFNLEVBQUUsQ0FBQztxQkFDVjtvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQzFCLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ3hCO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNOLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDbkI7aUJBQ0YsQ0FBQyxDQUFDO2FBQ0o7WUFBQyxLQUFLLENBQUMsQ0FBQyxpQkFBQSxDQUFDLEVBQUUsQ0FBQztnQkFDWCxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDWDtTQUVGLENBQUMsQ0FBQztLQUNKO0lBRUQ7Ozs7Ozs7Ozs7OztPQVlHOzs7Ozs7Ozs7Ozs7OztJQUNILDRCQUFFOzs7Ozs7Ozs7Ozs7O0lBQUYsVUFBRyxNQUFnQjtRQUNqQixNQUFNLENBQUMsSUFBSSxPQUFPLENBQU0sVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUV0QyxJQUFJLENBQUM7Z0JBQ0gsRUFBRSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBQyxRQUFhO29CQUMxQixFQUFFLENBQUEsQ0FBQyxDQUFDLFFBQVEsQ0FBQzt3QkFBQyxNQUFNLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7d0JBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDL0MsSUFBSTt3QkFBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3hCLENBQUMsQ0FBQzthQUNKO1lBQUMsS0FBSyxDQUFDLENBQUMsaUJBQUEsQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1g7U0FFRixDQUFDLENBQUM7S0FDSjtJQUVEOzs7O09BSUc7Ozs7OztJQUNILHdDQUFjOzs7OztJQUFkLFVBQWUsa0JBQTRCO1FBQ3pDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBYyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRTlDLElBQUksQ0FBQztnQkFDSCxFQUFFLENBQUMsY0FBYyxDQUFDLFVBQUMsUUFBcUI7b0JBQ3RDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt3QkFDZCxNQUFNLEVBQUUsQ0FBQztxQkFDVjtvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDTixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ25CO2lCQUNGLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzthQUN4QjtZQUFDLEtBQUssQ0FBQyxDQUFDLGlCQUFBLENBQUMsRUFBRSxDQUFDO2dCQUNYLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNYO1NBRUYsQ0FBQyxDQUFDO0tBQ0o7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O09BcUJHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQUNILCtCQUFLOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBQUwsVUFBTSxPQUFzQjtRQUMxQixNQUFNLENBQUMsSUFBSSxPQUFPLENBQWdCLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFaEQsSUFBSSxDQUFDO2dCQUNILEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBQyxRQUF1QjtvQkFDL0IsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7d0JBQzFCLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDbkI7b0JBQUEsSUFBSSxDQUFBLENBQUM7d0JBQ0osTUFBTSxFQUFFLENBQUM7cUJBQ1Y7aUJBQ0YsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNiO1lBQUMsS0FBSyxDQUFDLENBQUMsaUJBQUEsQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1g7U0FFRixDQUFDLENBQUM7S0FDSjtJQUVEOzs7Ozs7O09BT0c7Ozs7Ozs7OztJQUNILGdDQUFNOzs7Ozs7OztJQUFOO1FBQ0UsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFNLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFdEMsSUFBSSxDQUFDO2dCQUNILEVBQUUsQ0FBQyxNQUFNLENBQUMsVUFBQyxRQUFhO29CQUN0QixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ25CLENBQUMsQ0FBQzthQUNKO1lBQUMsS0FBSyxDQUFDLENBQUMsaUJBQUEsQ0FBQyxFQUFFLENBQUM7Z0JBQ1gsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1g7U0FFRixDQUFDLENBQUM7S0FDSjtJQUVEOzs7Ozs7Ozs7OztPQVdHOzs7Ozs7Ozs7Ozs7O0lBQ0gseUNBQWU7Ozs7Ozs7Ozs7OztJQUFmO1FBQ0UsSUFBSSxDQUFDO1lBQ0gsTUFBTSxtQkFBZSxFQUFFLENBQUMsZUFBZSxFQUFFLEVBQUM7U0FDM0M7UUFBQyxLQUFLLENBQUMsQ0FBQyxpQkFBQSxDQUFDLEVBQUUsQ0FBQztZQUNYLE9BQU8sQ0FBQyxLQUFLLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDeEM7S0FDRjs7Z0JBckxGLFVBQVU7OzBCQXBDWDs7U0FxQ2EsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEF1dGhSZXNwb25zZSB9IGZyb20gJy4uL21vZGVscy9hdXRoLXJlc3BvbnNlJztcbmltcG9ydCB7IEluaXRQYXJhbXMgfSBmcm9tICcuLi9tb2RlbHMvaW5pdC1wYXJhbXMnO1xuaW1wb3J0IHsgTG9naW5PcHRpb25zIH0gZnJvbSAnLi4vbW9kZWxzL2xvZ2luLW9wdGlvbnMnO1xuaW1wb3J0IHsgTG9naW5SZXNwb25zZSB9IGZyb20gJy4uL21vZGVscy9sb2dpbi1yZXNwb25zZSc7XG5pbXBvcnQgeyBMb2dpblN0YXR1cyB9IGZyb20gJy4uL21vZGVscy9sb2dpbi1zdGF0dXMnO1xuaW1wb3J0IHsgVUlQYXJhbXMgfSBmcm9tICcuLi9tb2RlbHMvdWktcGFyYW1zJztcbmltcG9ydCB7IFVJUmVzcG9uc2UgfSBmcm9tICcuLi9tb2RlbHMvdWktcmVzcG9uc2UnO1xuXG5kZWNsYXJlIHZhciBGQjogYW55O1xuXG4vKipcbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IHR5cGUgQXBpTWV0aG9kID0gJ2dldCcgfCAncG9zdCcgfCAnZGVsZXRlJztcblxuLyoqXG4gKiBAc2hvcnRkZXNjXG4gKiBBbmd1bGFyIDIgc2VydmljZSB0byBpbmplY3QgdG8gdXNlIEZhY2Vib29rJ3MgU0RLXG4gKiBAZGVzY3JpcHRpb25cbiAqIFlvdSBvbmx5IG5lZWQgdG8gaW5qZWN0IHRoaXMgc2VydmljZSBpbiB5b3VyIGFwcGxpY2F0aW9uIGlmIHlvdSBhcmVuJ3QgdXNpbmcgW2BGYWNlYm9va01vZHVsZWBdKC4uL2ZhY2Vib29rLW1vZHVsZSkuXG4gKiBAdXNhZ2VcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IEZhY2Vib29rU2VydmljZSwgSW5pdFBhcmFtcyB9IGZyb20gJ25nMi1mYWNlYm9vay1zZGsnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZhY2Vib29rU2VydmljZSkge1xuICpcbiAqICAgY29uc3QgcGFyYW1zOiBJbml0UGFyYW1zID0ge1xuICpcbiAqICAgfTtcbiAqXG4gKiAgIGZiLmluaXQocGFyYW1zKTtcbiAqXG4gKiB9XG4gKiBgYGBcbiAqL1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEZhY2Vib29rU2VydmljZSB7XG5cbiAgLyoqXG4gICAqIFRoaXMgbWV0aG9kIGlzIHVzZWQgdG8gaW5pdGlhbGl6ZSBhbmQgc2V0dXAgdGhlIFNESy5cbiAgICogQHBhcmFtIHBhcmFtcyB7SW5pdFBhcmFtc30gSW5pdGlhbGl6YXRpb24gcGFyYW1ldGVyc1xuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIGluaXQocGFyYW1zOiBJbml0UGFyYW1zKTogUHJvbWlzZTxhbnk+IHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShGQi5pbml0KHBhcmFtcykpO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgbGV0cyB5b3UgbWFrZSBjYWxscyB0byB0aGUgR3JhcGggQVBJXG4gICAqIEB1c2FnZVxuICAgKiBgYGB0eXBlc2NyaXB0XG4gICAqIHRoaXMuZmIuYXBpKCdzb21lcGF0aCcpXG4gICAqICAgLnRoZW4ocmVzID0+IGNvbnNvbGUubG9nKHJlcykpXG4gICAqICAgLmNhdGNoKGUgPT4gY29uc29sZS5sb2coZSkpO1xuICAgKiBgYGBcbiAgICogQHBhcmFtIHBhdGgge3N0cmluZ30gVGhlIEdyYXBoIEFQSSBlbmRwb2ludCBwYXRoIHRoYXQgeW91IHdhbnQgdG8gY2FsbC5cbiAgICogQHBhcmFtIFttZXRob2Q9Z2V0XSB7c3RyaW5nfSBUaGUgSFRUUCBtZXRob2QgdGhhdCB5b3Ugd2FudCB0byB1c2UgZm9yIHRoZSBBUEkgcmVxdWVzdC5cbiAgICogQHBhcmFtIFtwYXJhbXNdIHtPYmplY3R9IEFuIG9iamVjdCBjb25zaXN0aW5nIG9mIGFueSBwYXJhbWV0ZXJzIHRoYXQgeW91IHdhbnQgdG8gcGFzcyBpbnRvIHlvdXIgR3JhcGggQVBJIGNhbGwuXG4gICAqIEByZXR1cm5zIHJldHVybiB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgYXBpKHBhdGg6IHN0cmluZywgbWV0aG9kOiBBcGlNZXRob2QgPSAnZ2V0JywgcGFyYW1zOiBhbnkgPSB7fSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGFueT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICB0cnkge1xuICAgICAgICBGQi5hcGkocGF0aCwgbWV0aG9kLCBwYXJhbXMsIChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgaWYgKCFyZXNwb25zZSkge1xuICAgICAgICAgICAgcmVqZWN0KCk7XG4gICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZS5lcnJvcikge1xuICAgICAgICAgICAgcmVqZWN0KHJlc3BvbnNlLmVycm9yKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgaXMgdXNlZCB0byB0cmlnZ2VyIGRpZmZlcmVudCBmb3JtcyBvZiBGYWNlYm9vayBjcmVhdGVkIFVJIGRpYWxvZ3MuXG4gICAqIFRoZXNlIGRpYWxvZ3MgaW5jbHVkZTpcbiAgICogLSBTaGFyZSBkaWFsb2dcbiAgICogLSBMb2dpbiBkaWFsb2dcbiAgICogLSBBZGQgcGFnZSB0YWIgZGlhbG9nXG4gICAqIC0gUmVxdWVzdHMgZGlhbG9nXG4gICAqIC0gU2VuZCBkaWFsb2dcbiAgICogLSBQYXltZW50cyBkaWFsb2dcbiAgICogLSBHbyBMaXZlIGRpYWxvZ1xuICAgKiBAcGFyYW0gcGFyYW1zIHtVSVBhcmFtc30gQSBjb2xsZWN0aW9uIG9mIHBhcmFtZXRlcnMgdGhhdCBjb250cm9sIHdoaWNoIGRpYWxvZyBpcyBsb2FkZWQsIGFuZCByZWxldmFudCBzZXR0aW5ncy5cbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPFVJUmVzcG9uc2U+fVxuICAgKi9cbiAgdWkocGFyYW1zOiBVSVBhcmFtcyk6IFByb21pc2U8VUlSZXNwb25zZT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgdHJ5IHtcbiAgICAgICAgRkIudWkocGFyYW1zLCAocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICAgIGlmKCFyZXNwb25zZSkgcmVqZWN0KCk7XG4gICAgICAgICAgZWxzZSBpZihyZXNwb25zZS5lcnJvcikgcmVqZWN0KHJlc3BvbnNlLmVycm9yKTtcbiAgICAgICAgICBlbHNlIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICB9KTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgYWxsb3dzIHlvdSB0byBkZXRlcm1pbmUgaWYgYSB1c2VyIGlzIGxvZ2dlZCBpbiB0byBGYWNlYm9vayBhbmQgaGFzIGF1dGhlbnRpY2F0ZWQgeW91ciBhcHAuXG4gICAqIEBwYXJhbSBbZm9yY2VGcmVzaFJlc3BvbnNlPWZhbHNlXSB7Ym9vbGVhbn0gRm9yY2UgYSBmcmVzaCByZXNwb25zZS5cbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPExvZ2luU3RhdHVzPn1cbiAgICovXG4gIGdldExvZ2luU3RhdHVzKGZvcmNlRnJlc2hSZXNwb25zZT86IGJvb2xlYW4pOiBQcm9taXNlPExvZ2luU3RhdHVzPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPExvZ2luU3RhdHVzPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIEZCLmdldExvZ2luU3RhdHVzKChyZXNwb25zZTogTG9naW5TdGF0dXMpID0+IHtcbiAgICAgICAgICBpZiAoIXJlc3BvbnNlKSB7XG4gICAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LCBmb3JjZUZyZXNoUmVzcG9uc2UpO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb2dpbiB0aGUgdXNlclxuICAgKiBAdXNhZ2VcbiAgICogYGBgdHlwZXNjcmlwdFxuICAgKiAvLyBsb2dpbiB3aXRob3V0IG9wdGlvbnNcbiAgICogdGhpcy5mYi5sb2dpbigpXG4gICAqICAgLnRoZW4oKHJlc3BvbnNlOiBMb2dpblJlc3BvbnNlKSA9PiBjb25zb2xlLmxvZygnTG9nZ2VkIGluJywgcmVzcG9uc2UpKVxuICAgKiAgIC5jYXRjaChlID0+IGNvbnNvbGUuZXJyb3IoJ0Vycm9yIGxvZ2dpbmcgaW4nKSk7XG4gICAqXG4gICAqIC8vIGxvZ2luIHdpdGggb3B0aW9uc1xuICAgKiBjb25zdCBvcHRpb25zOiBMb2dpbk9wdGlvbnMgPSB7XG4gICAqICAgc2NvcGU6ICdwdWJsaWNfcHJvZmlsZSx1c2VyX2ZyaWVuZHMsZW1haWwscGFnZXNfc2hvd19saXN0JyxcbiAgICogICByZXR1cm5fc2NvcGVzOiB0cnVlLFxuICAgKiAgIGVuYWJsZV9wcm9maWxlX3NlbGVjdG9yOiB0cnVlXG4gICAqIH07XG4gICAqIHRoaXMuZmIubG9naW4ob3B0aW9ucylcbiAgICogICAudGhlbiguLi4pXG4gICAqICAgLmNhdGNoKC4uLik7XG4gICAqIGBgYFxuICAgKiBAcGFyYW0gW29wdGlvbnNdIHtMb2dpbk9wdGlvbnN9IExvZ2luIG9wdGlvbnNcbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPExvZ2luUmVzcG9uc2U+fSByZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdpdGggW0xvZ2luUmVzcG9uc2VdKC4uL2xvZ2luLXJlc3BvbnNlKSBvYmplY3QsIG9yIHJlamVjdHMgd2l0aCBhbiBlcnJvclxuICAgKi9cbiAgbG9naW4ob3B0aW9ucz86IExvZ2luT3B0aW9ucyk6IFByb21pc2U8TG9naW5SZXNwb25zZT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxMb2dpblJlc3BvbnNlPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIEZCLmxvZ2luKChyZXNwb25zZTogTG9naW5SZXNwb25zZSkgPT4ge1xuICAgICAgICAgIGlmIChyZXNwb25zZS5hdXRoUmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgcmVqZWN0KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LCBvcHRpb25zKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogTG9nb3V0IHRoZSB1c2VyXG4gICAqIEB1c2FnZVxuICAgKiBgYGB0eXBlc2NyaXB0XG4gICAqIHRoaXMuZmIubG9nb3V0KCkudGhlbigoKSA9PiBjb25zb2xlLmxvZygnTG9nZ2VkIG91dCEnKSk7XG4gICAqIGBgYFxuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8YW55Pn0gcmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIHRoZSB1c2VyIGlzIGxvZ2dlZCBvdXRcbiAgICovXG4gIGxvZ291dCgpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgdHJ5IHtcbiAgICAgICAgRkIubG9nb3V0KChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIHN5bmNocm9ub3VzIGZ1bmN0aW9uIHJldHVybnMgYmFjayB0aGUgY3VycmVudCBhdXRoUmVzcG9uc2UuXG4gICAqIEB1c2FnZVxuICAgKiBgYGB0eXBlc2NyaXB0XG4gICAqIGltcG9ydCB7IEF1dGhSZXNwb25zZSwgRmFjZWJvb2tTZXJ2aWNlIH0gZnJvbSAnbmcyLWZhY2Vib29rLXNkayc7XG4gICAqXG4gICAqIC4uLlxuICAgKlxuICAgKiBjb25zdCBhdXRoUmVzcG9uc2U6IEF1dGhSZXNwb25zZSA9IHRoaXMuZmIuZ2V0QXV0aFJlc3BvbnNlKCk7XG4gICAqIGBgYFxuICAgKiBAcmV0dXJucyByZXR1cm4ge0F1dGhSZXNwb25zZX0gcmV0dXJucyBhbiBbQXV0aFJlc3BvbnNlXSguLi9hdXRoLXJlc3BvbnNlKSBvYmplY3RcbiAgICovXG4gIGdldEF1dGhSZXNwb25zZSgpOiBBdXRoUmVzcG9uc2Uge1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gPEF1dGhSZXNwb25zZT5GQi5nZXRBdXRoUmVzcG9uc2UoKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBjb25zb2xlLmVycm9yKCduZzItZmFjZWJvb2stc2RrOiAnLCBlKTtcbiAgICB9XG4gIH1cblxufVxuXG4iXX0=