/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * This is the configuration that can be passed to the [init](../facebook-service/#init) method.
 * @record
 */
export function InitParams() { }
function InitParams_tsickle_Closure_declarations() {
    /**
     * Your application ID. If you don't have one find it in the App dashboard or go there to create a new app. Defaults to null.
     * @type {?|undefined}
     */
    InitParams.prototype.appId;
    /**
     * Determines which versions of the Graph API and any API dialogs or plugins are invoked when using the .api() and .ui() functions. Valid values are determined by currently available versions, such as 'v2.0'. This is a required parameter.
     * @type {?}
     */
    InitParams.prototype.version;
    /**
     * Determines whether a cookie is created for the session or not. If enabled, it can be accessed by server-side code. Defaults to false.
     * @type {?|undefined}
     */
    InitParams.prototype.cookie;
    /**
     * Determines whether the current login status of the user is freshly retrieved on every page load. If this is disabled, that status will have to be manually retrieved using .getLoginStatus(). Defaults to false.
     * @type {?|undefined}
     */
    InitParams.prototype.status;
    /**
     * Determines whether XFBML tags used by social plugins are parsed, and therefore whether the plugins are rendered or not. Defaults to false.
     * @type {?|undefined}
     */
    InitParams.prototype.xfbml;
    /**
     * Frictionless Requests are available to games on Facebook.com or on mobile web using the JavaScript SDK. This parameter determines whether they are enabled. Defaults to false.
     * @type {?|undefined}
     */
    InitParams.prototype.frictionlessRequests;
    /**
     * This specifies a function that is called whenever it is necessary to hide Adobe Flash objects on a page. This is used when .api() requests are made, as Flash objects will always have a higher z-index than any other DOM element. See our Custom Flash Hide Callback for more details on what to put in this function. Defaults to null.
     * @type {?|undefined}
     */
    InitParams.prototype.hideFlashCallback;
    /**
     * Determines whether events related to user interaction with the app are collected automatically. Defaults to true.
     * @type {?|undefined}
     */
    InitParams.prototype.autoLogAppEvents;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdC1wYXJhbXMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvbW9kZWxzL2luaXQtcGFyYW1zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFRoaXMgaXMgdGhlIGNvbmZpZ3VyYXRpb24gdGhhdCBjYW4gYmUgcGFzc2VkIHRvIHRoZSBbaW5pdF0oLi4vZmFjZWJvb2stc2VydmljZS8jaW5pdCkgbWV0aG9kLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIEluaXRQYXJhbXMge1xuICAgIC8qKlxuICAgICAqIFlvdXIgYXBwbGljYXRpb24gSUQuIElmIHlvdSBkb24ndCBoYXZlIG9uZSBmaW5kIGl0IGluIHRoZSBBcHAgZGFzaGJvYXJkIG9yIGdvIHRoZXJlIHRvIGNyZWF0ZSBhIG5ldyBhcHAuIERlZmF1bHRzIHRvIG51bGwuXG4gICAgICovXG4gICAgYXBwSWQ/OiBzdHJpbmc7XG5cbiAgICAvKipcbiAgICAgKiBEZXRlcm1pbmVzIHdoaWNoIHZlcnNpb25zIG9mIHRoZSBHcmFwaCBBUEkgYW5kIGFueSBBUEkgZGlhbG9ncyBvciBwbHVnaW5zIGFyZSBpbnZva2VkIHdoZW4gdXNpbmcgdGhlIC5hcGkoKSBhbmQgLnVpKCkgZnVuY3Rpb25zLiBWYWxpZCB2YWx1ZXMgYXJlIGRldGVybWluZWQgYnkgY3VycmVudGx5IGF2YWlsYWJsZSB2ZXJzaW9ucywgc3VjaCBhcyAndjIuMCcuIFRoaXMgaXMgYSByZXF1aXJlZCBwYXJhbWV0ZXIuXG4gICAgICovXG4gICAgdmVyc2lvbjogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogRGV0ZXJtaW5lcyB3aGV0aGVyIGEgY29va2llIGlzIGNyZWF0ZWQgZm9yIHRoZSBzZXNzaW9uIG9yIG5vdC4gSWYgZW5hYmxlZCwgaXQgY2FuIGJlIGFjY2Vzc2VkIGJ5IHNlcnZlci1zaWRlIGNvZGUuIERlZmF1bHRzIHRvIGZhbHNlLlxuICAgICAqL1xuICAgIGNvb2tpZT86IGJvb2xlYW47XG5cbiAgICAvKipcbiAgICAgKiBEZXRlcm1pbmVzIHdoZXRoZXIgdGhlIGN1cnJlbnQgbG9naW4gc3RhdHVzIG9mIHRoZSB1c2VyIGlzIGZyZXNobHkgcmV0cmlldmVkIG9uIGV2ZXJ5IHBhZ2UgbG9hZC4gSWYgdGhpcyBpcyBkaXNhYmxlZCwgdGhhdCBzdGF0dXMgd2lsbCBoYXZlIHRvIGJlIG1hbnVhbGx5IHJldHJpZXZlZCB1c2luZyAuZ2V0TG9naW5TdGF0dXMoKS4gRGVmYXVsdHMgdG8gZmFsc2UuXG4gICAgICovXG4gICAgc3RhdHVzPzogYm9vbGVhbjtcblxuICAgIC8qKlxuICAgICAqIERldGVybWluZXMgd2hldGhlciBYRkJNTCB0YWdzIHVzZWQgYnkgc29jaWFsIHBsdWdpbnMgYXJlIHBhcnNlZCwgYW5kIHRoZXJlZm9yZSB3aGV0aGVyIHRoZSBwbHVnaW5zIGFyZSByZW5kZXJlZCBvciBub3QuIERlZmF1bHRzIHRvIGZhbHNlLlxuICAgICAqL1xuICAgIHhmYm1sPzogYm9vbGVhbjtcblxuICAgIC8qKlxuICAgICAqIEZyaWN0aW9ubGVzcyBSZXF1ZXN0cyBhcmUgYXZhaWxhYmxlIHRvIGdhbWVzIG9uIEZhY2Vib29rLmNvbSBvciBvbiBtb2JpbGUgd2ViIHVzaW5nIHRoZSBKYXZhU2NyaXB0IFNESy4gVGhpcyBwYXJhbWV0ZXIgZGV0ZXJtaW5lcyB3aGV0aGVyIHRoZXkgYXJlIGVuYWJsZWQuIERlZmF1bHRzIHRvIGZhbHNlLlxuICAgICAqL1xuICAgIGZyaWN0aW9ubGVzc1JlcXVlc3RzPzogYm9vbGVhbjtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgc3BlY2lmaWVzIGEgZnVuY3Rpb24gdGhhdCBpcyBjYWxsZWQgd2hlbmV2ZXIgaXQgaXMgbmVjZXNzYXJ5IHRvIGhpZGUgQWRvYmUgRmxhc2ggb2JqZWN0cyBvbiBhIHBhZ2UuIFRoaXMgaXMgdXNlZCB3aGVuIC5hcGkoKSByZXF1ZXN0cyBhcmUgbWFkZSwgYXMgRmxhc2ggb2JqZWN0cyB3aWxsIGFsd2F5cyBoYXZlIGEgaGlnaGVyIHotaW5kZXggdGhhbiBhbnkgb3RoZXIgRE9NIGVsZW1lbnQuIFNlZSBvdXIgQ3VzdG9tIEZsYXNoIEhpZGUgQ2FsbGJhY2sgZm9yIG1vcmUgZGV0YWlscyBvbiB3aGF0IHRvIHB1dCBpbiB0aGlzIGZ1bmN0aW9uLiBEZWZhdWx0cyB0byBudWxsLlxuICAgICAqL1xuICAgIGhpZGVGbGFzaENhbGxiYWNrPzogYW55O1xuXG4gICAgLyoqXG4gICAgICogRGV0ZXJtaW5lcyB3aGV0aGVyIGV2ZW50cyByZWxhdGVkIHRvIHVzZXIgaW50ZXJhY3Rpb24gd2l0aCB0aGUgYXBwIGFyZSBjb2xsZWN0ZWQgYXV0b21hdGljYWxseS4gRGVmYXVsdHMgdG8gdHJ1ZS5cbiAgICAgKi9cbiAgICBhdXRvTG9nQXBwRXZlbnRzPzogYm9vbGVhbjtcbn1cbiJdfQ==