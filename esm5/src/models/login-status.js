/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * The object returned by the [getLoginStatus](../facebook-service/#getLoginStatus) method.
 * @record
 */
export function LoginStatus() { }
function LoginStatus_tsickle_Closure_declarations() {
    /**
     * Status as a string
     * @type {?}
     */
    LoginStatus.prototype.status;
    /**
     * Auth response object
     * @type {?}
     */
    LoginStatus.prototype.authResponse;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tc3RhdHVzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWZhY2Vib29rLyIsInNvdXJjZXMiOlsic3JjL21vZGVscy9sb2dpbi1zdGF0dXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEF1dGhSZXNwb25zZSB9IGZyb20gJy4vYXV0aC1yZXNwb25zZSc7XG5cbi8qKlxuICogVGhlIG9iamVjdCByZXR1cm5lZCBieSB0aGUgW2dldExvZ2luU3RhdHVzXSguLi9mYWNlYm9vay1zZXJ2aWNlLyNnZXRMb2dpblN0YXR1cykgbWV0aG9kLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIExvZ2luU3RhdHVzIHtcbiAgLyoqXG4gICAqIFN0YXR1cyBhcyBhIHN0cmluZ1xuICAgKi9cbiAgc3RhdHVzOiBzdHJpbmc7XG4gIC8qKlxuICAgKiBBdXRoIHJlc3BvbnNlIG9iamVjdFxuICAgKi9cbiAgYXV0aFJlc3BvbnNlOiBBdXRoUmVzcG9uc2U7XG59XG4iXX0=