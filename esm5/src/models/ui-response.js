/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Response object received after a Feed dialog is closed
 * @record
 */
export function FeedDialogResponse() { }
function FeedDialogResponse_tsickle_Closure_declarations() {
    /**
     * The ID of the posted story, if the person chose to publish to their timeline.
     * @type {?|undefined}
     */
    FeedDialogResponse.prototype.post_id;
}
/**
 * Response object received after a share dialog is closed
 * @record
 */
export function ShareDialogResponse() { }
function ShareDialogResponse_tsickle_Closure_declarations() {
    /**
     * Only available if the user is logged into your app using Facebook and has granted publish_actions. If present, this is the ID of the published Open Graph story.
     * @type {?|undefined}
     */
    ShareDialogResponse.prototype.post_id;
    /**
     * Only available if the user is logged into your app using Facebook Login.
     * @type {?|undefined}
     */
    ShareDialogResponse.prototype.error_message;
}
/**
 * @record
 */
export function UIResponse() { }
function UIResponse_tsickle_Closure_declarations() {
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktcmVzcG9uc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvbW9kZWxzL3VpLXJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFJlc3BvbnNlIG9iamVjdCByZWNlaXZlZCBhZnRlciBhIEZlZWQgZGlhbG9nIGlzIGNsb3NlZFxuICovXG5leHBvcnQgaW50ZXJmYWNlIEZlZWREaWFsb2dSZXNwb25zZSB7XG5cbiAgICAvKipcbiAgICAgKiBUaGUgSUQgb2YgdGhlIHBvc3RlZCBzdG9yeSwgaWYgdGhlIHBlcnNvbiBjaG9zZSB0byBwdWJsaXNoIHRvIHRoZWlyIHRpbWVsaW5lLlxuICAgICAqL1xuICAgIHBvc3RfaWQ/OiBzdHJpbmc7XG5cbn1cblxuLyoqXG4gKiBSZXNwb25zZSBvYmplY3QgcmVjZWl2ZWQgYWZ0ZXIgYSBzaGFyZSBkaWFsb2cgaXMgY2xvc2VkXG4gKi9cbmV4cG9ydCBpbnRlcmZhY2UgU2hhcmVEaWFsb2dSZXNwb25zZSB7XG5cbiAgICAvKipcbiAgICAgKiBPbmx5IGF2YWlsYWJsZSBpZiB0aGUgdXNlciBpcyBsb2dnZWQgaW50byB5b3VyIGFwcCB1c2luZyBGYWNlYm9vayBhbmQgaGFzIGdyYW50ZWQgcHVibGlzaF9hY3Rpb25zLiBJZiBwcmVzZW50LCB0aGlzIGlzIHRoZSBJRCBvZiB0aGUgcHVibGlzaGVkIE9wZW4gR3JhcGggc3RvcnkuXG4gICAgICovXG4gICAgcG9zdF9pZD86IHN0cmluZztcblxuICAgIC8qKlxuICAgICAqIE9ubHkgYXZhaWxhYmxlIGlmIHRoZSB1c2VyIGlzIGxvZ2dlZCBpbnRvIHlvdXIgYXBwIHVzaW5nIEZhY2Vib29rIExvZ2luLlxuICAgICAqL1xuICAgIGVycm9yX21lc3NhZ2U/OiBzdHJpbmc7XG5cbn1cblxuZXhwb3J0IGludGVyZmFjZSBVSVJlc3BvbnNlIGV4dGVuZHMgU2hhcmVEaWFsb2dSZXNwb25zZSwgRmVlZERpYWxvZ1Jlc3BvbnNlIHsgfVxuIl19