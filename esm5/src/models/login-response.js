/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Object returned by the [login](../facebook-service/#login) method.
 * @record
 */
export function LoginResponse() { }
function LoginResponse_tsickle_Closure_declarations() {
    /**
     * Auth Response object
     * @type {?}
     */
    LoginResponse.prototype.authResponse;
    /**
     * Status as string
     * @type {?}
     */
    LoginResponse.prototype.status;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tcmVzcG9uc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZmFjZWJvb2svIiwic291cmNlcyI6WyJzcmMvbW9kZWxzL2xvZ2luLXJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdXRoUmVzcG9uc2UgfSBmcm9tICcuL2F1dGgtcmVzcG9uc2UnO1xuXG4vKipcbiAqIE9iamVjdCByZXR1cm5lZCBieSB0aGUgW2xvZ2luXSguLi9mYWNlYm9vay1zZXJ2aWNlLyNsb2dpbikgbWV0aG9kLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIExvZ2luUmVzcG9uc2Uge1xuICAvKipcbiAgICogQXV0aCBSZXNwb25zZSBvYmplY3RcbiAgICovXG4gIGF1dGhSZXNwb25zZTogQXV0aFJlc3BvbnNlO1xuICAvKipcbiAgICogU3RhdHVzIGFzIHN0cmluZ1xuICAgKi9cbiAgc3RhdHVzOiBzdHJpbmc7XG59XG4iXX0=