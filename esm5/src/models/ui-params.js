/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Params that can be passed when creating a Share dialog.
 * @record
 */
export function ShareDialogParams() { }
function ShareDialogParams_tsickle_Closure_declarations() {
    /**
     * The link attached to this post. Required when using method share. Include open graph meta tags in the page at this URL to customize the story that is shared.
     * Defaults to Current URL
     * @type {?|undefined}
     */
    ShareDialogParams.prototype.href;
    /**
     * A hashtag specified by the developer to be added to the shared content. People will still have the opportunity to remove this hashtag in the dialog. The hashtag should include the hash symbol, e.g. #facebook.
     * @type {?|undefined}
     */
    ShareDialogParams.prototype.hashtag;
    /**
     * A quote to be shared along with the link, either highlighted by the user or predefined by the developer, as in a pull quote on an article.
     * @type {?|undefined}
     */
    ShareDialogParams.prototype.quote;
    /**
     * If set to true the share button will open the share dialog in an iframe on top of your website (For more information see Mobile Web Share Dialog. This option is only available for mobile, not desktop.
     * @type {?|undefined}
     */
    ShareDialogParams.prototype.mobile_iframe;
    /**
     * A string specifying which Open Graph action type to publish, e.g., og.likes for the built in like type. The dialog also supports approved custom types. Required when using method share_open_graph.
     * @type {?|undefined}
     */
    ShareDialogParams.prototype.action_type;
    /**
     * A JSON object of key/value pairs specifying parameters which correspond to the action_type being used. Valid key/value pairs are the same parameters that can be used when publishing Open Graph Actions using the API. Required when using method share_open_graph.
     * @type {?|undefined}
     */
    ShareDialogParams.prototype.action_properties;
}
/**
 * Params that can be passed when creating a Feed dialog.
 * @record
 */
export function FeedDialogParams() { }
function FeedDialogParams_tsickle_Closure_declarations() {
    /**
     * The ID of the person posting the message. If this is unspecified, it defaults to the current person. If specified, it must be the ID of the person or of a page that the person administers.
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.from;
    /**
     * The ID of the profile that this story will be published to. If this is unspecified, it defaults to the value of from. The ID must be a friend who also uses your app.
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.to;
    /**
     * The link attached to this post. With the Feed Dialog, people can also share plain text status updates with no content from your app; just leave the link parameter empty.
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.link;
    /**
     * The URL of a picture attached to this post. The picture must be at least 200px by 200px. See our documentation on sharing best practices for more information on sizes
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.picture;
    /**
     * The URL of a media file (either SWF or MP3) attached to this post. If SWF, you must also specify picture to provide a thumbnail for the video.
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.source;
    /**
     * The name of the link attachment.
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.name;
    /**
     * The caption of the link (appears beneath the link name). If not specified, this field is automatically populated with the URL of the link.
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.caption;
    /**
     * The description of the link (appears beneath the link caption). If not specified, this field is automatically populated by information scraped from the link, typically the title of the page.
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.description;
    /**
     * This argument is a comma-separated list, consisting of at most 5 distinct items, each of length at least 1 and at most 15 characters drawn from the set '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_'. Each category is used in Facebook Insights to help you measure the performance of different types of post
     * @type {?|undefined}
     */
    FeedDialogParams.prototype.ref;
}
/**
 * Params that can be passed when creating a send dialog
 * @record
 */
export function SendDialogParams() { }
function SendDialogParams_tsickle_Closure_declarations() {
    /**
     * A user ID of a recipient. Once the dialog comes up, the sender can specify additional people as recipients.
     * @type {?|undefined}
     */
    SendDialogParams.prototype.to;
    /**
     * Required parameter. The URL that is being sent in the message.
     * @type {?|undefined}
     */
    SendDialogParams.prototype.link;
}
/**
 * The object passed to the [ui](../facebook-service/#ui) method. The properties below can be passed to all three types of dialogs. Each type of dialog accepts additional parameters that are documented above.
 * @record
 */
export function UIParams() { }
function UIParams_tsickle_Closure_declarations() {
    /**
     * The UI dialog that is being invoked. This is a required field.
     * @type {?}
     */
    UIParams.prototype.method;
    /**
     * The UI dialog message.
     * @type {?|undefined}
     */
    UIParams.prototype.message;
    /**
     * Your app's unique identifier. Required.
     * @type {?|undefined}
     */
    UIParams.prototype.app_id;
    /**
     * The URL to redirect to after a person clicks a button on the dialog. Required when using URL redirection.
     * @type {?|undefined}
     */
    UIParams.prototype.redirect_uri;
    /**
     * Determines how the dialog is rendered.
     * - If you are using the URL redirect dialog implementation, then this will be a full page display, shown within Facebook.com. This display type is called page
     * - If you are using one of our iOS or Android SDKs to invoke the dialog, this is automatically specified and chooses an appropriate display type for the device.
     * - If you are using the Facebook SDK for JavaScript, this will default to a modal iframe type for people logged into your app or async when using within a game on Facebook.com, and a popup window for everyone else. You can also force the popup type when when you use the Facebook SDK for JavaScript, if necessary.
     * - Mobile web apps will always default to the touch display type.
     * @type {?|undefined}
     */
    UIParams.prototype.display;
    /**
     * - Dialog in create phase allows you to get stream url to upload video; Dialog in publish phase will provide preview and go live button. required
     * - phase = create | publish
     * @type {?|undefined}
     */
    UIParams.prototype.phase;
    /**
     * - This parameter is required for publish phase.
     * - The response object returned from either API or create phase.
     * @type {?|undefined}
     */
    UIParams.prototype.broadcast_data;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktcGFyYW1zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWZhY2Vib29rLyIsInNvdXJjZXMiOlsic3JjL21vZGVscy91aS1wYXJhbXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogUGFyYW1zIHRoYXQgY2FuIGJlIHBhc3NlZCB3aGVuIGNyZWF0aW5nIGEgU2hhcmUgZGlhbG9nLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIFNoYXJlRGlhbG9nUGFyYW1zIHtcbiAgICAvKipcbiAgICAgKiBUaGUgbGluayBhdHRhY2hlZCB0byB0aGlzIHBvc3QuIFJlcXVpcmVkIHdoZW4gdXNpbmcgbWV0aG9kIHNoYXJlLiBJbmNsdWRlIG9wZW4gZ3JhcGggbWV0YSB0YWdzIGluIHRoZSBwYWdlIGF0IHRoaXMgVVJMIHRvIGN1c3RvbWl6ZSB0aGUgc3RvcnkgdGhhdCBpcyBzaGFyZWQuXG4gICAgICogRGVmYXVsdHMgdG8gQ3VycmVudCBVUkxcbiAgICAgKi9cbiAgICBocmVmPzogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogQSBoYXNodGFnIHNwZWNpZmllZCBieSB0aGUgZGV2ZWxvcGVyIHRvIGJlIGFkZGVkIHRvIHRoZSBzaGFyZWQgY29udGVudC4gUGVvcGxlIHdpbGwgc3RpbGwgaGF2ZSB0aGUgb3Bwb3J0dW5pdHkgdG8gcmVtb3ZlIHRoaXMgaGFzaHRhZyBpbiB0aGUgZGlhbG9nLiBUaGUgaGFzaHRhZyBzaG91bGQgaW5jbHVkZSB0aGUgaGFzaCBzeW1ib2wsIGUuZy4gI2ZhY2Vib29rLlxuICAgICAqL1xuICAgIGhhc2h0YWc/OiBzdHJpbmc7XG5cbiAgICAvKipcbiAgICAgKiBBIHF1b3RlIHRvIGJlIHNoYXJlZCBhbG9uZyB3aXRoIHRoZSBsaW5rLCBlaXRoZXIgaGlnaGxpZ2h0ZWQgYnkgdGhlIHVzZXIgb3IgcHJlZGVmaW5lZCBieSB0aGUgZGV2ZWxvcGVyLCBhcyBpbiBhIHB1bGwgcXVvdGUgb24gYW4gYXJ0aWNsZS5cbiAgICAgKi9cbiAgICBxdW90ZT86IHN0cmluZztcblxuICAgIC8qKlxuICAgICAqIElmIHNldCB0byB0cnVlIHRoZSBzaGFyZSBidXR0b24gd2lsbCBvcGVuIHRoZSBzaGFyZSBkaWFsb2cgaW4gYW4gaWZyYW1lIG9uIHRvcCBvZiB5b3VyIHdlYnNpdGUgKEZvciBtb3JlIGluZm9ybWF0aW9uIHNlZSBNb2JpbGUgV2ViIFNoYXJlIERpYWxvZy4gVGhpcyBvcHRpb24gaXMgb25seSBhdmFpbGFibGUgZm9yIG1vYmlsZSwgbm90IGRlc2t0b3AuXG4gICAgICovXG4gICAgbW9iaWxlX2lmcmFtZT86IHN0cmluZztcblxuICAgIC8qKlxuICAgICAqIEEgc3RyaW5nIHNwZWNpZnlpbmcgd2hpY2ggT3BlbiBHcmFwaCBhY3Rpb24gdHlwZSB0byBwdWJsaXNoLCBlLmcuLCBvZy5saWtlcyBmb3IgdGhlIGJ1aWx0IGluIGxpa2UgdHlwZS4gVGhlIGRpYWxvZyBhbHNvIHN1cHBvcnRzIGFwcHJvdmVkIGN1c3RvbSB0eXBlcy4gUmVxdWlyZWQgd2hlbiB1c2luZyBtZXRob2Qgc2hhcmVfb3Blbl9ncmFwaC5cbiAgICAgKi9cbiAgICBhY3Rpb25fdHlwZT86IHN0cmluZztcblxuICAgIC8qKlxuICAgICAqIEEgSlNPTiBvYmplY3Qgb2Yga2V5L3ZhbHVlIHBhaXJzIHNwZWNpZnlpbmcgcGFyYW1ldGVycyB3aGljaCBjb3JyZXNwb25kIHRvIHRoZSBhY3Rpb25fdHlwZSBiZWluZyB1c2VkLiBWYWxpZCBrZXkvdmFsdWUgcGFpcnMgYXJlIHRoZSBzYW1lIHBhcmFtZXRlcnMgdGhhdCBjYW4gYmUgdXNlZCB3aGVuIHB1Ymxpc2hpbmcgT3BlbiBHcmFwaCBBY3Rpb25zIHVzaW5nIHRoZSBBUEkuIFJlcXVpcmVkIHdoZW4gdXNpbmcgbWV0aG9kIHNoYXJlX29wZW5fZ3JhcGguXG4gICAgICovXG4gICAgYWN0aW9uX3Byb3BlcnRpZXM/OiBzdHJpbmc7XG59XG5cbi8qKlxuICogUGFyYW1zIHRoYXQgY2FuIGJlIHBhc3NlZCB3aGVuIGNyZWF0aW5nIGEgRmVlZCBkaWFsb2cuXG4gKi9cbmV4cG9ydCBpbnRlcmZhY2UgRmVlZERpYWxvZ1BhcmFtcyB7XG4gICAgLyoqXG4gICAgICogVGhlIElEIG9mIHRoZSBwZXJzb24gcG9zdGluZyB0aGUgbWVzc2FnZS4gSWYgdGhpcyBpcyB1bnNwZWNpZmllZCwgaXQgZGVmYXVsdHMgdG8gdGhlIGN1cnJlbnQgcGVyc29uLiBJZiBzcGVjaWZpZWQsIGl0IG11c3QgYmUgdGhlIElEIG9mIHRoZSBwZXJzb24gb3Igb2YgYSBwYWdlIHRoYXQgdGhlIHBlcnNvbiBhZG1pbmlzdGVycy5cbiAgICAgKi9cbiAgICBmcm9tPzogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogVGhlIElEIG9mIHRoZSBwcm9maWxlIHRoYXQgdGhpcyBzdG9yeSB3aWxsIGJlIHB1Ymxpc2hlZCB0by4gSWYgdGhpcyBpcyB1bnNwZWNpZmllZCwgaXQgZGVmYXVsdHMgdG8gdGhlIHZhbHVlIG9mIGZyb20uIFRoZSBJRCBtdXN0IGJlIGEgZnJpZW5kIHdobyBhbHNvIHVzZXMgeW91ciBhcHAuXG4gICAgICovXG4gICAgdG8/OiBzdHJpbmc7XG5cbiAgICAvKipcbiAgICAgKiBUaGUgbGluayBhdHRhY2hlZCB0byB0aGlzIHBvc3QuIFdpdGggdGhlIEZlZWQgRGlhbG9nLCBwZW9wbGUgY2FuIGFsc28gc2hhcmUgcGxhaW4gdGV4dCBzdGF0dXMgdXBkYXRlcyB3aXRoIG5vIGNvbnRlbnQgZnJvbSB5b3VyIGFwcDsganVzdCBsZWF2ZSB0aGUgbGluayBwYXJhbWV0ZXIgZW1wdHkuXG4gICAgICovXG4gICAgbGluaz86IHN0cmluZztcblxuICAgIC8qKlxuICAgICAqIFRoZSBVUkwgb2YgYSBwaWN0dXJlIGF0dGFjaGVkIHRvIHRoaXMgcG9zdC4gVGhlIHBpY3R1cmUgbXVzdCBiZSBhdCBsZWFzdCAyMDBweCBieSAyMDBweC4gU2VlIG91ciBkb2N1bWVudGF0aW9uIG9uIHNoYXJpbmcgYmVzdCBwcmFjdGljZXMgZm9yIG1vcmUgaW5mb3JtYXRpb24gb24gc2l6ZXNcbiAgICAgKi9cbiAgICBwaWN0dXJlPzogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogVGhlIFVSTCBvZiBhIG1lZGlhIGZpbGUgKGVpdGhlciBTV0Ygb3IgTVAzKSBhdHRhY2hlZCB0byB0aGlzIHBvc3QuIElmIFNXRiwgeW91IG11c3QgYWxzbyBzcGVjaWZ5IHBpY3R1cmUgdG8gcHJvdmlkZSBhIHRodW1ibmFpbCBmb3IgdGhlIHZpZGVvLlxuICAgICAqL1xuICAgIHNvdXJjZT86IHN0cmluZztcblxuICAgIC8qKlxuICAgICAqIFRoZSBuYW1lIG9mIHRoZSBsaW5rIGF0dGFjaG1lbnQuXG4gICAgICovXG4gICAgbmFtZT86IHN0cmluZztcblxuICAgIC8qKlxuICAgICAqIFRoZSBjYXB0aW9uIG9mIHRoZSBsaW5rIChhcHBlYXJzIGJlbmVhdGggdGhlIGxpbmsgbmFtZSkuIElmIG5vdCBzcGVjaWZpZWQsIHRoaXMgZmllbGQgaXMgYXV0b21hdGljYWxseSBwb3B1bGF0ZWQgd2l0aCB0aGUgVVJMIG9mIHRoZSBsaW5rLlxuICAgICAqL1xuICAgIGNhcHRpb24/OiBzdHJpbmc7XG5cbiAgICAvKipcbiAgICAgKiBUaGUgZGVzY3JpcHRpb24gb2YgdGhlIGxpbmsgKGFwcGVhcnMgYmVuZWF0aCB0aGUgbGluayBjYXB0aW9uKS4gSWYgbm90IHNwZWNpZmllZCwgdGhpcyBmaWVsZCBpcyBhdXRvbWF0aWNhbGx5IHBvcHVsYXRlZCBieSBpbmZvcm1hdGlvbiBzY3JhcGVkIGZyb20gdGhlIGxpbmssIHR5cGljYWxseSB0aGUgdGl0bGUgb2YgdGhlIHBhZ2UuXG4gICAgICovXG4gICAgZGVzY3JpcHRpb24/OiBzdHJpbmc7XG5cbiAgICAvKipcbiAgICAgKiBUaGlzIGFyZ3VtZW50IGlzIGEgY29tbWEtc2VwYXJhdGVkIGxpc3QsIGNvbnNpc3Rpbmcgb2YgYXQgbW9zdCA1IGRpc3RpbmN0IGl0ZW1zLCBlYWNoIG9mIGxlbmd0aCBhdCBsZWFzdCAxIGFuZCBhdCBtb3N0IDE1IGNoYXJhY3RlcnMgZHJhd24gZnJvbSB0aGUgc2V0ICcwMTIzNDU2Nzg5QUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVphYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5el8nLiBFYWNoIGNhdGVnb3J5IGlzIHVzZWQgaW4gRmFjZWJvb2sgSW5zaWdodHMgdG8gaGVscCB5b3UgbWVhc3VyZSB0aGUgcGVyZm9ybWFuY2Ugb2YgZGlmZmVyZW50IHR5cGVzIG9mIHBvc3RcbiAgICAgKi9cbiAgICByZWY/OiBzdHJpbmc7XG59XG5cbi8qKlxuICogUGFyYW1zIHRoYXQgY2FuIGJlIHBhc3NlZCB3aGVuIGNyZWF0aW5nIGEgc2VuZCBkaWFsb2dcbiAqL1xuZXhwb3J0IGludGVyZmFjZSBTZW5kRGlhbG9nUGFyYW1zIHtcblxuICAgIC8qKlxuICAgICAqIEEgdXNlciBJRCBvZiBhIHJlY2lwaWVudC4gT25jZSB0aGUgZGlhbG9nIGNvbWVzIHVwLCB0aGUgc2VuZGVyIGNhbiBzcGVjaWZ5IGFkZGl0aW9uYWwgcGVvcGxlIGFzIHJlY2lwaWVudHMuXG4gICAgICovXG4gICAgdG8/OiBzdHJpbmc7XG5cbiAgICAvKipcbiAgICAgKiBSZXF1aXJlZCBwYXJhbWV0ZXIuIFRoZSBVUkwgdGhhdCBpcyBiZWluZyBzZW50IGluIHRoZSBtZXNzYWdlLlxuICAgICAqL1xuICAgIGxpbms/OiBzdHJpbmc7XG59XG5cbi8qKlxuICogVGhlIG9iamVjdCBwYXNzZWQgdG8gdGhlIFt1aV0oLi4vZmFjZWJvb2stc2VydmljZS8jdWkpIG1ldGhvZC4gVGhlIHByb3BlcnRpZXMgYmVsb3cgY2FuIGJlIHBhc3NlZCB0byBhbGwgdGhyZWUgdHlwZXMgb2YgZGlhbG9ncy4gRWFjaCB0eXBlIG9mIGRpYWxvZyBhY2NlcHRzIGFkZGl0aW9uYWwgcGFyYW1ldGVycyB0aGF0IGFyZSBkb2N1bWVudGVkIGFib3ZlLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIFVJUGFyYW1zIGV4dGVuZHMgU2hhcmVEaWFsb2dQYXJhbXMsIEZlZWREaWFsb2dQYXJhbXMsIFNlbmREaWFsb2dQYXJhbXMge1xuXG4gICAgLyoqXG4gICAgICogVGhlIFVJIGRpYWxvZyB0aGF0IGlzIGJlaW5nIGludm9rZWQuIFRoaXMgaXMgYSByZXF1aXJlZCBmaWVsZC5cbiAgICAgKi9cbiAgICBtZXRob2Q6IGFueTtcblxuICAgIC8qKlxuICAgICAqIFRoZSBVSSBkaWFsb2cgbWVzc2FnZS5cbiAgICAgKi9cbiAgICBtZXNzYWdlPzogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogWW91ciBhcHAncyB1bmlxdWUgaWRlbnRpZmllci4gUmVxdWlyZWQuXG4gICAgICovXG4gICAgYXBwX2lkPzogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogVGhlIFVSTCB0byByZWRpcmVjdCB0byBhZnRlciBhIHBlcnNvbiBjbGlja3MgYSBidXR0b24gb24gdGhlIGRpYWxvZy4gUmVxdWlyZWQgd2hlbiB1c2luZyBVUkwgcmVkaXJlY3Rpb24uXG4gICAgICovXG4gICAgcmVkaXJlY3RfdXJpPzogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogRGV0ZXJtaW5lcyBob3cgdGhlIGRpYWxvZyBpcyByZW5kZXJlZC5cbiAgICAgKiAtIElmIHlvdSBhcmUgdXNpbmcgdGhlIFVSTCByZWRpcmVjdCBkaWFsb2cgaW1wbGVtZW50YXRpb24sIHRoZW4gdGhpcyB3aWxsIGJlIGEgZnVsbCBwYWdlIGRpc3BsYXksIHNob3duIHdpdGhpbiBGYWNlYm9vay5jb20uIFRoaXMgZGlzcGxheSB0eXBlIGlzIGNhbGxlZCBwYWdlXG4gICAgICogLSBJZiB5b3UgYXJlIHVzaW5nIG9uZSBvZiBvdXIgaU9TIG9yIEFuZHJvaWQgU0RLcyB0byBpbnZva2UgdGhlIGRpYWxvZywgdGhpcyBpcyBhdXRvbWF0aWNhbGx5IHNwZWNpZmllZCBhbmQgY2hvb3NlcyBhbiBhcHByb3ByaWF0ZSBkaXNwbGF5IHR5cGUgZm9yIHRoZSBkZXZpY2UuXG4gICAgICogLSBJZiB5b3UgYXJlIHVzaW5nIHRoZSBGYWNlYm9vayBTREsgZm9yIEphdmFTY3JpcHQsIHRoaXMgd2lsbCBkZWZhdWx0IHRvIGEgbW9kYWwgaWZyYW1lIHR5cGUgZm9yIHBlb3BsZSBsb2dnZWQgaW50byB5b3VyIGFwcCBvciBhc3luYyB3aGVuIHVzaW5nIHdpdGhpbiBhIGdhbWUgb24gRmFjZWJvb2suY29tLCBhbmQgYSBwb3B1cCB3aW5kb3cgZm9yIGV2ZXJ5b25lIGVsc2UuIFlvdSBjYW4gYWxzbyBmb3JjZSB0aGUgcG9wdXAgdHlwZSB3aGVuIHdoZW4geW91IHVzZSB0aGUgRmFjZWJvb2sgU0RLIGZvciBKYXZhU2NyaXB0LCBpZiBuZWNlc3NhcnkuXG4gICAgICogLSBNb2JpbGUgd2ViIGFwcHMgd2lsbCBhbHdheXMgZGVmYXVsdCB0byB0aGUgdG91Y2ggZGlzcGxheSB0eXBlLlxuICAgICAqL1xuICAgIGRpc3BsYXk/OiBzdHJpbmc7XG5cbiAgICAvKipcbiAgICAgKiAtIERpYWxvZyBpbiBjcmVhdGUgcGhhc2UgYWxsb3dzIHlvdSB0byBnZXQgc3RyZWFtIHVybCB0byB1cGxvYWQgdmlkZW87IERpYWxvZyBpbiBwdWJsaXNoIHBoYXNlIHdpbGwgcHJvdmlkZSBwcmV2aWV3IGFuZCBnbyBsaXZlIGJ1dHRvbi4gcmVxdWlyZWRcbiAgICAgKiAtIHBoYXNlID0gY3JlYXRlIHwgcHVibGlzaFxuICAgICAqL1xuICAgIHBoYXNlPzogc3RyaW5nO1xuXG4gICAgLyoqXG4gICAgICogLSBUaGlzIHBhcmFtZXRlciBpcyByZXF1aXJlZCBmb3IgcHVibGlzaCBwaGFzZS5cbiAgICAgKiAtIFRoZSByZXNwb25zZSBvYmplY3QgcmV0dXJuZWQgZnJvbSBlaXRoZXIgQVBJIG9yIGNyZWF0ZSBwaGFzZS5cbiAgICAgKi9cbiAgICBicm9hZGNhc3RfZGF0YT86IGFueTtcblxufVxuIl19