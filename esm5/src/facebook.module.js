/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from '@angular/core';
import { FacebookService } from './providers/facebook';
import { FBCommentEmbedComponent } from './components/fb-comment-embed/fb-comment-embed';
import { FBCommentsComponent } from './components/fb-comments/fb-comments';
import { FBFollowComponent } from './components/fb-follow/fb-follow';
import { FBLikeComponent } from './components/fb-like/fb-like';
import { FBPageComponent } from './components/fb-page/fb-page';
import { FBPostComponent } from './components/fb-post/fb-post';
import { FBQuoteComponent } from './components/fb-quote/fb-quote';
import { FBSaveComponent } from './components/fb-save/fb-save';
import { FBSendComponent } from './components/fb-send/fb-send';
import { FBShareComponent } from './components/fb-share/fb-share';
import { FBVideoComponent } from './components/fb-video/fb-video';
var /** @type {?} */ components = [
    FBCommentEmbedComponent,
    FBCommentsComponent,
    FBFollowComponent,
    FBLikeComponent,
    FBPageComponent,
    FBPostComponent,
    FBQuoteComponent,
    FBSaveComponent,
    FBSendComponent,
    FBShareComponent,
    FBVideoComponent
];
/**
 * @return {?}
 */
export function getComponents() {
    return components;
}
/**
 * \@shortdesc The module to import to add this library
 * \@description
 * The main module to import into your application.
 * You only need to import this module if you wish to use the components in this library; otherwise, you could just import [FacebookService](../facebook-service) into your module instead.
 * This module will make all components and providers available in your application.
 *
 * \@usage
 * In order to use this library, you need to import `FacebookModule` into your app's main `NgModule`.
 *
 * ```typescript
 * import { FacebookModule } from 'ng2-facebook-sdk';
 *
 * \@NgModule({
 *   ...
 *   imports: [
 *     ...
 *     FacebookModule.forRoot()
 *   ],
 *   ...
 * })
 * export class AppModule { }
 * ```
 */
var FacebookModule = /** @class */ (function () {
    function FacebookModule() {
    }
    /**
     * @return {?}
     */
    FacebookModule.forRoot = /**
     * @return {?}
     */
    function () {
        return {
            ngModule: FacebookModule,
            providers: [FacebookService]
        };
    };
    FacebookModule.decorators = [
        { type: NgModule, args: [{
                    declarations: getComponents(),
                    exports: getComponents()
                },] }
    ];
    return FacebookModule;
}());
export { FacebookModule };
function FacebookModule_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    FacebookModule.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    FacebookModule.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZWJvb2subW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWZhY2Vib29rLyIsInNvdXJjZXMiOlsic3JjL2ZhY2Vib29rLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBdUIsTUFBTSxlQUFlLENBQUM7QUFFOUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXZELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDbEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFbEUscUJBQU0sVUFBVSxHQUFVO0lBQ3hCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixlQUFlO0lBQ2YsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixnQkFBZ0I7Q0FDakIsQ0FBQzs7OztBQUVGLE1BQU07SUFDSixNQUFNLENBQUMsVUFBVSxDQUFDO0NBQ25COzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBK0JRLHNCQUFPOzs7SUFBZDtRQUNFLE1BQU0sQ0FBQztZQUNMLFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFNBQVMsRUFBRSxDQUFDLGVBQWUsQ0FBQztTQUM3QixDQUFDO0tBQ0g7O2dCQVZGLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsYUFBYSxFQUFFO29CQUM3QixPQUFPLEVBQUUsYUFBYSxFQUFFO2lCQUN6Qjs7eUJBN0REOztTQThEYSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRmFjZWJvb2tTZXJ2aWNlIH0gZnJvbSAnLi9wcm92aWRlcnMvZmFjZWJvb2snO1xuXG5pbXBvcnQgeyBGQkNvbW1lbnRFbWJlZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1jb21tZW50LWVtYmVkL2ZiLWNvbW1lbnQtZW1iZWQnO1xuaW1wb3J0IHsgRkJDb21tZW50c0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1jb21tZW50cy9mYi1jb21tZW50cyc7XG5pbXBvcnQgeyBGQkZvbGxvd0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1mb2xsb3cvZmItZm9sbG93JztcbmltcG9ydCB7IEZCTGlrZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1saWtlL2ZiLWxpa2UnO1xuaW1wb3J0IHsgRkJQYWdlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLXBhZ2UvZmItcGFnZSc7XG5pbXBvcnQgeyBGQlBvc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItcG9zdC9mYi1wb3N0JztcbmltcG9ydCB7IEZCUXVvdGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItcXVvdGUvZmItcXVvdGUnO1xuaW1wb3J0IHsgRkJTYXZlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLXNhdmUvZmItc2F2ZSc7XG5pbXBvcnQgeyBGQlNlbmRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItc2VuZC9mYi1zZW5kJztcbmltcG9ydCB7IEZCU2hhcmVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItc2hhcmUvZmItc2hhcmUnO1xuaW1wb3J0IHsgRkJWaWRlb0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi12aWRlby9mYi12aWRlbyc7XG5cbmNvbnN0IGNvbXBvbmVudHM6IGFueVtdID0gW1xuICBGQkNvbW1lbnRFbWJlZENvbXBvbmVudCxcbiAgRkJDb21tZW50c0NvbXBvbmVudCxcbiAgRkJGb2xsb3dDb21wb25lbnQsXG4gIEZCTGlrZUNvbXBvbmVudCxcbiAgRkJQYWdlQ29tcG9uZW50LFxuICBGQlBvc3RDb21wb25lbnQsXG4gIEZCUXVvdGVDb21wb25lbnQsXG4gIEZCU2F2ZUNvbXBvbmVudCxcbiAgRkJTZW5kQ29tcG9uZW50LFxuICBGQlNoYXJlQ29tcG9uZW50LFxuICBGQlZpZGVvQ29tcG9uZW50XG5dO1xuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q29tcG9uZW50cygpIHtcbiAgcmV0dXJuIGNvbXBvbmVudHM7XG59XG5cbi8qKlxuICogQHNob3J0ZGVzYyBUaGUgbW9kdWxlIHRvIGltcG9ydCB0byBhZGQgdGhpcyBsaWJyYXJ5XG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBtYWluIG1vZHVsZSB0byBpbXBvcnQgaW50byB5b3VyIGFwcGxpY2F0aW9uLlxuICogWW91IG9ubHkgbmVlZCB0byBpbXBvcnQgdGhpcyBtb2R1bGUgaWYgeW91IHdpc2ggdG8gdXNlIHRoZSBjb21wb25lbnRzIGluIHRoaXMgbGlicmFyeTsgb3RoZXJ3aXNlLCB5b3UgY291bGQganVzdCBpbXBvcnQgW0ZhY2Vib29rU2VydmljZV0oLi4vZmFjZWJvb2stc2VydmljZSkgaW50byB5b3VyIG1vZHVsZSBpbnN0ZWFkLlxuICogVGhpcyBtb2R1bGUgd2lsbCBtYWtlIGFsbCBjb21wb25lbnRzIGFuZCBwcm92aWRlcnMgYXZhaWxhYmxlIGluIHlvdXIgYXBwbGljYXRpb24uXG4gKlxuICogQHVzYWdlXG4gKiBJbiBvcmRlciB0byB1c2UgdGhpcyBsaWJyYXJ5LCB5b3UgbmVlZCB0byBpbXBvcnQgYEZhY2Vib29rTW9kdWxlYCBpbnRvIHlvdXIgYXBwJ3MgbWFpbiBgTmdNb2R1bGVgLlxuICpcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IEZhY2Vib29rTW9kdWxlIH0gZnJvbSAnbmcyLWZhY2Vib29rLXNkayc7XG4gKlxuICogQE5nTW9kdWxlKHtcbiAqICAgLi4uXG4gKiAgIGltcG9ydHM6IFtcbiAqICAgICAuLi5cbiAqICAgICBGYWNlYm9va01vZHVsZS5mb3JSb290KClcbiAqICAgXSxcbiAqICAgLi4uXG4gKiB9KVxuICogZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cbiAqIGBgYFxuICovXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IGdldENvbXBvbmVudHMoKSxcbiAgZXhwb3J0czogZ2V0Q29tcG9uZW50cygpXG59KVxuZXhwb3J0IGNsYXNzIEZhY2Vib29rTW9kdWxlIHtcbiAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBGYWNlYm9va01vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW0ZhY2Vib29rU2VydmljZV1cbiAgICB9O1xuICB9XG59XG4iXX0=