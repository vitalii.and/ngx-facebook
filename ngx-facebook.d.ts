/**
 * Generated bundle index. Do not edit.
 */
export * from './index';
export { FBMLAttribute as ɵb, FBMLComponent as ɵd, FBMLInstanceMethod as ɵc } from './src/components/fbml-component';
export { getComponents as ɵa } from './src/facebook.module';
