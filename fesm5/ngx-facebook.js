import { __extends, __decorate, __metadata } from 'tslib';
import { Component, Input, ElementRef, Renderer, Output, EventEmitter, Injectable, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @hidden
 * @param {?} target
 * @param {?} key
 * @return {?}
 */
function FBMLAttribute(target, key) {
    var /** @type {?} */ processKey = function (_k) { return 'data-' + _k.toString().replace(/([a-z\d])([A-Z])/g, '$1-$2').toLowerCase(); };
    Object.defineProperty(target, key, {
        set: function (value) {
            value = value.toString();
            this.setAttribute(processKey(key), value);
        },
        get: function () {
            return this.getAttribute(processKey(key));
        },
        enumerable: true
    });
}
var ɵ0 = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    if (this._instance) {
        return this._instance[key].apply(this._instance, args);
    }
    else {
        console.warn('ng2-facebook-sdk: tried calling instance method before component is ready.');
        return null;
    }
};
/**
 * @hidden
 * @param {?} target
 * @param {?} key
 * @return {?}
 */
function FBMLInstanceMethod(target, key) {
    return {
        enumerable: true,
        value: ɵ0
    };
}
/**
 * @hidden
 */
var  /**
 * @hidden
 */
FBMLComponent = /** @class */ (function () {
    function FBMLComponent(el, rnd, fbClass) {
        this.el = el;
        this.rnd = rnd;
        this.fbClass = fbClass;
        this.nativeElement = this.el.nativeElement;
        this.rnd.setElementClass(this.nativeElement, this.fbClass, true);
    }
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    FBMLComponent.prototype.setAttribute = /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    function (name, value) {
        if (!name || !value)
            return;
        this.rnd.setElementAttribute(this.nativeElement, name, value);
    };
    /**
     * @param {?} name
     * @return {?}
     */
    FBMLComponent.prototype.getAttribute = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        if (!name)
            return;
        return this.nativeElement.getAttribute(name);
    };
    return FBMLComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Embedded Comments
 * \@shortdesc Embedded comments component
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-comments
 * \@description
 * Embedded comments are a simple way to put public post comments - by a Page or a person on Facebook - into the content of your web site or web page.
 * Only public comments from Facebook Pages and profiles can be embedded.
 * \@usage
 * ```html
 * <fb-comment-embed href="https://www.facebook.com/zuck/posts/10102735452532991?comment_id=1070233703036185" width="500"></fb-comment-embed>
 * ```
 */
var FBCommentEmbedComponent = /** @class */ (function (_super) {
    __extends(FBCommentEmbedComponent, _super);
    function FBCommentEmbedComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-comment-embed') || this;
    }
    FBCommentEmbedComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-comment-embed',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBCommentEmbedComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBCommentEmbedComponent.propDecorators = {
        "href": [{ type: Input },],
        "width": [{ type: Input },],
        "includeParent": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBCommentEmbedComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBCommentEmbedComponent.prototype, "width", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBCommentEmbedComponent.prototype, "includeParent", void 0);
    return FBCommentEmbedComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Comments
 * \@shortdesc Comments component
 * \@fbdoc https://developers.facebook.com/docs/plugins/comments
 * \@description
 * The comments plugin lets people comment on content on your site using their Facebook account.
 * People can choose to share their comment activity with their friends (and friends of their friends) on Facebook as well.
 * The comments plugin also includes built-in moderation tools and social relevance ranking.
 *
 * \@usage
 * ```html
 * <fb-comments></fb-comments>
 * ```
 */
var FBCommentsComponent = /** @class */ (function (_super) {
    __extends(FBCommentsComponent, _super);
    function FBCommentsComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-comments') || this;
        /**
         * The absolute URL that comments posted in the plugin will be permanently associated with.
         * All stories shared on Facebook about comments posted using the comments plugin will link to this URL.
         * Defaults to current URL.
         */
        _this.href = window.location.href;
        return _this;
    }
    FBCommentsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-comments',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBCommentsComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBCommentsComponent.propDecorators = {
        "colorscheme": [{ type: Input },],
        "href": [{ type: Input },],
        "mobile": [{ type: Input },],
        "numposts": [{ type: Input },],
        "orderBy": [{ type: Input },],
        "width": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBCommentsComponent.prototype, "colorscheme", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBCommentsComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBCommentsComponent.prototype, "mobile", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Number)
    ], FBCommentsComponent.prototype, "numposts", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBCommentsComponent.prototype, "orderBy", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBCommentsComponent.prototype, "width", void 0);
    return FBCommentsComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Follow Button
 * \@shortdesc Follow button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/follow-button
 * \@description The Follow button lets people subscribe to the public updates of others on Facebook.
 * \@usage
 * ```html
 * <fb-follow href="https://www.facebook.com/zuck"></fb-follow>
 * ```
 */
var FBFollowComponent = /** @class */ (function (_super) {
    __extends(FBFollowComponent, _super);
    function FBFollowComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-follow') || this;
    }
    FBFollowComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-follow',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBFollowComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBFollowComponent.propDecorators = {
        "colorScheme": [{ type: Input },],
        "href": [{ type: Input },],
        "kidDirectedSite": [{ type: Input },],
        "layout": [{ type: Input },],
        "showFaces": [{ type: Input },],
        "size": [{ type: Input },],
        "width": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBFollowComponent.prototype, "colorScheme", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBFollowComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBFollowComponent.prototype, "kidDirectedSite", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBFollowComponent.prototype, "layout", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBFollowComponent.prototype, "showFaces", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBFollowComponent.prototype, "size", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBFollowComponent.prototype, "width", void 0);
    return FBFollowComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Like Button
 * \@shortdesc Like button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/like-button
 * \@description
 * A single click on the Like button will 'like' pieces of content on the web and share them on Facebook.
 * You can also display a Share button next to the Like button to let people add a personal message and customize who they share with.
 * \@usage
 * ```html
 * <fb-like href="https://www.facebook.com/zuck"></fb-like>
 * ```
 */
var FBLikeComponent = /** @class */ (function (_super) {
    __extends(FBLikeComponent, _super);
    function FBLikeComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-like') || this;
        /**
         * The absolute URL of the page that will be liked.
         * Defaults to the current URL.
         */
        _this.href = window.location.href;
        return _this;
    }
    FBLikeComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-like',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBLikeComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBLikeComponent.propDecorators = {
        "action": [{ type: Input },],
        "colorScheme": [{ type: Input },],
        "href": [{ type: Input },],
        "kidDirectedSite": [{ type: Input },],
        "layout": [{ type: Input },],
        "ref": [{ type: Input },],
        "share": [{ type: Input },],
        "showFaces": [{ type: Input },],
        "size": [{ type: Input },],
        "width": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBLikeComponent.prototype, "action", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBLikeComponent.prototype, "colorScheme", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBLikeComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBLikeComponent.prototype, "kidDirectedSite", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBLikeComponent.prototype, "layout", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBLikeComponent.prototype, "ref", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBLikeComponent.prototype, "share", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBLikeComponent.prototype, "showFaces", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBLikeComponent.prototype, "size", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBLikeComponent.prototype, "width", void 0);
    return FBLikeComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Page Plugin
 * \@shortdesc Page plugin component
 * \@fbdoc https://developers.facebook.com/docs/plugins/page-plugin
 * \@description
 * The Page plugin lets you easily embed and promote any Facebook Page on your website. Just like on Facebook, your visitors can like and share the Page without leaving your site.
 * \@usage
 * ```html
 * <fb-page href="https://facebook.com/facebook"></fb-page>
 * ```
 */
var FBPageComponent = /** @class */ (function (_super) {
    __extends(FBPageComponent, _super);
    function FBPageComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-page') || this;
    }
    FBPageComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-page',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBPageComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBPageComponent.propDecorators = {
        "href": [{ type: Input },],
        "width": [{ type: Input },],
        "height": [{ type: Input },],
        "tabs": [{ type: Input },],
        "hideCover": [{ type: Input },],
        "showFacepile": [{ type: Input },],
        "hideCTA": [{ type: Input },],
        "smallHeader": [{ type: Input },],
        "adaptContainerWidth": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBPageComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Number)
    ], FBPageComponent.prototype, "width", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Number)
    ], FBPageComponent.prototype, "height", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBPageComponent.prototype, "tabs", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBPageComponent.prototype, "hideCover", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBPageComponent.prototype, "showFacepile", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBPageComponent.prototype, "hideCTA", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBPageComponent.prototype, "smallHeader", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBPageComponent.prototype, "adaptContainerWidth", void 0);
    return FBPageComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Embedded Posts
 * \@shortdesc Embedded post component
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-posts
 * \@description
 * Embedded Posts are a simple way to put public posts - by a Page or a person on Facebook - into the content of your web site or web page.
 * Only public posts from Facebook Pages and profiles can be embedded.
 * \@usage
 * ```html
 * <fb-post href="https://www.facebook.com/20531316728/posts/10154009990506729/"></fb-post>
 * ```
 */
var FBPostComponent = /** @class */ (function (_super) {
    __extends(FBPostComponent, _super);
    function FBPostComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-post') || this;
    }
    FBPostComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-post',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBPostComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBPostComponent.propDecorators = {
        "href": [{ type: Input },],
        "width": [{ type: Input },],
        "showText": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBPostComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBPostComponent.prototype, "width", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBPostComponent.prototype, "showText", void 0);
    return FBPostComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Quote Plugin
 * \@shortdesc Quote plugin component
 * \@fbdoc https://developers.facebook.com/docs/plugins/quote
 * \@description
 * The quote plugin lets people select text on your page and add it to their share, so they can tell a more expressive story.
 * Note that you do not need to implement Facebook login or request any additional permissions through app review in order to use this plugin.
 * \@usage
 * ```html
 * <fb-quote></fb-quote>
 * ```
 */
var FBQuoteComponent = /** @class */ (function (_super) {
    __extends(FBQuoteComponent, _super);
    function FBQuoteComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-quote') || this;
    }
    FBQuoteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-quote',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBQuoteComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBQuoteComponent.propDecorators = {
        "href": [{ type: Input },],
        "layout": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBQuoteComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBQuoteComponent.prototype, "layout", void 0);
    return FBQuoteComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Save Button
 * \@shortdesc Save button component.
 * \@fbdoc https://developers.facebook.com/docs/plugins/save
 * \@description
 * The Save button lets people save items or services to a private list on Facebook, share it with friends, and receive relevant notifications.
 * \@usage
 * ```html
 * <fb-save uri="https://github.com/zyra/ng2-facebook-sdk/"></fb-save>
 * ```
 */
var FBSaveComponent = /** @class */ (function (_super) {
    __extends(FBSaveComponent, _super);
    function FBSaveComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-save') || this;
        /**
         * The absolute link of the page that will be saved.
         * Current Link/Address
         */
        _this.uri = window.location.href;
        return _this;
    }
    FBSaveComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-save',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBSaveComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBSaveComponent.propDecorators = {
        "uri": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBSaveComponent.prototype, "uri", void 0);
    return FBSaveComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Send Button
 * \@shortdesc Send button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/send-button
 * \@description
 * The Send button lets people privately send content on your site to one or more friends in a Facebook message.
 * \@usage
 * ```html
 * <fb-send href="https://github.com/zyra/ng2-facebook-sdk/"></fb-send>
 * ```
 */
var FBSendComponent = /** @class */ (function (_super) {
    __extends(FBSendComponent, _super);
    function FBSendComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-send') || this;
        /**
         * The absolute URL of the page that will be sent. Defaults to the current URL.
         */
        _this.href = window.location.href;
        return _this;
    }
    FBSendComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-send',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBSendComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBSendComponent.propDecorators = {
        "colorScheme": [{ type: Input },],
        "href": [{ type: Input },],
        "kidDirectedSite": [{ type: Input },],
        "ref": [{ type: Input },],
        "size": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBSendComponent.prototype, "colorScheme", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBSendComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBSendComponent.prototype, "kidDirectedSite", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBSendComponent.prototype, "ref", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBSendComponent.prototype, "size", void 0);
    return FBSendComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Share Button
 * \@shortdesc Share button component
 * \@fbdoc https://developers.facebook.com/docs/plugins/share-button
 * \@description
 * The Share button lets people add a personalized message to links before sharing on their timeline, in groups, or to their friends via a Facebook Message.
 * \@usage
 * ```html
 * <fb-share href="https://github.com/zyra/ng2-facebook-sdk/"></fb-share>
 * ```
 */
var FBShareComponent = /** @class */ (function (_super) {
    __extends(FBShareComponent, _super);
    function FBShareComponent(el, rnd) {
        return _super.call(this, el, rnd, 'fb-share-button') || this;
    }
    FBShareComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-share',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBShareComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBShareComponent.propDecorators = {
        "href": [{ type: Input },],
        "layout": [{ type: Input },],
        "mobileIframe": [{ type: Input },],
        "size": [{ type: Input },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBShareComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBShareComponent.prototype, "layout", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBShareComponent.prototype, "mobileIframe", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBShareComponent.prototype, "size", void 0);
    return FBShareComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@name Embedded Video
 * \@shortdesc Component to embed Facebook videos
 * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-video-player
 * \@description Component to embed Facebook videos and control them.
 * \@usage
 * ```html
 * <!-- basic usage -->
 * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/"></fb-video>
 *
 * <!-- event emitters -->
 * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/" (paused)="onVideoPaused($event)"></fb-video>
 * ```
 *
 * To manually interact with the video player, fetch it using `ViewChild`.
 *
 * ```ts
 * import { Component, ViewChild } from '\@angular/core';
 * import { FBVideoComponent } from 'ng2-facebook-sdk';
 *
 * \@Component(...)
 * export class MyComponent {
 *
 *   \@ViewChild(FBVideoComponent) video: FBVideoComponent;
 *
 *   ngAfterViewInit() {
 *     this.video.play();
 *     this.video.pause();
 *     this.video.getVolume();
 *   }
 *
 * }
 *
 * ```
 */
var FBVideoComponent = /** @class */ (function (_super) {
    __extends(FBVideoComponent, _super);
    function FBVideoComponent(el, rnd) {
        var _this = _super.call(this, el, rnd, 'fb-video') || this;
        /**
         * Fired when video starts to play.
         */
        _this.startedPlaying = new EventEmitter();
        /**
         * Fired when video pauses.
         */
        _this.paused = new EventEmitter();
        /**
         *
         * Fired when video finishes playing.
         */
        _this.finishedPlaying = new EventEmitter();
        /**
         * Fired when video starts to buffer.
         */
        _this.startedBuffering = new EventEmitter();
        /**
         * Fired when video recovers from buffering.
         */
        _this.finishedBuffering = new EventEmitter();
        /**
         * Fired when an error occurs on the video.
         */
        _this.error = new EventEmitter();
        _this._listeners = [];
        _this.nativeElement.id = _this._id = 'video-' + String(Math.floor((Math.random() * 10000) + 1));
        return _this;
    }
    /**
     * @hidden
     */
    /**
     * @hidden
     * @return {?}
     */
    FBVideoComponent.prototype.ngOnInit = /**
     * @hidden
     * @return {?}
     */
    function () {
        var _this = this;
        FB.Event.subscribe('xfbml.ready', function (msg) {
            if (msg.type === 'video' && msg.id === _this._id) {
                _this._instance = msg.instance;
                _this._listeners.push(_this._instance.subscribe('startedPlaying', function (e) { return _this.startedPlaying.emit(e); }), _this._instance.subscribe('paused', function (e) { return _this.paused.emit(e); }), _this._instance.subscribe('finishedPlaying', function (e) { return _this.finishedPlaying.emit(e); }), _this._instance.subscribe('startedBuffering', function (e) { return _this.startedBuffering.emit(e); }), _this._instance.subscribe('finishedBuffering', function (e) { return _this.finishedBuffering.emit(e); }), _this._instance.subscribe('error', function (e) { return _this.error.emit(e); }));
            }
        });
    };
    /**
     * @hidden
     */
    /**
     * @hidden
     * @return {?}
     */
    FBVideoComponent.prototype.ngOnDestroy = /**
     * @hidden
     * @return {?}
     */
    function () {
        this._listeners.forEach(function (l) {
            if (typeof l.release === 'function') {
                l.release();
            }
        });
    };
    /**
     * Plays the video.
     */
    /**
     * Plays the video.
     * @return {?}
     */
    FBVideoComponent.prototype.play = /**
     * Plays the video.
     * @return {?}
     */
    function () { };
    /**
     * Pauses the video.
     */
    /**
     * Pauses the video.
     * @return {?}
     */
    FBVideoComponent.prototype.pause = /**
     * Pauses the video.
     * @return {?}
     */
    function () { };
    /**
     * Seeks to specified position.
     * @param seconds {number}
     */
    /**
     * Seeks to specified position.
     * @param {?} seconds {number}
     * @return {?}
     */
    FBVideoComponent.prototype.seek = /**
     * Seeks to specified position.
     * @param {?} seconds {number}
     * @return {?}
     */
    function (seconds) { };
    /**
     * Mute the video.
     */
    /**
     * Mute the video.
     * @return {?}
     */
    FBVideoComponent.prototype.mute = /**
     * Mute the video.
     * @return {?}
     */
    function () { };
    /**
     * Unmute the video.
     */
    /**
     * Unmute the video.
     * @return {?}
     */
    FBVideoComponent.prototype.unmute = /**
     * Unmute the video.
     * @return {?}
     */
    function () { };
    /**
     * Returns true if video is muted, false if not.
     */
    /**
     * Returns true if video is muted, false if not.
     * @return {?}
     */
    FBVideoComponent.prototype.isMuted = /**
     * Returns true if video is muted, false if not.
     * @return {?}
     */
    function () { return; };
    /**
     * Set the volume
     * @param volume
     */
    /**
     * Set the volume
     * @param {?} volume
     * @return {?}
     */
    FBVideoComponent.prototype.setVolume = /**
     * Set the volume
     * @param {?} volume
     * @return {?}
     */
    function (volume) { };
    /**
     * Get the volume
     */
    /**
     * Get the volume
     * @return {?}
     */
    FBVideoComponent.prototype.getVolume = /**
     * Get the volume
     * @return {?}
     */
    function () { return; };
    /**
     * Returns the current video time position in seconds
     */
    /**
     * Returns the current video time position in seconds
     * @return {?}
     */
    FBVideoComponent.prototype.getCurrentPosition = /**
     * Returns the current video time position in seconds
     * @return {?}
     */
    function () { };
    /**
     * Returns the video duration in seconds
     */
    /**
     * Returns the video duration in seconds
     * @return {?}
     */
    FBVideoComponent.prototype.getDuration = /**
     * Returns the video duration in seconds
     * @return {?}
     */
    function () { };
    FBVideoComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fb-video',
                    template: ''
                }] }
    ];
    /** @nocollapse */
    FBVideoComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Renderer, },
    ]; };
    FBVideoComponent.propDecorators = {
        "href": [{ type: Input },],
        "allowfullscreen": [{ type: Input },],
        "autoplay": [{ type: Input },],
        "width": [{ type: Input },],
        "showText": [{ type: Input },],
        "showCaptions": [{ type: Input },],
        "startedPlaying": [{ type: Output },],
        "paused": [{ type: Output },],
        "finishedPlaying": [{ type: Output },],
        "startedBuffering": [{ type: Output },],
        "finishedBuffering": [{ type: Output },],
        "error": [{ type: Output },],
    };
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBVideoComponent.prototype, "href", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "allowfullscreen", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "autoplay", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", String)
    ], FBVideoComponent.prototype, "width", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "showText", void 0);
    __decorate([
        FBMLAttribute,
        __metadata("design:type", Boolean)
    ], FBVideoComponent.prototype, "showCaptions", void 0);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "play", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "pause", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Number]),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "seek", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "mute", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "unmute", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Boolean)
    ], FBVideoComponent.prototype, "isMuted", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Number]),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "setVolume", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Number)
    ], FBVideoComponent.prototype, "getVolume", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "getCurrentPosition", null);
    __decorate([
        FBMLInstanceMethod,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FBVideoComponent.prototype, "getDuration", null);
    return FBVideoComponent;
}(FBMLComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * \@shortdesc
 * Angular 2 service to inject to use Facebook's SDK
 * \@description
 * You only need to inject this service in your application if you aren't using [`FacebookModule`](../facebook-module).
 * \@usage
 * ```typescript
 * import { FacebookService, InitParams } from 'ng2-facebook-sdk';
 *
 * constructor(private fb: FacebookService) {
 *
 *   const params: InitParams = {
 *
 *   };
 *
 *   fb.init(params);
 *
 * }
 * ```
 */
var FacebookService = /** @class */ (function () {
    function FacebookService() {
    }
    /**
     * This method is used to initialize and setup the SDK.
     * @param params {InitParams} Initialization parameters
     * @returns return {Promise<any>}
     */
    /**
     * This method is used to initialize and setup the SDK.
     * @param {?} params {InitParams} Initialization parameters
     * @return {?} return {Promise<any>}
     */
    FacebookService.prototype.init = /**
     * This method is used to initialize and setup the SDK.
     * @param {?} params {InitParams} Initialization parameters
     * @return {?} return {Promise<any>}
     */
    function (params) {
        try {
            return Promise.resolve(FB.init(params));
        }
        catch (/** @type {?} */ e) {
            return Promise.reject(e);
        }
    };
    /**
     * This method lets you make calls to the Graph API
     * @usage
     * ```typescript
     * this.fb.api('somepath')
     *   .then(res => console.log(res))
     *   .catch(e => console.log(e));
     * ```
     * @param path {string} The Graph API endpoint path that you want to call.
     * @param [method=get] {string} The HTTP method that you want to use for the API request.
     * @param [params] {Object} An object consisting of any parameters that you want to pass into your Graph API call.
     * @returns return {Promise<any>}
     */
    /**
     * This method lets you make calls to the Graph API
     * \@usage
     * ```typescript
     * this.fb.api('somepath')
     *   .then(res => console.log(res))
     *   .catch(e => console.log(e));
     * ```
     * @param {?} path {string} The Graph API endpoint path that you want to call.
     * @param {?=} method
     * @param {?=} params
     * @return {?} return {Promise<any>}
     */
    FacebookService.prototype.api = /**
     * This method lets you make calls to the Graph API
     * \@usage
     * ```typescript
     * this.fb.api('somepath')
     *   .then(res => console.log(res))
     *   .catch(e => console.log(e));
     * ```
     * @param {?} path {string} The Graph API endpoint path that you want to call.
     * @param {?=} method
     * @param {?=} params
     * @return {?} return {Promise<any>}
     */
    function (path, method, params) {
        if (method === void 0) { method = 'get'; }
        if (params === void 0) { params = {}; }
        return new Promise(function (resolve, reject) {
            try {
                FB.api(path, method, params, function (response) {
                    if (!response) {
                        reject();
                    }
                    else if (response.error) {
                        reject(response.error);
                    }
                    else {
                        resolve(response);
                    }
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * This method is used to trigger different forms of Facebook created UI dialogs.
     * These dialogs include:
     * - Share dialog
     * - Login dialog
     * - Add page tab dialog
     * - Requests dialog
     * - Send dialog
     * - Payments dialog
     * - Go Live dialog
     * @param params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
     * @returns return {Promise<UIResponse>}
     */
    /**
     * This method is used to trigger different forms of Facebook created UI dialogs.
     * These dialogs include:
     * - Share dialog
     * - Login dialog
     * - Add page tab dialog
     * - Requests dialog
     * - Send dialog
     * - Payments dialog
     * - Go Live dialog
     * @param {?} params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
     * @return {?} return {Promise<UIResponse>}
     */
    FacebookService.prototype.ui = /**
     * This method is used to trigger different forms of Facebook created UI dialogs.
     * These dialogs include:
     * - Share dialog
     * - Login dialog
     * - Add page tab dialog
     * - Requests dialog
     * - Send dialog
     * - Payments dialog
     * - Go Live dialog
     * @param {?} params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
     * @return {?} return {Promise<UIResponse>}
     */
    function (params) {
        return new Promise(function (resolve, reject) {
            try {
                FB.ui(params, function (response) {
                    if (!response)
                        reject();
                    else if (response.error)
                        reject(response.error);
                    else
                        resolve(response);
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * @param [forceFreshResponse=false] {boolean} Force a fresh response.
     * @returns return {Promise<LoginStatus>}
     */
    /**
     * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * @param {?=} forceFreshResponse
     * @return {?} return {Promise<LoginStatus>}
     */
    FacebookService.prototype.getLoginStatus = /**
     * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * @param {?=} forceFreshResponse
     * @return {?} return {Promise<LoginStatus>}
     */
    function (forceFreshResponse) {
        return new Promise(function (resolve, reject) {
            try {
                FB.getLoginStatus(function (response) {
                    if (!response) {
                        reject();
                    }
                    else {
                        resolve(response);
                    }
                }, forceFreshResponse);
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * Login the user
     * @usage
     * ```typescript
     * // login without options
     * this.fb.login()
     *   .then((response: LoginResponse) => console.log('Logged in', response))
     *   .catch(e => console.error('Error logging in'));
     *
     * // login with options
     * const options: LoginOptions = {
     *   scope: 'public_profile,user_friends,email,pages_show_list',
     *   return_scopes: true,
     *   enable_profile_selector: true
     * };
     * this.fb.login(options)
     *   .then(...)
     *   .catch(...);
     * ```
     * @param [options] {LoginOptions} Login options
     * @returns return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
     */
    /**
     * Login the user
     * \@usage
     * ```typescript
     * // login without options
     * this.fb.login()
     *   .then((response: LoginResponse) => console.log('Logged in', response))
     *   .catch(e => console.error('Error logging in'));
     *
     * // login with options
     * const options: LoginOptions = {
     *   scope: 'public_profile,user_friends,email,pages_show_list',
     *   return_scopes: true,
     *   enable_profile_selector: true
     * };
     * this.fb.login(options)
     *   .then(...)
     *   .catch(...);
     * ```
     * @param {?=} options
     * @return {?} return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
     */
    FacebookService.prototype.login = /**
     * Login the user
     * \@usage
     * ```typescript
     * // login without options
     * this.fb.login()
     *   .then((response: LoginResponse) => console.log('Logged in', response))
     *   .catch(e => console.error('Error logging in'));
     *
     * // login with options
     * const options: LoginOptions = {
     *   scope: 'public_profile,user_friends,email,pages_show_list',
     *   return_scopes: true,
     *   enable_profile_selector: true
     * };
     * this.fb.login(options)
     *   .then(...)
     *   .catch(...);
     * ```
     * @param {?=} options
     * @return {?} return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
     */
    function (options) {
        return new Promise(function (resolve, reject) {
            try {
                FB.login(function (response) {
                    if (response.authResponse) {
                        resolve(response);
                    }
                    else {
                        reject();
                    }
                }, options);
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * Logout the user
     * @usage
     * ```typescript
     * this.fb.logout().then(() => console.log('Logged out!'));
     * ```
     * @returns return {Promise<any>} returns a promise that resolves when the user is logged out
     */
    /**
     * Logout the user
     * \@usage
     * ```typescript
     * this.fb.logout().then(() => console.log('Logged out!'));
     * ```
     * @return {?} return {Promise<any>} returns a promise that resolves when the user is logged out
     */
    FacebookService.prototype.logout = /**
     * Logout the user
     * \@usage
     * ```typescript
     * this.fb.logout().then(() => console.log('Logged out!'));
     * ```
     * @return {?} return {Promise<any>} returns a promise that resolves when the user is logged out
     */
    function () {
        return new Promise(function (resolve, reject) {
            try {
                FB.logout(function (response) {
                    resolve(response);
                });
            }
            catch (/** @type {?} */ e) {
                reject(e);
            }
        });
    };
    /**
     * This synchronous function returns back the current authResponse.
     * @usage
     * ```typescript
     * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
     *
     * ...
     *
     * const authResponse: AuthResponse = this.fb.getAuthResponse();
     * ```
     * @returns return {AuthResponse} returns an [AuthResponse](../auth-response) object
     */
    /**
     * This synchronous function returns back the current authResponse.
     * \@usage
     * ```typescript
     * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
     *
     * ...
     *
     * const authResponse: AuthResponse = this.fb.getAuthResponse();
     * ```
     * @return {?} return {AuthResponse} returns an [AuthResponse](../auth-response) object
     */
    FacebookService.prototype.getAuthResponse = /**
     * This synchronous function returns back the current authResponse.
     * \@usage
     * ```typescript
     * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
     *
     * ...
     *
     * const authResponse: AuthResponse = this.fb.getAuthResponse();
     * ```
     * @return {?} return {AuthResponse} returns an [AuthResponse](../auth-response) object
     */
    function () {
        try {
            return /** @type {?} */ (FB.getAuthResponse());
        }
        catch (/** @type {?} */ e) {
            console.error('ng2-facebook-sdk: ', e);
        }
    };
    FacebookService.decorators = [
        { type: Injectable }
    ];
    return FacebookService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ components = [
    FBCommentEmbedComponent,
    FBCommentsComponent,
    FBFollowComponent,
    FBLikeComponent,
    FBPageComponent,
    FBPostComponent,
    FBQuoteComponent,
    FBSaveComponent,
    FBSendComponent,
    FBShareComponent,
    FBVideoComponent
];
/**
 * @return {?}
 */
function getComponents() {
    return components;
}
/**
 * \@shortdesc The module to import to add this library
 * \@description
 * The main module to import into your application.
 * You only need to import this module if you wish to use the components in this library; otherwise, you could just import [FacebookService](../facebook-service) into your module instead.
 * This module will make all components and providers available in your application.
 *
 * \@usage
 * In order to use this library, you need to import `FacebookModule` into your app's main `NgModule`.
 *
 * ```typescript
 * import { FacebookModule } from 'ng2-facebook-sdk';
 *
 * \@NgModule({
 *   ...
 *   imports: [
 *     ...
 *     FacebookModule.forRoot()
 *   ],
 *   ...
 * })
 * export class AppModule { }
 * ```
 */
var FacebookModule = /** @class */ (function () {
    function FacebookModule() {
    }
    /**
     * @return {?}
     */
    FacebookModule.forRoot = /**
     * @return {?}
     */
    function () {
        return {
            ngModule: FacebookModule,
            providers: [FacebookService]
        };
    };
    FacebookModule.decorators = [
        { type: NgModule, args: [{
                    declarations: getComponents(),
                    exports: getComponents()
                },] }
    ];
    return FacebookModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { FBCommentEmbedComponent, FBCommentsComponent, FBFollowComponent, FBLikeComponent, FBPageComponent, FBPostComponent, FBQuoteComponent, FBSaveComponent, FBSendComponent, FBShareComponent, FBVideoComponent, FacebookService, FacebookModule, FBMLAttribute as ɵb, FBMLComponent as ɵd, FBMLInstanceMethod as ɵc, getComponents as ɵa };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWZhY2Vib29rLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9uZ3gtZmFjZWJvb2svc3JjL2NvbXBvbmVudHMvZmJtbC1jb21wb25lbnQudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1jb21tZW50LWVtYmVkL2ZiLWNvbW1lbnQtZW1iZWQudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1jb21tZW50cy9mYi1jb21tZW50cy50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLWZvbGxvdy9mYi1mb2xsb3cudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1saWtlL2ZiLWxpa2UudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1wYWdlL2ZiLXBhZ2UudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1wb3N0L2ZiLXBvc3QudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1xdW90ZS9mYi1xdW90ZS50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLXNhdmUvZmItc2F2ZS50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLXNlbmQvZmItc2VuZC50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLXNoYXJlL2ZiLXNoYXJlLnRzIiwibmc6Ly9uZ3gtZmFjZWJvb2svc3JjL2NvbXBvbmVudHMvZmItdmlkZW8vZmItdmlkZW8udHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvcHJvdmlkZXJzL2ZhY2Vib29rLnRzIiwibmc6Ly9uZ3gtZmFjZWJvb2svc3JjL2ZhY2Vib29rLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG4vKipcbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIEZCTUxBdHRyaWJ1dGUodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nKSB7XG4gIGNvbnN0IHByb2Nlc3NLZXkgPSAoX2s6IHN0cmluZykgPT4gJ2RhdGEtJyArIF9rLnRvU3RyaW5nKCkucmVwbGFjZSgvKFthLXpcXGRdKShbQS1aXSkvZywgJyQxLSQyJykudG9Mb3dlckNhc2UoKTtcbiAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCB7XG4gICAgc2V0OiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgdmFsdWUgPSB2YWx1ZS50b1N0cmluZygpO1xuICAgICAgdGhpcy5zZXRBdHRyaWJ1dGUocHJvY2Vzc0tleShrZXkpLCB2YWx1ZSk7XG4gICAgfSxcbiAgICBnZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0QXR0cmlidXRlKHByb2Nlc3NLZXkoa2V5KSk7XG4gICAgfSxcbiAgICBlbnVtZXJhYmxlOiB0cnVlXG4gIH0pO1xufVxuXG4vKipcbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIEZCTUxJbnN0YW5jZU1ldGhvZCh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcpIHtcbiAgcmV0dXJuIHtcbiAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgIHZhbHVlOiBmdW5jdGlvbiguLi5hcmdzOiBhbnlbXSkge1xuICAgICAgaWYgKHRoaXMuX2luc3RhbmNlKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pbnN0YW5jZVtrZXldLmFwcGx5KHRoaXMuX2luc3RhbmNlLCBhcmdzKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUud2FybignbmcyLWZhY2Vib29rLXNkazogdHJpZWQgY2FsbGluZyBpbnN0YW5jZSBtZXRob2QgYmVmb3JlIGNvbXBvbmVudCBpcyByZWFkeS4nKTtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG4gICAgfVxuICB9O1xufVxuXG4vKipcbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IGNsYXNzIEZCTUxDb21wb25lbnQge1xuXG4gIHByb3RlY3RlZCBuYXRpdmVFbGVtZW50OiBIVE1MRWxlbWVudDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGVsOiBFbGVtZW50UmVmLFxuICAgIHByaXZhdGUgcm5kOiBSZW5kZXJlcixcbiAgICBwcml2YXRlIGZiQ2xhc3M6IHN0cmluZ1xuICApIHtcbiAgICB0aGlzLm5hdGl2ZUVsZW1lbnQgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQ7XG4gICAgdGhpcy5ybmQuc2V0RWxlbWVudENsYXNzKHRoaXMubmF0aXZlRWxlbWVudCwgdGhpcy5mYkNsYXNzLCB0cnVlKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBzZXRBdHRyaWJ1dGUobmFtZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nKSB7XG4gICAgaWYgKCFuYW1lIHx8ICF2YWx1ZSkgcmV0dXJuO1xuICAgIHRoaXMucm5kLnNldEVsZW1lbnRBdHRyaWJ1dGUodGhpcy5uYXRpdmVFbGVtZW50LCBuYW1lLCB2YWx1ZSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0QXR0cmlidXRlKG5hbWU6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgaWYgKCFuYW1lKSByZXR1cm47XG4gICAgcmV0dXJuIHRoaXMubmF0aXZlRWxlbWVudC5nZXRBdHRyaWJ1dGUobmFtZSk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgRW1iZWRkZWQgQ29tbWVudHNcbiAqIEBzaG9ydGRlc2MgRW1iZWRkZWQgY29tbWVudHMgY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZW1iZWRkZWQtY29tbWVudHNcbiAqIEBkZXNjcmlwdGlvblxuICogRW1iZWRkZWQgY29tbWVudHMgYXJlIGEgc2ltcGxlIHdheSB0byBwdXQgcHVibGljIHBvc3QgY29tbWVudHMgLSBieSBhIFBhZ2Ugb3IgYSBwZXJzb24gb24gRmFjZWJvb2sgLSBpbnRvIHRoZSBjb250ZW50IG9mIHlvdXIgd2ViIHNpdGUgb3Igd2ViIHBhZ2UuXG4gKiBPbmx5IHB1YmxpYyBjb21tZW50cyBmcm9tIEZhY2Vib29rIFBhZ2VzIGFuZCBwcm9maWxlcyBjYW4gYmUgZW1iZWRkZWQuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1jb21tZW50LWVtYmVkIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20venVjay9wb3N0cy8xMDEwMjczNTQ1MjUzMjk5MT9jb21tZW50X2lkPTEwNzAyMzM3MDMwMzYxODVcIiB3aWR0aD1cIjUwMFwiPjwvZmItY29tbWVudC1lbWJlZD5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1jb21tZW50LWVtYmVkJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCQ29tbWVudEVtYmVkQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIGNvbW1lbnQuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSB3aWR0aCBvZiB0aGUgZW1iZWRkZWQgY29tbWVudCBjb250YWluZXIuIE1pbi4gYDIyMHB4YC4gRGVmYXVsdHMgdG8gYDU2MHB4YC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNldCB0byBgdHJ1ZWAgdG8gaW5jbHVkZSBwYXJlbnQgY29tbWVudCAoaWYgVVJMIGlzIGEgcmVwbHkpLiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaW5jbHVkZVBhcmVudDogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1jb21tZW50LWVtYmVkJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgQ29tbWVudHNcbiAqIEBzaG9ydGRlc2MgQ29tbWVudHMgY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvY29tbWVudHNcbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIGNvbW1lbnRzIHBsdWdpbiBsZXRzIHBlb3BsZSBjb21tZW50IG9uIGNvbnRlbnQgb24geW91ciBzaXRlIHVzaW5nIHRoZWlyIEZhY2Vib29rIGFjY291bnQuXG4gKiBQZW9wbGUgY2FuIGNob29zZSB0byBzaGFyZSB0aGVpciBjb21tZW50IGFjdGl2aXR5IHdpdGggdGhlaXIgZnJpZW5kcyAoYW5kIGZyaWVuZHMgb2YgdGhlaXIgZnJpZW5kcykgb24gRmFjZWJvb2sgYXMgd2VsbC5cbiAqIFRoZSBjb21tZW50cyBwbHVnaW4gYWxzbyBpbmNsdWRlcyBidWlsdC1pbiBtb2RlcmF0aW9uIHRvb2xzIGFuZCBzb2NpYWwgcmVsZXZhbmNlIHJhbmtpbmcuXG4gKlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItY29tbWVudHM+PC9mYi1jb21tZW50cz5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1jb21tZW50cycsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQkNvbW1lbnRzQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBzY2hlbWUgdXNlZCBieSB0aGUgY29tbWVudHMgcGx1Z2luLiBDYW4gYmUgYGxpZ2h0YCBvciBgZGFya2AuIERlZmF1bHRzIHRvIGBsaWdodGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBjb2xvcnNjaGVtZTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIHRoYXQgY29tbWVudHMgcG9zdGVkIGluIHRoZSBwbHVnaW4gd2lsbCBiZSBwZXJtYW5lbnRseSBhc3NvY2lhdGVkIHdpdGguXG4gICAqIEFsbCBzdG9yaWVzIHNoYXJlZCBvbiBGYWNlYm9vayBhYm91dCBjb21tZW50cyBwb3N0ZWQgdXNpbmcgdGhlIGNvbW1lbnRzIHBsdWdpbiB3aWxsIGxpbmsgdG8gdGhpcyBVUkwuXG4gICAqIERlZmF1bHRzIHRvIGN1cnJlbnQgVVJMLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XG5cbiAgLyoqXG4gICAqIEEgYm9vbGVhbiB2YWx1ZSB0aGF0IHNwZWNpZmllcyB3aGV0aGVyIHRvIHNob3cgdGhlIG1vYmlsZS1vcHRpbWl6ZWQgdmVyc2lvbiBvciBub3QuIElmIG5vIHZhbHVlIGlzIGdpdmVuLCBpdCB3aWxsIGJlIGF1dG9tYXRpY2FsbHkgZGV0ZWN0ZWQuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBtb2JpbGU6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFRoZSBudW1iZXIgb2YgY29tbWVudHMgdG8gc2hvdyBieSBkZWZhdWx0LiBUaGUgbWluaW11bSB2YWx1ZSBpcyBgMWAuIERlZmF1bHRzIHRvIGAxMGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBudW1wb3N0czogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBUaGUgb3JkZXIgdG8gdXNlIHdoZW4gZGlzcGxheWluZyBjb21tZW50cy4gQ2FuIGJlIGBzb2NpYWxgLCBgcmV2ZXJzZV90aW1lYCwgb3IgYHRpbWVgLiBUaGUgZGlmZmVyZW50IG9yZGVyIHR5cGVzIGFyZSBleHBsYWluZWQgW2luIHRoZSBGQVFdKGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2NvbW1lbnRzI2ZhcW9yZGVyKS4gRGVmYXVsdHMgdG8gYHNvY2lhbGBcbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIG9yZGVyQnk6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSBjb21tZW50cyBwbHVnaW4gb24gdGhlIHdlYnBhZ2UuXG4gICAqIFRoaXMgY2FuIGJlIGVpdGhlciBhIHBpeGVsIHZhbHVlIG9yIGEgcGVyY2VudGFnZSAoc3VjaCBhcyAxMDAlKSBmb3IgZmx1aWQgd2lkdGguXG4gICAqIFRoZSBtb2JpbGUgdmVyc2lvbiBvZiB0aGUgY29tbWVudHMgcGx1Z2luIGlnbm9yZXMgdGhlIHdpZHRoIHBhcmFtZXRlciBhbmQgaW5zdGVhZCBoYXMgYSBmbHVpZCB3aWR0aCBvZiAxMDAlLlxuICAgKiBUaGUgbWluaW11bSB3aWR0aCBzdXBwb3J0ZWQgYnkgdGhlIGNvbW1lbnRzIHBsdWdpbiBpcyAzMjBweC5cbiAgICogRGVmYXVsdHMgdG8gYDU1MHB4YC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItY29tbWVudHMnKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBGb2xsb3cgQnV0dG9uXG4gKiBAc2hvcnRkZXNjIEZvbGxvdyBidXR0b24gY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZm9sbG93LWJ1dHRvblxuICogQGRlc2NyaXB0aW9uIFRoZSBGb2xsb3cgYnV0dG9uIGxldHMgcGVvcGxlIHN1YnNjcmliZSB0byB0aGUgcHVibGljIHVwZGF0ZXMgb2Ygb3RoZXJzIG9uIEZhY2Vib29rLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItZm9sbG93IGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20venVja1wiPjwvZmItZm9sbG93PlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLWZvbGxvdycsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQkZvbGxvd0NvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igc2NoZW1lIHVzZWQgYnkgdGhlIHBsdWdpbi4gQ2FuIGJlIGBsaWdodGAgb3IgYGRhcmtgLiBEZWZhdWx0cyB0byBgbGlnaHRgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgY29sb3JTY2hlbWU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIEZhY2Vib29rLmNvbSBwcm9maWxlIFVSTCBvZiB0aGUgdXNlciB0byBmb2xsb3cuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIElmIHlvdXIgd2ViIHNpdGUgb3Igb25saW5lIHNlcnZpY2UsIG9yIGEgcG9ydGlvbiBvZiB5b3VyIHNlcnZpY2UsIGlzIGRpcmVjdGVkIHRvIGNoaWxkcmVuIHVuZGVyIDEzIHlvdSBtdXN0IGVuYWJsZSB0aGlzLiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAga2lkRGlyZWN0ZWRTaXRlOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBTZWxlY3RzIG9uZSBvZiB0aGUgZGlmZmVyZW50IGxheW91dHMgdGhhdCBhcmUgYXZhaWxhYmxlIGZvciB0aGUgcGx1Z2luLiBDYW4gYmUgb25lIG9mIGBzdGFuZGFyZGAsIGBidXR0b25fY291bnRgLCBvciBgYm94X2NvdW50YC5cbiAgICogRGVmYXVsdHMgdG8gYHN0YW5kYXJkYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGxheW91dDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTcGVjaWZpZXMgd2hldGhlciB0byBkaXNwbGF5IHByb2ZpbGUgcGhvdG9zIGJlbG93IHRoZSBidXR0b24uIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93RmFjZXM6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGJ1dHRvbiBpcyBvZmZlcmVkIGluIDIgc2l6ZXMgaS5lLiBgbGFyZ2VgIGFuZCBgc21hbGxgLiBEZWZhdWx0cyB0byBgc21hbGxgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2l6ZTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIHBsdWdpbi4gVGhlIGxheW91dCB5b3UgY2hvb3NlIGFmZmVjdHMgdGhlIG1pbmltdW0gYW5kIGRlZmF1bHQgd2lkdGhzIHlvdSBjYW4gdXNlLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgd2lkdGg6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1mb2xsb3cnKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBMaWtlIEJ1dHRvblxuICogQHNob3J0ZGVzYyBMaWtlIGJ1dHRvbiBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9saWtlLWJ1dHRvblxuICogQGRlc2NyaXB0aW9uXG4gKiBBIHNpbmdsZSBjbGljayBvbiB0aGUgTGlrZSBidXR0b24gd2lsbCAnbGlrZScgcGllY2VzIG9mIGNvbnRlbnQgb24gdGhlIHdlYiBhbmQgc2hhcmUgdGhlbSBvbiBGYWNlYm9vay5cbiAqIFlvdSBjYW4gYWxzbyBkaXNwbGF5IGEgU2hhcmUgYnV0dG9uIG5leHQgdG8gdGhlIExpa2UgYnV0dG9uIHRvIGxldCBwZW9wbGUgYWRkIGEgcGVyc29uYWwgbWVzc2FnZSBhbmQgY3VzdG9taXplIHdobyB0aGV5IHNoYXJlIHdpdGguXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1saWtlIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20venVja1wiPjwvZmItbGlrZT5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1saWtlJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCTGlrZUNvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgdmVyYiB0byBkaXNwbGF5IG9uIHRoZSBidXR0b24uIENhbiBiZSBlaXRoZXIgYGxpa2VgIG9yIGByZWNvbW1lbmRgLlxuICAgKiBEZWZhdWx0cyB0byBgbGlrZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBhY3Rpb246IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGNvbG9yIHNjaGVtZSB1c2VkIGJ5IHRoZSBwbHVnaW4gZm9yIGFueSB0ZXh0IG91dHNpZGUgb2YgdGhlIGJ1dHRvbiBpdHNlbGYuIENhbiBiZSBgbGlnaHRgIG9yIGBkYXJrYC5cbiAgICogRGVmYXVsdHMgdG8gYGxpZ2h0YC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGNvbG9yU2NoZW1lOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHBhZ2UgdGhhdCB3aWxsIGJlIGxpa2VkLlxuICAgKiBEZWZhdWx0cyB0byB0aGUgY3VycmVudCBVUkwuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmcgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcblxuICAvKipcbiAgICogSWYgeW91ciB3ZWIgc2l0ZSBvciBvbmxpbmUgc2VydmljZSwgb3IgYSBwb3J0aW9uIG9mIHlvdXIgc2VydmljZSwgaXMgZGlyZWN0ZWQgdG8gY2hpbGRyZW4gdW5kZXIgMTMgW3lvdSBtdXN0IGVuYWJsZSB0aGlzXShodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9yZXN0cmljdGlvbnMvKS5cbiAgICogRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGtpZERpcmVjdGVkU2l0ZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2VsZWN0cyBvbmUgb2YgdGhlIGRpZmZlcmVudCBsYXlvdXRzIHRoYXQgYXJlIGF2YWlsYWJsZSBmb3IgdGhlIHBsdWdpbi5cbiAgICogQ2FuIGJlIG9uZSBvZiBgc3RhbmRhcmRgLCBgYnV0dG9uX2NvdW50YCwgYGJ1dHRvbmAgb3IgYGJveF9jb3VudGAuXG4gICAqIFNlZSB0aGUgW0ZBUV0oaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvbGlrZS1idXR0b24jZmFxbGF5b3V0KSBmb3IgbW9yZSBkZXRhaWxzLlxuICAgKiBEZWZhdWx0cyB0byBgc3RhbmRhcmRgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbGF5b3V0OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEEgbGFiZWwgZm9yIHRyYWNraW5nIHJlZmVycmFscyB3aGljaCBtdXN0IGJlIGxlc3MgdGhhbiA1MCBjaGFyYWN0ZXJzIGFuZCBjYW4gY29udGFpbiBhbHBoYW51bWVyaWMgY2hhcmFjdGVycyBhbmQgc29tZSBwdW5jdHVhdGlvbiAoY3VycmVudGx5ICsvPS0uOl8pLlxuICAgKiBTZWUgdGhlIFtGQVFdKGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2ZhcXMjcmVmKSBmb3IgbW9yZSBkZXRhaWxzLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgcmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNwZWNpZmllcyB3aGV0aGVyIHRvIGluY2x1ZGUgYSBzaGFyZSBidXR0b24gYmVzaWRlIHRoZSBMaWtlIGJ1dHRvbi5cbiAgICogVGhpcyBvbmx5IHdvcmtzIHdpdGggdGhlIFhGQk1MIHZlcnNpb24uXG4gICAqIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaGFyZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogU3BlY2lmaWVzIHdoZXRoZXIgdG8gZGlzcGxheSBwcm9maWxlIHBob3RvcyBiZWxvdyB0aGUgYnV0dG9uIChzdGFuZGFyZCBsYXlvdXQgb25seSkuXG4gICAqIFlvdSBtdXN0IG5vdCBlbmFibGUgdGhpcyBvbiBjaGlsZC1kaXJlY3RlZCBzaXRlcy5cbiAgICogRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNob3dGYWNlczogYm9vbGVhbjtcblxuICAvKipcbiAgICogVGhlIGJ1dHRvbiBpcyBvZmZlcmVkIGluIDIgc2l6ZXMgaS5lLiBgbGFyZ2VgIGFuZCBgc21hbGxgLlxuICAgKiBEZWZhdWx0cyB0byBgc21hbGxgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2l6ZTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIHBsdWdpbiAoc3RhbmRhcmQgbGF5b3V0IG9ubHkpLCB3aGljaCBpcyBzdWJqZWN0IHRvIHRoZSBtaW5pbXVtIGFuZCBkZWZhdWx0IHdpZHRoLlxuICAgKiBTZWUgW0xheW91dCBTZXR0aW5nc10oaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvbGlrZS1idXR0b24jZmFxbGF5b3V0KSBpbiB0aGUgb2ZmaWNpYWwgZG9jcyBmb3IgbW9yZSBkZXRhaWxzLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgd2lkdGg6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihlbDogRWxlbWVudFJlZiwgcm5kOiBSZW5kZXJlcikge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1saWtlJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgUGFnZSBQbHVnaW5cbiAqIEBzaG9ydGRlc2MgUGFnZSBwbHVnaW4gY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvcGFnZS1wbHVnaW5cbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIFBhZ2UgcGx1Z2luIGxldHMgeW91IGVhc2lseSBlbWJlZCBhbmQgcHJvbW90ZSBhbnkgRmFjZWJvb2sgUGFnZSBvbiB5b3VyIHdlYnNpdGUuIEp1c3QgbGlrZSBvbiBGYWNlYm9vaywgeW91ciB2aXNpdG9ycyBjYW4gbGlrZSBhbmQgc2hhcmUgdGhlIFBhZ2Ugd2l0aG91dCBsZWF2aW5nIHlvdXIgc2l0ZS5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLXBhZ2UgaHJlZj1cImh0dHBzOi8vZmFjZWJvb2suY29tL2ZhY2Vib29rXCI+PC9mYi1wYWdlPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXBhZ2UnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJQYWdlQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBVUkwgb2YgdGhlIEZhY2Vib29rIFBhZ2VcbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHBpeGVsIHdpZHRoIG9mIHRoZSBwbHVnaW4uIE1pbi4gaXMgYDE4MGAgJiBNYXguIGlzIGA1MDBgLlxuICAgKiBEZWZhdWx0cyB0byBgMzQwYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBudW1iZXI7XG5cbiAgLyoqXG4gICAqIFRoZSBwaXhlbCBoZWlnaHQgb2YgdGhlIHBsdWdpbi4gTWluLiBpcyBgNzBgLlxuICAgKiBEZWZhdWx0cyB0byBgNTAwYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhlaWdodDogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBUYWJzIHRvIHJlbmRlciBpLmUuIGB0aW1lbGluZWAsIGBldmVudHNgLCBgbWVzc2FnZXNgLiBVc2UgYSBjb21tYS1zZXBhcmF0ZWQgbGlzdCB0byBhZGQgbXVsdGlwbGUgdGFicywgaS5lLiBgdGltZWxpbmVgLCBgZXZlbnRzYC5cbiAgICogRGVmYXVsdHMgdG8gYHRpbWVsaW5lYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHRhYnM6IHN0cmluZztcblxuICAvKipcbiAgICogSGlkZSBjb3ZlciBwaG90byBpbiB0aGUgaGVhZGVyLlxuICAgKiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaGlkZUNvdmVyOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBTaG93IHByb2ZpbGUgcGhvdG9zIHdoZW4gZnJpZW5kcyBsaWtlIHRoaXMuXG4gICAqIERlZmF1bHRzIHRvIGB0cnVlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNob3dGYWNlcGlsZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogSGlkZSB0aGUgY3VzdG9tIGNhbGwgdG8gYWN0aW9uIGJ1dHRvbiAoaWYgYXZhaWxhYmxlKS5cbiAgICogRGVmYXVsdCB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaGlkZUNUQTogYm9vbGVhbjtcblxuICAvKipcbiAgICogVXNlIHRoZSBzbWFsbCBoZWFkZXIgaW5zdGVhZC5cbiAgICogRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNtYWxsSGVhZGVyOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBUcnkgdG8gZml0IGluc2lkZSB0aGUgY29udGFpbmVyIHdpZHRoLlxuICAgKiBEZWZhdWx0cyB0byBgdHJ1ZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBhZGFwdENvbnRhaW5lcldpZHRoOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLXBhZ2UnKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBFbWJlZGRlZCBQb3N0c1xuICogQHNob3J0ZGVzYyBFbWJlZGRlZCBwb3N0IGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2VtYmVkZGVkLXBvc3RzXG4gKiBAZGVzY3JpcHRpb25cbiAqIEVtYmVkZGVkIFBvc3RzIGFyZSBhIHNpbXBsZSB3YXkgdG8gcHV0IHB1YmxpYyBwb3N0cyAtIGJ5IGEgUGFnZSBvciBhIHBlcnNvbiBvbiBGYWNlYm9vayAtIGludG8gdGhlIGNvbnRlbnQgb2YgeW91ciB3ZWIgc2l0ZSBvciB3ZWIgcGFnZS5cbiAqIE9ubHkgcHVibGljIHBvc3RzIGZyb20gRmFjZWJvb2sgUGFnZXMgYW5kIHByb2ZpbGVzIGNhbiBiZSBlbWJlZGRlZC5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLXBvc3QgaHJlZj1cImh0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS8yMDUzMTMxNjcyOC9wb3N0cy8xMDE1NDAwOTk5MDUwNjcyOS9cIj48L2ZiLXBvc3Q+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItcG9zdCcsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQlBvc3RDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgcG9zdC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSBwb3N0LiBNaW4uIGAzNTBgIHBpeGVsOyBNYXguIGA3NTBgIHBpeGVsLiBTZXQgdG8gYXV0byB0byB1c2UgZmx1aWQgd2lkdGguIERlZmF1bHRzIHRvIGBhdXRvYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEFwcGxpZWQgdG8gcGhvdG8gcG9zdC4gU2V0IHRvIGB0cnVlYCB0byBpbmNsdWRlIHRoZSB0ZXh0IGZyb20gdGhlIEZhY2Vib29rIHBvc3QsIGlmIGFueS4gRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNob3dUZXh0OiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLXBvc3QnKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBRdW90ZSBQbHVnaW5cbiAqIEBzaG9ydGRlc2MgUXVvdGUgcGx1Z2luIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3F1b3RlXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBxdW90ZSBwbHVnaW4gbGV0cyBwZW9wbGUgc2VsZWN0IHRleHQgb24geW91ciBwYWdlIGFuZCBhZGQgaXQgdG8gdGhlaXIgc2hhcmUsIHNvIHRoZXkgY2FuIHRlbGwgYSBtb3JlIGV4cHJlc3NpdmUgc3RvcnkuXG4gKiBOb3RlIHRoYXQgeW91IGRvIG5vdCBuZWVkIHRvIGltcGxlbWVudCBGYWNlYm9vayBsb2dpbiBvciByZXF1ZXN0IGFueSBhZGRpdGlvbmFsIHBlcm1pc3Npb25zIHRocm91Z2ggYXBwIHJldmlldyBpbiBvcmRlciB0byB1c2UgdGhpcyBwbHVnaW4uXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1xdW90ZT48L2ZiLXF1b3RlPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXF1b3RlJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCUXVvdGVDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgcGFnZSB0aGF0IHdpbGwgYmUgcXVvdGVkLlxuICAgKiBEZWZhdWx0cyB0byB0aGUgY3VycmVudCBVUkxcbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZztcblxuICAvKipcbiAgICogQ2FuIGJlIHNldCB0byBxdW90ZSBvciBidXR0b24uIERlZmF1bHRzIHRvIHF1b3RlLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbGF5b3V0OiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItcXVvdGUnKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBTYXZlIEJ1dHRvblxuICogQHNob3J0ZGVzYyBTYXZlIGJ1dHRvbiBjb21wb25lbnQuXG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvc2F2ZVxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgU2F2ZSBidXR0b24gbGV0cyBwZW9wbGUgc2F2ZSBpdGVtcyBvciBzZXJ2aWNlcyB0byBhIHByaXZhdGUgbGlzdCBvbiBGYWNlYm9vaywgc2hhcmUgaXQgd2l0aCBmcmllbmRzLCBhbmQgcmVjZWl2ZSByZWxldmFudCBub3RpZmljYXRpb25zLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItc2F2ZSB1cmk9XCJodHRwczovL2dpdGh1Yi5jb20venlyYS9uZzItZmFjZWJvb2stc2RrL1wiPjwvZmItc2F2ZT5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1zYXZlJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCU2F2ZUNvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgbGluayBvZiB0aGUgcGFnZSB0aGF0IHdpbGwgYmUgc2F2ZWQuXG4gICAqIEN1cnJlbnQgTGluay9BZGRyZXNzXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB1cmk6IHN0cmluZyA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLXNhdmUnKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBTZW5kIEJ1dHRvblxuICogQHNob3J0ZGVzYyBTZW5kIGJ1dHRvbiBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9zZW5kLWJ1dHRvblxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgU2VuZCBidXR0b24gbGV0cyBwZW9wbGUgcHJpdmF0ZWx5IHNlbmQgY29udGVudCBvbiB5b3VyIHNpdGUgdG8gb25lIG9yIG1vcmUgZnJpZW5kcyBpbiBhIEZhY2Vib29rIG1lc3NhZ2UuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1zZW5kIGhyZWY9XCJodHRwczovL2dpdGh1Yi5jb20venlyYS9uZzItZmFjZWJvb2stc2RrL1wiPjwvZmItc2VuZD5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1zZW5kJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCU2VuZENvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igc2NoZW1lIHVzZWQgYnkgdGhlIHBsdWdpbi4gQ2FuIGJlIFwibGlnaHRcIiBvciBcImRhcmtcIi4gRGVmYXVsdHMgdG8gbGlnaHQuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBjb2xvclNjaGVtZTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBwYWdlIHRoYXQgd2lsbCBiZSBzZW50LiBEZWZhdWx0cyB0byB0aGUgY3VycmVudCBVUkwuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmcgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcblxuICAvKipcbiAgICogSWYgeW91ciB3ZWIgc2l0ZSBvciBvbmxpbmUgc2VydmljZSwgb3IgYSBwb3J0aW9uIG9mIHlvdXIgc2VydmljZSwgaXMgZGlyZWN0ZWQgdG8gY2hpbGRyZW4gdW5kZXIgMTMgeW91IG11c3QgZW5hYmxlIHRoaXMuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBraWREaXJlY3RlZFNpdGU6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEEgbGFiZWwgZm9yIHRyYWNraW5nIHJlZmVycmFscyB3aGljaCBtdXN0IGJlIGxlc3MgdGhhbiA1MCBjaGFyYWN0ZXJzLCBhbmQgY2FuIGNvbnRhaW4gYWxwaGFudW1lcmljIGNoYXJhY3RlcnMgYW5kIHNvbWUgcHVuY3R1YXRpb24gKGN1cnJlbnRseSArLz0tLjpfKS5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTaXplIG9mIHRoZSBidXR0b24uIERlZmF1bHRzIHRvIHNtYWxsLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2l6ZTogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLXNlbmQnKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCB9IGZyb20gJy4uL2ZibWwtY29tcG9uZW50JztcblxuLyoqXG4gKiBAbmFtZSBTaGFyZSBCdXR0b25cbiAqIEBzaG9ydGRlc2MgU2hhcmUgYnV0dG9uIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3NoYXJlLWJ1dHRvblxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgU2hhcmUgYnV0dG9uIGxldHMgcGVvcGxlIGFkZCBhIHBlcnNvbmFsaXplZCBtZXNzYWdlIHRvIGxpbmtzIGJlZm9yZSBzaGFyaW5nIG9uIHRoZWlyIHRpbWVsaW5lLCBpbiBncm91cHMsIG9yIHRvIHRoZWlyIGZyaWVuZHMgdmlhIGEgRmFjZWJvb2sgTWVzc2FnZS5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLXNoYXJlIGhyZWY9XCJodHRwczovL2dpdGh1Yi5jb20venlyYS9uZzItZmFjZWJvb2stc2RrL1wiPjwvZmItc2hhcmU+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItc2hhcmUnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJTaGFyZUNvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBwYWdlIHRoYXQgd2lsbCBiZSBzaGFyZWQuIERlZmF1bHRzIHRvIHRoZSBjdXJyZW50IFVSTC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZztcblxuICAvKipcbiAgICogU2VsZWN0cyBvbmUgb2YgdGhlIGRpZmZlcmVudCBsYXlvdXRzIHRoYXQgYXJlIGF2YWlsYWJsZSBmb3IgdGhlIHBsdWdpbi4gQ2FuIGJlIG9uZSBvZiBgYm94X2NvdW50YCwgYGJ1dHRvbl9jb3VudGAsIGBidXR0b25gLiBEZWZhdWx0cyB0byBgaWNvbl9saW5rYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGxheW91dDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBJZiBzZXQgdG8gdHJ1ZSwgdGhlIHNoYXJlIGJ1dHRvbiB3aWxsIG9wZW4gdGhlIHNoYXJlIGRpYWxvZyBpbiBhbiBpZnJhbWUgKGluc3RlYWQgb2YgYSBwb3B1cCkgb24gdG9wIG9mIHlvdXIgd2Vic2l0ZSBvbiBtb2JpbGUuIFRoaXMgb3B0aW9uIGlzIG9ubHkgYXZhaWxhYmxlIGZvciBtb2JpbGUsIG5vdCBkZXNrdG9wLiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbW9iaWxlSWZyYW1lOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBUaGUgYnV0dG9uIGlzIG9mZmVyZWQgaW4gMiBzaXplcyBpLmUuIGxhcmdlIGFuZCBzbWFsbC4gRGVmYXVsdHMgdG8gYHNtYWxsYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNpemU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1zaGFyZS1idXR0b24nKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyLCBPbkluaXQsIE9uRGVzdHJveSwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50LCBGQk1MSW5zdGFuY2VNZXRob2QgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5kZWNsYXJlIHZhciBGQjogYW55O1xuXG4vKipcbiAqIEBuYW1lIEVtYmVkZGVkIFZpZGVvXG4gKiBAc2hvcnRkZXNjIENvbXBvbmVudCB0byBlbWJlZCBGYWNlYm9vayB2aWRlb3NcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9lbWJlZGRlZC12aWRlby1wbGF5ZXJcbiAqIEBkZXNjcmlwdGlvbiBDb21wb25lbnQgdG8gZW1iZWQgRmFjZWJvb2sgdmlkZW9zIGFuZCBjb250cm9sIHRoZW0uXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDwhLS0gYmFzaWMgdXNhZ2UgLS0+XG4gKiA8ZmItdmlkZW8gaHJlZj1cImh0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS9mYWNlYm9vay92aWRlb3MvMTAxNTMyMzEzNzk5NDY3MjkvXCI+PC9mYi12aWRlbz5cbiAqXG4gKiA8IS0tIGV2ZW50IGVtaXR0ZXJzIC0tPlxuICogPGZiLXZpZGVvIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vZmFjZWJvb2svdmlkZW9zLzEwMTUzMjMxMzc5OTQ2NzI5L1wiIChwYXVzZWQpPVwib25WaWRlb1BhdXNlZCgkZXZlbnQpXCI+PC9mYi12aWRlbz5cbiAqIGBgYFxuICpcbiAqIFRvIG1hbnVhbGx5IGludGVyYWN0IHdpdGggdGhlIHZpZGVvIHBsYXllciwgZmV0Y2ggaXQgdXNpbmcgYFZpZXdDaGlsZGAuXG4gKlxuICogYGBgdHNcbiAqIGltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG4gKiBpbXBvcnQgeyBGQlZpZGVvQ29tcG9uZW50IH0gZnJvbSAnbmcyLWZhY2Vib29rLXNkayc7XG4gKlxuICogQENvbXBvbmVudCguLi4pXG4gKiBleHBvcnQgY2xhc3MgTXlDb21wb25lbnQge1xuICpcbiAqICAgQFZpZXdDaGlsZChGQlZpZGVvQ29tcG9uZW50KSB2aWRlbzogRkJWaWRlb0NvbXBvbmVudDtcbiAqXG4gKiAgIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAqICAgICB0aGlzLnZpZGVvLnBsYXkoKTtcbiAqICAgICB0aGlzLnZpZGVvLnBhdXNlKCk7XG4gKiAgICAgdGhpcy52aWRlby5nZXRWb2x1bWUoKTtcbiAqICAgfVxuICpcbiAqIH1cbiAqXG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItdmlkZW8nLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJWaWRlb0NvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG5cbiAgcHJpdmF0ZSBfaW5zdGFuY2U6IGFueTtcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgdmlkZW8uXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEFsbG93IHRoZSB2aWRlbyB0byBiZSBwbGF5ZWQgaW4gZnVsbHNjcmVlbiBtb2RlLiBDYW4gYmUgZmFsc2Ugb3IgdHJ1ZS4gRGVmYXVsdHMgdG8gZmFsc2U7XG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBhbGxvd2Z1bGxzY3JlZW46IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEF1dG9tYXRpY2FsbHkgc3RhcnQgcGxheWluZyB0aGUgdmlkZW8gd2hlbiB0aGUgcGFnZSBsb2Fkcy4gVGhlIHZpZGVvIHdpbGwgYmUgcGxheWVkIHdpdGhvdXQgc291bmQgKG11dGVkKS4gUGVvcGxlIGNhbiB0dXJuIG9uIHNvdW5kIHZpYSB0aGUgdmlkZW8gcGxheWVyIGNvbnRyb2xzLiBUaGlzIHNldHRpbmcgZG9lcyBub3QgYXBwbHkgdG8gbW9iaWxlIGRldmljZXMuIENhbiBiZSBmYWxzZSBvciB0cnVlLiBEZWZhdWx0cyB0byBmYWxzZS5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGF1dG9wbGF5OiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIHZpZGVvIGNvbnRhaW5lci4gTWluLiAyMjBweC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNldCB0byB0cnVlIHRvIGluY2x1ZGUgdGhlIHRleHQgZnJvbSB0aGUgRmFjZWJvb2sgcG9zdCBhc3NvY2lhdGVkIHdpdGggdGhlIHZpZGVvLCBpZiBhbnkuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93VGV4dDogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2V0IHRvIHRydWUgdG8gc2hvdyBjYXB0aW9ucyAoaWYgYXZhaWxhYmxlKSBieSBkZWZhdWx0LiBDYXB0aW9ucyBhcmUgb25seSBhdmFpbGFibGUgb24gZGVza3RvcC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNob3dDYXB0aW9uczogYm9vbGVhbjtcblxuICAvKipcbiAgICogRmlyZWQgd2hlbiB2aWRlbyBzdGFydHMgdG8gcGxheS5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBzdGFydGVkUGxheWluZzogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAvKipcbiAgICogRmlyZWQgd2hlbiB2aWRlbyBwYXVzZXMuXG4gICAqL1xuICBAT3V0cHV0KClcbiAgcGF1c2VkOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKlxuICAgKlxuICAgRmlyZWQgd2hlbiB2aWRlbyBmaW5pc2hlcyBwbGF5aW5nLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIGZpbmlzaGVkUGxheWluZzogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAvKipcbiAgICogRmlyZWQgd2hlbiB2aWRlbyBzdGFydHMgdG8gYnVmZmVyLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIHN0YXJ0ZWRCdWZmZXJpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gcmVjb3ZlcnMgZnJvbSBidWZmZXJpbmcuXG4gICAqL1xuICBAT3V0cHV0KClcbiAgZmluaXNoZWRCdWZmZXJpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gYW4gZXJyb3Igb2NjdXJzIG9uIHRoZSB2aWRlby5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBlcnJvcjogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICBwcml2YXRlIF9pZDogc3RyaW5nO1xuXG4gIHByaXZhdGUgX2xpc3RlbmVyczogYW55W10gPSBbXTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi12aWRlbycpO1xuICAgIHRoaXMubmF0aXZlRWxlbWVudC5pZCA9IHRoaXMuX2lkID0gJ3ZpZGVvLScgKyBTdHJpbmcoTWF0aC5mbG9vcigoTWF0aC5yYW5kb20oKSAqIDEwMDAwKSArIDEpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAaGlkZGVuXG4gICAqL1xuICBuZ09uSW5pdCgpIHtcbiAgICBGQi5FdmVudC5zdWJzY3JpYmUoJ3hmYm1sLnJlYWR5JywgKG1zZzogYW55KSA9PiB7XG4gICAgICBpZiAobXNnLnR5cGUgPT09ICd2aWRlbycgJiYgbXNnLmlkID09PSB0aGlzLl9pZCkge1xuICAgICAgICB0aGlzLl9pbnN0YW5jZSA9IG1zZy5pbnN0YW5jZTtcbiAgICAgICAgdGhpcy5fbGlzdGVuZXJzLnB1c2goXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdzdGFydGVkUGxheWluZycsIChlOiBhbnkpID0+IHRoaXMuc3RhcnRlZFBsYXlpbmcuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdwYXVzZWQnLCAoZTogYW55KSA9PiB0aGlzLnBhdXNlZC5lbWl0KGUpKSxcbiAgICAgICAgICB0aGlzLl9pbnN0YW5jZS5zdWJzY3JpYmUoJ2ZpbmlzaGVkUGxheWluZycsIChlOiBhbnkpID0+IHRoaXMuZmluaXNoZWRQbGF5aW5nLmVtaXQoZSkpLFxuICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnN1YnNjcmliZSgnc3RhcnRlZEJ1ZmZlcmluZycsIChlOiBhbnkpID0+IHRoaXMuc3RhcnRlZEJ1ZmZlcmluZy5lbWl0KGUpKSxcbiAgICAgICAgICB0aGlzLl9pbnN0YW5jZS5zdWJzY3JpYmUoJ2ZpbmlzaGVkQnVmZmVyaW5nJywgKGU6IGFueSkgPT4gdGhpcy5maW5pc2hlZEJ1ZmZlcmluZy5lbWl0KGUpKSxcbiAgICAgICAgICB0aGlzLl9pbnN0YW5jZS5zdWJzY3JpYmUoJ2Vycm9yJywgKGU6IGFueSkgPT4gdGhpcy5lcnJvci5lbWl0KGUpKVxuICAgICAgICApO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIEBoaWRkZW5cbiAgICovXG4gIG5nT25EZXN0cm95KCkge1xuICAgIHRoaXMuX2xpc3RlbmVycy5mb3JFYWNoKGwgPT4ge1xuICAgICAgaWYgKHR5cGVvZiBsLnJlbGVhc2UgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgbC5yZWxlYXNlKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogUGxheXMgdGhlIHZpZGVvLlxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBwbGF5KCkge31cblxuICAvKipcbiAgICogUGF1c2VzIHRoZSB2aWRlby5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgcGF1c2UoKSB7fVxuXG4gIC8qKlxuICAgKiBTZWVrcyB0byBzcGVjaWZpZWQgcG9zaXRpb24uXG4gICAqIEBwYXJhbSBzZWNvbmRzIHtudW1iZXJ9XG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIHNlZWsoc2Vjb25kczogbnVtYmVyKSB7fVxuXG4gIC8qKlxuICAgKiBNdXRlIHRoZSB2aWRlby5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgbXV0ZSgpIHt9XG5cbiAgLyoqXG4gICAqIFVubXV0ZSB0aGUgdmlkZW8uXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIHVubXV0ZSgpIHt9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdHJ1ZSBpZiB2aWRlbyBpcyBtdXRlZCwgZmFsc2UgaWYgbm90LlxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBpc011dGVkKCk6IGJvb2xlYW4geyByZXR1cm47IH1cblxuICAvKipcbiAgICogU2V0IHRoZSB2b2x1bWVcbiAgICogQHBhcmFtIHZvbHVtZVxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBzZXRWb2x1bWUodm9sdW1lOiBudW1iZXIpIHt9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgdm9sdW1lXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIGdldFZvbHVtZSgpOiBudW1iZXIgeyByZXR1cm47IH1cblxuICAvKipcbiAgICogUmV0dXJucyB0aGUgY3VycmVudCB2aWRlbyB0aW1lIHBvc2l0aW9uIGluIHNlY29uZHNcbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgZ2V0Q3VycmVudFBvc2l0aW9uKCkge31cblxuICAvKipcbiAgICogUmV0dXJucyB0aGUgdmlkZW8gZHVyYXRpb24gaW4gc2Vjb25kc1xuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBnZXREdXJhdGlvbigpIHt9XG5cbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEF1dGhSZXNwb25zZSB9IGZyb20gJy4uL21vZGVscy9hdXRoLXJlc3BvbnNlJztcbmltcG9ydCB7IEluaXRQYXJhbXMgfSBmcm9tICcuLi9tb2RlbHMvaW5pdC1wYXJhbXMnO1xuaW1wb3J0IHsgTG9naW5PcHRpb25zIH0gZnJvbSAnLi4vbW9kZWxzL2xvZ2luLW9wdGlvbnMnO1xuaW1wb3J0IHsgTG9naW5SZXNwb25zZSB9IGZyb20gJy4uL21vZGVscy9sb2dpbi1yZXNwb25zZSc7XG5pbXBvcnQgeyBMb2dpblN0YXR1cyB9IGZyb20gJy4uL21vZGVscy9sb2dpbi1zdGF0dXMnO1xuaW1wb3J0IHsgVUlQYXJhbXMgfSBmcm9tICcuLi9tb2RlbHMvdWktcGFyYW1zJztcbmltcG9ydCB7IFVJUmVzcG9uc2UgfSBmcm9tICcuLi9tb2RlbHMvdWktcmVzcG9uc2UnO1xuXG5kZWNsYXJlIHZhciBGQjogYW55O1xuXG4vKipcbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IHR5cGUgQXBpTWV0aG9kID0gJ2dldCcgfCAncG9zdCcgfCAnZGVsZXRlJztcblxuLyoqXG4gKiBAc2hvcnRkZXNjXG4gKiBBbmd1bGFyIDIgc2VydmljZSB0byBpbmplY3QgdG8gdXNlIEZhY2Vib29rJ3MgU0RLXG4gKiBAZGVzY3JpcHRpb25cbiAqIFlvdSBvbmx5IG5lZWQgdG8gaW5qZWN0IHRoaXMgc2VydmljZSBpbiB5b3VyIGFwcGxpY2F0aW9uIGlmIHlvdSBhcmVuJ3QgdXNpbmcgW2BGYWNlYm9va01vZHVsZWBdKC4uL2ZhY2Vib29rLW1vZHVsZSkuXG4gKiBAdXNhZ2VcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IEZhY2Vib29rU2VydmljZSwgSW5pdFBhcmFtcyB9IGZyb20gJ25nMi1mYWNlYm9vay1zZGsnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgZmI6IEZhY2Vib29rU2VydmljZSkge1xuICpcbiAqICAgY29uc3QgcGFyYW1zOiBJbml0UGFyYW1zID0ge1xuICpcbiAqICAgfTtcbiAqXG4gKiAgIGZiLmluaXQocGFyYW1zKTtcbiAqXG4gKiB9XG4gKiBgYGBcbiAqL1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEZhY2Vib29rU2VydmljZSB7XG5cbiAgLyoqXG4gICAqIFRoaXMgbWV0aG9kIGlzIHVzZWQgdG8gaW5pdGlhbGl6ZSBhbmQgc2V0dXAgdGhlIFNESy5cbiAgICogQHBhcmFtIHBhcmFtcyB7SW5pdFBhcmFtc30gSW5pdGlhbGl6YXRpb24gcGFyYW1ldGVyc1xuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIGluaXQocGFyYW1zOiBJbml0UGFyYW1zKTogUHJvbWlzZTxhbnk+IHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShGQi5pbml0KHBhcmFtcykpO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgbGV0cyB5b3UgbWFrZSBjYWxscyB0byB0aGUgR3JhcGggQVBJXG4gICAqIEB1c2FnZVxuICAgKiBgYGB0eXBlc2NyaXB0XG4gICAqIHRoaXMuZmIuYXBpKCdzb21lcGF0aCcpXG4gICAqICAgLnRoZW4ocmVzID0+IGNvbnNvbGUubG9nKHJlcykpXG4gICAqICAgLmNhdGNoKGUgPT4gY29uc29sZS5sb2coZSkpO1xuICAgKiBgYGBcbiAgICogQHBhcmFtIHBhdGgge3N0cmluZ30gVGhlIEdyYXBoIEFQSSBlbmRwb2ludCBwYXRoIHRoYXQgeW91IHdhbnQgdG8gY2FsbC5cbiAgICogQHBhcmFtIFttZXRob2Q9Z2V0XSB7c3RyaW5nfSBUaGUgSFRUUCBtZXRob2QgdGhhdCB5b3Ugd2FudCB0byB1c2UgZm9yIHRoZSBBUEkgcmVxdWVzdC5cbiAgICogQHBhcmFtIFtwYXJhbXNdIHtPYmplY3R9IEFuIG9iamVjdCBjb25zaXN0aW5nIG9mIGFueSBwYXJhbWV0ZXJzIHRoYXQgeW91IHdhbnQgdG8gcGFzcyBpbnRvIHlvdXIgR3JhcGggQVBJIGNhbGwuXG4gICAqIEByZXR1cm5zIHJldHVybiB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgYXBpKHBhdGg6IHN0cmluZywgbWV0aG9kOiBBcGlNZXRob2QgPSAnZ2V0JywgcGFyYW1zOiBhbnkgPSB7fSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGFueT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICB0cnkge1xuICAgICAgICBGQi5hcGkocGF0aCwgbWV0aG9kLCBwYXJhbXMsIChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgaWYgKCFyZXNwb25zZSkge1xuICAgICAgICAgICAgcmVqZWN0KCk7XG4gICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZS5lcnJvcikge1xuICAgICAgICAgICAgcmVqZWN0KHJlc3BvbnNlLmVycm9yKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgaXMgdXNlZCB0byB0cmlnZ2VyIGRpZmZlcmVudCBmb3JtcyBvZiBGYWNlYm9vayBjcmVhdGVkIFVJIGRpYWxvZ3MuXG4gICAqIFRoZXNlIGRpYWxvZ3MgaW5jbHVkZTpcbiAgICogLSBTaGFyZSBkaWFsb2dcbiAgICogLSBMb2dpbiBkaWFsb2dcbiAgICogLSBBZGQgcGFnZSB0YWIgZGlhbG9nXG4gICAqIC0gUmVxdWVzdHMgZGlhbG9nXG4gICAqIC0gU2VuZCBkaWFsb2dcbiAgICogLSBQYXltZW50cyBkaWFsb2dcbiAgICogLSBHbyBMaXZlIGRpYWxvZ1xuICAgKiBAcGFyYW0gcGFyYW1zIHtVSVBhcmFtc30gQSBjb2xsZWN0aW9uIG9mIHBhcmFtZXRlcnMgdGhhdCBjb250cm9sIHdoaWNoIGRpYWxvZyBpcyBsb2FkZWQsIGFuZCByZWxldmFudCBzZXR0aW5ncy5cbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPFVJUmVzcG9uc2U+fVxuICAgKi9cbiAgdWkocGFyYW1zOiBVSVBhcmFtcyk6IFByb21pc2U8VUlSZXNwb25zZT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgdHJ5IHtcbiAgICAgICAgRkIudWkocGFyYW1zLCAocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICAgIGlmKCFyZXNwb25zZSkgcmVqZWN0KCk7XG4gICAgICAgICAgZWxzZSBpZihyZXNwb25zZS5lcnJvcikgcmVqZWN0KHJlc3BvbnNlLmVycm9yKTtcbiAgICAgICAgICBlbHNlIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICB9KTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2QgYWxsb3dzIHlvdSB0byBkZXRlcm1pbmUgaWYgYSB1c2VyIGlzIGxvZ2dlZCBpbiB0byBGYWNlYm9vayBhbmQgaGFzIGF1dGhlbnRpY2F0ZWQgeW91ciBhcHAuXG4gICAqIEBwYXJhbSBbZm9yY2VGcmVzaFJlc3BvbnNlPWZhbHNlXSB7Ym9vbGVhbn0gRm9yY2UgYSBmcmVzaCByZXNwb25zZS5cbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPExvZ2luU3RhdHVzPn1cbiAgICovXG4gIGdldExvZ2luU3RhdHVzKGZvcmNlRnJlc2hSZXNwb25zZT86IGJvb2xlYW4pOiBQcm9taXNlPExvZ2luU3RhdHVzPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPExvZ2luU3RhdHVzPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIEZCLmdldExvZ2luU3RhdHVzKChyZXNwb25zZTogTG9naW5TdGF0dXMpID0+IHtcbiAgICAgICAgICBpZiAoIXJlc3BvbnNlKSB7XG4gICAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LCBmb3JjZUZyZXNoUmVzcG9uc2UpO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb2dpbiB0aGUgdXNlclxuICAgKiBAdXNhZ2VcbiAgICogYGBgdHlwZXNjcmlwdFxuICAgKiAvLyBsb2dpbiB3aXRob3V0IG9wdGlvbnNcbiAgICogdGhpcy5mYi5sb2dpbigpXG4gICAqICAgLnRoZW4oKHJlc3BvbnNlOiBMb2dpblJlc3BvbnNlKSA9PiBjb25zb2xlLmxvZygnTG9nZ2VkIGluJywgcmVzcG9uc2UpKVxuICAgKiAgIC5jYXRjaChlID0+IGNvbnNvbGUuZXJyb3IoJ0Vycm9yIGxvZ2dpbmcgaW4nKSk7XG4gICAqXG4gICAqIC8vIGxvZ2luIHdpdGggb3B0aW9uc1xuICAgKiBjb25zdCBvcHRpb25zOiBMb2dpbk9wdGlvbnMgPSB7XG4gICAqICAgc2NvcGU6ICdwdWJsaWNfcHJvZmlsZSx1c2VyX2ZyaWVuZHMsZW1haWwscGFnZXNfc2hvd19saXN0JyxcbiAgICogICByZXR1cm5fc2NvcGVzOiB0cnVlLFxuICAgKiAgIGVuYWJsZV9wcm9maWxlX3NlbGVjdG9yOiB0cnVlXG4gICAqIH07XG4gICAqIHRoaXMuZmIubG9naW4ob3B0aW9ucylcbiAgICogICAudGhlbiguLi4pXG4gICAqICAgLmNhdGNoKC4uLik7XG4gICAqIGBgYFxuICAgKiBAcGFyYW0gW29wdGlvbnNdIHtMb2dpbk9wdGlvbnN9IExvZ2luIG9wdGlvbnNcbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPExvZ2luUmVzcG9uc2U+fSByZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdpdGggW0xvZ2luUmVzcG9uc2VdKC4uL2xvZ2luLXJlc3BvbnNlKSBvYmplY3QsIG9yIHJlamVjdHMgd2l0aCBhbiBlcnJvclxuICAgKi9cbiAgbG9naW4ob3B0aW9ucz86IExvZ2luT3B0aW9ucyk6IFByb21pc2U8TG9naW5SZXNwb25zZT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxMb2dpblJlc3BvbnNlPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIEZCLmxvZ2luKChyZXNwb25zZTogTG9naW5SZXNwb25zZSkgPT4ge1xuICAgICAgICAgIGlmIChyZXNwb25zZS5hdXRoUmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgcmVqZWN0KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LCBvcHRpb25zKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogTG9nb3V0IHRoZSB1c2VyXG4gICAqIEB1c2FnZVxuICAgKiBgYGB0eXBlc2NyaXB0XG4gICAqIHRoaXMuZmIubG9nb3V0KCkudGhlbigoKSA9PiBjb25zb2xlLmxvZygnTG9nZ2VkIG91dCEnKSk7XG4gICAqIGBgYFxuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8YW55Pn0gcmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIHRoZSB1c2VyIGlzIGxvZ2dlZCBvdXRcbiAgICovXG4gIGxvZ291dCgpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgdHJ5IHtcbiAgICAgICAgRkIubG9nb3V0KChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9XG5cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIHN5bmNocm9ub3VzIGZ1bmN0aW9uIHJldHVybnMgYmFjayB0aGUgY3VycmVudCBhdXRoUmVzcG9uc2UuXG4gICAqIEB1c2FnZVxuICAgKiBgYGB0eXBlc2NyaXB0XG4gICAqIGltcG9ydCB7IEF1dGhSZXNwb25zZSwgRmFjZWJvb2tTZXJ2aWNlIH0gZnJvbSAnbmcyLWZhY2Vib29rLXNkayc7XG4gICAqXG4gICAqIC4uLlxuICAgKlxuICAgKiBjb25zdCBhdXRoUmVzcG9uc2U6IEF1dGhSZXNwb25zZSA9IHRoaXMuZmIuZ2V0QXV0aFJlc3BvbnNlKCk7XG4gICAqIGBgYFxuICAgKiBAcmV0dXJucyByZXR1cm4ge0F1dGhSZXNwb25zZX0gcmV0dXJucyBhbiBbQXV0aFJlc3BvbnNlXSguLi9hdXRoLXJlc3BvbnNlKSBvYmplY3RcbiAgICovXG4gIGdldEF1dGhSZXNwb25zZSgpOiBBdXRoUmVzcG9uc2Uge1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gPEF1dGhSZXNwb25zZT5GQi5nZXRBdXRoUmVzcG9uc2UoKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBjb25zb2xlLmVycm9yKCduZzItZmFjZWJvb2stc2RrOiAnLCBlKTtcbiAgICB9XG4gIH1cblxufVxuXG4iLCJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGYWNlYm9va1NlcnZpY2UgfSBmcm9tICcuL3Byb3ZpZGVycy9mYWNlYm9vayc7XG5cbmltcG9ydCB7IEZCQ29tbWVudEVtYmVkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLWNvbW1lbnQtZW1iZWQvZmItY29tbWVudC1lbWJlZCc7XG5pbXBvcnQgeyBGQkNvbW1lbnRzQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLWNvbW1lbnRzL2ZiLWNvbW1lbnRzJztcbmltcG9ydCB7IEZCRm9sbG93Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLWZvbGxvdy9mYi1mb2xsb3cnO1xuaW1wb3J0IHsgRkJMaWtlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLWxpa2UvZmItbGlrZSc7XG5pbXBvcnQgeyBGQlBhZ2VDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItcGFnZS9mYi1wYWdlJztcbmltcG9ydCB7IEZCUG9zdENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1wb3N0L2ZiLXBvc3QnO1xuaW1wb3J0IHsgRkJRdW90ZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1xdW90ZS9mYi1xdW90ZSc7XG5pbXBvcnQgeyBGQlNhdmVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItc2F2ZS9mYi1zYXZlJztcbmltcG9ydCB7IEZCU2VuZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1zZW5kL2ZiLXNlbmQnO1xuaW1wb3J0IHsgRkJTaGFyZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1zaGFyZS9mYi1zaGFyZSc7XG5pbXBvcnQgeyBGQlZpZGVvQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLXZpZGVvL2ZiLXZpZGVvJztcblxuY29uc3QgY29tcG9uZW50czogYW55W10gPSBbXG4gIEZCQ29tbWVudEVtYmVkQ29tcG9uZW50LFxuICBGQkNvbW1lbnRzQ29tcG9uZW50LFxuICBGQkZvbGxvd0NvbXBvbmVudCxcbiAgRkJMaWtlQ29tcG9uZW50LFxuICBGQlBhZ2VDb21wb25lbnQsXG4gIEZCUG9zdENvbXBvbmVudCxcbiAgRkJRdW90ZUNvbXBvbmVudCxcbiAgRkJTYXZlQ29tcG9uZW50LFxuICBGQlNlbmRDb21wb25lbnQsXG4gIEZCU2hhcmVDb21wb25lbnQsXG4gIEZCVmlkZW9Db21wb25lbnRcbl07XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRDb21wb25lbnRzKCkge1xuICByZXR1cm4gY29tcG9uZW50cztcbn1cblxuLyoqXG4gKiBAc2hvcnRkZXNjIFRoZSBtb2R1bGUgdG8gaW1wb3J0IHRvIGFkZCB0aGlzIGxpYnJhcnlcbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIG1haW4gbW9kdWxlIHRvIGltcG9ydCBpbnRvIHlvdXIgYXBwbGljYXRpb24uXG4gKiBZb3Ugb25seSBuZWVkIHRvIGltcG9ydCB0aGlzIG1vZHVsZSBpZiB5b3Ugd2lzaCB0byB1c2UgdGhlIGNvbXBvbmVudHMgaW4gdGhpcyBsaWJyYXJ5OyBvdGhlcndpc2UsIHlvdSBjb3VsZCBqdXN0IGltcG9ydCBbRmFjZWJvb2tTZXJ2aWNlXSguLi9mYWNlYm9vay1zZXJ2aWNlKSBpbnRvIHlvdXIgbW9kdWxlIGluc3RlYWQuXG4gKiBUaGlzIG1vZHVsZSB3aWxsIG1ha2UgYWxsIGNvbXBvbmVudHMgYW5kIHByb3ZpZGVycyBhdmFpbGFibGUgaW4geW91ciBhcHBsaWNhdGlvbi5cbiAqXG4gKiBAdXNhZ2VcbiAqIEluIG9yZGVyIHRvIHVzZSB0aGlzIGxpYnJhcnksIHlvdSBuZWVkIHRvIGltcG9ydCBgRmFjZWJvb2tNb2R1bGVgIGludG8geW91ciBhcHAncyBtYWluIGBOZ01vZHVsZWAuXG4gKlxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgRmFjZWJvb2tNb2R1bGUgfSBmcm9tICduZzItZmFjZWJvb2stc2RrJztcbiAqXG4gKiBATmdNb2R1bGUoe1xuICogICAuLi5cbiAqICAgaW1wb3J0czogW1xuICogICAgIC4uLlxuICogICAgIEZhY2Vib29rTW9kdWxlLmZvclJvb3QoKVxuICogICBdLFxuICogICAuLi5cbiAqIH0pXG4gKiBleHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuICogYGBgXG4gKi9cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogZ2V0Q29tcG9uZW50cygpLFxuICBleHBvcnRzOiBnZXRDb21wb25lbnRzKClcbn0pXG5leHBvcnQgY2xhc3MgRmFjZWJvb2tNb2R1bGUge1xuICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IEZhY2Vib29rTW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbRmFjZWJvb2tTZXJ2aWNlXVxuICAgIH07XG4gIH1cbn1cbiJdLCJuYW1lcyI6WyJ0c2xpYl8xLl9fZXh0ZW5kcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUtBLHVCQUE4QixNQUFXLEVBQUUsR0FBVztJQUNwRCxxQkFBTSxVQUFVLEdBQUcsVUFBQyxFQUFVLElBQUssT0FBQSxPQUFPLEdBQUcsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxPQUFPLENBQUMsQ0FBQyxXQUFXLEVBQUUsR0FBQSxDQUFDO0lBQy9HLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRTtRQUNqQyxHQUFHLEVBQUUsVUFBUyxLQUFLO1lBQ2pCLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDM0M7UUFDRCxHQUFHLEVBQUU7WUFDSCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDM0M7UUFDRCxVQUFVLEVBQUUsSUFBSTtLQUNqQixDQUFDLENBQUM7Q0FDSjtTQVFVO0lBQVMsY0FBYztTQUFkLFVBQWMsRUFBZCxxQkFBYyxFQUFkLElBQWM7UUFBZCx5QkFBYzs7SUFDNUIsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztLQUN4RDtTQUFNO1FBQ0wsT0FBTyxDQUFDLElBQUksQ0FBQyw0RUFBNEUsQ0FBQyxDQUFDO1FBQzNGLE9BQU8sSUFBSSxDQUFDO0tBQ2I7Q0FDRjs7Ozs7OztBQVZMLDRCQUFtQyxNQUFXLEVBQUUsR0FBVztJQUN6RCxPQUFPO1FBQ0wsVUFBVSxFQUFFLElBQUk7UUFDaEIsS0FBSyxJQU9KO0tBQ0YsQ0FBQztDQUNIOzs7O0FBS0Q7OztBQUFBO0lBSUUsdUJBQ1UsSUFDQSxLQUNBO1FBRkEsT0FBRSxHQUFGLEVBQUU7UUFDRixRQUFHLEdBQUgsR0FBRztRQUNILFlBQU8sR0FBUCxPQUFPO1FBRWYsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQztRQUMzQyxJQUFJLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDbEU7Ozs7OztJQUVTLG9DQUFZOzs7OztJQUF0QixVQUF1QixJQUFZLEVBQUUsS0FBYTtRQUNoRCxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSztZQUFFLE9BQU87UUFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztLQUMvRDs7Ozs7SUFFUyxvQ0FBWTs7OztJQUF0QixVQUF1QixJQUFZO1FBQ2pDLElBQUksQ0FBQyxJQUFJO1lBQUUsT0FBTztRQUNsQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQzlDO3dCQTVESDtJQThEQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQzNDNENBLDJDQUFhO0lBdUJ4RCxpQ0FDRSxFQUFjLEVBQ2QsR0FBYTtlQUViLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsa0JBQWtCLENBQUM7S0FDbkM7O2dCQWhDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBbEIwQixVQUFVO2dCQUFFLFFBQVE7Ozt5QkF3QjVDLEtBQUs7MEJBT0wsS0FBSztrQ0FPTCxLQUFLOzs7UUFiTCxhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7OztrQ0F2Q2hCO0VBbUI2QyxhQUFhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNFakJBLHVDQUFhO0lBa0RwRCw2QkFDRSxFQUFjLEVBQ2QsR0FBYTtRQUZmLFlBSUUsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxhQUFhLENBQUMsU0FDOUI7Ozs7OztxQkF2Q2MsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJOztLQXVDbEM7O2dCQTNERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLFFBQVEsRUFBRSxFQUFFO2lCQUNiOzs7O2dCQXBCMEIsVUFBVTtnQkFBRSxRQUFROzs7Z0NBMEI1QyxLQUFLO3lCQVNMLEtBQUs7MkJBT0wsS0FBSzs2QkFPTCxLQUFLOzRCQU9MLEtBQUs7MEJBV0wsS0FBSzs7O1FBeENMLGFBQWE7Ozs7UUFTYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBV2IsYUFBYTs7OzhCQXBFaEI7RUFxQnlDLGFBQWE7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDSmZBLHFDQUFhO0lBb0RsRCwyQkFDRSxFQUFjLEVBQ2QsR0FBYTtlQUViLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsV0FBVyxDQUFDO0tBQzVCOztnQkE3REYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxXQUFXO29CQUNyQixRQUFRLEVBQUUsRUFBRTtpQkFDYjs7OztnQkFoQjBCLFVBQVU7Z0JBQUUsUUFBUTs7O2dDQXNCNUMsS0FBSzt5QkFPTCxLQUFLO29DQU9MLEtBQUs7MkJBUUwsS0FBSzs4QkFPTCxLQUFLO3lCQU9MLEtBQUs7MEJBT0wsS0FBSzs7O1FBMUNMLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQVFiLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs0QkFsRWhCO0VBaUJ1QyxhQUFhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDRWZBLG1DQUFhO0lBc0ZoRCx5QkFBWSxFQUFjLEVBQUUsR0FBYTtRQUF6QyxZQUNFLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsU0FBUyxDQUFDLFNBQzFCOzs7OztxQkFoRWMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJOztLQWdFbEM7O2dCQTVGRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFFBQVEsRUFBRSxFQUFFO2lCQUNiOzs7O2dCQWxCMEIsVUFBVTtnQkFBRSxRQUFROzs7MkJBeUI1QyxLQUFLO2dDQVFMLEtBQUs7eUJBUUwsS0FBSztvQ0FRTCxLQUFLOzJCQVVMLEtBQUs7d0JBUUwsS0FBSzswQkFTTCxLQUFLOzhCQVNMLEtBQUs7eUJBUUwsS0FBSzswQkFRTCxLQUFLOzs7UUEzRUwsYUFBYTs7OztRQVFiLGFBQWE7Ozs7UUFRYixhQUFhOzs7O1FBUWIsYUFBYTs7OztRQVViLGFBQWE7Ozs7UUFRYixhQUFhOzs7O1FBU2IsYUFBYTs7OztRQVNiLGFBQWE7Ozs7UUFRYixhQUFhOzs7O1FBUWIsYUFBYTs7OzBCQXRHaEI7RUFtQnFDLGFBQWE7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0RiQSxtQ0FBYTtJQXlFaEQseUJBQ0UsRUFBYyxFQUNkLEdBQWE7ZUFFYixrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQztLQUMxQjs7Z0JBbEZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsU0FBUztvQkFDbkIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBakIwQixVQUFVO2dCQUFFLFFBQVE7Ozt5QkF1QjVDLEtBQUs7MEJBUUwsS0FBSzsyQkFRTCxLQUFLO3lCQVFMLEtBQUs7OEJBUUwsS0FBSztpQ0FRTCxLQUFLOzRCQVFMLEtBQUs7Z0NBUUwsS0FBSzt3Q0FRTCxLQUFLOzs7UUEvREwsYUFBYTs7OztRQVFiLGFBQWE7Ozs7UUFRYixhQUFhOzs7O1FBUWIsYUFBYTs7OztRQVFiLGFBQWE7Ozs7UUFRYixhQUFhOzs7O1FBUWIsYUFBYTs7OztRQVFiLGFBQWE7Ozs7UUFRYixhQUFhOzs7MEJBeEZoQjtFQWtCcUMsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0NiQSxtQ0FBYTtJQXVCaEQseUJBQ0UsRUFBYyxFQUNkLEdBQWE7ZUFFYixrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQztLQUMxQjs7Z0JBaENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsU0FBUztvQkFDbkIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBbEIwQixVQUFVO2dCQUFFLFFBQVE7Ozt5QkF3QjVDLEtBQUs7MEJBT0wsS0FBSzs2QkFPTCxLQUFLOzs7UUFiTCxhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7OzswQkF2Q2hCO0VBbUJxQyxhQUFhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDQVpBLG9DQUFhO0lBaUJqRCwwQkFDRSxFQUFjLEVBQ2QsR0FBYTtlQUViLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDO0tBQzNCOztnQkExQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxVQUFVO29CQUNwQixRQUFRLEVBQUUsRUFBRTtpQkFDYjs7OztnQkFsQjBCLFVBQVU7Z0JBQUUsUUFBUTs7O3lCQXlCNUMsS0FBSzsyQkFPTCxLQUFLOzs7UUFOTCxhQUFhOzs7O1FBT2IsYUFBYTs7OzJCQWpDaEI7RUFtQnNDLGFBQWE7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0RkQSxtQ0FBYTtJQVVoRCx5QkFDRSxFQUFjLEVBQ2QsR0FBYTtRQUZmLFlBSUUsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsU0FDMUI7Ozs7O29CQVBhLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTs7S0FPakM7O2dCQW5CRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFFBQVEsRUFBRSxFQUFFO2lCQUNiOzs7O2dCQWpCMEIsVUFBVTtnQkFBRSxRQUFROzs7d0JBd0I1QyxLQUFLOzs7UUFDTCxhQUFhOzs7MEJBekJoQjtFQWtCcUMsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDQWJBLG1DQUFhO0lBcUNoRCx5QkFDRSxFQUFjLEVBQ2QsR0FBYTtRQUZmLFlBSUUsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsU0FDMUI7Ozs7cUJBNUJjLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTs7S0E0QmxDOztnQkE5Q0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxTQUFTO29CQUNuQixRQUFRLEVBQUUsRUFBRTtpQkFDYjs7OztnQkFqQjBCLFVBQVU7Z0JBQUUsUUFBUTs7O2dDQXVCNUMsS0FBSzt5QkFPTCxLQUFLO29DQU9MLEtBQUs7d0JBT0wsS0FBSzt5QkFPTCxLQUFLOzs7UUEzQkwsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7OzswQkFwRGhCO0VBa0JxQyxhQUFhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNBWkEsb0NBQWE7SUE4QmpELDBCQUNFLEVBQWMsRUFDZCxHQUFhO2VBRWIsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQztLQUNsQzs7Z0JBdkNGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsVUFBVTtvQkFDcEIsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Ozs7Z0JBakIwQixVQUFVO2dCQUFFLFFBQVE7Ozt5QkF1QjVDLEtBQUs7MkJBT0wsS0FBSztpQ0FPTCxLQUFLO3lCQU9MLEtBQUs7OztRQXBCTCxhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7MkJBN0NoQjtFQWtCc0MsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDeUJiQSxvQ0FBYTtJQXVGakQsMEJBQ0UsRUFBYyxFQUNkLEdBQWE7UUFGZixZQUlFLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDLFNBRTNCOzs7OytCQTNDbUMsSUFBSSxZQUFZLEVBQU87Ozs7dUJBTS9CLElBQUksWUFBWSxFQUFPOzs7OztnQ0FPZCxJQUFJLFlBQVksRUFBTzs7OztpQ0FNdEIsSUFBSSxZQUFZLEVBQU87Ozs7a0NBTXRCLElBQUksWUFBWSxFQUFPOzs7O3NCQU1uQyxJQUFJLFlBQVksRUFBTzsyQkFJdEIsRUFBRTtRQU81QixLQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsR0FBRyxLQUFJLENBQUMsR0FBRyxHQUFHLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs7S0FDL0Y7Ozs7Ozs7O0lBS0QsbUNBQVE7Ozs7SUFBUjtRQUFBLGlCQWNDO1FBYkMsRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLFVBQUMsR0FBUTtZQUN6QyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssT0FBTyxJQUFJLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSSxDQUFDLEdBQUcsRUFBRTtnQkFDL0MsS0FBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO2dCQUM5QixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FDbEIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsVUFBQyxDQUFNLElBQUssT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQSxDQUFDLEVBQ25GLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsRUFDbkUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsVUFBQyxDQUFNLElBQUssT0FBQSxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQSxDQUFDLEVBQ3JGLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLFVBQUMsQ0FBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQSxDQUFDLEVBQ3ZGLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLFVBQUMsQ0FBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQSxDQUFDLEVBQ3pGLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsQ0FDbEUsQ0FBQzthQUNIO1NBQ0YsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7O0lBS0Qsc0NBQVc7Ozs7SUFBWDtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQztZQUN2QixJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sS0FBSyxVQUFVLEVBQUU7Z0JBQ25DLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUNiO1NBQ0YsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7O0lBTUQsK0JBQUk7Ozs7bUJBQUs7Ozs7Ozs7O0lBTVQsZ0NBQUs7Ozs7bUJBQUs7Ozs7Ozs7Ozs7SUFPViwrQkFBSTs7Ozs7Y0FBQyxPQUFlLEtBQUk7Ozs7Ozs7O0lBTXhCLCtCQUFJOzs7O21CQUFLOzs7Ozs7OztJQU1ULGlDQUFNOzs7O21CQUFLOzs7Ozs7OztJQU1YLGtDQUFPOzs7O2tCQUFjLE9BQU8sRUFBRTs7Ozs7Ozs7OztJQU85QixvQ0FBUzs7Ozs7Y0FBQyxNQUFjLEtBQUk7Ozs7Ozs7O0lBTTVCLG9DQUFTOzs7O2tCQUFhLE9BQU8sRUFBRTs7Ozs7Ozs7SUFNL0IsNkNBQWtCOzs7O21CQUFLOzs7Ozs7OztJQU12QixzQ0FBVzs7OzttQkFBSzs7Z0JBN0xqQixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLFFBQVEsRUFBRSxFQUFFO2lCQUNiOzs7O2dCQTFDa0MsVUFBVTtnQkFBRSxRQUFROzs7eUJBa0RwRCxLQUFLO29DQU9MLEtBQUs7NkJBT0wsS0FBSzswQkFPTCxLQUFLOzZCQU9MLEtBQUs7aUNBT0wsS0FBSzttQ0FPTCxNQUFNOzJCQU1OLE1BQU07b0NBT04sTUFBTTtxQ0FNTixNQUFNO3NDQU1OLE1BQU07MEJBTU4sTUFBTTs7O1FBeEVOLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQU9iLGFBQWE7Ozs7UUFPYixhQUFhOzs7O1FBT2IsYUFBYTs7OztRQXFGYixrQkFBa0I7Ozs7Z0RBQ1Y7O1FBS1Isa0JBQWtCOzs7O2lEQUNUOztRQU1ULGtCQUFrQjs7OztnREFDSzs7UUFLdkIsa0JBQWtCOzs7O2dEQUNWOztRQUtSLGtCQUFrQjs7OztrREFDUjs7UUFLVixrQkFBa0I7Ozs7bURBQ1c7O1FBTTdCLGtCQUFrQjs7OztxREFDUzs7UUFLM0Isa0JBQWtCOzs7O3FEQUNZOztRQUs5QixrQkFBa0I7Ozs7OERBQ0k7O1FBS3RCLGtCQUFrQjs7Ozt1REFDSDsyQkFwT2xCO0VBMkNzQyxhQUFhOzs7Ozs7QUMzQ25EOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUE0Q0UsOEJBQUk7Ozs7O0lBQUosVUFBSyxNQUFrQjtRQUNyQixJQUFJO1lBQ0YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUN6QztRQUFDLHdCQUFPLENBQUMsRUFBRTtZQUNWLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxQjtLQUNGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFlRCw2QkFBRzs7Ozs7Ozs7Ozs7OztJQUFILFVBQUksSUFBWSxFQUFFLE1BQXlCLEVBQUUsTUFBZ0I7UUFBM0MsdUJBQUEsRUFBQSxjQUF5QjtRQUFFLHVCQUFBLEVBQUEsV0FBZ0I7UUFDM0QsT0FBTyxJQUFJLE9BQU8sQ0FBTSxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRXRDLElBQUk7Z0JBQ0YsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxVQUFDLFFBQWE7b0JBQ3pDLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQ2IsTUFBTSxFQUFFLENBQUM7cUJBQ1Y7eUJBQU0sSUFBSSxRQUFRLENBQUMsS0FBSyxFQUFFO3dCQUN6QixNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN4Qjt5QkFBTTt3QkFDTCxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ25CO2lCQUNGLENBQUMsQ0FBQzthQUNKO1lBQUMsd0JBQU8sQ0FBQyxFQUFFO2dCQUNWLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNYO1NBRUYsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQWVELDRCQUFFOzs7Ozs7Ozs7Ozs7O0lBQUYsVUFBRyxNQUFnQjtRQUNqQixPQUFPLElBQUksT0FBTyxDQUFNLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFdEMsSUFBSTtnQkFDRixFQUFFLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFDLFFBQWE7b0JBQzFCLElBQUcsQ0FBQyxRQUFRO3dCQUFFLE1BQU0sRUFBRSxDQUFDO3lCQUNsQixJQUFHLFFBQVEsQ0FBQyxLQUFLO3dCQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7O3dCQUMxQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3hCLENBQUMsQ0FBQzthQUNKO1lBQUMsd0JBQU8sQ0FBQyxFQUFFO2dCQUNWLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNYO1NBRUYsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7Ozs7O0lBT0Qsd0NBQWM7Ozs7O0lBQWQsVUFBZSxrQkFBNEI7UUFDekMsT0FBTyxJQUFJLE9BQU8sQ0FBYyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRTlDLElBQUk7Z0JBQ0YsRUFBRSxDQUFDLGNBQWMsQ0FBQyxVQUFDLFFBQXFCO29CQUN0QyxJQUFJLENBQUMsUUFBUSxFQUFFO3dCQUNiLE1BQU0sRUFBRSxDQUFDO3FCQUNWO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDbkI7aUJBQ0YsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO2FBQ3hCO1lBQUMsd0JBQU8sQ0FBQyxFQUFFO2dCQUNWLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNYO1NBRUYsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXdCRCwrQkFBSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQUFMLFVBQU0sT0FBc0I7UUFDMUIsT0FBTyxJQUFJLE9BQU8sQ0FBZ0IsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUVoRCxJQUFJO2dCQUNGLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBQyxRQUF1QjtvQkFDL0IsSUFBSSxRQUFRLENBQUMsWUFBWSxFQUFFO3dCQUN6QixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ25CO3lCQUFJO3dCQUNILE1BQU0sRUFBRSxDQUFDO3FCQUNWO2lCQUNGLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDYjtZQUFDLHdCQUFPLENBQUMsRUFBRTtnQkFDVixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDWDtTQUVGLENBQUMsQ0FBQztLQUNKOzs7Ozs7Ozs7Ozs7Ozs7OztJQVVELGdDQUFNOzs7Ozs7OztJQUFOO1FBQ0UsT0FBTyxJQUFJLE9BQU8sQ0FBTSxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRXRDLElBQUk7Z0JBQ0YsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFDLFFBQWE7b0JBQ3RCLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDbkIsQ0FBQyxDQUFDO2FBQ0o7WUFBQyx3QkFBTyxDQUFDLEVBQUU7Z0JBQ1YsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1g7U0FFRixDQUFDLENBQUM7S0FDSjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQWNELHlDQUFlOzs7Ozs7Ozs7Ozs7SUFBZjtRQUNFLElBQUk7WUFDRix5QkFBcUIsRUFBRSxDQUFDLGVBQWUsRUFBRSxFQUFDO1NBQzNDO1FBQUMsd0JBQU8sQ0FBQyxFQUFFO1lBQ1YsT0FBTyxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN4QztLQUNGOztnQkFyTEYsVUFBVTs7MEJBcENYOzs7Ozs7O0FDQUEsQUFnQkEscUJBQU0sVUFBVSxHQUFVO0lBQ3hCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixlQUFlO0lBQ2YsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixnQkFBZ0I7Q0FDakIsQ0FBQzs7OztBQUVGO0lBQ0UsT0FBTyxVQUFVLENBQUM7Q0FDbkI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUErQlEsc0JBQU87OztJQUFkO1FBQ0UsT0FBTztZQUNMLFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFNBQVMsRUFBRSxDQUFDLGVBQWUsQ0FBQztTQUM3QixDQUFDO0tBQ0g7O2dCQVZGLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsYUFBYSxFQUFFO29CQUM3QixPQUFPLEVBQUUsYUFBYSxFQUFFO2lCQUN6Qjs7eUJBN0REOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==