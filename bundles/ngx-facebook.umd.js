(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('ngx-facebook', ['exports', '@angular/core'], factory) :
    (factory((global['ngx-facebook'] = {}),global.ng.core));
}(this, (function (exports,core) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b)
            if (b.hasOwnProperty(p))
                d[p] = b[p]; };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @hidden
     * @param {?} target
     * @param {?} key
     * @return {?}
     */
    function FBMLAttribute(target, key) {
        var /** @type {?} */ processKey = function (_k) { return 'data-' + _k.toString().replace(/([a-z\d])([A-Z])/g, '$1-$2').toLowerCase(); };
        Object.defineProperty(target, key, {
            set: function (value) {
                value = value.toString();
                this.setAttribute(processKey(key), value);
            },
            get: function () {
                return this.getAttribute(processKey(key));
            },
            enumerable: true
        });
    }
    var ɵ0 = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this._instance) {
            return this._instance[key].apply(this._instance, args);
        }
        else {
            console.warn('ng2-facebook-sdk: tried calling instance method before component is ready.');
            return null;
        }
    };
    /**
     * @hidden
     * @param {?} target
     * @param {?} key
     * @return {?}
     */
    function FBMLInstanceMethod(target, key) {
        return {
            enumerable: true,
            value: ɵ0
        };
    }
    /**
     * @hidden
     */
    var /**
     * @hidden
     */ FBMLComponent = (function () {
        function FBMLComponent(el, rnd, fbClass) {
            this.el = el;
            this.rnd = rnd;
            this.fbClass = fbClass;
            this.nativeElement = this.el.nativeElement;
            this.rnd.setElementClass(this.nativeElement, this.fbClass, true);
        }
        /**
         * @param {?} name
         * @param {?} value
         * @return {?}
         */
        FBMLComponent.prototype.setAttribute = /**
         * @param {?} name
         * @param {?} value
         * @return {?}
         */
            function (name, value) {
                if (!name || !value)
                    return;
                this.rnd.setElementAttribute(this.nativeElement, name, value);
            };
        /**
         * @param {?} name
         * @return {?}
         */
        FBMLComponent.prototype.getAttribute = /**
         * @param {?} name
         * @return {?}
         */
            function (name) {
                if (!name)
                    return;
                return this.nativeElement.getAttribute(name);
            };
        return FBMLComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Embedded Comments
     * \@shortdesc Embedded comments component
     * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-comments
     * \@description
     * Embedded comments are a simple way to put public post comments - by a Page or a person on Facebook - into the content of your web site or web page.
     * Only public comments from Facebook Pages and profiles can be embedded.
     * \@usage
     * ```html
     * <fb-comment-embed href="https://www.facebook.com/zuck/posts/10102735452532991?comment_id=1070233703036185" width="500"></fb-comment-embed>
     * ```
     */
    var FBCommentEmbedComponent = (function (_super) {
        __extends(FBCommentEmbedComponent, _super);
        function FBCommentEmbedComponent(el, rnd) {
            return _super.call(this, el, rnd, 'fb-comment-embed') || this;
        }
        FBCommentEmbedComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-comment-embed',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBCommentEmbedComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBCommentEmbedComponent.propDecorators = {
            "href": [{ type: core.Input },],
            "width": [{ type: core.Input },],
            "includeParent": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBCommentEmbedComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBCommentEmbedComponent.prototype, "width", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBCommentEmbedComponent.prototype, "includeParent", void 0);
        return FBCommentEmbedComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Comments
     * \@shortdesc Comments component
     * \@fbdoc https://developers.facebook.com/docs/plugins/comments
     * \@description
     * The comments plugin lets people comment on content on your site using their Facebook account.
     * People can choose to share their comment activity with their friends (and friends of their friends) on Facebook as well.
     * The comments plugin also includes built-in moderation tools and social relevance ranking.
     *
     * \@usage
     * ```html
     * <fb-comments></fb-comments>
     * ```
     */
    var FBCommentsComponent = (function (_super) {
        __extends(FBCommentsComponent, _super);
        function FBCommentsComponent(el, rnd) {
            var _this = _super.call(this, el, rnd, 'fb-comments') || this;
            /**
             * The absolute URL that comments posted in the plugin will be permanently associated with.
             * All stories shared on Facebook about comments posted using the comments plugin will link to this URL.
             * Defaults to current URL.
             */
            _this.href = window.location.href;
            return _this;
        }
        FBCommentsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-comments',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBCommentsComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBCommentsComponent.propDecorators = {
            "colorscheme": [{ type: core.Input },],
            "href": [{ type: core.Input },],
            "mobile": [{ type: core.Input },],
            "numposts": [{ type: core.Input },],
            "orderBy": [{ type: core.Input },],
            "width": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBCommentsComponent.prototype, "colorscheme", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBCommentsComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBCommentsComponent.prototype, "mobile", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Number)
        ], FBCommentsComponent.prototype, "numposts", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBCommentsComponent.prototype, "orderBy", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBCommentsComponent.prototype, "width", void 0);
        return FBCommentsComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Follow Button
     * \@shortdesc Follow button component
     * \@fbdoc https://developers.facebook.com/docs/plugins/follow-button
     * \@description The Follow button lets people subscribe to the public updates of others on Facebook.
     * \@usage
     * ```html
     * <fb-follow href="https://www.facebook.com/zuck"></fb-follow>
     * ```
     */
    var FBFollowComponent = (function (_super) {
        __extends(FBFollowComponent, _super);
        function FBFollowComponent(el, rnd) {
            return _super.call(this, el, rnd, 'fb-follow') || this;
        }
        FBFollowComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-follow',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBFollowComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBFollowComponent.propDecorators = {
            "colorScheme": [{ type: core.Input },],
            "href": [{ type: core.Input },],
            "kidDirectedSite": [{ type: core.Input },],
            "layout": [{ type: core.Input },],
            "showFaces": [{ type: core.Input },],
            "size": [{ type: core.Input },],
            "width": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBFollowComponent.prototype, "colorScheme", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBFollowComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBFollowComponent.prototype, "kidDirectedSite", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBFollowComponent.prototype, "layout", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBFollowComponent.prototype, "showFaces", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBFollowComponent.prototype, "size", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBFollowComponent.prototype, "width", void 0);
        return FBFollowComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Like Button
     * \@shortdesc Like button component
     * \@fbdoc https://developers.facebook.com/docs/plugins/like-button
     * \@description
     * A single click on the Like button will 'like' pieces of content on the web and share them on Facebook.
     * You can also display a Share button next to the Like button to let people add a personal message and customize who they share with.
     * \@usage
     * ```html
     * <fb-like href="https://www.facebook.com/zuck"></fb-like>
     * ```
     */
    var FBLikeComponent = (function (_super) {
        __extends(FBLikeComponent, _super);
        function FBLikeComponent(el, rnd) {
            var _this = _super.call(this, el, rnd, 'fb-like') || this;
            /**
             * The absolute URL of the page that will be liked.
             * Defaults to the current URL.
             */
            _this.href = window.location.href;
            return _this;
        }
        FBLikeComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-like',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBLikeComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBLikeComponent.propDecorators = {
            "action": [{ type: core.Input },],
            "colorScheme": [{ type: core.Input },],
            "href": [{ type: core.Input },],
            "kidDirectedSite": [{ type: core.Input },],
            "layout": [{ type: core.Input },],
            "ref": [{ type: core.Input },],
            "share": [{ type: core.Input },],
            "showFaces": [{ type: core.Input },],
            "size": [{ type: core.Input },],
            "width": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBLikeComponent.prototype, "action", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBLikeComponent.prototype, "colorScheme", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBLikeComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBLikeComponent.prototype, "kidDirectedSite", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBLikeComponent.prototype, "layout", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBLikeComponent.prototype, "ref", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBLikeComponent.prototype, "share", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBLikeComponent.prototype, "showFaces", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBLikeComponent.prototype, "size", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBLikeComponent.prototype, "width", void 0);
        return FBLikeComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Page Plugin
     * \@shortdesc Page plugin component
     * \@fbdoc https://developers.facebook.com/docs/plugins/page-plugin
     * \@description
     * The Page plugin lets you easily embed and promote any Facebook Page on your website. Just like on Facebook, your visitors can like and share the Page without leaving your site.
     * \@usage
     * ```html
     * <fb-page href="https://facebook.com/facebook"></fb-page>
     * ```
     */
    var FBPageComponent = (function (_super) {
        __extends(FBPageComponent, _super);
        function FBPageComponent(el, rnd) {
            return _super.call(this, el, rnd, 'fb-page') || this;
        }
        FBPageComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-page',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBPageComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBPageComponent.propDecorators = {
            "href": [{ type: core.Input },],
            "width": [{ type: core.Input },],
            "height": [{ type: core.Input },],
            "tabs": [{ type: core.Input },],
            "hideCover": [{ type: core.Input },],
            "showFacepile": [{ type: core.Input },],
            "hideCTA": [{ type: core.Input },],
            "smallHeader": [{ type: core.Input },],
            "adaptContainerWidth": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBPageComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Number)
        ], FBPageComponent.prototype, "width", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Number)
        ], FBPageComponent.prototype, "height", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBPageComponent.prototype, "tabs", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBPageComponent.prototype, "hideCover", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBPageComponent.prototype, "showFacepile", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBPageComponent.prototype, "hideCTA", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBPageComponent.prototype, "smallHeader", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBPageComponent.prototype, "adaptContainerWidth", void 0);
        return FBPageComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Embedded Posts
     * \@shortdesc Embedded post component
     * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-posts
     * \@description
     * Embedded Posts are a simple way to put public posts - by a Page or a person on Facebook - into the content of your web site or web page.
     * Only public posts from Facebook Pages and profiles can be embedded.
     * \@usage
     * ```html
     * <fb-post href="https://www.facebook.com/20531316728/posts/10154009990506729/"></fb-post>
     * ```
     */
    var FBPostComponent = (function (_super) {
        __extends(FBPostComponent, _super);
        function FBPostComponent(el, rnd) {
            return _super.call(this, el, rnd, 'fb-post') || this;
        }
        FBPostComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-post',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBPostComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBPostComponent.propDecorators = {
            "href": [{ type: core.Input },],
            "width": [{ type: core.Input },],
            "showText": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBPostComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBPostComponent.prototype, "width", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBPostComponent.prototype, "showText", void 0);
        return FBPostComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Quote Plugin
     * \@shortdesc Quote plugin component
     * \@fbdoc https://developers.facebook.com/docs/plugins/quote
     * \@description
     * The quote plugin lets people select text on your page and add it to their share, so they can tell a more expressive story.
     * Note that you do not need to implement Facebook login or request any additional permissions through app review in order to use this plugin.
     * \@usage
     * ```html
     * <fb-quote></fb-quote>
     * ```
     */
    var FBQuoteComponent = (function (_super) {
        __extends(FBQuoteComponent, _super);
        function FBQuoteComponent(el, rnd) {
            return _super.call(this, el, rnd, 'fb-quote') || this;
        }
        FBQuoteComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-quote',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBQuoteComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBQuoteComponent.propDecorators = {
            "href": [{ type: core.Input },],
            "layout": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBQuoteComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBQuoteComponent.prototype, "layout", void 0);
        return FBQuoteComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Save Button
     * \@shortdesc Save button component.
     * \@fbdoc https://developers.facebook.com/docs/plugins/save
     * \@description
     * The Save button lets people save items or services to a private list on Facebook, share it with friends, and receive relevant notifications.
     * \@usage
     * ```html
     * <fb-save uri="https://github.com/zyra/ng2-facebook-sdk/"></fb-save>
     * ```
     */
    var FBSaveComponent = (function (_super) {
        __extends(FBSaveComponent, _super);
        function FBSaveComponent(el, rnd) {
            var _this = _super.call(this, el, rnd, 'fb-save') || this;
            /**
             * The absolute link of the page that will be saved.
             * Current Link/Address
             */
            _this.uri = window.location.href;
            return _this;
        }
        FBSaveComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-save',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBSaveComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBSaveComponent.propDecorators = {
            "uri": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBSaveComponent.prototype, "uri", void 0);
        return FBSaveComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Send Button
     * \@shortdesc Send button component
     * \@fbdoc https://developers.facebook.com/docs/plugins/send-button
     * \@description
     * The Send button lets people privately send content on your site to one or more friends in a Facebook message.
     * \@usage
     * ```html
     * <fb-send href="https://github.com/zyra/ng2-facebook-sdk/"></fb-send>
     * ```
     */
    var FBSendComponent = (function (_super) {
        __extends(FBSendComponent, _super);
        function FBSendComponent(el, rnd) {
            var _this = _super.call(this, el, rnd, 'fb-send') || this;
            /**
             * The absolute URL of the page that will be sent. Defaults to the current URL.
             */
            _this.href = window.location.href;
            return _this;
        }
        FBSendComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-send',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBSendComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBSendComponent.propDecorators = {
            "colorScheme": [{ type: core.Input },],
            "href": [{ type: core.Input },],
            "kidDirectedSite": [{ type: core.Input },],
            "ref": [{ type: core.Input },],
            "size": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBSendComponent.prototype, "colorScheme", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBSendComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBSendComponent.prototype, "kidDirectedSite", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBSendComponent.prototype, "ref", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBSendComponent.prototype, "size", void 0);
        return FBSendComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Share Button
     * \@shortdesc Share button component
     * \@fbdoc https://developers.facebook.com/docs/plugins/share-button
     * \@description
     * The Share button lets people add a personalized message to links before sharing on their timeline, in groups, or to their friends via a Facebook Message.
     * \@usage
     * ```html
     * <fb-share href="https://github.com/zyra/ng2-facebook-sdk/"></fb-share>
     * ```
     */
    var FBShareComponent = (function (_super) {
        __extends(FBShareComponent, _super);
        function FBShareComponent(el, rnd) {
            return _super.call(this, el, rnd, 'fb-share-button') || this;
        }
        FBShareComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-share',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBShareComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBShareComponent.propDecorators = {
            "href": [{ type: core.Input },],
            "layout": [{ type: core.Input },],
            "mobileIframe": [{ type: core.Input },],
            "size": [{ type: core.Input },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBShareComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBShareComponent.prototype, "layout", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBShareComponent.prototype, "mobileIframe", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBShareComponent.prototype, "size", void 0);
        return FBShareComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@name Embedded Video
     * \@shortdesc Component to embed Facebook videos
     * \@fbdoc https://developers.facebook.com/docs/plugins/embedded-video-player
     * \@description Component to embed Facebook videos and control them.
     * \@usage
     * ```html
     * <!-- basic usage -->
     * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/"></fb-video>
     *
     * <!-- event emitters -->
     * <fb-video href="https://www.facebook.com/facebook/videos/10153231379946729/" (paused)="onVideoPaused($event)"></fb-video>
     * ```
     *
     * To manually interact with the video player, fetch it using `ViewChild`.
     *
     * ```ts
     * import { Component, ViewChild } from '\@angular/core';
     * import { FBVideoComponent } from 'ng2-facebook-sdk';
     *
     * \@Component(...)
     * export class MyComponent {
     *
     *   \@ViewChild(FBVideoComponent) video: FBVideoComponent;
     *
     *   ngAfterViewInit() {
     *     this.video.play();
     *     this.video.pause();
     *     this.video.getVolume();
     *   }
     *
     * }
     *
     * ```
     */
    var FBVideoComponent = (function (_super) {
        __extends(FBVideoComponent, _super);
        function FBVideoComponent(el, rnd) {
            var _this = _super.call(this, el, rnd, 'fb-video') || this;
            /**
             * Fired when video starts to play.
             */
            _this.startedPlaying = new core.EventEmitter();
            /**
             * Fired when video pauses.
             */
            _this.paused = new core.EventEmitter();
            /**
             *
             * Fired when video finishes playing.
             */
            _this.finishedPlaying = new core.EventEmitter();
            /**
             * Fired when video starts to buffer.
             */
            _this.startedBuffering = new core.EventEmitter();
            /**
             * Fired when video recovers from buffering.
             */
            _this.finishedBuffering = new core.EventEmitter();
            /**
             * Fired when an error occurs on the video.
             */
            _this.error = new core.EventEmitter();
            _this._listeners = [];
            _this.nativeElement.id = _this._id = 'video-' + String(Math.floor((Math.random() * 10000) + 1));
            return _this;
        }
        /**
         * @hidden
         */
        /**
         * @hidden
         * @return {?}
         */
        FBVideoComponent.prototype.ngOnInit = /**
         * @hidden
         * @return {?}
         */
            function () {
                var _this = this;
                FB.Event.subscribe('xfbml.ready', function (msg) {
                    if (msg.type === 'video' && msg.id === _this._id) {
                        _this._instance = msg.instance;
                        _this._listeners.push(_this._instance.subscribe('startedPlaying', function (e) { return _this.startedPlaying.emit(e); }), _this._instance.subscribe('paused', function (e) { return _this.paused.emit(e); }), _this._instance.subscribe('finishedPlaying', function (e) { return _this.finishedPlaying.emit(e); }), _this._instance.subscribe('startedBuffering', function (e) { return _this.startedBuffering.emit(e); }), _this._instance.subscribe('finishedBuffering', function (e) { return _this.finishedBuffering.emit(e); }), _this._instance.subscribe('error', function (e) { return _this.error.emit(e); }));
                    }
                });
            };
        /**
         * @hidden
         */
        /**
         * @hidden
         * @return {?}
         */
        FBVideoComponent.prototype.ngOnDestroy = /**
         * @hidden
         * @return {?}
         */
            function () {
                this._listeners.forEach(function (l) {
                    if (typeof l.release === 'function') {
                        l.release();
                    }
                });
            };
        /**
         * Plays the video.
         */
        /**
         * Plays the video.
         * @return {?}
         */
        FBVideoComponent.prototype.play = /**
         * Plays the video.
         * @return {?}
         */
            function () { };
        /**
         * Pauses the video.
         */
        /**
         * Pauses the video.
         * @return {?}
         */
        FBVideoComponent.prototype.pause = /**
         * Pauses the video.
         * @return {?}
         */
            function () { };
        /**
         * Seeks to specified position.
         * @param seconds {number}
         */
        /**
         * Seeks to specified position.
         * @param {?} seconds {number}
         * @return {?}
         */
        FBVideoComponent.prototype.seek = /**
         * Seeks to specified position.
         * @param {?} seconds {number}
         * @return {?}
         */
            function (seconds) { };
        /**
         * Mute the video.
         */
        /**
         * Mute the video.
         * @return {?}
         */
        FBVideoComponent.prototype.mute = /**
         * Mute the video.
         * @return {?}
         */
            function () { };
        /**
         * Unmute the video.
         */
        /**
         * Unmute the video.
         * @return {?}
         */
        FBVideoComponent.prototype.unmute = /**
         * Unmute the video.
         * @return {?}
         */
            function () { };
        /**
         * Returns true if video is muted, false if not.
         */
        /**
         * Returns true if video is muted, false if not.
         * @return {?}
         */
        FBVideoComponent.prototype.isMuted = /**
         * Returns true if video is muted, false if not.
         * @return {?}
         */
            function () { return; };
        /**
         * Set the volume
         * @param volume
         */
        /**
         * Set the volume
         * @param {?} volume
         * @return {?}
         */
        FBVideoComponent.prototype.setVolume = /**
         * Set the volume
         * @param {?} volume
         * @return {?}
         */
            function (volume) { };
        /**
         * Get the volume
         */
        /**
         * Get the volume
         * @return {?}
         */
        FBVideoComponent.prototype.getVolume = /**
         * Get the volume
         * @return {?}
         */
            function () { return; };
        /**
         * Returns the current video time position in seconds
         */
        /**
         * Returns the current video time position in seconds
         * @return {?}
         */
        FBVideoComponent.prototype.getCurrentPosition = /**
         * Returns the current video time position in seconds
         * @return {?}
         */
            function () { };
        /**
         * Returns the video duration in seconds
         */
        /**
         * Returns the video duration in seconds
         * @return {?}
         */
        FBVideoComponent.prototype.getDuration = /**
         * Returns the video duration in seconds
         * @return {?}
         */
            function () { };
        FBVideoComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'fb-video',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        FBVideoComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.Renderer, },
            ];
        };
        FBVideoComponent.propDecorators = {
            "href": [{ type: core.Input },],
            "allowfullscreen": [{ type: core.Input },],
            "autoplay": [{ type: core.Input },],
            "width": [{ type: core.Input },],
            "showText": [{ type: core.Input },],
            "showCaptions": [{ type: core.Input },],
            "startedPlaying": [{ type: core.Output },],
            "paused": [{ type: core.Output },],
            "finishedPlaying": [{ type: core.Output },],
            "startedBuffering": [{ type: core.Output },],
            "finishedBuffering": [{ type: core.Output },],
            "error": [{ type: core.Output },],
        };
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBVideoComponent.prototype, "href", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBVideoComponent.prototype, "allowfullscreen", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBVideoComponent.prototype, "autoplay", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", String)
        ], FBVideoComponent.prototype, "width", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBVideoComponent.prototype, "showText", void 0);
        __decorate([
            FBMLAttribute,
            __metadata("design:type", Boolean)
        ], FBVideoComponent.prototype, "showCaptions", void 0);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "play", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "pause", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Number]),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "seek", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "mute", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "unmute", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", Boolean)
        ], FBVideoComponent.prototype, "isMuted", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Number]),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "setVolume", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", Number)
        ], FBVideoComponent.prototype, "getVolume", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "getCurrentPosition", null);
        __decorate([
            FBMLInstanceMethod,
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], FBVideoComponent.prototype, "getDuration", null);
        return FBVideoComponent;
    }(FBMLComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * \@shortdesc
     * Angular 2 service to inject to use Facebook's SDK
     * \@description
     * You only need to inject this service in your application if you aren't using [`FacebookModule`](../facebook-module).
     * \@usage
     * ```typescript
     * import { FacebookService, InitParams } from 'ng2-facebook-sdk';
     *
     * constructor(private fb: FacebookService) {
     *
     *   const params: InitParams = {
     *
     *   };
     *
     *   fb.init(params);
     *
     * }
     * ```
     */
    var FacebookService = (function () {
        function FacebookService() {
        }
        /**
         * This method is used to initialize and setup the SDK.
         * @param params {InitParams} Initialization parameters
         * @returns return {Promise<any>}
         */
        /**
         * This method is used to initialize and setup the SDK.
         * @param {?} params {InitParams} Initialization parameters
         * @return {?} return {Promise<any>}
         */
        FacebookService.prototype.init = /**
         * This method is used to initialize and setup the SDK.
         * @param {?} params {InitParams} Initialization parameters
         * @return {?} return {Promise<any>}
         */
            function (params) {
                try {
                    return Promise.resolve(FB.init(params));
                }
                catch (e) {
                    return Promise.reject(e);
                }
            };
        /**
         * This method lets you make calls to the Graph API
         * @usage
         * ```typescript
         * this.fb.api('somepath')
         *   .then(res => console.log(res))
         *   .catch(e => console.log(e));
         * ```
         * @param path {string} The Graph API endpoint path that you want to call.
         * @param [method=get] {string} The HTTP method that you want to use for the API request.
         * @param [params] {Object} An object consisting of any parameters that you want to pass into your Graph API call.
         * @returns return {Promise<any>}
         */
        /**
         * This method lets you make calls to the Graph API
         * \@usage
         * ```typescript
         * this.fb.api('somepath')
         *   .then(res => console.log(res))
         *   .catch(e => console.log(e));
         * ```
         * @param {?} path {string} The Graph API endpoint path that you want to call.
         * @param {?=} method
         * @param {?=} params
         * @return {?} return {Promise<any>}
         */
        FacebookService.prototype.api = /**
         * This method lets you make calls to the Graph API
         * \@usage
         * ```typescript
         * this.fb.api('somepath')
         *   .then(res => console.log(res))
         *   .catch(e => console.log(e));
         * ```
         * @param {?} path {string} The Graph API endpoint path that you want to call.
         * @param {?=} method
         * @param {?=} params
         * @return {?} return {Promise<any>}
         */
            function (path, method, params) {
                if (method === void 0) {
                    method = 'get';
                }
                if (params === void 0) {
                    params = {};
                }
                return new Promise(function (resolve, reject) {
                    try {
                        FB.api(path, method, params, function (response) {
                            if (!response) {
                                reject();
                            }
                            else if (response.error) {
                                reject(response.error);
                            }
                            else {
                                resolve(response);
                            }
                        });
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            };
        /**
         * This method is used to trigger different forms of Facebook created UI dialogs.
         * These dialogs include:
         * - Share dialog
         * - Login dialog
         * - Add page tab dialog
         * - Requests dialog
         * - Send dialog
         * - Payments dialog
         * - Go Live dialog
         * @param params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
         * @returns return {Promise<UIResponse>}
         */
        /**
         * This method is used to trigger different forms of Facebook created UI dialogs.
         * These dialogs include:
         * - Share dialog
         * - Login dialog
         * - Add page tab dialog
         * - Requests dialog
         * - Send dialog
         * - Payments dialog
         * - Go Live dialog
         * @param {?} params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
         * @return {?} return {Promise<UIResponse>}
         */
        FacebookService.prototype.ui = /**
         * This method is used to trigger different forms of Facebook created UI dialogs.
         * These dialogs include:
         * - Share dialog
         * - Login dialog
         * - Add page tab dialog
         * - Requests dialog
         * - Send dialog
         * - Payments dialog
         * - Go Live dialog
         * @param {?} params {UIParams} A collection of parameters that control which dialog is loaded, and relevant settings.
         * @return {?} return {Promise<UIResponse>}
         */
            function (params) {
                return new Promise(function (resolve, reject) {
                    try {
                        FB.ui(params, function (response) {
                            if (!response)
                                reject();
                            else if (response.error)
                                reject(response.error);
                            else
                                resolve(response);
                        });
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            };
        /**
         * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
         * @param [forceFreshResponse=false] {boolean} Force a fresh response.
         * @returns return {Promise<LoginStatus>}
         */
        /**
         * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
         * @param {?=} forceFreshResponse
         * @return {?} return {Promise<LoginStatus>}
         */
        FacebookService.prototype.getLoginStatus = /**
         * This method allows you to determine if a user is logged in to Facebook and has authenticated your app.
         * @param {?=} forceFreshResponse
         * @return {?} return {Promise<LoginStatus>}
         */
            function (forceFreshResponse) {
                return new Promise(function (resolve, reject) {
                    try {
                        FB.getLoginStatus(function (response) {
                            if (!response) {
                                reject();
                            }
                            else {
                                resolve(response);
                            }
                        }, forceFreshResponse);
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            };
        /**
         * Login the user
         * @usage
         * ```typescript
         * // login without options
         * this.fb.login()
         *   .then((response: LoginResponse) => console.log('Logged in', response))
         *   .catch(e => console.error('Error logging in'));
         *
         * // login with options
         * const options: LoginOptions = {
         *   scope: 'public_profile,user_friends,email,pages_show_list',
         *   return_scopes: true,
         *   enable_profile_selector: true
         * };
         * this.fb.login(options)
         *   .then(...)
         *   .catch(...);
         * ```
         * @param [options] {LoginOptions} Login options
         * @returns return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
         */
        /**
         * Login the user
         * \@usage
         * ```typescript
         * // login without options
         * this.fb.login()
         *   .then((response: LoginResponse) => console.log('Logged in', response))
         *   .catch(e => console.error('Error logging in'));
         *
         * // login with options
         * const options: LoginOptions = {
         *   scope: 'public_profile,user_friends,email,pages_show_list',
         *   return_scopes: true,
         *   enable_profile_selector: true
         * };
         * this.fb.login(options)
         *   .then(...)
         *   .catch(...);
         * ```
         * @param {?=} options
         * @return {?} return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
         */
        FacebookService.prototype.login = /**
         * Login the user
         * \@usage
         * ```typescript
         * // login without options
         * this.fb.login()
         *   .then((response: LoginResponse) => console.log('Logged in', response))
         *   .catch(e => console.error('Error logging in'));
         *
         * // login with options
         * const options: LoginOptions = {
         *   scope: 'public_profile,user_friends,email,pages_show_list',
         *   return_scopes: true,
         *   enable_profile_selector: true
         * };
         * this.fb.login(options)
         *   .then(...)
         *   .catch(...);
         * ```
         * @param {?=} options
         * @return {?} return {Promise<LoginResponse>} returns a promise that resolves with [LoginResponse](../login-response) object, or rejects with an error
         */
            function (options) {
                return new Promise(function (resolve, reject) {
                    try {
                        FB.login(function (response) {
                            if (response.authResponse) {
                                resolve(response);
                            }
                            else {
                                reject();
                            }
                        }, options);
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            };
        /**
         * Logout the user
         * @usage
         * ```typescript
         * this.fb.logout().then(() => console.log('Logged out!'));
         * ```
         * @returns return {Promise<any>} returns a promise that resolves when the user is logged out
         */
        /**
         * Logout the user
         * \@usage
         * ```typescript
         * this.fb.logout().then(() => console.log('Logged out!'));
         * ```
         * @return {?} return {Promise<any>} returns a promise that resolves when the user is logged out
         */
        FacebookService.prototype.logout = /**
         * Logout the user
         * \@usage
         * ```typescript
         * this.fb.logout().then(() => console.log('Logged out!'));
         * ```
         * @return {?} return {Promise<any>} returns a promise that resolves when the user is logged out
         */
            function () {
                return new Promise(function (resolve, reject) {
                    try {
                        FB.logout(function (response) {
                            resolve(response);
                        });
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            };
        /**
         * This synchronous function returns back the current authResponse.
         * @usage
         * ```typescript
         * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
         *
         * ...
         *
         * const authResponse: AuthResponse = this.fb.getAuthResponse();
         * ```
         * @returns return {AuthResponse} returns an [AuthResponse](../auth-response) object
         */
        /**
         * This synchronous function returns back the current authResponse.
         * \@usage
         * ```typescript
         * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
         *
         * ...
         *
         * const authResponse: AuthResponse = this.fb.getAuthResponse();
         * ```
         * @return {?} return {AuthResponse} returns an [AuthResponse](../auth-response) object
         */
        FacebookService.prototype.getAuthResponse = /**
         * This synchronous function returns back the current authResponse.
         * \@usage
         * ```typescript
         * import { AuthResponse, FacebookService } from 'ng2-facebook-sdk';
         *
         * ...
         *
         * const authResponse: AuthResponse = this.fb.getAuthResponse();
         * ```
         * @return {?} return {AuthResponse} returns an [AuthResponse](../auth-response) object
         */
            function () {
                try {
                    return /** @type {?} */ (FB.getAuthResponse());
                }
                catch (e) {
                    console.error('ng2-facebook-sdk: ', e);
                }
            };
        FacebookService.decorators = [
            { type: core.Injectable }
        ];
        return FacebookService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ components = [
        FBCommentEmbedComponent,
        FBCommentsComponent,
        FBFollowComponent,
        FBLikeComponent,
        FBPageComponent,
        FBPostComponent,
        FBQuoteComponent,
        FBSaveComponent,
        FBSendComponent,
        FBShareComponent,
        FBVideoComponent
    ];
    /**
     * @return {?}
     */
    function getComponents() {
        return components;
    }
    /**
     * \@shortdesc The module to import to add this library
     * \@description
     * The main module to import into your application.
     * You only need to import this module if you wish to use the components in this library; otherwise, you could just import [FacebookService](../facebook-service) into your module instead.
     * This module will make all components and providers available in your application.
     *
     * \@usage
     * In order to use this library, you need to import `FacebookModule` into your app's main `NgModule`.
     *
     * ```typescript
     * import { FacebookModule } from 'ng2-facebook-sdk';
     *
     * \@NgModule({
     *   ...
     *   imports: [
     *     ...
     *     FacebookModule.forRoot()
     *   ],
     *   ...
     * })
     * export class AppModule { }
     * ```
     */
    var FacebookModule = (function () {
        function FacebookModule() {
        }
        /**
         * @return {?}
         */
        FacebookModule.forRoot = /**
         * @return {?}
         */
            function () {
                return {
                    ngModule: FacebookModule,
                    providers: [FacebookService]
                };
            };
        FacebookModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: getComponents(),
                        exports: getComponents()
                    },] }
        ];
        return FacebookModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.FBCommentEmbedComponent = FBCommentEmbedComponent;
    exports.FBCommentsComponent = FBCommentsComponent;
    exports.FBFollowComponent = FBFollowComponent;
    exports.FBLikeComponent = FBLikeComponent;
    exports.FBPageComponent = FBPageComponent;
    exports.FBPostComponent = FBPostComponent;
    exports.FBQuoteComponent = FBQuoteComponent;
    exports.FBSaveComponent = FBSaveComponent;
    exports.FBSendComponent = FBSendComponent;
    exports.FBShareComponent = FBShareComponent;
    exports.FBVideoComponent = FBVideoComponent;
    exports.FacebookService = FacebookService;
    exports.FacebookModule = FacebookModule;
    exports.ɵb = FBMLAttribute;
    exports.ɵd = FBMLComponent;
    exports.ɵc = FBMLInstanceMethod;
    exports.ɵa = getComponents;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWZhY2Vib29rLnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbbnVsbCwibmc6Ly9uZ3gtZmFjZWJvb2svc3JjL2NvbXBvbmVudHMvZmJtbC1jb21wb25lbnQudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1jb21tZW50LWVtYmVkL2ZiLWNvbW1lbnQtZW1iZWQudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1jb21tZW50cy9mYi1jb21tZW50cy50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLWZvbGxvdy9mYi1mb2xsb3cudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1saWtlL2ZiLWxpa2UudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1wYWdlL2ZiLXBhZ2UudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1wb3N0L2ZiLXBvc3QudHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvY29tcG9uZW50cy9mYi1xdW90ZS9mYi1xdW90ZS50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLXNhdmUvZmItc2F2ZS50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLXNlbmQvZmItc2VuZC50cyIsIm5nOi8vbmd4LWZhY2Vib29rL3NyYy9jb21wb25lbnRzL2ZiLXNoYXJlL2ZiLXNoYXJlLnRzIiwibmc6Ly9uZ3gtZmFjZWJvb2svc3JjL2NvbXBvbmVudHMvZmItdmlkZW8vZmItdmlkZW8udHMiLCJuZzovL25neC1mYWNlYm9vay9zcmMvcHJvdmlkZXJzL2ZhY2Vib29rLnRzIiwibmc6Ly9uZ3gtZmFjZWJvb2svc3JjL2ZhY2Vib29rLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKiEgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuQ29weXJpZ2h0IChjKSBNaWNyb3NvZnQgQ29ycG9yYXRpb24uIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZVxyXG50aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS4gWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZVxyXG5MaWNlbnNlIGF0IGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVEhJUyBDT0RFIElTIFBST1ZJREVEIE9OIEFOICpBUyBJUyogQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxyXG5LSU5ELCBFSVRIRVIgRVhQUkVTUyBPUiBJTVBMSUVELCBJTkNMVURJTkcgV0lUSE9VVCBMSU1JVEFUSU9OIEFOWSBJTVBMSUVEXHJcbldBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBUSVRMRSwgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UsXHJcbk1FUkNIQU5UQUJMSVRZIE9SIE5PTi1JTkZSSU5HRU1FTlQuXHJcblxyXG5TZWUgdGhlIEFwYWNoZSBWZXJzaW9uIDIuMCBMaWNlbnNlIGZvciBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnNcclxuYW5kIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xyXG4vKiBnbG9iYWwgUmVmbGVjdCwgUHJvbWlzZSAqL1xyXG5cclxudmFyIGV4dGVuZFN0YXRpY3MgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHxcclxuICAgICh7IF9fcHJvdG9fXzogW10gfSBpbnN0YW5jZW9mIEFycmF5ICYmIGZ1bmN0aW9uIChkLCBiKSB7IGQuX19wcm90b19fID0gYjsgfSkgfHxcclxuICAgIGZ1bmN0aW9uIChkLCBiKSB7IGZvciAodmFyIHAgaW4gYikgaWYgKGIuaGFzT3duUHJvcGVydHkocCkpIGRbcF0gPSBiW3BdOyB9O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXh0ZW5kcyhkLCBiKSB7XHJcbiAgICBleHRlbmRTdGF0aWNzKGQsIGIpO1xyXG4gICAgZnVuY3Rpb24gX18oKSB7IHRoaXMuY29uc3RydWN0b3IgPSBkOyB9XHJcbiAgICBkLnByb3RvdHlwZSA9IGIgPT09IG51bGwgPyBPYmplY3QuY3JlYXRlKGIpIDogKF9fLnByb3RvdHlwZSA9IGIucHJvdG90eXBlLCBuZXcgX18oKSk7XHJcbn1cclxuXHJcbmV4cG9ydCB2YXIgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uIF9fYXNzaWduKHQpIHtcclxuICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG4gICAgICAgIHMgPSBhcmd1bWVudHNbaV07XHJcbiAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKSB0W3BdID0gc1twXTtcclxuICAgIH1cclxuICAgIHJldHVybiB0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19yZXN0KHMsIGUpIHtcclxuICAgIHZhciB0ID0ge307XHJcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcclxuICAgICAgICB0W3BdID0gc1twXTtcclxuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcclxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMClcclxuICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XHJcbiAgICByZXR1cm4gdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3BhcmFtKHBhcmFtSW5kZXgsIGRlY29yYXRvcikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIGtleSkgeyBkZWNvcmF0b3IodGFyZ2V0LCBrZXksIHBhcmFtSW5kZXgpOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKSB7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEobWV0YWRhdGFLZXksIG1ldGFkYXRhVmFsdWUpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hd2FpdGVyKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xyXG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZShyZXN1bHQudmFsdWUpOyB9KS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XHJcbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2dlbmVyYXRvcih0aGlzQXJnLCBib2R5KSB7XHJcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xyXG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcclxuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XHJcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XHJcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcclxuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcclxuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cclxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXhwb3J0U3RhcihtLCBleHBvcnRzKSB7XHJcbiAgICBmb3IgKHZhciBwIGluIG0pIGlmICghZXhwb3J0cy5oYXNPd25Qcm9wZXJ0eShwKSkgZXhwb3J0c1twXSA9IG1bcF07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3ZhbHVlcyhvKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl0sIGkgPSAwO1xyXG4gICAgaWYgKG0pIHJldHVybiBtLmNhbGwobyk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG5leHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgaWYgKG8gJiYgaSA+PSBvLmxlbmd0aCkgbyA9IHZvaWQgMDtcclxuICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6IG8gJiYgb1tpKytdLCBkb25lOiAhbyB9O1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3JlYWQobywgbikge1xyXG4gICAgdmFyIG0gPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb1tTeW1ib2wuaXRlcmF0b3JdO1xyXG4gICAgaWYgKCFtKSByZXR1cm4gbztcclxuICAgIHZhciBpID0gbS5jYWxsKG8pLCByLCBhciA9IFtdLCBlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICB3aGlsZSAoKG4gPT09IHZvaWQgMCB8fCBuLS0gPiAwKSAmJiAhKHIgPSBpLm5leHQoKSkuZG9uZSkgYXIucHVzaChyLnZhbHVlKTtcclxuICAgIH1cclxuICAgIGNhdGNoIChlcnJvcikgeyBlID0geyBlcnJvcjogZXJyb3IgfTsgfVxyXG4gICAgZmluYWxseSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKHIgJiYgIXIuZG9uZSAmJiAobSA9IGlbXCJyZXR1cm5cIl0pKSBtLmNhbGwoaSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpbmFsbHkgeyBpZiAoZSkgdGhyb3cgZS5lcnJvcjsgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19zcHJlYWQoKSB7XHJcbiAgICBmb3IgKHZhciBhciA9IFtdLCBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKylcclxuICAgICAgICBhciA9IGFyLmNvbmNhdChfX3JlYWQoYXJndW1lbnRzW2ldKSk7XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2F3YWl0KHYpIHtcclxuICAgIHJldHVybiB0aGlzIGluc3RhbmNlb2YgX19hd2FpdCA/ICh0aGlzLnYgPSB2LCB0aGlzKSA6IG5ldyBfX2F3YWl0KHYpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY0dlbmVyYXRvcih0aGlzQXJnLCBfYXJndW1lbnRzLCBnZW5lcmF0b3IpIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgZyA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSwgaSwgcSA9IFtdO1xyXG4gICAgcmV0dXJuIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IGlmIChnW25dKSBpW25dID0gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChhLCBiKSB7IHEucHVzaChbbiwgdiwgYSwgYl0pID4gMSB8fCByZXN1bWUobiwgdik7IH0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiByZXN1bWUobiwgdikgeyB0cnkgeyBzdGVwKGdbbl0odikpOyB9IGNhdGNoIChlKSB7IHNldHRsZShxWzBdWzNdLCBlKTsgfSB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKHIpIHsgci52YWx1ZSBpbnN0YW5jZW9mIF9fYXdhaXQgPyBQcm9taXNlLnJlc29sdmUoci52YWx1ZS52KS50aGVuKGZ1bGZpbGwsIHJlamVjdCkgOiBzZXR0bGUocVswXVsyXSwgcik7IH1cclxuICAgIGZ1bmN0aW9uIGZ1bGZpbGwodmFsdWUpIHsgcmVzdW1lKFwibmV4dFwiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHJlamVjdCh2YWx1ZSkgeyByZXN1bWUoXCJ0aHJvd1wiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShmLCB2KSB7IGlmIChmKHYpLCBxLnNoaWZ0KCksIHEubGVuZ3RoKSByZXN1bWUocVswXVswXSwgcVswXVsxXSk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNEZWxlZ2F0b3Iobykge1xyXG4gICAgdmFyIGksIHA7XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIsIGZ1bmN0aW9uIChlKSB7IHRocm93IGU7IH0pLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuLCBmKSB7IGlbbl0gPSBvW25dID8gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIChwID0gIXApID8geyB2YWx1ZTogX19hd2FpdChvW25dKHYpKSwgZG9uZTogbiA9PT0gXCJyZXR1cm5cIiB9IDogZiA/IGYodikgOiB2OyB9IDogZjsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY1ZhbHVlcyhvKSB7XHJcbiAgICBpZiAoIVN5bWJvbC5hc3luY0l0ZXJhdG9yKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3ltYm9sLmFzeW5jSXRlcmF0b3IgaXMgbm90IGRlZmluZWQuXCIpO1xyXG4gICAgdmFyIG0gPSBvW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSwgaTtcclxuICAgIHJldHVybiBtID8gbS5jYWxsKG8pIDogKG8gPSB0eXBlb2YgX192YWx1ZXMgPT09IFwiZnVuY3Rpb25cIiA/IF9fdmFsdWVzKG8pIDogb1tTeW1ib2wuaXRlcmF0b3JdKCksIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpKTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyBpW25dID0gb1tuXSAmJiBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkgeyB2ID0gb1tuXSh2KSwgc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgdi5kb25lLCB2LnZhbHVlKTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShyZXNvbHZlLCByZWplY3QsIGQsIHYpIHsgUHJvbWlzZS5yZXNvbHZlKHYpLnRoZW4oZnVuY3Rpb24odikgeyByZXNvbHZlKHsgdmFsdWU6IHYsIGRvbmU6IGQgfSk7IH0sIHJlamVjdCk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fbWFrZVRlbXBsYXRlT2JqZWN0KGNvb2tlZCwgcmF3KSB7XHJcbiAgICBpZiAoT2JqZWN0LmRlZmluZVByb3BlcnR5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb29rZWQsIFwicmF3XCIsIHsgdmFsdWU6IHJhdyB9KTsgfSBlbHNlIHsgY29va2VkLnJhdyA9IHJhdzsgfVxyXG4gICAgcmV0dXJuIGNvb2tlZDtcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydFN0YXIobW9kKSB7XHJcbiAgICBpZiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSByZXR1cm4gbW9kO1xyXG4gICAgdmFyIHJlc3VsdCA9IHt9O1xyXG4gICAgaWYgKG1vZCAhPSBudWxsKSBmb3IgKHZhciBrIGluIG1vZCkgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcclxuICAgIHJlc3VsdC5kZWZhdWx0ID0gbW9kO1xyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9faW1wb3J0RGVmYXVsdChtb2QpIHtcclxuICAgIHJldHVybiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSA/IG1vZCA6IHsgZGVmYXVsdDogbW9kIH07XHJcbn1cclxuIiwiaW1wb3J0IHsgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBGQk1MQXR0cmlidXRlKHRhcmdldDogYW55LCBrZXk6IHN0cmluZykge1xuICBjb25zdCBwcm9jZXNzS2V5ID0gKF9rOiBzdHJpbmcpID0+ICdkYXRhLScgKyBfay50b1N0cmluZygpLnJlcGxhY2UoLyhbYS16XFxkXSkoW0EtWl0pL2csICckMS0kMicpLnRvTG93ZXJDYXNlKCk7XG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwge1xuICAgIHNldDogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgIHZhbHVlID0gdmFsdWUudG9TdHJpbmcoKTtcbiAgICAgIHRoaXMuc2V0QXR0cmlidXRlKHByb2Nlc3NLZXkoa2V5KSwgdmFsdWUpO1xuICAgIH0sXG4gICAgZ2V0OiBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLmdldEF0dHJpYnV0ZShwcm9jZXNzS2V5KGtleSkpO1xuICAgIH0sXG4gICAgZW51bWVyYWJsZTogdHJ1ZVxuICB9KTtcbn1cblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBGQk1MSW5zdGFuY2VNZXRob2QodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nKSB7XG4gIHJldHVybiB7XG4gICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICB2YWx1ZTogZnVuY3Rpb24oLi4uYXJnczogYW55W10pIHtcbiAgICAgIGlmICh0aGlzLl9pbnN0YW5jZSkge1xuICAgICAgICByZXR1cm4gdGhpcy5faW5zdGFuY2Vba2V5XS5hcHBseSh0aGlzLl9pbnN0YW5jZSwgYXJncyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLndhcm4oJ25nMi1mYWNlYm9vay1zZGs6IHRyaWVkIGNhbGxpbmcgaW5zdGFuY2UgbWV0aG9kIGJlZm9yZSBjb21wb25lbnQgaXMgcmVhZHkuJyk7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuICAgIH1cbiAgfTtcbn1cblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBjbGFzcyBGQk1MQ29tcG9uZW50IHtcblxuICBwcm90ZWN0ZWQgbmF0aXZlRWxlbWVudDogSFRNTEVsZW1lbnQ7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZixcbiAgICBwcml2YXRlIHJuZDogUmVuZGVyZXIsXG4gICAgcHJpdmF0ZSBmYkNsYXNzOiBzdHJpbmdcbiAgKSB7XG4gICAgdGhpcy5uYXRpdmVFbGVtZW50ID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50O1xuICAgIHRoaXMucm5kLnNldEVsZW1lbnRDbGFzcyh0aGlzLm5hdGl2ZUVsZW1lbnQsIHRoaXMuZmJDbGFzcywgdHJ1ZSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgc2V0QXR0cmlidXRlKG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZykge1xuICAgIGlmICghbmFtZSB8fCAhdmFsdWUpIHJldHVybjtcbiAgICB0aGlzLnJuZC5zZXRFbGVtZW50QXR0cmlidXRlKHRoaXMubmF0aXZlRWxlbWVudCwgbmFtZSwgdmFsdWUpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEF0dHJpYnV0ZShuYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGlmICghbmFtZSkgcmV0dXJuO1xuICAgIHJldHVybiB0aGlzLm5hdGl2ZUVsZW1lbnQuZ2V0QXR0cmlidXRlKG5hbWUpO1xuICB9XG5cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIEVtYmVkZGVkIENvbW1lbnRzXG4gKiBAc2hvcnRkZXNjIEVtYmVkZGVkIGNvbW1lbnRzIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2VtYmVkZGVkLWNvbW1lbnRzXG4gKiBAZGVzY3JpcHRpb25cbiAqIEVtYmVkZGVkIGNvbW1lbnRzIGFyZSBhIHNpbXBsZSB3YXkgdG8gcHV0IHB1YmxpYyBwb3N0IGNvbW1lbnRzIC0gYnkgYSBQYWdlIG9yIGEgcGVyc29uIG9uIEZhY2Vib29rIC0gaW50byB0aGUgY29udGVudCBvZiB5b3VyIHdlYiBzaXRlIG9yIHdlYiBwYWdlLlxuICogT25seSBwdWJsaWMgY29tbWVudHMgZnJvbSBGYWNlYm9vayBQYWdlcyBhbmQgcHJvZmlsZXMgY2FuIGJlIGVtYmVkZGVkLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItY29tbWVudC1lbWJlZCBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL3p1Y2svcG9zdHMvMTAxMDI3MzU0NTI1MzI5OTE/Y29tbWVudF9pZD0xMDcwMjMzNzAzMDM2MTg1XCIgd2lkdGg9XCI1MDBcIj48L2ZiLWNvbW1lbnQtZW1iZWQ+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItY29tbWVudC1lbWJlZCcsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQkNvbW1lbnRFbWJlZENvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBjb21tZW50LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgd2lkdGggb2YgdGhlIGVtYmVkZGVkIGNvbW1lbnQgY29udGFpbmVyLiBNaW4uIGAyMjBweGAuIERlZmF1bHRzIHRvIGA1NjBweGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTZXQgdG8gYHRydWVgIHRvIGluY2x1ZGUgcGFyZW50IGNvbW1lbnQgKGlmIFVSTCBpcyBhIHJlcGx5KS4gRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGluY2x1ZGVQYXJlbnQ6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItY29tbWVudC1lbWJlZCcpO1xuICB9XG5cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIENvbW1lbnRzXG4gKiBAc2hvcnRkZXNjIENvbW1lbnRzIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2NvbW1lbnRzXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBjb21tZW50cyBwbHVnaW4gbGV0cyBwZW9wbGUgY29tbWVudCBvbiBjb250ZW50IG9uIHlvdXIgc2l0ZSB1c2luZyB0aGVpciBGYWNlYm9vayBhY2NvdW50LlxuICogUGVvcGxlIGNhbiBjaG9vc2UgdG8gc2hhcmUgdGhlaXIgY29tbWVudCBhY3Rpdml0eSB3aXRoIHRoZWlyIGZyaWVuZHMgKGFuZCBmcmllbmRzIG9mIHRoZWlyIGZyaWVuZHMpIG9uIEZhY2Vib29rIGFzIHdlbGwuXG4gKiBUaGUgY29tbWVudHMgcGx1Z2luIGFsc28gaW5jbHVkZXMgYnVpbHQtaW4gbW9kZXJhdGlvbiB0b29scyBhbmQgc29jaWFsIHJlbGV2YW5jZSByYW5raW5nLlxuICpcbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLWNvbW1lbnRzPjwvZmItY29tbWVudHM+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItY29tbWVudHMnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJDb21tZW50c0NvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igc2NoZW1lIHVzZWQgYnkgdGhlIGNvbW1lbnRzIHBsdWdpbi4gQ2FuIGJlIGBsaWdodGAgb3IgYGRhcmtgLiBEZWZhdWx0cyB0byBgbGlnaHRgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgY29sb3JzY2hlbWU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCB0aGF0IGNvbW1lbnRzIHBvc3RlZCBpbiB0aGUgcGx1Z2luIHdpbGwgYmUgcGVybWFuZW50bHkgYXNzb2NpYXRlZCB3aXRoLlxuICAgKiBBbGwgc3RvcmllcyBzaGFyZWQgb24gRmFjZWJvb2sgYWJvdXQgY29tbWVudHMgcG9zdGVkIHVzaW5nIHRoZSBjb21tZW50cyBwbHVnaW4gd2lsbCBsaW5rIHRvIHRoaXMgVVJMLlxuICAgKiBEZWZhdWx0cyB0byBjdXJyZW50IFVSTC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhyZWY6IHN0cmluZyA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuXG4gIC8qKlxuICAgKiBBIGJvb2xlYW4gdmFsdWUgdGhhdCBzcGVjaWZpZXMgd2hldGhlciB0byBzaG93IHRoZSBtb2JpbGUtb3B0aW1pemVkIHZlcnNpb24gb3Igbm90LiBJZiBubyB2YWx1ZSBpcyBnaXZlbiwgaXQgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IGRldGVjdGVkLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbW9iaWxlOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBUaGUgbnVtYmVyIG9mIGNvbW1lbnRzIHRvIHNob3cgYnkgZGVmYXVsdC4gVGhlIG1pbmltdW0gdmFsdWUgaXMgYDFgLiBEZWZhdWx0cyB0byBgMTBgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgbnVtcG9zdHM6IG51bWJlcjtcblxuICAvKipcbiAgICogVGhlIG9yZGVyIHRvIHVzZSB3aGVuIGRpc3BsYXlpbmcgY29tbWVudHMuIENhbiBiZSBgc29jaWFsYCwgYHJldmVyc2VfdGltZWAsIG9yIGB0aW1lYC4gVGhlIGRpZmZlcmVudCBvcmRlciB0eXBlcyBhcmUgZXhwbGFpbmVkIFtpbiB0aGUgRkFRXShodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9jb21tZW50cyNmYXFvcmRlcikuIERlZmF1bHRzIHRvIGBzb2NpYWxgXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBvcmRlckJ5OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSB3aWR0aCBvZiB0aGUgY29tbWVudHMgcGx1Z2luIG9uIHRoZSB3ZWJwYWdlLlxuICAgKiBUaGlzIGNhbiBiZSBlaXRoZXIgYSBwaXhlbCB2YWx1ZSBvciBhIHBlcmNlbnRhZ2UgKHN1Y2ggYXMgMTAwJSkgZm9yIGZsdWlkIHdpZHRoLlxuICAgKiBUaGUgbW9iaWxlIHZlcnNpb24gb2YgdGhlIGNvbW1lbnRzIHBsdWdpbiBpZ25vcmVzIHRoZSB3aWR0aCBwYXJhbWV0ZXIgYW5kIGluc3RlYWQgaGFzIGEgZmx1aWQgd2lkdGggb2YgMTAwJS5cbiAgICogVGhlIG1pbmltdW0gd2lkdGggc3VwcG9ydGVkIGJ5IHRoZSBjb21tZW50cyBwbHVnaW4gaXMgMzIwcHguXG4gICAqIERlZmF1bHRzIHRvIGA1NTBweGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLWNvbW1lbnRzJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgRm9sbG93IEJ1dHRvblxuICogQHNob3J0ZGVzYyBGb2xsb3cgYnV0dG9uIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2ZvbGxvdy1idXR0b25cbiAqIEBkZXNjcmlwdGlvbiBUaGUgRm9sbG93IGJ1dHRvbiBsZXRzIHBlb3BsZSBzdWJzY3JpYmUgdG8gdGhlIHB1YmxpYyB1cGRhdGVzIG9mIG90aGVycyBvbiBGYWNlYm9vay5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLWZvbGxvdyBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL3p1Y2tcIj48L2ZiLWZvbGxvdz5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1mb2xsb3cnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJGb2xsb3dDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGNvbG9yIHNjaGVtZSB1c2VkIGJ5IHRoZSBwbHVnaW4uIENhbiBiZSBgbGlnaHRgIG9yIGBkYXJrYC4gRGVmYXVsdHMgdG8gYGxpZ2h0YC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGNvbG9yU2NoZW1lOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBGYWNlYm9vay5jb20gcHJvZmlsZSBVUkwgb2YgdGhlIHVzZXIgdG8gZm9sbG93LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBJZiB5b3VyIHdlYiBzaXRlIG9yIG9ubGluZSBzZXJ2aWNlLCBvciBhIHBvcnRpb24gb2YgeW91ciBzZXJ2aWNlLCBpcyBkaXJlY3RlZCB0byBjaGlsZHJlbiB1bmRlciAxMyB5b3UgbXVzdCBlbmFibGUgdGhpcy4gRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGtpZERpcmVjdGVkU2l0ZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2VsZWN0cyBvbmUgb2YgdGhlIGRpZmZlcmVudCBsYXlvdXRzIHRoYXQgYXJlIGF2YWlsYWJsZSBmb3IgdGhlIHBsdWdpbi4gQ2FuIGJlIG9uZSBvZiBgc3RhbmRhcmRgLCBgYnV0dG9uX2NvdW50YCwgb3IgYGJveF9jb3VudGAuXG4gICAqIERlZmF1bHRzIHRvIGBzdGFuZGFyZGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBsYXlvdXQ6IHN0cmluZztcblxuICAvKipcbiAgICogU3BlY2lmaWVzIHdoZXRoZXIgdG8gZGlzcGxheSBwcm9maWxlIHBob3RvcyBiZWxvdyB0aGUgYnV0dG9uLiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hvd0ZhY2VzOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBidXR0b24gaXMgb2ZmZXJlZCBpbiAyIHNpemVzIGkuZS4gYGxhcmdlYCBhbmQgYHNtYWxsYC4gRGVmYXVsdHMgdG8gYHNtYWxsYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNpemU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSBwbHVnaW4uIFRoZSBsYXlvdXQgeW91IGNob29zZSBhZmZlY3RzIHRoZSBtaW5pbXVtIGFuZCBkZWZhdWx0IHdpZHRocyB5b3UgY2FuIHVzZS5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItZm9sbG93Jyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgTGlrZSBCdXR0b25cbiAqIEBzaG9ydGRlc2MgTGlrZSBidXR0b24gY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvbGlrZS1idXR0b25cbiAqIEBkZXNjcmlwdGlvblxuICogQSBzaW5nbGUgY2xpY2sgb24gdGhlIExpa2UgYnV0dG9uIHdpbGwgJ2xpa2UnIHBpZWNlcyBvZiBjb250ZW50IG9uIHRoZSB3ZWIgYW5kIHNoYXJlIHRoZW0gb24gRmFjZWJvb2suXG4gKiBZb3UgY2FuIGFsc28gZGlzcGxheSBhIFNoYXJlIGJ1dHRvbiBuZXh0IHRvIHRoZSBMaWtlIGJ1dHRvbiB0byBsZXQgcGVvcGxlIGFkZCBhIHBlcnNvbmFsIG1lc3NhZ2UgYW5kIGN1c3RvbWl6ZSB3aG8gdGhleSBzaGFyZSB3aXRoLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItbGlrZSBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL3p1Y2tcIj48L2ZiLWxpa2U+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItbGlrZScsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQkxpa2VDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIHZlcmIgdG8gZGlzcGxheSBvbiB0aGUgYnV0dG9uLiBDYW4gYmUgZWl0aGVyIGBsaWtlYCBvciBgcmVjb21tZW5kYC5cbiAgICogRGVmYXVsdHMgdG8gYGxpa2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgYWN0aW9uOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBzY2hlbWUgdXNlZCBieSB0aGUgcGx1Z2luIGZvciBhbnkgdGV4dCBvdXRzaWRlIG9mIHRoZSBidXR0b24gaXRzZWxmLiBDYW4gYmUgYGxpZ2h0YCBvciBgZGFya2AuXG4gICAqIERlZmF1bHRzIHRvIGBsaWdodGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBjb2xvclNjaGVtZTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgYWJzb2x1dGUgVVJMIG9mIHRoZSBwYWdlIHRoYXQgd2lsbCBiZSBsaWtlZC5cbiAgICogRGVmYXVsdHMgdG8gdGhlIGN1cnJlbnQgVVJMLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XG5cbiAgLyoqXG4gICAqIElmIHlvdXIgd2ViIHNpdGUgb3Igb25saW5lIHNlcnZpY2UsIG9yIGEgcG9ydGlvbiBvZiB5b3VyIHNlcnZpY2UsIGlzIGRpcmVjdGVkIHRvIGNoaWxkcmVuIHVuZGVyIDEzIFt5b3UgbXVzdCBlbmFibGUgdGhpc10oaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvcmVzdHJpY3Rpb25zLykuXG4gICAqIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBraWREaXJlY3RlZFNpdGU6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFNlbGVjdHMgb25lIG9mIHRoZSBkaWZmZXJlbnQgbGF5b3V0cyB0aGF0IGFyZSBhdmFpbGFibGUgZm9yIHRoZSBwbHVnaW4uXG4gICAqIENhbiBiZSBvbmUgb2YgYHN0YW5kYXJkYCwgYGJ1dHRvbl9jb3VudGAsIGBidXR0b25gIG9yIGBib3hfY291bnRgLlxuICAgKiBTZWUgdGhlIFtGQVFdKGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2xpa2UtYnV0dG9uI2ZhcWxheW91dCkgZm9yIG1vcmUgZGV0YWlscy5cbiAgICogRGVmYXVsdHMgdG8gYHN0YW5kYXJkYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGxheW91dDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBBIGxhYmVsIGZvciB0cmFja2luZyByZWZlcnJhbHMgd2hpY2ggbXVzdCBiZSBsZXNzIHRoYW4gNTAgY2hhcmFjdGVycyBhbmQgY2FuIGNvbnRhaW4gYWxwaGFudW1lcmljIGNoYXJhY3RlcnMgYW5kIHNvbWUgcHVuY3R1YXRpb24gKGN1cnJlbnRseSArLz0tLjpfKS5cbiAgICogU2VlIHRoZSBbRkFRXShodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9mYXFzI3JlZikgZm9yIG1vcmUgZGV0YWlscy5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTcGVjaWZpZXMgd2hldGhlciB0byBpbmNsdWRlIGEgc2hhcmUgYnV0dG9uIGJlc2lkZSB0aGUgTGlrZSBidXR0b24uXG4gICAqIFRoaXMgb25seSB3b3JrcyB3aXRoIHRoZSBYRkJNTCB2ZXJzaW9uLlxuICAgKiBEZWZhdWx0cyB0byBgZmFsc2VgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hhcmU6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFNwZWNpZmllcyB3aGV0aGVyIHRvIGRpc3BsYXkgcHJvZmlsZSBwaG90b3MgYmVsb3cgdGhlIGJ1dHRvbiAoc3RhbmRhcmQgbGF5b3V0IG9ubHkpLlxuICAgKiBZb3UgbXVzdCBub3QgZW5hYmxlIHRoaXMgb24gY2hpbGQtZGlyZWN0ZWQgc2l0ZXMuXG4gICAqIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93RmFjZXM6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFRoZSBidXR0b24gaXMgb2ZmZXJlZCBpbiAyIHNpemVzIGkuZS4gYGxhcmdlYCBhbmQgYHNtYWxsYC5cbiAgICogRGVmYXVsdHMgdG8gYHNtYWxsYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNpemU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSBwbHVnaW4gKHN0YW5kYXJkIGxheW91dCBvbmx5KSwgd2hpY2ggaXMgc3ViamVjdCB0byB0aGUgbWluaW11bSBhbmQgZGVmYXVsdCB3aWR0aC5cbiAgICogU2VlIFtMYXlvdXQgU2V0dGluZ3NdKGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL2xpa2UtYnV0dG9uI2ZhcWxheW91dCkgaW4gdGhlIG9mZmljaWFsIGRvY3MgZm9yIG1vcmUgZGV0YWlscy5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHdpZHRoOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoZWw6IEVsZW1lbnRSZWYsIHJuZDogUmVuZGVyZXIpIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItbGlrZScpO1xuICB9XG5cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGQk1MQXR0cmlidXRlLCBGQk1MQ29tcG9uZW50IH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuXG4vKipcbiAqIEBuYW1lIFBhZ2UgUGx1Z2luXG4gKiBAc2hvcnRkZXNjIFBhZ2UgcGx1Z2luIGNvbXBvbmVudFxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3BhZ2UtcGx1Z2luXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBQYWdlIHBsdWdpbiBsZXRzIHlvdSBlYXNpbHkgZW1iZWQgYW5kIHByb21vdGUgYW55IEZhY2Vib29rIFBhZ2Ugb24geW91ciB3ZWJzaXRlLiBKdXN0IGxpa2Ugb24gRmFjZWJvb2ssIHlvdXIgdmlzaXRvcnMgY2FuIGxpa2UgYW5kIHNoYXJlIHRoZSBQYWdlIHdpdGhvdXQgbGVhdmluZyB5b3VyIHNpdGUuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1wYWdlIGhyZWY9XCJodHRwczovL2ZhY2Vib29rLmNvbS9mYWNlYm9va1wiPjwvZmItcGFnZT5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1wYWdlJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCUGFnZUNvbXBvbmVudCBleHRlbmRzIEZCTUxDb21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgVVJMIG9mIHRoZSBGYWNlYm9vayBQYWdlXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBwaXhlbCB3aWR0aCBvZiB0aGUgcGx1Z2luLiBNaW4uIGlzIGAxODBgICYgTWF4LiBpcyBgNTAwYC5cbiAgICogRGVmYXVsdHMgdG8gYDM0MGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBUaGUgcGl4ZWwgaGVpZ2h0IG9mIHRoZSBwbHVnaW4uIE1pbi4gaXMgYDcwYC5cbiAgICogRGVmYXVsdHMgdG8gYDUwMGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBoZWlnaHQ6IG51bWJlcjtcblxuICAvKipcbiAgICogVGFicyB0byByZW5kZXIgaS5lLiBgdGltZWxpbmVgLCBgZXZlbnRzYCwgYG1lc3NhZ2VzYC4gVXNlIGEgY29tbWEtc2VwYXJhdGVkIGxpc3QgdG8gYWRkIG11bHRpcGxlIHRhYnMsIGkuZS4gYHRpbWVsaW5lYCwgYGV2ZW50c2AuXG4gICAqIERlZmF1bHRzIHRvIGB0aW1lbGluZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB0YWJzOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEhpZGUgY292ZXIgcGhvdG8gaW4gdGhlIGhlYWRlci5cbiAgICogRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhpZGVDb3ZlcjogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2hvdyBwcm9maWxlIHBob3RvcyB3aGVuIGZyaWVuZHMgbGlrZSB0aGlzLlxuICAgKiBEZWZhdWx0cyB0byBgdHJ1ZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93RmFjZXBpbGU6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEhpZGUgdGhlIGN1c3RvbSBjYWxsIHRvIGFjdGlvbiBidXR0b24gKGlmIGF2YWlsYWJsZSkuXG4gICAqIERlZmF1bHQgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGhpZGVDVEE6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFVzZSB0aGUgc21hbGwgaGVhZGVyIGluc3RlYWQuXG4gICAqIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzbWFsbEhlYWRlcjogYm9vbGVhbjtcblxuICAvKipcbiAgICogVHJ5IHRvIGZpdCBpbnNpZGUgdGhlIGNvbnRhaW5lciB3aWR0aC5cbiAgICogRGVmYXVsdHMgdG8gYHRydWVgLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgYWRhcHRDb250YWluZXJXaWR0aDogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1wYWdlJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgRW1iZWRkZWQgUG9zdHNcbiAqIEBzaG9ydGRlc2MgRW1iZWRkZWQgcG9zdCBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9lbWJlZGRlZC1wb3N0c1xuICogQGRlc2NyaXB0aW9uXG4gKiBFbWJlZGRlZCBQb3N0cyBhcmUgYSBzaW1wbGUgd2F5IHRvIHB1dCBwdWJsaWMgcG9zdHMgLSBieSBhIFBhZ2Ugb3IgYSBwZXJzb24gb24gRmFjZWJvb2sgLSBpbnRvIHRoZSBjb250ZW50IG9mIHlvdXIgd2ViIHNpdGUgb3Igd2ViIHBhZ2UuXG4gKiBPbmx5IHB1YmxpYyBwb3N0cyBmcm9tIEZhY2Vib29rIFBhZ2VzIGFuZCBwcm9maWxlcyBjYW4gYmUgZW1iZWRkZWQuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1wb3N0IGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vMjA1MzEzMTY3MjgvcG9zdHMvMTAxNTQwMDk5OTA1MDY3MjkvXCI+PC9mYi1wb3N0PlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXBvc3QnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRkJQb3N0Q29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHBvc3QuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSB3aWR0aCBvZiB0aGUgcG9zdC4gTWluLiBgMzUwYCBwaXhlbDsgTWF4LiBgNzUwYCBwaXhlbC4gU2V0IHRvIGF1dG8gdG8gdXNlIGZsdWlkIHdpZHRoLiBEZWZhdWx0cyB0byBgYXV0b2AuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBBcHBsaWVkIHRvIHBob3RvIHBvc3QuIFNldCB0byBgdHJ1ZWAgdG8gaW5jbHVkZSB0aGUgdGV4dCBmcm9tIHRoZSBGYWNlYm9vayBwb3N0LCBpZiBhbnkuIERlZmF1bHRzIHRvIGBmYWxzZWAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93VGV4dDogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1wb3N0Jyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgUXVvdGUgUGx1Z2luXG4gKiBAc2hvcnRkZXNjIFF1b3RlIHBsdWdpbiBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9xdW90ZVxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgcXVvdGUgcGx1Z2luIGxldHMgcGVvcGxlIHNlbGVjdCB0ZXh0IG9uIHlvdXIgcGFnZSBhbmQgYWRkIGl0IHRvIHRoZWlyIHNoYXJlLCBzbyB0aGV5IGNhbiB0ZWxsIGEgbW9yZSBleHByZXNzaXZlIHN0b3J5LlxuICogTm90ZSB0aGF0IHlvdSBkbyBub3QgbmVlZCB0byBpbXBsZW1lbnQgRmFjZWJvb2sgbG9naW4gb3IgcmVxdWVzdCBhbnkgYWRkaXRpb25hbCBwZXJtaXNzaW9ucyB0aHJvdWdoIGFwcCByZXZpZXcgaW4gb3JkZXIgdG8gdXNlIHRoaXMgcGx1Z2luLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItcXVvdGU+PC9mYi1xdW90ZT5cbiAqIGBgYFxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmYi1xdW90ZScsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQlF1b3RlQ29tcG9uZW50IGV4dGVuZHMgRkJNTENvbXBvbmVudCB7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHBhZ2UgdGhhdCB3aWxsIGJlIHF1b3RlZC5cbiAgICogRGVmYXVsdHMgdG8gdGhlIGN1cnJlbnQgVVJMXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIENhbiBiZSBzZXQgdG8gcXVvdGUgb3IgYnV0dG9uLiBEZWZhdWx0cyB0byBxdW90ZS5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIGxheW91dDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHJuZDogUmVuZGVyZXJcbiAgKSB7XG4gICAgc3VwZXIoZWwsIHJuZCwgJ2ZiLXF1b3RlJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgU2F2ZSBCdXR0b25cbiAqIEBzaG9ydGRlc2MgU2F2ZSBidXR0b24gY29tcG9uZW50LlxuICogQGZiZG9jIGh0dHBzOi8vZGV2ZWxvcGVycy5mYWNlYm9vay5jb20vZG9jcy9wbHVnaW5zL3NhdmVcbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIFNhdmUgYnV0dG9uIGxldHMgcGVvcGxlIHNhdmUgaXRlbXMgb3Igc2VydmljZXMgdG8gYSBwcml2YXRlIGxpc3Qgb24gRmFjZWJvb2ssIHNoYXJlIGl0IHdpdGggZnJpZW5kcywgYW5kIHJlY2VpdmUgcmVsZXZhbnQgbm90aWZpY2F0aW9ucy5cbiAqIEB1c2FnZVxuICogYGBgaHRtbFxuICogPGZiLXNhdmUgdXJpPVwiaHR0cHM6Ly9naXRodWIuY29tL3p5cmEvbmcyLWZhY2Vib29rLXNkay9cIj48L2ZiLXNhdmU+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItc2F2ZScsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQlNhdmVDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIGxpbmsgb2YgdGhlIHBhZ2UgdGhhdCB3aWxsIGJlIHNhdmVkLlxuICAgKiBDdXJyZW50IExpbmsvQWRkcmVzc1xuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgdXJpOiBzdHJpbmcgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1zYXZlJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgU2VuZCBCdXR0b25cbiAqIEBzaG9ydGRlc2MgU2VuZCBidXR0b24gY29tcG9uZW50XG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvc2VuZC1idXR0b25cbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIFNlbmQgYnV0dG9uIGxldHMgcGVvcGxlIHByaXZhdGVseSBzZW5kIGNvbnRlbnQgb24geW91ciBzaXRlIHRvIG9uZSBvciBtb3JlIGZyaWVuZHMgaW4gYSBGYWNlYm9vayBtZXNzYWdlLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8ZmItc2VuZCBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3p5cmEvbmcyLWZhY2Vib29rLXNkay9cIj48L2ZiLXNlbmQ+XG4gKiBgYGBcbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZmItc2VuZCcsXG4gIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBGQlNlbmRDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGNvbG9yIHNjaGVtZSB1c2VkIGJ5IHRoZSBwbHVnaW4uIENhbiBiZSBcImxpZ2h0XCIgb3IgXCJkYXJrXCIuIERlZmF1bHRzIHRvIGxpZ2h0LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgY29sb3JTY2hlbWU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgcGFnZSB0aGF0IHdpbGwgYmUgc2VudC4gRGVmYXVsdHMgdG8gdGhlIGN1cnJlbnQgVVJMLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XG5cbiAgLyoqXG4gICAqIElmIHlvdXIgd2ViIHNpdGUgb3Igb25saW5lIHNlcnZpY2UsIG9yIGEgcG9ydGlvbiBvZiB5b3VyIHNlcnZpY2UsIGlzIGRpcmVjdGVkIHRvIGNoaWxkcmVuIHVuZGVyIDEzIHlvdSBtdXN0IGVuYWJsZSB0aGlzLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAga2lkRGlyZWN0ZWRTaXRlOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBBIGxhYmVsIGZvciB0cmFja2luZyByZWZlcnJhbHMgd2hpY2ggbXVzdCBiZSBsZXNzIHRoYW4gNTAgY2hhcmFjdGVycywgYW5kIGNhbiBjb250YWluIGFscGhhbnVtZXJpYyBjaGFyYWN0ZXJzIGFuZCBzb21lIHB1bmN0dWF0aW9uIChjdXJyZW50bHkgKy89LS46XykuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICByZWY6IHN0cmluZztcblxuICAvKipcbiAgICogU2l6ZSBvZiB0aGUgYnV0dG9uLiBEZWZhdWx0cyB0byBzbWFsbC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIHNpemU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBybmQ6IFJlbmRlcmVyXG4gICkge1xuICAgIHN1cGVyKGVsLCBybmQsICdmYi1zZW5kJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgRWxlbWVudFJlZiwgUmVuZGVyZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZCTUxBdHRyaWJ1dGUsIEZCTUxDb21wb25lbnQgfSBmcm9tICcuLi9mYm1sLWNvbXBvbmVudCc7XG5cbi8qKlxuICogQG5hbWUgU2hhcmUgQnV0dG9uXG4gKiBAc2hvcnRkZXNjIFNoYXJlIGJ1dHRvbiBjb21wb25lbnRcbiAqIEBmYmRvYyBodHRwczovL2RldmVsb3BlcnMuZmFjZWJvb2suY29tL2RvY3MvcGx1Z2lucy9zaGFyZS1idXR0b25cbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIFNoYXJlIGJ1dHRvbiBsZXRzIHBlb3BsZSBhZGQgYSBwZXJzb25hbGl6ZWQgbWVzc2FnZSB0byBsaW5rcyBiZWZvcmUgc2hhcmluZyBvbiB0aGVpciB0aW1lbGluZSwgaW4gZ3JvdXBzLCBvciB0byB0aGVpciBmcmllbmRzIHZpYSBhIEZhY2Vib29rIE1lc3NhZ2UuXG4gKiBAdXNhZ2VcbiAqIGBgYGh0bWxcbiAqIDxmYi1zaGFyZSBocmVmPVwiaHR0cHM6Ly9naXRodWIuY29tL3p5cmEvbmcyLWZhY2Vib29rLXNkay9cIj48L2ZiLXNoYXJlPlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXNoYXJlJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCU2hhcmVDb21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGFic29sdXRlIFVSTCBvZiB0aGUgcGFnZSB0aGF0IHdpbGwgYmUgc2hhcmVkLiBEZWZhdWx0cyB0byB0aGUgY3VycmVudCBVUkwuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBocmVmOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNlbGVjdHMgb25lIG9mIHRoZSBkaWZmZXJlbnQgbGF5b3V0cyB0aGF0IGFyZSBhdmFpbGFibGUgZm9yIHRoZSBwbHVnaW4uIENhbiBiZSBvbmUgb2YgYGJveF9jb3VudGAsIGBidXR0b25fY291bnRgLCBgYnV0dG9uYC4gRGVmYXVsdHMgdG8gYGljb25fbGlua2AuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBsYXlvdXQ6IHN0cmluZztcblxuICAvKipcbiAgICogSWYgc2V0IHRvIHRydWUsIHRoZSBzaGFyZSBidXR0b24gd2lsbCBvcGVuIHRoZSBzaGFyZSBkaWFsb2cgaW4gYW4gaWZyYW1lIChpbnN0ZWFkIG9mIGEgcG9wdXApIG9uIHRvcCBvZiB5b3VyIHdlYnNpdGUgb24gbW9iaWxlLiBUaGlzIG9wdGlvbiBpcyBvbmx5IGF2YWlsYWJsZSBmb3IgbW9iaWxlLCBub3QgZGVza3RvcC4gRGVmYXVsdHMgdG8gYGZhbHNlYC5cbiAgICovXG4gIEBJbnB1dCgpXG4gIEBGQk1MQXR0cmlidXRlXG4gIG1vYmlsZUlmcmFtZTogYm9vbGVhbjtcblxuICAvKipcbiAgICogVGhlIGJ1dHRvbiBpcyBvZmZlcmVkIGluIDIgc2l6ZXMgaS5lLiBsYXJnZSBhbmQgc21hbGwuIERlZmF1bHRzIHRvIGBzbWFsbGAuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaXplOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItc2hhcmUtYnV0dG9uJyk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFbGVtZW50UmVmLCBSZW5kZXJlciwgT25Jbml0LCBPbkRlc3Ryb3ksIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRkJNTEF0dHJpYnV0ZSwgRkJNTENvbXBvbmVudCwgRkJNTEluc3RhbmNlTWV0aG9kIH0gZnJvbSAnLi4vZmJtbC1jb21wb25lbnQnO1xuZGVjbGFyZSB2YXIgRkI6IGFueTtcblxuLyoqXG4gKiBAbmFtZSBFbWJlZGRlZCBWaWRlb1xuICogQHNob3J0ZGVzYyBDb21wb25lbnQgdG8gZW1iZWQgRmFjZWJvb2sgdmlkZW9zXG4gKiBAZmJkb2MgaHR0cHM6Ly9kZXZlbG9wZXJzLmZhY2Vib29rLmNvbS9kb2NzL3BsdWdpbnMvZW1iZWRkZWQtdmlkZW8tcGxheWVyXG4gKiBAZGVzY3JpcHRpb24gQ29tcG9uZW50IHRvIGVtYmVkIEZhY2Vib29rIHZpZGVvcyBhbmQgY29udHJvbCB0aGVtLlxuICogQHVzYWdlXG4gKiBgYGBodG1sXG4gKiA8IS0tIGJhc2ljIHVzYWdlIC0tPlxuICogPGZiLXZpZGVvIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vZmFjZWJvb2svdmlkZW9zLzEwMTUzMjMxMzc5OTQ2NzI5L1wiPjwvZmItdmlkZW8+XG4gKlxuICogPCEtLSBldmVudCBlbWl0dGVycyAtLT5cbiAqIDxmYi12aWRlbyBocmVmPVwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL2ZhY2Vib29rL3ZpZGVvcy8xMDE1MzIzMTM3OTk0NjcyOS9cIiAocGF1c2VkKT1cIm9uVmlkZW9QYXVzZWQoJGV2ZW50KVwiPjwvZmItdmlkZW8+XG4gKiBgYGBcbiAqXG4gKiBUbyBtYW51YWxseSBpbnRlcmFjdCB3aXRoIHRoZSB2aWRlbyBwbGF5ZXIsIGZldGNoIGl0IHVzaW5nIGBWaWV3Q2hpbGRgLlxuICpcbiAqIGBgYHRzXG4gKiBpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuICogaW1wb3J0IHsgRkJWaWRlb0NvbXBvbmVudCB9IGZyb20gJ25nMi1mYWNlYm9vay1zZGsnO1xuICpcbiAqIEBDb21wb25lbnQoLi4uKVxuICogZXhwb3J0IGNsYXNzIE15Q29tcG9uZW50IHtcbiAqXG4gKiAgIEBWaWV3Q2hpbGQoRkJWaWRlb0NvbXBvbmVudCkgdmlkZW86IEZCVmlkZW9Db21wb25lbnQ7XG4gKlxuICogICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gKiAgICAgdGhpcy52aWRlby5wbGF5KCk7XG4gKiAgICAgdGhpcy52aWRlby5wYXVzZSgpO1xuICogICAgIHRoaXMudmlkZW8uZ2V0Vm9sdW1lKCk7XG4gKiAgIH1cbiAqXG4gKiB9XG4gKlxuICogYGBgXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2ZiLXZpZGVvJyxcbiAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIEZCVmlkZW9Db21wb25lbnQgZXh0ZW5kcyBGQk1MQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG4gIHByaXZhdGUgX2luc3RhbmNlOiBhbnk7XG5cbiAgLyoqXG4gICAqIFRoZSBhYnNvbHV0ZSBVUkwgb2YgdGhlIHZpZGVvLlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgaHJlZjogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBBbGxvdyB0aGUgdmlkZW8gdG8gYmUgcGxheWVkIGluIGZ1bGxzY3JlZW4gbW9kZS4gQ2FuIGJlIGZhbHNlIG9yIHRydWUuIERlZmF1bHRzIHRvIGZhbHNlO1xuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgYWxsb3dmdWxsc2NyZWVuOiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiBBdXRvbWF0aWNhbGx5IHN0YXJ0IHBsYXlpbmcgdGhlIHZpZGVvIHdoZW4gdGhlIHBhZ2UgbG9hZHMuIFRoZSB2aWRlbyB3aWxsIGJlIHBsYXllZCB3aXRob3V0IHNvdW5kIChtdXRlZCkuIFBlb3BsZSBjYW4gdHVybiBvbiBzb3VuZCB2aWEgdGhlIHZpZGVvIHBsYXllciBjb250cm9scy4gVGhpcyBzZXR0aW5nIGRvZXMgbm90IGFwcGx5IHRvIG1vYmlsZSBkZXZpY2VzLiBDYW4gYmUgZmFsc2Ugb3IgdHJ1ZS4gRGVmYXVsdHMgdG8gZmFsc2UuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBhdXRvcGxheTogYm9vbGVhbjtcblxuICAvKipcbiAgICogVGhlIHdpZHRoIG9mIHRoZSB2aWRlbyBjb250YWluZXIuIE1pbi4gMjIwcHguXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICB3aWR0aDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTZXQgdG8gdHJ1ZSB0byBpbmNsdWRlIHRoZSB0ZXh0IGZyb20gdGhlIEZhY2Vib29rIHBvc3QgYXNzb2NpYXRlZCB3aXRoIHRoZSB2aWRlbywgaWYgYW55LlxuICAgKi9cbiAgQElucHV0KClcbiAgQEZCTUxBdHRyaWJ1dGVcbiAgc2hvd1RleHQ6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIFNldCB0byB0cnVlIHRvIHNob3cgY2FwdGlvbnMgKGlmIGF2YWlsYWJsZSkgYnkgZGVmYXVsdC4gQ2FwdGlvbnMgYXJlIG9ubHkgYXZhaWxhYmxlIG9uIGRlc2t0b3AuXG4gICAqL1xuICBASW5wdXQoKVxuICBARkJNTEF0dHJpYnV0ZVxuICBzaG93Q2FwdGlvbnM6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gc3RhcnRzIHRvIHBsYXkuXG4gICAqL1xuICBAT3V0cHV0KClcbiAgc3RhcnRlZFBsYXlpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gcGF1c2VzLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIHBhdXNlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAvKipcbiAgICpcbiAgIEZpcmVkIHdoZW4gdmlkZW8gZmluaXNoZXMgcGxheWluZy5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBmaW5pc2hlZFBsYXlpbmc6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdmlkZW8gc3RhcnRzIHRvIGJ1ZmZlci5cbiAgICovXG4gIEBPdXRwdXQoKVxuICBzdGFydGVkQnVmZmVyaW5nOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIHZpZGVvIHJlY292ZXJzIGZyb20gYnVmZmVyaW5nLlxuICAgKi9cbiAgQE91dHB1dCgpXG4gIGZpbmlzaGVkQnVmZmVyaW5nOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIGFuIGVycm9yIG9jY3VycyBvbiB0aGUgdmlkZW8uXG4gICAqL1xuICBAT3V0cHV0KClcbiAgZXJyb3I6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgcHJpdmF0ZSBfaWQ6IHN0cmluZztcblxuICBwcml2YXRlIF9saXN0ZW5lcnM6IGFueVtdID0gW107XG5cbiAgY29uc3RydWN0b3IoXG4gICAgZWw6IEVsZW1lbnRSZWYsXG4gICAgcm5kOiBSZW5kZXJlclxuICApIHtcbiAgICBzdXBlcihlbCwgcm5kLCAnZmItdmlkZW8nKTtcbiAgICB0aGlzLm5hdGl2ZUVsZW1lbnQuaWQgPSB0aGlzLl9pZCA9ICd2aWRlby0nICsgU3RyaW5nKE1hdGguZmxvb3IoKE1hdGgucmFuZG9tKCkgKiAxMDAwMCkgKyAxKSk7XG4gIH1cblxuICAvKipcbiAgICogQGhpZGRlblxuICAgKi9cbiAgbmdPbkluaXQoKSB7XG4gICAgRkIuRXZlbnQuc3Vic2NyaWJlKCd4ZmJtbC5yZWFkeScsIChtc2c6IGFueSkgPT4ge1xuICAgICAgaWYgKG1zZy50eXBlID09PSAndmlkZW8nICYmIG1zZy5pZCA9PT0gdGhpcy5faWQpIHtcbiAgICAgICAgdGhpcy5faW5zdGFuY2UgPSBtc2cuaW5zdGFuY2U7XG4gICAgICAgIHRoaXMuX2xpc3RlbmVycy5wdXNoKFxuICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnN1YnNjcmliZSgnc3RhcnRlZFBsYXlpbmcnLCAoZTogYW55KSA9PiB0aGlzLnN0YXJ0ZWRQbGF5aW5nLmVtaXQoZSkpLFxuICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnN1YnNjcmliZSgncGF1c2VkJywgKGU6IGFueSkgPT4gdGhpcy5wYXVzZWQuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdmaW5pc2hlZFBsYXlpbmcnLCAoZTogYW55KSA9PiB0aGlzLmZpbmlzaGVkUGxheWluZy5lbWl0KGUpKSxcbiAgICAgICAgICB0aGlzLl9pbnN0YW5jZS5zdWJzY3JpYmUoJ3N0YXJ0ZWRCdWZmZXJpbmcnLCAoZTogYW55KSA9PiB0aGlzLnN0YXJ0ZWRCdWZmZXJpbmcuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdmaW5pc2hlZEJ1ZmZlcmluZycsIChlOiBhbnkpID0+IHRoaXMuZmluaXNoZWRCdWZmZXJpbmcuZW1pdChlKSksXG4gICAgICAgICAgdGhpcy5faW5zdGFuY2Uuc3Vic2NyaWJlKCdlcnJvcicsIChlOiBhbnkpID0+IHRoaXMuZXJyb3IuZW1pdChlKSlcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAaGlkZGVuXG4gICAqL1xuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLl9saXN0ZW5lcnMuZm9yRWFjaChsID0+IHtcbiAgICAgIGlmICh0eXBlb2YgbC5yZWxlYXNlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGwucmVsZWFzZSgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFBsYXlzIHRoZSB2aWRlby5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgcGxheSgpIHt9XG5cbiAgLyoqXG4gICAqIFBhdXNlcyB0aGUgdmlkZW8uXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIHBhdXNlKCkge31cblxuICAvKipcbiAgICogU2Vla3MgdG8gc3BlY2lmaWVkIHBvc2l0aW9uLlxuICAgKiBAcGFyYW0gc2Vjb25kcyB7bnVtYmVyfVxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBzZWVrKHNlY29uZHM6IG51bWJlcikge31cblxuICAvKipcbiAgICogTXV0ZSB0aGUgdmlkZW8uXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIG11dGUoKSB7fVxuXG4gIC8qKlxuICAgKiBVbm11dGUgdGhlIHZpZGVvLlxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICB1bm11dGUoKSB7fVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRydWUgaWYgdmlkZW8gaXMgbXV0ZWQsIGZhbHNlIGlmIG5vdC5cbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgaXNNdXRlZCgpOiBib29sZWFuIHsgcmV0dXJuOyB9XG5cbiAgLyoqXG4gICAqIFNldCB0aGUgdm9sdW1lXG4gICAqIEBwYXJhbSB2b2x1bWVcbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgc2V0Vm9sdW1lKHZvbHVtZTogbnVtYmVyKSB7fVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIHZvbHVtZVxuICAgKi9cbiAgQEZCTUxJbnN0YW5jZU1ldGhvZFxuICBnZXRWb2x1bWUoKTogbnVtYmVyIHsgcmV0dXJuOyB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIGN1cnJlbnQgdmlkZW8gdGltZSBwb3NpdGlvbiBpbiBzZWNvbmRzXG4gICAqL1xuICBARkJNTEluc3RhbmNlTWV0aG9kXG4gIGdldEN1cnJlbnRQb3NpdGlvbigpIHt9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIHZpZGVvIGR1cmF0aW9uIGluIHNlY29uZHNcbiAgICovXG4gIEBGQk1MSW5zdGFuY2VNZXRob2RcbiAgZ2V0RHVyYXRpb24oKSB7fVxuXG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBdXRoUmVzcG9uc2UgfSBmcm9tICcuLi9tb2RlbHMvYXV0aC1yZXNwb25zZSc7XG5pbXBvcnQgeyBJbml0UGFyYW1zIH0gZnJvbSAnLi4vbW9kZWxzL2luaXQtcGFyYW1zJztcbmltcG9ydCB7IExvZ2luT3B0aW9ucyB9IGZyb20gJy4uL21vZGVscy9sb2dpbi1vcHRpb25zJztcbmltcG9ydCB7IExvZ2luUmVzcG9uc2UgfSBmcm9tICcuLi9tb2RlbHMvbG9naW4tcmVzcG9uc2UnO1xuaW1wb3J0IHsgTG9naW5TdGF0dXMgfSBmcm9tICcuLi9tb2RlbHMvbG9naW4tc3RhdHVzJztcbmltcG9ydCB7IFVJUGFyYW1zIH0gZnJvbSAnLi4vbW9kZWxzL3VpLXBhcmFtcyc7XG5pbXBvcnQgeyBVSVJlc3BvbnNlIH0gZnJvbSAnLi4vbW9kZWxzL3VpLXJlc3BvbnNlJztcblxuZGVjbGFyZSB2YXIgRkI6IGFueTtcblxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCB0eXBlIEFwaU1ldGhvZCA9ICdnZXQnIHwgJ3Bvc3QnIHwgJ2RlbGV0ZSc7XG5cbi8qKlxuICogQHNob3J0ZGVzY1xuICogQW5ndWxhciAyIHNlcnZpY2UgdG8gaW5qZWN0IHRvIHVzZSBGYWNlYm9vaydzIFNES1xuICogQGRlc2NyaXB0aW9uXG4gKiBZb3Ugb25seSBuZWVkIHRvIGluamVjdCB0aGlzIHNlcnZpY2UgaW4geW91ciBhcHBsaWNhdGlvbiBpZiB5b3UgYXJlbid0IHVzaW5nIFtgRmFjZWJvb2tNb2R1bGVgXSguLi9mYWNlYm9vay1tb2R1bGUpLlxuICogQHVzYWdlXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBGYWNlYm9va1NlcnZpY2UsIEluaXRQYXJhbXMgfSBmcm9tICduZzItZmFjZWJvb2stc2RrJztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGYWNlYm9va1NlcnZpY2UpIHtcbiAqXG4gKiAgIGNvbnN0IHBhcmFtczogSW5pdFBhcmFtcyA9IHtcbiAqXG4gKiAgIH07XG4gKlxuICogICBmYi5pbml0KHBhcmFtcyk7XG4gKlxuICogfVxuICogYGBgXG4gKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBGYWNlYm9va1NlcnZpY2Uge1xuXG4gIC8qKlxuICAgKiBUaGlzIG1ldGhvZCBpcyB1c2VkIHRvIGluaXRpYWxpemUgYW5kIHNldHVwIHRoZSBTREsuXG4gICAqIEBwYXJhbSBwYXJhbXMge0luaXRQYXJhbXN9IEluaXRpYWxpemF0aW9uIHBhcmFtZXRlcnNcbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBpbml0KHBhcmFtczogSW5pdFBhcmFtcyk6IFByb21pc2U8YW55PiB7XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoRkIuaW5pdChwYXJhbXMpKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgbWV0aG9kIGxldHMgeW91IG1ha2UgY2FsbHMgdG8gdGhlIEdyYXBoIEFQSVxuICAgKiBAdXNhZ2VcbiAgICogYGBgdHlwZXNjcmlwdFxuICAgKiB0aGlzLmZiLmFwaSgnc29tZXBhdGgnKVxuICAgKiAgIC50aGVuKHJlcyA9PiBjb25zb2xlLmxvZyhyZXMpKVxuICAgKiAgIC5jYXRjaChlID0+IGNvbnNvbGUubG9nKGUpKTtcbiAgICogYGBgXG4gICAqIEBwYXJhbSBwYXRoIHtzdHJpbmd9IFRoZSBHcmFwaCBBUEkgZW5kcG9pbnQgcGF0aCB0aGF0IHlvdSB3YW50IHRvIGNhbGwuXG4gICAqIEBwYXJhbSBbbWV0aG9kPWdldF0ge3N0cmluZ30gVGhlIEhUVFAgbWV0aG9kIHRoYXQgeW91IHdhbnQgdG8gdXNlIGZvciB0aGUgQVBJIHJlcXVlc3QuXG4gICAqIEBwYXJhbSBbcGFyYW1zXSB7T2JqZWN0fSBBbiBvYmplY3QgY29uc2lzdGluZyBvZiBhbnkgcGFyYW1ldGVycyB0aGF0IHlvdSB3YW50IHRvIHBhc3MgaW50byB5b3VyIEdyYXBoIEFQSSBjYWxsLlxuICAgKiBAcmV0dXJucyByZXR1cm4ge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIGFwaShwYXRoOiBzdHJpbmcsIG1ldGhvZDogQXBpTWV0aG9kID0gJ2dldCcsIHBhcmFtczogYW55ID0ge30pOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxhbnk+KChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgdHJ5IHtcbiAgICAgICAgRkIuYXBpKHBhdGgsIG1ldGhvZCwgcGFyYW1zLCAocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICAgIGlmICghcmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHJlamVjdCgpO1xuICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UuZXJyb3IpIHtcbiAgICAgICAgICAgIHJlamVjdChyZXNwb25zZS5lcnJvcik7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJlamVjdChlKTtcbiAgICAgIH1cblxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgbWV0aG9kIGlzIHVzZWQgdG8gdHJpZ2dlciBkaWZmZXJlbnQgZm9ybXMgb2YgRmFjZWJvb2sgY3JlYXRlZCBVSSBkaWFsb2dzLlxuICAgKiBUaGVzZSBkaWFsb2dzIGluY2x1ZGU6XG4gICAqIC0gU2hhcmUgZGlhbG9nXG4gICAqIC0gTG9naW4gZGlhbG9nXG4gICAqIC0gQWRkIHBhZ2UgdGFiIGRpYWxvZ1xuICAgKiAtIFJlcXVlc3RzIGRpYWxvZ1xuICAgKiAtIFNlbmQgZGlhbG9nXG4gICAqIC0gUGF5bWVudHMgZGlhbG9nXG4gICAqIC0gR28gTGl2ZSBkaWFsb2dcbiAgICogQHBhcmFtIHBhcmFtcyB7VUlQYXJhbXN9IEEgY29sbGVjdGlvbiBvZiBwYXJhbWV0ZXJzIHRoYXQgY29udHJvbCB3aGljaCBkaWFsb2cgaXMgbG9hZGVkLCBhbmQgcmVsZXZhbnQgc2V0dGluZ3MuXG4gICAqIEByZXR1cm5zIHJldHVybiB7UHJvbWlzZTxVSVJlc3BvbnNlPn1cbiAgICovXG4gIHVpKHBhcmFtczogVUlQYXJhbXMpOiBQcm9taXNlPFVJUmVzcG9uc2U+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIEZCLnVpKHBhcmFtcywgKHJlc3BvbnNlOiBhbnkpID0+IHtcbiAgICAgICAgICBpZighcmVzcG9uc2UpIHJlamVjdCgpO1xuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UuZXJyb3IpIHJlamVjdChyZXNwb25zZS5lcnJvcik7XG4gICAgICAgICAgZWxzZSByZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJlamVjdChlKTtcbiAgICAgIH1cblxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgbWV0aG9kIGFsbG93cyB5b3UgdG8gZGV0ZXJtaW5lIGlmIGEgdXNlciBpcyBsb2dnZWQgaW4gdG8gRmFjZWJvb2sgYW5kIGhhcyBhdXRoZW50aWNhdGVkIHlvdXIgYXBwLlxuICAgKiBAcGFyYW0gW2ZvcmNlRnJlc2hSZXNwb25zZT1mYWxzZV0ge2Jvb2xlYW59IEZvcmNlIGEgZnJlc2ggcmVzcG9uc2UuXG4gICAqIEByZXR1cm5zIHJldHVybiB7UHJvbWlzZTxMb2dpblN0YXR1cz59XG4gICAqL1xuICBnZXRMb2dpblN0YXR1cyhmb3JjZUZyZXNoUmVzcG9uc2U/OiBib29sZWFuKTogUHJvbWlzZTxMb2dpblN0YXR1cz4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxMb2dpblN0YXR1cz4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICB0cnkge1xuICAgICAgICBGQi5nZXRMb2dpblN0YXR1cygocmVzcG9uc2U6IExvZ2luU3RhdHVzKSA9PiB7XG4gICAgICAgICAgaWYgKCFyZXNwb25zZSkge1xuICAgICAgICAgICAgcmVqZWN0KCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSwgZm9yY2VGcmVzaFJlc3BvbnNlKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogTG9naW4gdGhlIHVzZXJcbiAgICogQHVzYWdlXG4gICAqIGBgYHR5cGVzY3JpcHRcbiAgICogLy8gbG9naW4gd2l0aG91dCBvcHRpb25zXG4gICAqIHRoaXMuZmIubG9naW4oKVxuICAgKiAgIC50aGVuKChyZXNwb25zZTogTG9naW5SZXNwb25zZSkgPT4gY29uc29sZS5sb2coJ0xvZ2dlZCBpbicsIHJlc3BvbnNlKSlcbiAgICogICAuY2F0Y2goZSA9PiBjb25zb2xlLmVycm9yKCdFcnJvciBsb2dnaW5nIGluJykpO1xuICAgKlxuICAgKiAvLyBsb2dpbiB3aXRoIG9wdGlvbnNcbiAgICogY29uc3Qgb3B0aW9uczogTG9naW5PcHRpb25zID0ge1xuICAgKiAgIHNjb3BlOiAncHVibGljX3Byb2ZpbGUsdXNlcl9mcmllbmRzLGVtYWlsLHBhZ2VzX3Nob3dfbGlzdCcsXG4gICAqICAgcmV0dXJuX3Njb3BlczogdHJ1ZSxcbiAgICogICBlbmFibGVfcHJvZmlsZV9zZWxlY3RvcjogdHJ1ZVxuICAgKiB9O1xuICAgKiB0aGlzLmZiLmxvZ2luKG9wdGlvbnMpXG4gICAqICAgLnRoZW4oLi4uKVxuICAgKiAgIC5jYXRjaCguLi4pO1xuICAgKiBgYGBcbiAgICogQHBhcmFtIFtvcHRpb25zXSB7TG9naW5PcHRpb25zfSBMb2dpbiBvcHRpb25zXG4gICAqIEByZXR1cm5zIHJldHVybiB7UHJvbWlzZTxMb2dpblJlc3BvbnNlPn0gcmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIFtMb2dpblJlc3BvbnNlXSguLi9sb2dpbi1yZXNwb25zZSkgb2JqZWN0LCBvciByZWplY3RzIHdpdGggYW4gZXJyb3JcbiAgICovXG4gIGxvZ2luKG9wdGlvbnM/OiBMb2dpbk9wdGlvbnMpOiBQcm9taXNlPExvZ2luUmVzcG9uc2U+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8TG9naW5SZXNwb25zZT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICB0cnkge1xuICAgICAgICBGQi5sb2dpbigocmVzcG9uc2U6IExvZ2luUmVzcG9uc2UpID0+IHtcbiAgICAgICAgICBpZiAocmVzcG9uc2UuYXV0aFJlc3BvbnNlKSB7XG4gICAgICAgICAgICByZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHJlamVjdCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSwgb3B0aW9ucyk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJlamVjdChlKTtcbiAgICAgIH1cblxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIExvZ291dCB0aGUgdXNlclxuICAgKiBAdXNhZ2VcbiAgICogYGBgdHlwZXNjcmlwdFxuICAgKiB0aGlzLmZiLmxvZ291dCgpLnRoZW4oKCkgPT4gY29uc29sZS5sb2coJ0xvZ2dlZCBvdXQhJykpO1xuICAgKiBgYGBcbiAgICogQHJldHVybnMgcmV0dXJuIHtQcm9taXNlPGFueT59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2hlbiB0aGUgdXNlciBpcyBsb2dnZWQgb3V0XG4gICAqL1xuICBsb2dvdXQoKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIEZCLmxvZ291dCgocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICAgICAgICB9KTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBzeW5jaHJvbm91cyBmdW5jdGlvbiByZXR1cm5zIGJhY2sgdGhlIGN1cnJlbnQgYXV0aFJlc3BvbnNlLlxuICAgKiBAdXNhZ2VcbiAgICogYGBgdHlwZXNjcmlwdFxuICAgKiBpbXBvcnQgeyBBdXRoUmVzcG9uc2UsIEZhY2Vib29rU2VydmljZSB9IGZyb20gJ25nMi1mYWNlYm9vay1zZGsnO1xuICAgKlxuICAgKiAuLi5cbiAgICpcbiAgICogY29uc3QgYXV0aFJlc3BvbnNlOiBBdXRoUmVzcG9uc2UgPSB0aGlzLmZiLmdldEF1dGhSZXNwb25zZSgpO1xuICAgKiBgYGBcbiAgICogQHJldHVybnMgcmV0dXJuIHtBdXRoUmVzcG9uc2V9IHJldHVybnMgYW4gW0F1dGhSZXNwb25zZV0oLi4vYXV0aC1yZXNwb25zZSkgb2JqZWN0XG4gICAqL1xuICBnZXRBdXRoUmVzcG9uc2UoKTogQXV0aFJlc3BvbnNlIHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIDxBdXRoUmVzcG9uc2U+RkIuZ2V0QXV0aFJlc3BvbnNlKCk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgY29uc29sZS5lcnJvcignbmcyLWZhY2Vib29rLXNkazogJywgZSk7XG4gICAgfVxuICB9XG5cbn1cblxuIiwiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRmFjZWJvb2tTZXJ2aWNlIH0gZnJvbSAnLi9wcm92aWRlcnMvZmFjZWJvb2snO1xuXG5pbXBvcnQgeyBGQkNvbW1lbnRFbWJlZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1jb21tZW50LWVtYmVkL2ZiLWNvbW1lbnQtZW1iZWQnO1xuaW1wb3J0IHsgRkJDb21tZW50c0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1jb21tZW50cy9mYi1jb21tZW50cyc7XG5pbXBvcnQgeyBGQkZvbGxvd0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1mb2xsb3cvZmItZm9sbG93JztcbmltcG9ydCB7IEZCTGlrZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi1saWtlL2ZiLWxpa2UnO1xuaW1wb3J0IHsgRkJQYWdlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLXBhZ2UvZmItcGFnZSc7XG5pbXBvcnQgeyBGQlBvc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItcG9zdC9mYi1wb3N0JztcbmltcG9ydCB7IEZCUXVvdGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItcXVvdGUvZmItcXVvdGUnO1xuaW1wb3J0IHsgRkJTYXZlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZiLXNhdmUvZmItc2F2ZSc7XG5pbXBvcnQgeyBGQlNlbmRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItc2VuZC9mYi1zZW5kJztcbmltcG9ydCB7IEZCU2hhcmVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmItc2hhcmUvZmItc2hhcmUnO1xuaW1wb3J0IHsgRkJWaWRlb0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mYi12aWRlby9mYi12aWRlbyc7XG5cbmNvbnN0IGNvbXBvbmVudHM6IGFueVtdID0gW1xuICBGQkNvbW1lbnRFbWJlZENvbXBvbmVudCxcbiAgRkJDb21tZW50c0NvbXBvbmVudCxcbiAgRkJGb2xsb3dDb21wb25lbnQsXG4gIEZCTGlrZUNvbXBvbmVudCxcbiAgRkJQYWdlQ29tcG9uZW50LFxuICBGQlBvc3RDb21wb25lbnQsXG4gIEZCUXVvdGVDb21wb25lbnQsXG4gIEZCU2F2ZUNvbXBvbmVudCxcbiAgRkJTZW5kQ29tcG9uZW50LFxuICBGQlNoYXJlQ29tcG9uZW50LFxuICBGQlZpZGVvQ29tcG9uZW50XG5dO1xuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q29tcG9uZW50cygpIHtcbiAgcmV0dXJuIGNvbXBvbmVudHM7XG59XG5cbi8qKlxuICogQHNob3J0ZGVzYyBUaGUgbW9kdWxlIHRvIGltcG9ydCB0byBhZGQgdGhpcyBsaWJyYXJ5XG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBtYWluIG1vZHVsZSB0byBpbXBvcnQgaW50byB5b3VyIGFwcGxpY2F0aW9uLlxuICogWW91IG9ubHkgbmVlZCB0byBpbXBvcnQgdGhpcyBtb2R1bGUgaWYgeW91IHdpc2ggdG8gdXNlIHRoZSBjb21wb25lbnRzIGluIHRoaXMgbGlicmFyeTsgb3RoZXJ3aXNlLCB5b3UgY291bGQganVzdCBpbXBvcnQgW0ZhY2Vib29rU2VydmljZV0oLi4vZmFjZWJvb2stc2VydmljZSkgaW50byB5b3VyIG1vZHVsZSBpbnN0ZWFkLlxuICogVGhpcyBtb2R1bGUgd2lsbCBtYWtlIGFsbCBjb21wb25lbnRzIGFuZCBwcm92aWRlcnMgYXZhaWxhYmxlIGluIHlvdXIgYXBwbGljYXRpb24uXG4gKlxuICogQHVzYWdlXG4gKiBJbiBvcmRlciB0byB1c2UgdGhpcyBsaWJyYXJ5LCB5b3UgbmVlZCB0byBpbXBvcnQgYEZhY2Vib29rTW9kdWxlYCBpbnRvIHlvdXIgYXBwJ3MgbWFpbiBgTmdNb2R1bGVgLlxuICpcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IEZhY2Vib29rTW9kdWxlIH0gZnJvbSAnbmcyLWZhY2Vib29rLXNkayc7XG4gKlxuICogQE5nTW9kdWxlKHtcbiAqICAgLi4uXG4gKiAgIGltcG9ydHM6IFtcbiAqICAgICAuLi5cbiAqICAgICBGYWNlYm9va01vZHVsZS5mb3JSb290KClcbiAqICAgXSxcbiAqICAgLi4uXG4gKiB9KVxuICogZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cbiAqIGBgYFxuICovXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IGdldENvbXBvbmVudHMoKSxcbiAgZXhwb3J0czogZ2V0Q29tcG9uZW50cygpXG59KVxuZXhwb3J0IGNsYXNzIEZhY2Vib29rTW9kdWxlIHtcbiAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBGYWNlYm9va01vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW0ZhY2Vib29rU2VydmljZV1cbiAgICB9O1xuICB9XG59XG4iXSwibmFtZXMiOlsidHNsaWJfMS5fX2V4dGVuZHMiLCJDb21wb25lbnQiLCJFbGVtZW50UmVmIiwiUmVuZGVyZXIiLCJJbnB1dCIsIkV2ZW50RW1pdHRlciIsIk91dHB1dCIsIkluamVjdGFibGUiLCJOZ01vZHVsZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQUE7Ozs7Ozs7Ozs7Ozs7O0lBY0E7SUFFQSxJQUFJLGFBQWEsR0FBRyxNQUFNLENBQUMsY0FBYztTQUNwQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsWUFBWSxLQUFLLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM1RSxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQUUsSUFBSSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztBQUUvRSx1QkFBMEIsQ0FBQyxFQUFFLENBQUM7UUFDMUIsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQixnQkFBZ0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsRUFBRTtRQUN2QyxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsS0FBSyxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7QUFFRCx3QkFrQjJCLFVBQVUsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLElBQUk7UUFDcEQsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLEdBQUcsSUFBSSxLQUFLLElBQUksR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQzdILElBQUksT0FBTyxPQUFPLEtBQUssUUFBUSxJQUFJLE9BQU8sT0FBTyxDQUFDLFFBQVEsS0FBSyxVQUFVO1lBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7O1lBQzFILEtBQUssSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQUUsSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xKLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNsRSxDQUFDO0FBRUQsd0JBSTJCLFdBQVcsRUFBRSxhQUFhO1FBQ2pELElBQUksT0FBTyxPQUFPLEtBQUssUUFBUSxJQUFJLE9BQU8sT0FBTyxDQUFDLFFBQVEsS0FBSyxVQUFVO1lBQUUsT0FBTyxPQUFPLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQztJQUNuSSxDQUFDOzs7Ozs7Ozs7Ozs7QUNwREQsMkJBQThCLE1BQVcsRUFBRSxHQUFXO1FBQ3BELHFCQUFNLFVBQVUsR0FBRyxVQUFDLEVBQVUsSUFBSyxPQUFBLE9BQU8sR0FBRyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxDQUFDLG1CQUFtQixFQUFFLE9BQU8sQ0FBQyxDQUFDLFdBQVcsRUFBRSxHQUFBLENBQUM7UUFDL0csTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFO1lBQ2pDLEdBQUcsRUFBRSxVQUFTLEtBQUs7Z0JBQ2pCLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQzNDO1lBQ0QsR0FBRyxFQUFFO2dCQUNILE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUMzQztZQUNELFVBQVUsRUFBRSxJQUFJO1NBQ2pCLENBQUMsQ0FBQztLQUNKO2FBUVU7UUFBUyxjQUFjO2FBQWQsVUFBYyxFQUFkLHFCQUFjLEVBQWQsSUFBYztZQUFkLHlCQUFjOztRQUM1QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3hEO2FBQU07WUFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLDRFQUE0RSxDQUFDLENBQUM7WUFDM0YsT0FBTyxJQUFJLENBQUM7U0FDYjtLQUNGOzs7Ozs7O0FBVkwsZ0NBQW1DLE1BQVcsRUFBRSxHQUFXO1FBQ3pELE9BQU87WUFDTCxVQUFVLEVBQUUsSUFBSTtZQUNoQixLQUFLLElBT0o7U0FDRixDQUFDO0tBQ0g7Ozs7QUFLRDs7UUFBQTtRQUlFLHVCQUNVLElBQ0EsS0FDQTtZQUZBLE9BQUUsR0FBRixFQUFFO1lBQ0YsUUFBRyxHQUFILEdBQUc7WUFDSCxZQUFPLEdBQVAsT0FBTztZQUVmLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUM7WUFDM0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ2xFOzs7Ozs7UUFFUyxvQ0FBWTs7Ozs7WUFBdEIsVUFBdUIsSUFBWSxFQUFFLEtBQWE7Z0JBQ2hELElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLO29CQUFFLE9BQU87Z0JBQzVCLElBQUksQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDL0Q7Ozs7O1FBRVMsb0NBQVk7Ozs7WUFBdEIsVUFBdUIsSUFBWTtnQkFDakMsSUFBSSxDQUFDLElBQUk7b0JBQUUsT0FBTztnQkFDbEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM5Qzs0QkE1REg7UUE4REM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUMzQzRDQSwyQ0FBYTtRQXVCeEQsaUNBQ0UsRUFBYyxFQUNkLEdBQWE7bUJBRWIsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxrQkFBa0IsQ0FBQztTQUNuQzs7b0JBaENGQyxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjt3QkFDNUIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQWxCMEJDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7NkJBd0I1Q0MsVUFBSzs4QkFPTEEsVUFBSztzQ0FPTEEsVUFBSzs7O1lBYkwsYUFBYTs7OztZQU9iLGFBQWE7Ozs7WUFPYixhQUFhOzs7c0NBdkNoQjtNQW1CNkMsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDRWpCSix1Q0FBYTtRQWtEcEQsNkJBQ0UsRUFBYyxFQUNkLEdBQWE7WUFGZixZQUlFLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsYUFBYSxDQUFDLFNBQzlCOzs7Ozs7eUJBdkNjLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTs7U0F1Q2xDOztvQkEzREZDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsYUFBYTt3QkFDdkIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQXBCMEJDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7b0NBMEI1Q0MsVUFBSzs2QkFTTEEsVUFBSzsrQkFPTEEsVUFBSztpQ0FPTEEsVUFBSztnQ0FPTEEsVUFBSzs4QkFXTEEsVUFBSzs7O1lBeENMLGFBQWE7Ozs7WUFTYixhQUFhOzs7O1lBT2IsYUFBYTs7OztZQU9iLGFBQWE7Ozs7WUFPYixhQUFhOzs7O1lBV2IsYUFBYTs7O2tDQXBFaEI7TUFxQnlDLGFBQWE7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDSmZKLHFDQUFhO1FBb0RsRCwyQkFDRSxFQUFjLEVBQ2QsR0FBYTttQkFFYixrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQztTQUM1Qjs7b0JBN0RGQyxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLFdBQVc7d0JBQ3JCLFFBQVEsRUFBRSxFQUFFO3FCQUNiOzs7Ozt3QkFoQjBCQyxlQUFVO3dCQUFFQyxhQUFROzs7O29DQXNCNUNDLFVBQUs7NkJBT0xBLFVBQUs7d0NBT0xBLFVBQUs7K0JBUUxBLFVBQUs7a0NBT0xBLFVBQUs7NkJBT0xBLFVBQUs7OEJBT0xBLFVBQUs7OztZQTFDTCxhQUFhOzs7O1lBT2IsYUFBYTs7OztZQU9iLGFBQWE7Ozs7WUFRYixhQUFhOzs7O1lBT2IsYUFBYTs7OztZQU9iLGFBQWE7Ozs7WUFPYixhQUFhOzs7Z0NBbEVoQjtNQWlCdUMsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQ0VmSixtQ0FBYTtRQXNGaEQseUJBQVksRUFBYyxFQUFFLEdBQWE7WUFBekMsWUFDRSxrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQyxTQUMxQjs7Ozs7eUJBaEVjLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTs7U0FnRWxDOztvQkE1RkZDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsU0FBUzt3QkFDbkIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQWxCMEJDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7K0JBeUI1Q0MsVUFBSztvQ0FRTEEsVUFBSzs2QkFRTEEsVUFBSzt3Q0FRTEEsVUFBSzsrQkFVTEEsVUFBSzs0QkFRTEEsVUFBSzs4QkFTTEEsVUFBSztrQ0FTTEEsVUFBSzs2QkFRTEEsVUFBSzs4QkFRTEEsVUFBSzs7O1lBM0VMLGFBQWE7Ozs7WUFRYixhQUFhOzs7O1lBUWIsYUFBYTs7OztZQVFiLGFBQWE7Ozs7WUFVYixhQUFhOzs7O1lBUWIsYUFBYTs7OztZQVNiLGFBQWE7Ozs7WUFTYixhQUFhOzs7O1lBUWIsYUFBYTs7OztZQVFiLGFBQWE7Ozs4QkF0R2hCO01BbUJxQyxhQUFhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7UUNEYkosbUNBQWE7UUF5RWhELHlCQUNFLEVBQWMsRUFDZCxHQUFhO21CQUViLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsU0FBUyxDQUFDO1NBQzFCOztvQkFsRkZDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsU0FBUzt3QkFDbkIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQWpCMEJDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7NkJBdUI1Q0MsVUFBSzs4QkFRTEEsVUFBSzsrQkFRTEEsVUFBSzs2QkFRTEEsVUFBSztrQ0FRTEEsVUFBSztxQ0FRTEEsVUFBSztnQ0FRTEEsVUFBSztvQ0FRTEEsVUFBSzs0Q0FRTEEsVUFBSzs7O1lBL0RMLGFBQWE7Ozs7WUFRYixhQUFhOzs7O1lBUWIsYUFBYTs7OztZQVFiLGFBQWE7Ozs7WUFRYixhQUFhOzs7O1lBUWIsYUFBYTs7OztZQVFiLGFBQWE7Ozs7WUFRYixhQUFhOzs7O1lBUWIsYUFBYTs7OzhCQXhGaEI7TUFrQnFDLGFBQWE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUNDYkosbUNBQWE7UUF1QmhELHlCQUNFLEVBQWMsRUFDZCxHQUFhO21CQUViLGtCQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsU0FBUyxDQUFDO1NBQzFCOztvQkFoQ0ZDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsU0FBUzt3QkFDbkIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQWxCMEJDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7NkJBd0I1Q0MsVUFBSzs4QkFPTEEsVUFBSztpQ0FPTEEsVUFBSzs7O1lBYkwsYUFBYTs7OztZQU9iLGFBQWE7Ozs7WUFPYixhQUFhOzs7OEJBdkNoQjtNQW1CcUMsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQ0FaSixvQ0FBYTtRQWlCakQsMEJBQ0UsRUFBYyxFQUNkLEdBQWE7bUJBRWIsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUM7U0FDM0I7O29CQTFCRkMsY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxVQUFVO3dCQUNwQixRQUFRLEVBQUUsRUFBRTtxQkFDYjs7Ozs7d0JBbEIwQkMsZUFBVTt3QkFBRUMsYUFBUTs7Ozs2QkF5QjVDQyxVQUFLOytCQU9MQSxVQUFLOzs7WUFOTCxhQUFhOzs7O1lBT2IsYUFBYTs7OytCQWpDaEI7TUFtQnNDLGFBQWE7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQ0RkSixtQ0FBYTtRQVVoRCx5QkFDRSxFQUFjLEVBQ2QsR0FBYTtZQUZmLFlBSUUsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsU0FDMUI7Ozs7O3dCQVBhLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTs7U0FPakM7O29CQW5CRkMsY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxTQUFTO3dCQUNuQixRQUFRLEVBQUUsRUFBRTtxQkFDYjs7Ozs7d0JBakIwQkMsZUFBVTt3QkFBRUMsYUFBUTs7Ozs0QkF3QjVDQyxVQUFLOzs7WUFDTCxhQUFhOzs7OEJBekJoQjtNQWtCcUMsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDQWJKLG1DQUFhO1FBcUNoRCx5QkFDRSxFQUFjLEVBQ2QsR0FBYTtZQUZmLFlBSUUsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsU0FDMUI7Ozs7eUJBNUJjLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSTs7U0E0QmxDOztvQkE5Q0ZDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsU0FBUzt3QkFDbkIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQWpCMEJDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7b0NBdUI1Q0MsVUFBSzs2QkFPTEEsVUFBSzt3Q0FPTEEsVUFBSzs0QkFPTEEsVUFBSzs2QkFPTEEsVUFBSzs7O1lBM0JMLGFBQWE7Ozs7WUFPYixhQUFhOzs7O1lBT2IsYUFBYTs7OztZQU9iLGFBQWE7Ozs7WUFPYixhQUFhOzs7OEJBcERoQjtNQWtCcUMsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDQVpKLG9DQUFhO1FBOEJqRCwwQkFDRSxFQUFjLEVBQ2QsR0FBYTttQkFFYixrQkFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixDQUFDO1NBQ2xDOztvQkF2Q0ZDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsVUFBVTt3QkFDcEIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQWpCMEJDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7NkJBdUI1Q0MsVUFBSzsrQkFPTEEsVUFBSztxQ0FPTEEsVUFBSzs2QkFPTEEsVUFBSzs7O1lBcEJMLGFBQWE7Ozs7WUFPYixhQUFhOzs7O1lBT2IsYUFBYTs7OztZQU9iLGFBQWE7OzsrQkE3Q2hCO01Ba0JzQyxhQUFhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUN5QmJKLG9DQUFhO1FBdUZqRCwwQkFDRSxFQUFjLEVBQ2QsR0FBYTtZQUZmLFlBSUUsa0JBQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUMsU0FFM0I7Ozs7bUNBM0NtQyxJQUFJSyxpQkFBWSxFQUFPOzs7OzJCQU0vQixJQUFJQSxpQkFBWSxFQUFPOzs7OztvQ0FPZCxJQUFJQSxpQkFBWSxFQUFPOzs7O3FDQU10QixJQUFJQSxpQkFBWSxFQUFPOzs7O3NDQU10QixJQUFJQSxpQkFBWSxFQUFPOzs7OzBCQU1uQyxJQUFJQSxpQkFBWSxFQUFPOytCQUl0QixFQUFFO1lBTzVCLEtBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxHQUFHLEtBQUksQ0FBQyxHQUFHLEdBQUcsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOztTQUMvRjs7Ozs7Ozs7UUFLRCxtQ0FBUTs7OztZQUFSO2dCQUFBLGlCQWNDO2dCQWJDLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxVQUFDLEdBQVE7b0JBQ3pDLElBQUksR0FBRyxDQUFDLElBQUksS0FBSyxPQUFPLElBQUksR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsR0FBRyxFQUFFO3dCQUMvQyxLQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUM7d0JBQzlCLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUNsQixLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsRUFDbkYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLFVBQUMsQ0FBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUEsQ0FBQyxFQUNuRSxLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxVQUFDLENBQU0sSUFBSyxPQUFBLEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsRUFDckYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsVUFBQyxDQUFNLElBQUssT0FBQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsRUFDdkYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLEVBQUUsVUFBQyxDQUFNLElBQUssT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsRUFDekYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLFVBQUMsQ0FBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUEsQ0FBQyxDQUNsRSxDQUFDO3FCQUNIO2lCQUNGLENBQUMsQ0FBQzthQUNKOzs7Ozs7OztRQUtELHNDQUFXOzs7O1lBQVg7Z0JBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDO29CQUN2QixJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sS0FBSyxVQUFVLEVBQUU7d0JBQ25DLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztxQkFDYjtpQkFDRixDQUFDLENBQUM7YUFDSjs7Ozs7Ozs7UUFNRCwrQkFBSTs7OzsyQkFBSzs7Ozs7Ozs7UUFNVCxnQ0FBSzs7OzsyQkFBSzs7Ozs7Ozs7OztRQU9WLCtCQUFJOzs7OztzQkFBQyxPQUFlLEtBQUk7Ozs7Ozs7O1FBTXhCLCtCQUFJOzs7OzJCQUFLOzs7Ozs7OztRQU1ULGlDQUFNOzs7OzJCQUFLOzs7Ozs7OztRQU1YLGtDQUFPOzs7OzBCQUFjLE9BQU8sRUFBRTs7Ozs7Ozs7OztRQU85QixvQ0FBUzs7Ozs7c0JBQUMsTUFBYyxLQUFJOzs7Ozs7OztRQU01QixvQ0FBUzs7OzswQkFBYSxPQUFPLEVBQUU7Ozs7Ozs7O1FBTS9CLDZDQUFrQjs7OzsyQkFBSzs7Ozs7Ozs7UUFNdkIsc0NBQVc7Ozs7MkJBQUs7O29CQTdMakJKLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsVUFBVTt3QkFDcEIsUUFBUSxFQUFFLEVBQUU7cUJBQ2I7Ozs7O3dCQTFDa0NDLGVBQVU7d0JBQUVDLGFBQVE7Ozs7NkJBa0RwREMsVUFBSzt3Q0FPTEEsVUFBSztpQ0FPTEEsVUFBSzs4QkFPTEEsVUFBSztpQ0FPTEEsVUFBSztxQ0FPTEEsVUFBSzt1Q0FPTEUsV0FBTTsrQkFNTkEsV0FBTTt3Q0FPTkEsV0FBTTt5Q0FNTkEsV0FBTTswQ0FNTkEsV0FBTTs4QkFNTkEsV0FBTTs7O1lBeEVOLGFBQWE7Ozs7WUFPYixhQUFhOzs7O1lBT2IsYUFBYTs7OztZQU9iLGFBQWE7Ozs7WUFPYixhQUFhOzs7O1lBT2IsYUFBYTs7OztZQXFGYixrQkFBa0I7Ozs7b0RBQ1Y7O1lBS1Isa0JBQWtCOzs7O3FEQUNUOztZQU1ULGtCQUFrQjs7OztvREFDSzs7WUFLdkIsa0JBQWtCOzs7O29EQUNWOztZQUtSLGtCQUFrQjs7OztzREFDUjs7WUFLVixrQkFBa0I7Ozs7dURBQ1c7O1lBTTdCLGtCQUFrQjs7Ozt5REFDUzs7WUFLM0Isa0JBQWtCOzs7O3lEQUNZOztZQUs5QixrQkFBa0I7Ozs7a0VBQ0k7O1lBS3RCLGtCQUFrQjs7OzsyREFDSDsrQkFwT2xCO01BMkNzQyxhQUFhOzs7Ozs7QUMzQ25EOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUE0Q0UsOEJBQUk7Ozs7O1lBQUosVUFBSyxNQUFrQjtnQkFDckIsSUFBSTtvQkFDRixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUN6QztnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFDVixPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzFCO2FBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQWVELDZCQUFHOzs7Ozs7Ozs7Ozs7O1lBQUgsVUFBSSxJQUFZLEVBQUUsTUFBeUIsRUFBRSxNQUFnQjtnQkFBM0MsdUJBQUE7b0JBQUEsY0FBeUI7O2dCQUFFLHVCQUFBO29CQUFBLFdBQWdCOztnQkFDM0QsT0FBTyxJQUFJLE9BQU8sQ0FBTSxVQUFDLE9BQU8sRUFBRSxNQUFNO29CQUV0QyxJQUFJO3dCQUNGLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsVUFBQyxRQUFhOzRCQUN6QyxJQUFJLENBQUMsUUFBUSxFQUFFO2dDQUNiLE1BQU0sRUFBRSxDQUFDOzZCQUNWO2lDQUFNLElBQUksUUFBUSxDQUFDLEtBQUssRUFBRTtnQ0FDekIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDeEI7aUNBQU07Z0NBQ0wsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDOzZCQUNuQjt5QkFDRixDQUFDLENBQUM7cUJBQ0o7b0JBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQ1YsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNYO2lCQUVGLENBQUMsQ0FBQzthQUNKOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUFlRCw0QkFBRTs7Ozs7Ozs7Ozs7OztZQUFGLFVBQUcsTUFBZ0I7Z0JBQ2pCLE9BQU8sSUFBSSxPQUFPLENBQU0sVUFBQyxPQUFPLEVBQUUsTUFBTTtvQkFFdEMsSUFBSTt3QkFDRixFQUFFLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFDLFFBQWE7NEJBQzFCLElBQUcsQ0FBQyxRQUFRO2dDQUFFLE1BQU0sRUFBRSxDQUFDO2lDQUNsQixJQUFHLFFBQVEsQ0FBQyxLQUFLO2dDQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7O2dDQUMxQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQ3hCLENBQUMsQ0FBQztxQkFDSjtvQkFBQyxPQUFPLENBQUMsRUFBRTt3QkFDVixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ1g7aUJBRUYsQ0FBQyxDQUFDO2FBQ0o7Ozs7Ozs7Ozs7O1FBT0Qsd0NBQWM7Ozs7O1lBQWQsVUFBZSxrQkFBNEI7Z0JBQ3pDLE9BQU8sSUFBSSxPQUFPLENBQWMsVUFBQyxPQUFPLEVBQUUsTUFBTTtvQkFFOUMsSUFBSTt3QkFDRixFQUFFLENBQUMsY0FBYyxDQUFDLFVBQUMsUUFBcUI7NEJBQ3RDLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0NBQ2IsTUFBTSxFQUFFLENBQUM7NkJBQ1Y7aUNBQU07Z0NBQ0wsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDOzZCQUNuQjt5QkFDRixFQUFFLGtCQUFrQixDQUFDLENBQUM7cUJBQ3hCO29CQUFDLE9BQU8sQ0FBQyxFQUFFO3dCQUNWLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDWDtpQkFFRixDQUFDLENBQUM7YUFDSjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBd0JELCtCQUFLOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBQUwsVUFBTSxPQUFzQjtnQkFDMUIsT0FBTyxJQUFJLE9BQU8sQ0FBZ0IsVUFBQyxPQUFPLEVBQUUsTUFBTTtvQkFFaEQsSUFBSTt3QkFDRixFQUFFLENBQUMsS0FBSyxDQUFDLFVBQUMsUUFBdUI7NEJBQy9CLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTtnQ0FDekIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDOzZCQUNuQjtpQ0FBSTtnQ0FDSCxNQUFNLEVBQUUsQ0FBQzs2QkFDVjt5QkFDRixFQUFFLE9BQU8sQ0FBQyxDQUFDO3FCQUNiO29CQUFDLE9BQU8sQ0FBQyxFQUFFO3dCQUNWLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDWDtpQkFFRixDQUFDLENBQUM7YUFDSjs7Ozs7Ozs7Ozs7Ozs7Ozs7UUFVRCxnQ0FBTTs7Ozs7Ozs7WUFBTjtnQkFDRSxPQUFPLElBQUksT0FBTyxDQUFNLFVBQUMsT0FBTyxFQUFFLE1BQU07b0JBRXRDLElBQUk7d0JBQ0YsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFDLFFBQWE7NEJBQ3RCLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzt5QkFDbkIsQ0FBQyxDQUFDO3FCQUNKO29CQUFDLE9BQU8sQ0FBQyxFQUFFO3dCQUNWLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDWDtpQkFFRixDQUFDLENBQUM7YUFDSjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQWNELHlDQUFlOzs7Ozs7Ozs7Ozs7WUFBZjtnQkFDRSxJQUFJO29CQUNGLHlCQUFxQixFQUFFLENBQUMsZUFBZSxFQUFFLEVBQUM7aUJBQzNDO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNWLE9BQU8sQ0FBQyxLQUFLLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3hDO2FBQ0Y7O29CQXJMRkMsZUFBVTs7OEJBcENYOzs7Ozs7O0FDQUEsSUFnQkEscUJBQU0sVUFBVSxHQUFVO1FBQ3hCLHVCQUF1QjtRQUN2QixtQkFBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLGVBQWU7UUFDZixlQUFlO1FBQ2YsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixlQUFlO1FBQ2YsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixnQkFBZ0I7S0FDakIsQ0FBQzs7OztBQUVGO1FBQ0UsT0FBTyxVQUFVLENBQUM7S0FDbkI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUErQlEsc0JBQU87OztZQUFkO2dCQUNFLE9BQU87b0JBQ0wsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLFNBQVMsRUFBRSxDQUFDLGVBQWUsQ0FBQztpQkFDN0IsQ0FBQzthQUNIOztvQkFWRkMsYUFBUSxTQUFDO3dCQUNSLFlBQVksRUFBRSxhQUFhLEVBQUU7d0JBQzdCLE9BQU8sRUFBRSxhQUFhLEVBQUU7cUJBQ3pCOzs2QkE3REQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9